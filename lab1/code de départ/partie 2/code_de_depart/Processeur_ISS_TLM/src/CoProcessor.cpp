///////////////////////////////////////////////////////////////////////////////
//
//	CoProcessor.cpp
//
///////////////////////////////////////////////////////////////////////////////
#include "CoProcessor.h"
#include <iostream>
#include <vector>
using namespace std;


///////////////////////////////////////////////////////////////////////////////
//
//	Constructeur
//
///////////////////////////////////////////////////////////////////////////////
CoProcessor::CoProcessor( sc_module_name zName )
: sc_module(zName)
{
	SC_THREAD(thread);
	sensitive << ClockPort.pos();
		
	reg[0] = 0x0000;
	reg[1] = 0x0000;
	reg[2] = 0x0000;
	reg[3] = 0x0000;
}


///////////////////////////////////////////////////////////////////////////////
//
//	Destructeur
//
///////////////////////////////////////////////////////////////////////////////
CoProcessor::~CoProcessor()
{
}


///////////////////////////////////////////////////////////////////////////////
//
//	thread
//
///////////////////////////////////////////////////////////////////////////////
void CoProcessor::thread(void)
{
	// Variables locales
	int i = 0;
	unsigned int* data;
	unsigned int* output = new unsigned int[1];
	unsigned int lenght = 0;
	unsigned int addr = 0;
	


	// Boucle infinie
	while(1)
	{
		
	    // On attend une transaction 
		wait(CoProcessor_RW_OutPort.default_event());
		
		// On s'assure qu'une donnée est valide (enable)
		if(CoProcessor_Enable_InPort.read() == false) return;

		//Lecture adresse		
		addr = CoProcessor_Data_InPort.read();

		// Write (du point de vue du processeur)
		/*
			À compléter. Selon l'adresse qui vient du processeur:
				- Lecture et stockage du nombre d'éléments à trier et 
				- lecture des éléments à trier
				- Tri (appel à bubbleSort)
				- Ne pas oublier d'utiliser les bons registres
		*/
			
		// Read (du point de vue du processeur)
		/*
			À compléter. Selon l'adresse qui vient du processeur:
				- Lecture du registre d'état du coprocesseur 
				- lecture du nombre d'éléments à envoyer au processeur
				- Lecture des élément triés
		*/

		if (addr == 0x2000){
			reg[2] = (0x0000);
			CoProcessor_Ready_OutPort.write(true);
			wait(CoProcessor_Enable_InPort.default_event());
			lenght = CoProcessor_Data_InPort.read();
			reg[0] = (lenght);
			data = new unsigned int(lenght);
			CoProcessor_Ready_OutPort.write(false);

		}else if(addr == 0x2001){
			CoProcessor_Ready_OutPort.write(true);
			wait(CoProcessor_Enable_InPort.default_event());
			data[i] = CoProcessor_Data_InPort.read();
			i++;	
			CoProcessor_Ready_OutPort.write(false);
			if(i >= reg[0]){
				reg[2].write(0x0001);
				bubbleSort(data, reg[0]);
				i = 0;
			}

		}else if(addr == 0x2002){
			output[0] = reg[2];
			CoProcessor_Data_OutPort.write(output[0]);
			CoProcessor_Ready_OutPort.write(true);
			wait(CoProcessor_Enable_InPort.default_event());
			CoProcessor_Ready_OutPort.write(false);

		}else if(addr == 0x2003){
			CoProcessor_Data_OutPort.write(lenght);
			CoProcessor_Ready_OutPort.write(true);
			wait(CoProcessor_Enable_InPort.default_event());	
			CoProcessor_Ready_OutPort.write(false);

		}else if(addr == 0x2004){
			CoProcessor_Data_OutPort.write(data[i]);
			CoProcessor_Ready_OutPort.write(true);
			wait(CoProcessor_Enable_InPort.default_event());
			CoProcessor_Ready_OutPort.write(false);
			i++;		
		}

	}
}


///////////////////////////////////////////////////////////////////////////////
//
//	bubbleSort
//
///////////////////////////////////////////////////////////////////////////////
void CoProcessor::bubbleSort(unsigned int *ptr, int n_elements)
{
	/*

		À compléter. Ne pas oublier de modifier l'état du bon registre du coprocesseur à la fin du traitement (voir énoncé)

	*/
	bool securityCheck = true;
    while (securityCheck) {
        wait();
        securityCheck = false;
        for (int i = 0; i < (n_elements - 1); i++) {
            if (ptr[i] > ptr[i + 1]) {
                unsigned int tmp = ptr[i];
                ptr[i] = ptr[i + 1];
                ptr[i + 1] = tmp;
                securityCheck = true;
            }
        }
    }
    reg[2] = (0x002);
}