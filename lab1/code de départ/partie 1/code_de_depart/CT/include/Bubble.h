///////////////////////////////////////////////////////////////////////////////
//
//	Bubble.h
//
///////////////////////////////////////////////////////////////////////////////
#ifndef _BUBBLE_H_
#define _BUBBLE_H_

#include <systemc.h>
#include <stdio.h>
#include "InterfaceRead.h"

///////////////////////////////////////////////////////////////////////////////
//
//	Class Bubble
//
///////////////////////////////////////////////////////////////////////////////
class Bubble : public sc_module
{
public:
	// Ports    
	/*
	À compléter
	*/
	sc_in_clk clk;
	sc_out<unsigned int> address;
	sc_in<unsigned int> data;
	sc_out<bool> request;
	sc_in<bool> ack;

	// Constructor
	Bubble(sc_module_name zName);

	// Destructor
	~Bubble();

private:
	// Process SystemC
	SC_HAS_PROCESS(Bubble);

	void bubble(void);
	void print();

	int state = 0;
	unsigned int addr = 0x00;
	bool securityCheck;
	int lenght = 0;
	unsigned int* dataPtr;
	unsigned int index = 0;
};

#endif