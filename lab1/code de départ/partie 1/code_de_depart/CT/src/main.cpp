///////////////////////////////////////////////////////////////////////////////
//
//	main.cpp
//
///////////////////////////////////////////////////////////////////////////////
#include <systemc.h>
#include "Bubble.h"
#include "Reader.h"
#include "DataRAM.h"

#define RAMSIZE 0x400

// Global variables
bool m_bError = false;

///////////////////////////////////////////////////////////////////////////////
//
//	Main
//
///////////////////////////////////////////////////////////////////////////////
int sc_main(int arg_count, char** arg_value)
{
	// Variables
	int sim_units = 2; //SC_NS 

	// Instanciation des composants
	/*
	À compléter
	*/
	Bubble iBubble("Bubble");
	Reader iReader("Reader");
	DataRAM iDataRAM("DataRAM", "memory/mem.hex", RAMSIZE, false);

	// Déclaration des signaux
	/*
	À compléter
	*/

	sc_clock clk("clk");
	
	sc_buffer<bool> ack;
	sc_buffer<bool> request;
	sc_buffer<unsigned int> data;
	sc_buffer<unsigned int> address;
	

	// Connexion des ports
	/*
	À compléter
	*/
	iReader.dataPortRAM(iDataRAM);
	iReader.data(data);
	iReader.address(address);
	iReader.ack(ack);
	iReader.request(request);
	iReader.clk(clk);

	iBubble.data(data);
	iBubble.address(address);
	iBubble.ack(ack);
	iBubble.request(request);
	iBubble.clk(clk);

	// Démarrage de l'application
	if (!m_bError)
	{
		cout << "Demarrage de la simulation." << endl;
		sc_start(-1, sc_core::sc_time_unit(sim_units));
		cout << endl << "Simulation s'est terminee a " << sc_time_stamp() << " ns";
	}
	// Fin du programme
	return 0;
}