///////////////////////////////////////////////////////////////////////////////
//
//	Bubble.cpp
//
///////////////////////////////////////////////////////////////////////////////
#include "Bubble.h"


///////////////////////////////////////////////////////////////////////////////
//
//	Constructeur
//
///////////////////////////////////////////////////////////////////////////////
Bubble::Bubble(sc_module_name zName)
	: sc_module(zName)
{
	/*
	À compléter
	*/
	SC_METHOD(bubble);
	sensitive << clk.pos();
}


///////////////////////////////////////////////////////////////////////////////
//
//	Destructeur
//
///////////////////////////////////////////////////////////////////////////////
Bubble::~Bubble()
{
}

void Bubble::print(){
	for (int i = 0; i < lenght; i++) 
	{
		cout << dataPtr[i]<< ", ";
	}
	cout << endl;
}


///////////////////////////////////////////////////////////////////////////////
//
//	Bubble: communication + traitement dans une machine à états
//
///////////////////////////////////////////////////////////////////////////////
void Bubble::bubble(void)
{
	/*		
	Machine à états. Exemple:
	- demande des données à Reader
	- lecture du nombre d'éléments
	- lecture des données (1 lecture par cycle d'hrologe)
	- Tri: a chaque passage dans l'état on fait un chagement de position si nécessaire. On reste dans l'état tant que le tri n'est pas terminé (1 opération par cycle d'hrologe)
	- Affichage (pas de wait nécessaire)
	- Arrêt de la simulations
	*/
	switch (state)
	{
	case 0:
		address.write(addr);
		request.write(true);
		if (ack.read())
		{
			state = 1;
			break;
		}	
		break;


	case 1:
		lenght = data.read();
		dataPtr = new unsigned int(lenght);
		request.write(false);
		state = 2;
		break;

	case 2:
		if (index != lenght)
		{
			addr += 0x04;
			address.write(addr);
			request.write(true);
			state = 3;
			break;
		}
		else
			print();
			state = 5;
			break;

	case 3:
		if (ack.read())
		{
			state = 4;
			break;
		}
		break;

	case 4:
		dataPtr[index] = data.read();
		request.write(false);
		index++;
		state = 2;
		break;

	case 5:
		securityCheck = true;
		cout << endl << "Bubble Begin " << sc_time_stamp() << endl;
		for (int index = 0; index < (lenght - 1); index++) {
			if (dataPtr[index] > dataPtr[index + 1]) {
				unsigned int tmp = dataPtr[index];
				dataPtr[index] = dataPtr[index + 1];
				dataPtr[index + 1] = tmp;
				securityCheck = false;
			}
		}
		cout << "Bubble end " << sc_time_stamp() << endl;

		if(securityCheck)
		{
			print();
			sc_stop();
		}
		break;
	}
}
