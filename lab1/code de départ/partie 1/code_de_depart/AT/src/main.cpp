///////////////////////////////////////////////////////////////////////////////
//
//	main.cpp
//
///////////////////////////////////////////////////////////////////////////////
#include <systemc.h>
#include "Bubble.h"
#include "Reader.h"
#include "DataRAM.h"

#define RAMSIZE 0x400

// Global variables
bool m_bError = false;

///////////////////////////////////////////////////////////////////////////////
//
//	Main
//
///////////////////////////////////////////////////////////////////////////////
int sc_main(int arg_count, char **arg_value)
{
	// Variables
	int sim_units = 2; //SC_NS 

	// Clock
	sc_clock sClk( "SysClock", 4000, SC_NS, 0.5 );
	
	// Instanciation des composants
	Bubble iBubble("Bubble");
	/*
	À compléter
	*/
	Reader iReader("Reader");
	DataRAM iDataRAM("DataRAM", "memory/mem.hex", RAMSIZE, false);
	
	// Signaux
	/*
	À compléter
	*/
	sc_buffer<unsigned int> bData;
	sc_buffer<unsigned int> bAddress;
	sc_buffer<bool> bAck;
	sc_buffer<bool> bRequest;
	
	// Connexion des ports
	iBubble.clk(sClk);
	/*
	À compléter
	*/
	iReader.dataPortRAM(iDataRAM);
	iReader.data(bData);
	iReader.address(bAddress);
	iReader.ack(bAck);
	iReader.request(bRequest);
	iReader.clk(sClk);

	iBubble.data(bData);
	iBubble.address(bAddress);
	iBubble.ack(bAck);
	iBubble.request(bRequest);
	
	// Démarrage de l'application
	if (!m_bError)
	{
		cout << "Demarrage de la simulation." << endl;
		sc_start( 20, SC_MS);
		cout << endl << "Simulation s'est terminee a " << sc_time_stamp() << endl;
	}
	// Fin du programme
	return 0;
}
