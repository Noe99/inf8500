///////////////////////////////////////////////////////////////////////////////
//
//	Bubble.cpp
//
///////////////////////////////////////////////////////////////////////////////
#include "Bubble.h"


///////////////////////////////////////////////////////////////////////////////
//
//	Constructeur
//
///////////////////////////////////////////////////////////////////////////////
Bubble::Bubble( sc_module_name zName )
: sc_module(zName)
{
	/*
	À compléter
	*/
	SC_THREAD(interface);
}


///////////////////////////////////////////////////////////////////////////////
//
//	Destructeur
//
///////////////////////////////////////////////////////////////////////////////
Bubble::~Bubble()
{
}

void Bubble::print(){
	for (int i = 0; i < lenght; i++) 
	{
		cout << dataPtr[i]<< ", ";
	}
	cout << endl;
}

///////////////////////////////////////////////////////////////////////////////
//
//	Interface avec le module Reader
//
///////////////////////////////////////////////////////////////////////////////
void Bubble::interface(void)
{
	/*
	À compléter
	*/
	address.write(addr);
	request.write(true);

	do {
		wait(clk->posedge_event());
	} while (!ack.read());

	lenght = data.read();
	request.write(false);

	dataPtr = new unsigned int(lenght);

	// Lecture des éléments à trier
	for (int index = 0; index < lenght; index++) {
		addr += 0x04;
		address.write(addr);
		request.write(true);

		do {
			wait(clk->posedge_event());
		} while (!ack.read());

		dataPtr[index] = data.read();
		request.write(false);
	}
	
	bubbleSort(dataPtr, lenght);
		
	// Arrêt de la simulation
	sc_stop();
}


///////////////////////////////////////////////////////////////////////////////
//
//	bubbleSort
//
///////////////////////////////////////////////////////////////////////////////
void Bubble::bubbleSort(unsigned int *ptr, int counter)
{
	// Affichage avant tri
	/*
	À compléter
	*/
	print();

	// Tri
	/*
	À compléter
	*/
	while (!securityCheck) {
		cout << endl << "Bubble Begin " << sc_time_stamp() << endl;
		wait(clk->posedge_event());
		securityCheck = true;
		for (int index = 0; index < (counter - 1); index++) {
			if (ptr[index] > ptr[index + 1]) {
				unsigned int tmp = ptr[index];
				ptr[index] = ptr[index + 1];
				ptr[index + 1] = tmp;
				securityCheck = false;
			}
		}
		cout << "Bubble end " << sc_time_stamp() << endl;
	}
	
	// Affichage après tri
	/*
	À compléter
	*/
	print();

	return;
}
