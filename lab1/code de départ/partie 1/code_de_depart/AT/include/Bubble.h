///////////////////////////////////////////////////////////////////////////////
//
//	Bubble.h
//
///////////////////////////////////////////////////////////////////////////////
#ifndef _BUBBLE_H_
#define _BUBBLE_H_

#include <systemc.h>
#include <stdio.h>
#include "InterfaceRead.h"


///////////////////////////////////////////////////////////////////////////////
//
//	Class Bubble
//
///////////////////////////////////////////////////////////////////////////////
class Bubble : public sc_module
{
	public: 
		// Ports    
		sc_in_clk				clk;  
		/*
		À compléter		
		*/
		sc_in<bool> ack;
		sc_out<unsigned int> address;
		sc_in<unsigned int> data;
		sc_out<bool> request;
	
		// Constructor
		Bubble( sc_module_name zName );
		
		// Destructor
		~Bubble();
		
	private:
		// Process SystemC
		SC_HAS_PROCESS(Bubble);
		
		void print();
		void interface(void);
		void bubbleSort(unsigned int *ptr, int counter);

		int lenght = 0;
		bool securityCheck = false;
		unsigned int addr = 0x00;
		unsigned int* dataPtr;
};

#endif
