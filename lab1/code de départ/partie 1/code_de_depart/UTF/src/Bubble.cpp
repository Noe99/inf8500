///////////////////////////////////////////////////////////////////////////////
//
//	Bubble.cpp
//
///////////////////////////////////////////////////////////////////////////////
#include "../include/Bubble.h"


///////////////////////////////////////////////////////////////////////////////
//
//	Constructeur
//
///////////////////////////////////////////////////////////////////////////////
Bubble::Bubble( sc_module_name zName )
: sc_module(zName)
{
	/*
	
	À compléter
	
	*/
	SC_THREAD(interface);
}


///////////////////////////////////////////////////////////////////////////////
//
//	Destructeur
//
///////////////////////////////////////////////////////////////////////////////
Bubble::~Bubble()
{
}


///////////////////////////////////////////////////////////////////////////////
//
//	Interface avec le module Reader: sc_thread ou sc_method
//
///////////////////////////////////////////////////////////////////////////////
void Bubble::interface(void)
{	
	// 1ere lecture: nombre d'éléments à trier
	// Variable
	int numValues = 0;
	unsigned int addr = 0x00;
	unsigned int* dataPtr;

	// 1ere lecture: nombre d'éléments à trier
	/*

	À compléter

	*/
	numValues = readPort->Read(addr);
	dataPtr = new unsigned int(numValues);

	// Lecture des éléments à trier
	/*

	À compléter

	*/
	for (int i = 0; i < numValues; i++) {
		addr += 0x04;
		dataPtr[i] = readPort->Read(addr);
	}

	//Appel à bubble sort
	/*

	À compléter

	*/
	bubbleSort(dataPtr, numValues);

	// Arrêt de la simulation
	sc_stop();

}


///////////////////////////////////////////////////////////////////////////////
//
//	bubbleSort
//
///////////////////////////////////////////////////////////////////////////////
void Bubble::bubbleSort(unsigned int *ptr, int counter)
{
	// Affichage avant tri
	/*

	À compléter

	*/
	for (int i = 0; i < counter; i++) {
		cout <<  ptr[i] << ", ";
	}
	cout << endl;

	// Tri
	/*

	À compléter

	*/
	bool isOrdered = false;

	while (!isOrdered) {
		isOrdered = true;
		for (int itr = 0; itr < (counter - 1); itr++) {
			if (ptr[itr] > ptr[itr + 1]) {
				unsigned int tmp = ptr[itr];
				ptr[itr] = ptr[itr + 1];
				ptr[itr + 1] = tmp;
				isOrdered = false;
			}
		}
	}

	// Affichage après tri
	/*
cout << endl << "Bubble Begin " << sc_time_stamp() << endl ;
cout << "Bubble end " << sc_time_stamp() << endl ;
	À compléter

	*/
	for (int i = 0; i < counter; i++) {
		cout <<  ptr[i] << ", ";
	}

	return;
}