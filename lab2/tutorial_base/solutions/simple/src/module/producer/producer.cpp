///////////////////////////////////////////////////////////////////////////////
//
// Filename : producer.cpp
//
// This file was initially generated by SpaceStudio but is meant to be modified
// by the user.
//
///////////////////////////////////////////////////////////////////////////////
#include "producer.h"

#include "SpaceDisplay.h"
#include "SpaceTypes.h"

producer::producer(sc_core::sc_module_name name, double period, sc_core::sc_time_unit unit, unsigned int id, unsigned char priority, bool verbose)
	: abstract_module(name, period, unit, id, priority, verbose)
{
	SC_THREAD(thread);
}

void producer::thread() {
	uint32_t reg_value = 240;
	uint32_t value = 240;
	uint32_t new_value = 0;
	while(1) {
		DeviceWrite(DDR_MEM_ID, INDEX*sizeof(uint32_t), value);
		RegisterWrite(REGISTER_FILE0_ID, INDEX, reg_value);

		/* synchronize */
		ModuleWrite(CONSUMER_ID[INDEX], SPACE_BLOCKING);
		ModuleRead(CONSUMER_ID[INDEX], SPACE_BLOCKING);

		value++;
		reg_value++;
	}
}
