//////////////////////////////////////////////////////////////////////////////
///
/// Space Codesign System Inc. - (https://www.spacecodesign.com)
/// Copyright 2005-2022. All rights reserved
///
/// No authorization to modify or use this file for
/// commercial purposes without prior written consent
/// of its author(s)
///
/// Created by: SpaceStudio generation engine
///
/// Warning : This file content will be overwritten by the next generation process.
///
//////////////////////////////////////////////////////////////////////////////
#ifndef PLATFORM_DEFINITIONS_H
#define PLATFORM_DEFINITIONS_H

#ifndef REGISTER_FILE0_ID
#define REGISTER_FILE0_ID 4
#endif
#ifndef PRODUCER0_ID
#define PRODUCER0_ID 3
#endif
#ifndef CONSUMER0_ID
#define CONSUMER0_ID 2
#endif
#ifndef DDR_MEM_ID
#define DDR_MEM_ID 8
#endif

#ifndef PRODUCER_GROUP_SIZE
#define PRODUCER_GROUP_SIZE 1
#endif
#ifndef CONSUMER_GROUP_SIZE
#define CONSUMER_GROUP_SIZE 1
#endif

#ifndef HW_MAPPING
#define HW_MAPPING 1
#endif
#ifndef SW_MAPPING
#define SW_MAPPING 2
#endif
#ifndef PRODUCER0_MAPPING
#define PRODUCER0_MAPPING SW_MAPPING
#endif
#ifndef CONSUMER0_MAPPING
#define CONSUMER0_MAPPING SW_MAPPING
#endif

#ifndef SPACE_OS_MICROC
#define SPACE_OS_MICROC 0
#endif
#ifndef SPACE_OS_BAREMETAL
#define SPACE_OS_BAREMETAL 1
#endif
#ifndef SPACE_OS_LINUX
#define SPACE_OS_LINUX 2
#endif

#ifndef SPACE_PROCESSOR_ZYNQ_APU
#define SPACE_PROCESSOR_ZYNQ_APU 0
#endif
#ifndef SPACE_PROCESSOR_MICROBLAZE
#define SPACE_PROCESSOR_MICROBLAZE 1
#endif
#ifndef SPACE_PROCESSOR_ZYNQ_ULTRASCALE_APU
#define SPACE_PROCESSOR_ZYNQ_ULTRASCALE_APU 2
#endif
#ifndef SPACE_INTERCONNECT_AXI4_INTERCONNECT
#define SPACE_INTERCONNECT_AXI4_INTERCONNECT 0
#endif

// Ordered by increasing INDEX (which is not necessarily the same as the
// integer in the name of the instance's ID).
const unsigned int PRODUCER_ID[] = {PRODUCER0_ID};
// Ordered by increasing INDEX (which is not necessarily the same as the
// integer in the name of the instance's ID).
const unsigned int CONSUMER_ID[] = {CONSUMER0_ID};

#endif
