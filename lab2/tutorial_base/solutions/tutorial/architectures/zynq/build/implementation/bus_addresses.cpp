//////////////////////////////////////////////////////////////////////////////
///
/// Space Codesign System Inc. - (https://www.spacecodesign.com)
/// Copyright 2005-2022. All rights reserved
///
/// No authorization to modify or use this file for
/// commercial purposes without prior written consent
/// of its author(s)
///
/// Created by: SpaceStudio generation engine
///
/// Warning : This file content will be overwritten by the next generation process.
///
//////////////////////////////////////////////////////////////////////////////
#include "spacelib_global.h"

namespace spacelib_global {
	extern bus_container axi4_interconnect0_container;
	extern bus_container zynq_interconnect0_container;

	bus_connection axi4_interconnect0_connections[] = {
			{ 0, { 10, 0x40000000, 0x40000FFF }, 0 },
			{ 1, { 11, 0x40001000, 0x40001FFF }, 0 },
			{ 2, { 13, 0x40002000, 0x40002FFF }, 0 },
			{ 3, { 14, 0x40003000, 0x40003FFF }, 0 }
	};
	bus_container axi4_interconnect0_container = { 4, axi4_interconnect0_connections };

	bus_connection zynq_interconnect0_connections[] = {
			{ 0, { 8, 0x00000000, 0x3FFFFFFF }, 0 },
			{ 1, { -1, 0x40000000, 0x7FFFFFFF }, 0 }
	};
	bus_container zynq_interconnect0_container = { 2, zynq_interconnect0_connections };

	addressable_id reserved_address_range[] = {
			{ 8, 0x10000000, 0x3FFFFFFF }
	};
	addressable_id_container reserved_address_range_container = { 1, reserved_address_range };

	addressable_id addressable_component[] = {
			{ 11, 0x40001000, 0x40001FFF },
			{ 14, 0x40003000, 0x40003FFF },
			{ 10, 0x40000000, 0x40000FFF },
			{ 13, 0x40002000, 0x40002FFF },
			{ 8, 0x00000000, 0x3FFFFFFF }
	};
	addressable_id_container addressable_component_container = { 5, addressable_component };
}
