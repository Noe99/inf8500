//////////////////////////////////////////////////////////////////////////////
///
/// Space Codesign System Inc. - (https://www.spacecodesign.com)
/// Copyright 2005-2022. All rights reserved
///
/// No authorization to modify or use this file for
/// commercial purposes without prior written consent
/// of its author(s)
///
/// Created by: SpaceStudio generation engine
///
/// Warning : This file content will be overwritten by the next generation process.
///
//////////////////////////////////////////////////////////////////////////////
#include "main.h"

#include <iostream>
#include <cstdlib>

#include <time.h>

using sc_core::SC_ABORT;
using sc_core::SC_CACHE_REPORT;
using sc_core::SC_DISPLAY;
using sc_core::SC_DO_NOTHING;
using sc_core::SC_ERROR;
using sc_core::SC_FATAL;
using sc_core::SC_LOG;
using sc_core::SC_STOP;
using sc_core::SC_THROW;
using sc_core::SC_DEFAULT_ERROR_ACTIONS;
using sc_core::SC_DEFAULT_FATAL_ACTIONS;

using sc_core::SC_SEC;
using sc_core::SC_MS;
using sc_core::SC_US;
using sc_core::SC_NS;
using sc_core::SC_PS;
using sc_core::SC_FS;

using sc_core::sc_clock;

FILE* log_file;

namespace spacelib_global {
	extern bus_container axi4_interconnect0_container;
	extern bus_container zynq_interconnect0_container;
}

int sc_main(int arg_count, char** arg_values) {
	sc_core::sc_report_handler::suppress(SC_THROW);
	sc_core::sc_report_handler::suppress(SC_ABORT);
	sc_core::sc_report_handler::set_actions("object already exists", SC_DO_NOTHING);
	sc_core::sc_report_handler::set_actions(SC_ERROR, SC_DEFAULT_ERROR_ACTIONS | SC_DISPLAY | SC_STOP);
	sc_core::sc_report_handler::set_actions(SC_FATAL, SC_DEFAULT_FATAL_ACTIONS | SC_DISPLAY | SC_STOP);

	// Variables for simulation time evaluation
	time_t simulation_time_begin = 0;
	time_t simulation_time_end = 0;

	// Variables for sorting the arguments
	int simulation_time = -1;
	int simulation_unit = 2; /* NS */

	log_file = fopen("log.txt", "w");

	if(spacelib_initialize() == -1)
		return 0;

	monitor_engine engine("monitor_engine");
	ddr ddr_mem("ddr_mem");
	axi4_interconnect zynq_interconnect0("zynq_interconnect0", 1, 10.0, SC_NS, 0x4, 0x1, (void*)&spacelib_global::zynq_interconnect0_container, false);
	zynq_apu zynq_apu0("zynq_apu0", 9, "localhost", "49155", false, false);
	register_adapter register_adapter1("register_adapter1", 10.0, SC_NS);
	reset_manager reset_manager0("reset_manager0", 10.0, SC_NS, false);
	pic pic0("pic0", 14, 10.0, SC_NS, false);
	processor_fifo processor_fifo1("processor_fifo1", new generic_fifo<4>(64), 7, false);
	register_file register_file0("register_file0");
	processor_fifo_adapter processor_fifo_adapter1("processor_fifo_adapter1");
	processor_fifo processor_fifo0("processor_fifo0", new generic_fifo<4>(64), 6, false);
	axi4_interconnect axi4_interconnect0("axi4_interconnect0", 5, 10.0, SC_NS, 0x4, 0x1, (void*)&spacelib_global::axi4_interconnect0_container, false);
	simulation_helper simulation_helper0("simulation_helper0", 13, 10.0, SC_NS, false);
	zynq zynq0("zynq0", &zynq_apu0, &zynq_interconnect0, &ddr_mem);
	sc_core::sc_signal< bool > pic0_irq_out;
	sc_core::sc_signal< bool > processor_fifo0_irq_has_room;
	sc_core::sc_signal< bool > processor_fifo1_irq_has_data;
	sc_core::sc_signal< bool > processor_fifo0_irq_has_data;
	sc_core::sc_signal< bool > processor_fifo1_irq_has_room;
	sc_core::sc_signal< bool > reset_signal;

	zynq0.axi_gp0(axi4_interconnect0.axi_slave[0]);
	zynq0.axi_hp[0](zynq_interconnect0.axi_slave[1]);
	zynq0.axi_hp[1](zynq_interconnect0.axi_slave[2]);
	zynq0.axi_hp[2](zynq_interconnect0.axi_slave[3]);
	zynq0.axi_hp[3](zynq_interconnect0.axi_slave[4]);
	register_adapter1.attach(&engine);
	processor_fifo1.attach(&engine);
	processor_fifo0.attach(&engine);
	axi4_interconnect0.attach(&engine);
	pic0.irq_out(pic0_irq_out);
	zynq0.spi[0](pic0_irq_out);
	axi4_interconnect0.axi_master[0](processor_fifo_adapter1.axi_slave);
	axi4_interconnect0.axi_master[1](register_adapter1.axi_slave);
	register_adapter1.register_master(register_file0.register_master);
	register_adapter1.register_slave(register_file0.register_slave);
	axi4_interconnect0.axi_master[2](simulation_helper0.axi_slave);
	axi4_interconnect0.axi_master[3](pic0.axi_slave);
	processor_fifo0.irq_has_room(processor_fifo0_irq_has_room);
	pic0.irq_in[0](processor_fifo0_irq_has_room);
	processor_fifo1.irq_has_data(processor_fifo1_irq_has_data);
	pic0.irq_in[1](processor_fifo1_irq_has_data);
	processor_fifo0.irq_has_data(processor_fifo0_irq_has_data);
	pic0.irq_in[2](processor_fifo0_irq_has_data);
	processor_fifo1.irq_has_room(processor_fifo1_irq_has_room);
	pic0.irq_in[3](processor_fifo1_irq_has_room);
	processor_fifo_adapter1.fifo_mm[0](processor_fifo0.fifo_write_mm);
	processor_fifo_adapter1.fifo_mm[1](processor_fifo1.fifo_read_mm);
	processor_fifo_adapter1.fifo_mm[2](processor_fifo0.fifo_read_mm);
	processor_fifo_adapter1.fifo_mm[3](processor_fifo1.fifo_write_mm);
	reset_manager0.reset(reset_signal);
	pic0.reset(reset_signal);
	processor_fifo0.reset(reset_signal);
	processor_fifo1.reset(reset_signal);
	ddr_mem.attach(&engine);
	zynq_interconnect0.attach(&engine);
	zynq_apu0.attach(&engine);
	zynq_apu0.axi_master(zynq_interconnect0.axi_slave[0]);
	zynq_interconnect0.axi_master[0](ddr_mem.axi_slave);
	zynq_interconnect0.axi_master[1](zynq0.axi_gp0);
	zynq_apu0.spi[0](zynq0.spi[0]);
	zynq_apu0.spi[1](zynq0.spi[1]);

	/* this is the free section */

	// No buffer for the output.
	setvbuf(stdout, NULL, _IONBF, 0);

	// Simulation section
	std::cout << "Starting simulation.\n";
	time(&simulation_time_begin);
	sc_core::sc_start(simulation_time, sc_core::sc_time_unit(simulation_unit));
	time(&simulation_time_end);

	std::cout << std::endl << "Simulation has ended @" << sc_core::sc_time_stamp().to_seconds() << " s";
	std::cout << std::endl << "Simulation wall clock time: " << (unsigned long)(simulation_time_end - simulation_time_begin) << " seconds." << std::endl << std::endl;
	std::cout.flush();

	fclose(log_file);

    return 0;
}
