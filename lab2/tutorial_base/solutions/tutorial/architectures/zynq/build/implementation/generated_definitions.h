//////////////////////////////////////////////////////////////////////////////
///
/// Space Codesign System Inc. - (https://www.spacecodesign.com)
/// Copyright 2005-2022. All rights reserved
///
/// No authorization to modify or use this file for
/// commercial purposes without prior written consent
/// of its author(s)
///
/// Created by: SpaceStudio generation engine
///
/// Warning : This file content will be overwritten by the next generation process.
///
//////////////////////////////////////////////////////////////////////////////
#ifndef GENERATED_DEFINITIONS_H
#define GENERATED_DEFINITIONS_H

#ifndef REGISTER_FILE0_ID
#define REGISTER_FILE0_ID 4
#endif
#ifndef PRODUCER0_ID
#define PRODUCER0_ID 3
#endif
#ifndef CONSUMER0_ID
#define CONSUMER0_ID 2
#endif


#endif
