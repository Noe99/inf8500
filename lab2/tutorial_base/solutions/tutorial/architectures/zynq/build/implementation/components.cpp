//////////////////////////////////////////////////////////////////////////////
///
/// Space Codesign System Inc. - (https://www.spacecodesign.com)
/// Copyright 2005-2022. All rights reserved
///
/// No authorization to modify or use this file for
/// commercial purposes without prior written consent
/// of its author(s)
///
/// Created by: SpaceStudio generation engine
///
/// Warning : This file content will be overwritten by the next generation process.
///
//////////////////////////////////////////////////////////////////////////////
#include "spacelib_global.h"

namespace spacelib_global {
	component_spec component_specs[] = {
		{ 5, -1, SPACE_DEVICE }, // axi4_interconnect0
		{ 2, 0, SPACE_MODULE }, // consumer0
		{ 8, -1, SPACE_DEVICE }, // ddr_mem
		{ 14, -1, SPACE_DEVICE }, // pic0
		{ 6, -1, SPACE_DEVICE }, // processor_fifo0
		{ 7, -1, SPACE_DEVICE }, // processor_fifo1
		{ 10, -1, SPACE_PROCESSOR_FIFO_ADAPTER }, // processor_fifo_adapter1
		{ 3, 0, SPACE_MODULE }, // producer0
		{ 11, -1, SPACE_DEVICE }, // register_adapter1
		{ 4, -1, SPACE_REGISTER_FILE }, // register_file0
		{ 13, -1, SPACE_DEVICE }, // simulation_helper0
		{ 9, -1, SPACE_DEVICE }, // zynq_apu0
		{ 1, -1, SPACE_DEVICE } // zynq_interconnect0
	};

	component_container components = { 13, component_specs };
}
