///////////////////////////////////////////////////////////////////////////////
///
///         Space Codesign System Inc. - (http://www.spacecodesign.com)
///         (c) All Rights Reserved. 2017
///
///         No authorization to modify or use this file for
///         commercial purposes without prior written consent
///         of its author(s)
///
///////////////////////////////////////////////////////////////////////////////
#ifndef ABSTRACT_MODULE_H
#define ABSTRACT_MODULE_H

#include "SpaceTypes.h"
#include "systemc"

#include "ap_int.h"

class abstract_module;

#define SPACE_ALIGNED

#endif
