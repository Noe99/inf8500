////////////////////////////////////////////////////////////////////////////////
//
// Space Codesign System Inc. - (https://www.spacecodesign.com)
// Copyright 2005-2022. All rights reserved
//
// No authorization to modify or use this file for
// commercial purposes without prior written consent
// of its author(s)
//
// Created by: SpaceStudio generation engine
//
// Warning : This file content will be overwritten by the next generation process.
//
////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//
// Filename : producer.cpp
//
// This file was initially generated by SpaceStudio but is meant to be modified
// by the user.
//
///////////////////////////////////////////////////////////////////////////////
#include "producer0.h"
#define HW_MAPPING 1
#define SW_MAPPING 2
#define MAPPING HW_MAPPING
#define SPACE_LITTLE_ENDIAN 1234
#define SPACE_BIG_ENDIAN 4321
#define SPACE_BYTE_ORDER SPACE_LITTLE_ENDIAN
#define SPACE_CONNECTION SPACE_SOC_COMPONENT.ZYNQ0
#define INDEX 0

#include "SpaceDisplay.h"
#include "SpaceTypes.h"


unsigned int fifo_read_id(unsigned int module_id, unsigned data_width) {
	switch(module_id) {
	case 2:
		return 0;
		break;
	default:
		return -1;
		break;
	}
}


unsigned int fifo_write_id(unsigned int module_id, unsigned data_width) {
	switch(module_id) {
	case 2:
		return 0;
		break;
	default:
		return -1;
		break;
	}
}

///////////////////////////////////
/// extraIncludes.cpp
// Empty for now
///////////////////////////////////

#include "SpaceSerialization.h"
#include "SpaceSerializationVivado.h"

using sc_core::sc_module;

producer0::producer0(sc_core::sc_module_name name)
:sc_module(name) {
	SC_CTHREAD(thread, clock.pos());
	reset_signal_is(reset, true);
}



void producer0::thread() {
	uint32_t reg_value = 240;
	uint32_t value = 240;
	uint32_t new_value = 0;
	MakeSignalsInactive();
		::sc_core::wait();
		::sc_core::wait();
		while(1) {
		DeviceWrite_single(DDR_MEM_ID, INDEX*sizeof(uint32_t), value);
		RegisterWrite(REGISTER_FILE0_ID, INDEX, reg_value);

		/* synchronize */
		ModuleWrite_0(fifo_write_id(CONSUMER_ID[INDEX], 4), 4, SPACE_BLOCKING);
		ModuleRead_0(fifo_read_id(CONSUMER_ID[INDEX], 4), 4, SPACE_BLOCKING);

		value = value + 1;
		reg_value = reg_value + 1;
	}
}
template <bool IS_ARR, class BusDataT, class DataT, class BusType>
typename space_enable_if<
	true
	, eSpaceStatus
>::type
	DeviceReadStruct(BusType& bus, uintptr_t startAddress, DataT* data, unsigned long nb_elements) {
	const bool FALSE_BUT_DEPENDS_ON_DATA_TYPE = sizeof(DataT) < 0;

	// Compile error if trying to use this override.
	int cause_compile_error_PLEASE_SEE_REST_OF_THIS_LINE[("If this array declaration causes a compile error, it means a DeviceRead was called for a struct for which no code was generated. This is likely a SpaceStudio bug. Please contact a Space Codesign representative." && FALSE_BUT_DEPENDS_ON_DATA_TYPE) ? 1 : -1];
}

template <bool IS_ARR, class BusDataT, class DataT, class BusType>
typename space_enable_if<
	true
	, eSpaceStatus
>::type
	DeviceWriteStruct(BusType& bus, uintptr_t startAddress, const DataT* data, unsigned long nb_elements) {
	const bool FALSE_BUT_DEPENDS_ON_DATA_TYPE = sizeof(DataT) < 0;

	// Compile error if trying to use this override.
	int cause_compile_error_PLEASE_SEE_REST_OF_THIS_LINE[("If this array declaration causes a compile error, it means a DeviceWrite was called for a struct for which no code was generated. This is likely a SpaceStudio bug. Please contact a Space Codesign representative." && FALSE_BUT_DEPENDS_ON_DATA_TYPE) ? 1 : -1];
}
///////////////////////////////////////////////////////////////////////////////
// To dev: See device_write.cpp for comments of DeviceWrite() code below
///////////////////////////////////////////////////////////////////////////////

#ifndef SPACE_EVALUATE
#define SPACE_EVALUATE(v) v
#endif

#define SPACE_DEVICE_WRITE_BUS_SIZE_MULTIPLE_OF_DATA_SIZE_IMPL(PREPARE_VALUE, VALUE_ACCESS)				\
	const uint32_t AXI_WIDTH = sizeof(BusDataT);															\
	const uint32_t AXI_BITWIDTH = 8 * AXI_WIDTH;															\
	const uint32_t ELEMS_PER_TRANF = (AXI_WIDTH - 1) / sizeof(DataT) + 1;									\
	const uint32_t ELEM_BITWIDTH = 8 * sizeof(DataT);														\
	const unsigned long AXI_ADDRESS = startAddress / AXI_WIDTH;												\
	const uint32_t NUM_TRANSFERS = nb_elements / ELEMS_PER_TRANF;											\
																											\
	DW_loop_bw_mult_dw: for (uint32_t i = 0; i < NUM_TRANSFERS; ++i) {										\
		_Pragma("HLS pipeline")																				\
		ap_uint<AXI_BITWIDTH> dataBufObj = 0;																\
		for (uint32_t j = 0; j < ELEMS_PER_TRANF; ++j) {													\
			_Pragma("HLS unroll")																			\
			const size_t currentElement = i * ELEMS_PER_TRANF + j;											\
			SPACE_EVALUATE(PREPARE_VALUE)																	\
			dataBufObj.range(ELEM_BITWIDTH * (j + 1) - 1, ELEM_BITWIDTH * j) = SPACE_EVALUATE(VALUE_ACCESS);\
		}																									\
		BusDataT rawBuf = dataBufObj;																		\
		bus->write(AXI_ADDRESS + i, &rawBuf);																\
	}																										\
																											\
	return SPACE_OK;

template <bool IS_ARR, class BusDataT, class DataT, class BusType>
inline typename space_enable_if<
	space_is_integral<DataT>::value
	, eSpaceStatus
>::type DeviceWriteBusBitwidthIsMultipleOfDataBitwidth(BusType& bus, uintptr_t startAddress, const DataT* data, unsigned long nb_elements) {
	SPACE_DEVICE_WRITE_BUS_SIZE_MULTIPLE_OF_DATA_SIZE_IMPL(, data[currentElement])
}

template <bool IS_ARR, class BusDataT, class DataT, class BusType>
inline typename space_enable_if<
	space_is_floating_point<DataT>::value &&
		space_is_floating_point_supported<DataT>::value
	, eSpaceStatus
>::type DeviceWriteBusBitwidthIsMultipleOfDataBitwidth(BusType& bus, uintptr_t startAddress, const DataT* data, unsigned long nb_elements) {
	#define SPACE_PREPARE_VALUE_FLOAT																		\
		space_floating_point_and_integral_union<DataT> space_float_reinterpreter;							\
		space_float_reinterpreter.space_f = data[currentElement];

	SPACE_DEVICE_WRITE_BUS_SIZE_MULTIPLE_OF_DATA_SIZE_IMPL(SPACE_PREPARE_VALUE_FLOAT, space_float_reinterpreter.space_i)
	#undef SPACE_PREPARE_VALUE_FLOAT
}

template <bool IS_ARR, class BusDataT, class DataT, class BusType>
inline typename space_enable_if<
	space_is_floating_point<DataT>::value &&
		!space_is_floating_point_supported<DataT>::value
	, eSpaceStatus
>::type DeviceWriteBusBitwidthIsMultipleOfDataBitwidth(BusType& bus, uintptr_t startAddress, const DataT* data, unsigned long nb_elements) {
	const bool FALSE_BUT_DEPENDS_ON_DATA_TYPE = sizeof(DataT) < 0;

	// Compile error if trying to use this override.
	int cause_compile_error_PLEASE_SEE_REST_OF_THIS_LINE[("If this array declaration causes a compile error, it means a DeviceWrite is trying to transfer an unsupported floating-point type." && FALSE_BUT_DEPENDS_ON_DATA_TYPE) ? 1 : -1];
}

template <bool IS_ARR, class BusDataT, class DataT, class BusType>
inline typename space_enable_if<
	space_is_integral<DataT>::value
	, eSpaceStatus
>::type DeviceWriteBusBitwidthIsMultipleOfDataBitwidth_single(BusType& bus, uintptr_t startAddress, const DataT* data, unsigned long nb_elements) {
	SPACE_DEVICE_WRITE_BUS_SIZE_MULTIPLE_OF_DATA_SIZE_IMPL(, *data)
}

template <bool IS_ARR, class BusDataT, class DataT, class BusType>
inline typename space_enable_if<
	space_is_floating_point<DataT>::value &&
		space_is_floating_point_supported<DataT>::value
	, eSpaceStatus
>::type DeviceWriteBusBitwidthIsMultipleOfDataBitwidth_single(BusType& bus, uintptr_t startAddress, const DataT* data, unsigned long nb_elements) {
	#define SPACE_PREPARE_VALUE_FLOAT																		\
		space_floating_point_and_integral_union<DataT> space_float_reinterpreter;							\
		space_float_reinterpreter.space_f = *data;

	SPACE_DEVICE_WRITE_BUS_SIZE_MULTIPLE_OF_DATA_SIZE_IMPL(SPACE_PREPARE_VALUE_FLOAT, space_float_reinterpreter.space_i)
	#undef SPACE_PREPARE_VALUE_FLOAT
}

template <bool IS_ARR, class BusDataT, class DataT, class BusType>
inline typename space_enable_if<
	space_is_floating_point<DataT>::value &&
		!space_is_floating_point_supported<DataT>::value
	, eSpaceStatus
>::type DeviceWriteBusBitwidthIsMultipleOfDataBitwidth_single(BusType& bus, uintptr_t startAddress, const DataT* data, unsigned long nb_elements) {
	const bool FALSE_BUT_DEPENDS_ON_DATA_TYPE = sizeof(DataT) < 0;

	// Compile error if trying to use this override.
	int cause_compile_error_PLEASE_SEE_REST_OF_THIS_LINE[("If this array declaration causes a compile error, it means a DeviceWrite is trying to transfer an unsupported floating-point type." && FALSE_BUT_DEPENDS_ON_DATA_TYPE) ? 1 : -1];
}

#undef SPACE_DEVICE_WRITE_BUS_SIZE_MULTIPLE_OF_DATA_SIZE_IMPL

#define SPACE_DEVICE_WRITE_DATA_SIZE_MULTIPLE_OF_BUS_SIZE_IMPL(PREPARE_VALUE)								\
	const uint32_t AXI_WIDTH = sizeof(BusDataT);															\
	const uint32_t AXI_BITWIDTH = 8 * AXI_WIDTH;															\
	const uint32_t TRANSF_PER_ELEM = (sizeof(DataT) - 1) / AXI_WIDTH + 1;									\
	const uint32_t ELEM_BITWIDTH = 8 * sizeof(DataT);														\
	const unsigned long AXI_ADDRESS = startAddress / AXI_WIDTH;												\
	const uint32_t NUM_TRANSFERS = nb_elements * TRANSF_PER_ELEM;											\
																											\
	uint8_t transferIdxInCurrentElem = 0;																	\
	uint32_t currentElement = 0;																			\
	BusDataT rawBuf[TRANSF_PER_ELEM];																		\
	DW_loop_dw_mult_bw: for (uint32_t i = 0; i < NUM_TRANSFERS; ++i) {										\
		_Pragma("HLS pipeline")																				\
		bool getNextElement = transferIdxInCurrentElem == 0;												\
		if (getNextElement) {																				\
			SPACE_EVALUATE(PREPARE_VALUE)																	\
			for (uint32_t j = 0; j < TRANSF_PER_ELEM; ++j) {												\
				_Pragma("HLS unroll")																		\
				rawBuf[j] = bufObj.range(AXI_BITWIDTH * (j + 1) - 1, AXI_BITWIDTH * j);						\
			}																								\
		}																									\
																											\
		BusDataT* bufToWrite;																				\
		for (uint8_t j = 0; j < TRANSF_PER_ELEM; ++j) {														\
			_Pragma("HLS unroll")																			\
			if (transferIdxInCurrentElem == j) {															\
				bufToWrite = &rawBuf[j];																	\
			}																								\
		}																									\
																											\
		bus->write(AXI_ADDRESS + i, bufToWrite);															\
																											\
		_Pragma("clang diagnostic push")																	\
		_Pragma("clang diagnostic ignored \"-Wtautological-compare\"")										\
		bool elementFinished = transferIdxInCurrentElem >= TRANSF_PER_ELEM - 1;								\
		_Pragma("clang diagnostic pop")																		\
																											\
		if (elementFinished) {																				\
			transferIdxInCurrentElem = 0;																	\
			++currentElement;																				\
		}																									\
		else {																								\
			++transferIdxInCurrentElem;																		\
		}																									\
	}																										\
																											\
	return SPACE_OK;

template <bool IS_ARR, class BusDataT, class DataT, class BusType>
inline typename space_enable_if<
	space_is_integral<DataT>::value
	, eSpaceStatus
>::type DeviceWriteDataBitwidthIsMultipleOfBusBitwidth(BusType& bus, uintptr_t startAddress, const DataT* data, unsigned long nb_elements) {
	SPACE_DEVICE_WRITE_DATA_SIZE_MULTIPLE_OF_BUS_SIZE_IMPL(ap_uint<ELEM_BITWIDTH> bufObj = data[currentElement];)
}

template <bool IS_ARR, class BusDataT, class DataT, class BusType>
inline typename space_enable_if<
	space_is_floating_point<DataT>::value &&
		space_is_floating_point_supported<DataT>::value
	, eSpaceStatus
>::type DeviceWriteDataBitwidthIsMultipleOfBusBitwidth(BusType& bus, uintptr_t startAddress, const DataT* data, unsigned long nb_elements) {
	#define SPACE_PREPARE_VALUE_FLOAT																		\
		space_floating_point_and_integral_union<DataT> space_float_reinterpreter;							\
		space_float_reinterpreter.space_f = data[currentElement];											\
		ap_uint<ELEM_BITWIDTH> bufObj = space_float_reinterpreter.space_i;

	SPACE_DEVICE_WRITE_DATA_SIZE_MULTIPLE_OF_BUS_SIZE_IMPL(SPACE_PREPARE_VALUE_FLOAT)
	#undef SPACE_PREPARE_VALUE_FLOAT
}

template <bool IS_ARR, class BusDataT, class DataT, class BusType>
inline typename space_enable_if<
	space_is_floating_point<DataT>::value &&
		!space_is_floating_point_supported<DataT>::value
	, eSpaceStatus
>::type DeviceWriteDataBitwidthIsMultipleOfBusBitwidth(BusType& bus, uintptr_t startAddress, const DataT* data, unsigned long nb_elements) {
	const bool FALSE_BUT_DEPENDS_ON_DATA_TYPE = sizeof(DataT) < 0;

	// Compile error if trying to use this override.
	int cause_compile_error_PLEASE_SEE_REST_OF_THIS_LINE[("If this array declaration causes a compile error, it means a DeviceWrite is trying to transfer an unsupported floating-point type." && FALSE_BUT_DEPENDS_ON_DATA_TYPE) ? 1 : -1];
}

template <bool IS_ARR, class BusDataT, class DataT, class BusType>
inline typename space_enable_if<
	space_is_integral<DataT>::value
	, eSpaceStatus
>::type DeviceWriteDataBitwidthIsMultipleOfBusBitwidth_single(BusType& bus, uintptr_t startAddress, const DataT* data, unsigned long nb_elements) {
	SPACE_DEVICE_WRITE_DATA_SIZE_MULTIPLE_OF_BUS_SIZE_IMPL(ap_uint<ELEM_BITWIDTH> bufObj = *data;)
}

template <bool IS_ARR, class BusDataT, class DataT, class BusType>
inline typename space_enable_if<
	space_is_floating_point<DataT>::value &&
		space_is_floating_point_supported<DataT>::value
	, eSpaceStatus
>::type DeviceWriteDataBitwidthIsMultipleOfBusBitwidth_single(BusType& bus, uintptr_t startAddress, const DataT* data, unsigned long nb_elements) {
	#define SPACE_PREPARE_VALUE_FLOAT																		\
		space_floating_point_and_integral_union<DataT> space_float_reinterpreter;							\
		space_float_reinterpreter.space_f = *data;															\
		ap_uint<ELEM_BITWIDTH> bufObj = space_float_reinterpreter.space_i;

	SPACE_DEVICE_WRITE_DATA_SIZE_MULTIPLE_OF_BUS_SIZE_IMPL(SPACE_PREPARE_VALUE_FLOAT)
	#undef SPACE_PREPARE_VALUE_FLOAT
}

template <bool IS_ARR, class BusDataT, class DataT, class BusType>
inline typename space_enable_if<
	space_is_floating_point<DataT>::value &&
		!space_is_floating_point_supported<DataT>::value
	, eSpaceStatus
>::type DeviceWriteDataBitwidthIsMultipleOfBusBitwidth_single(BusType& bus, uintptr_t startAddress, const DataT* data, unsigned long nb_elements) {
	const bool FALSE_BUT_DEPENDS_ON_DATA_TYPE = sizeof(DataT) < 0;

	// Compile error if trying to use this override.
	int cause_compile_error_PLEASE_SEE_REST_OF_THIS_LINE[("If this array declaration causes a compile error, it means a DeviceWrite is trying to transfer an unsupported floating-point type." && FALSE_BUT_DEPENDS_ON_DATA_TYPE) ? 1 : -1];
}

#undef SPACE_DEVICE_WRITE_DATA_SIZE_MULTIPLE_OF_BUS_SIZE_IMPL

#define SPACE_DEVICE_WRITE_COMMON_IMPL(BUS_SIZE_MULTIPLE_OF_DATA_SIZE_FUNC_NAME, DATA_SIZE_MULTIPLE_OF_BUS_SIZE_FUNC_NAME, IS_TRANSFERRING_ARRAY)	\
	const uint32_t AXI_WIDTH = sizeof(BusDataT);															\
	const uint32_t AXI_BITWIDTH = 8 * AXI_WIDTH;															\
	const bool BUSWIDTH_IS_MULTIPLE_OF_DATAWIDTH = (AXI_WIDTH % sizeof(NPtr)) == 0;							\
	const bool DATAWIDTH_IS_MULTIPLE_OF_BUSWIDTH = (sizeof(NPtr) % AXI_WIDTH) == 0;							\
																											\
	const size_t TOTAL_TRANSFER_SIZE = nb_elements * sizeof(NPtr);											\
	const bool TOTAL_TRANSFER_SIZE_IS_MULTIPLE_OF_AXIWIDTH = (TOTAL_TRANSFER_SIZE % AXI_WIDTH) == 0;		\
																											\
	{																										\
		/* Compile error if total transfer size is not a multiple of bus width.							*/	\
		int cause_compile_error_PLEASE_SEE_REST_OF_THIS_LINE[("If this array declaration causes a compile error, it means a DeviceWrite is trying to write a total number of bytes not a multiple of the bus width (sizeof(BusDataT)). Note that this error is also perfectly normal when writing a multi-dimensional array if the total size of the last dimension is not a multiple of the bus width. Please add some padding to the data your are writing so as to make a transfer a total size that is a multiple of the bus width. This requirement is because the underlying hardware and tools usually only support transfers of the size of the data bus." && (TOTAL_TRANSFER_SIZE_IS_MULTIPLE_OF_AXIWIDTH)) ? 1 : -1];	\
	}																										\
	{																										\
		/* Compile error if element size is not a multiple or divisor of bus width.						*/	\
		int cause_compile_error_PLEASE_SEE_REST_OF_THIS_LINE[("If this array declaration causes a compile error, it means a DeviceWrite is trying to write elements whose size is not a multiple of the bus width (sizeof(BusDataT)) nor a divisor of it. Please add some padding to the data your are writing so as to meet this criterion. This requirement is because the underlying hardware and tools usually only support transfers of the size of the data bus." && (BUSWIDTH_IS_MULTIPLE_OF_DATAWIDTH || DATAWIDTH_IS_MULTIPLE_OF_BUSWIDTH)) ? 1 : -1];	\
	}																										\
																											\
	if (BUSWIDTH_IS_MULTIPLE_OF_DATAWIDTH) {																\
		return BUS_SIZE_MULTIPLE_OF_DATA_SIZE_FUNC_NAME<(IS_TRANSFERRING_ARRAY), BusDataT>(bus, startAddress, data, nb_elements);	\
	}																										\
	return DATA_SIZE_MULTIPLE_OF_BUS_SIZE_FUNC_NAME<(IS_TRANSFERRING_ARRAY), BusDataT>(bus, startAddress, data, nb_elements);

template <class BusDataT, class NPtr, class BusType>
inline typename space_enable_if<
		space_is_arithmetic<NPtr>::value || space_is_enum<NPtr>::value
	, eSpaceStatus
>::type
DeviceWriteRouted(BusType& bus, uintptr_t startAddress, const NPtr* data, unsigned long nb_elements) {
	#pragma HLS inline
	SPACE_DEVICE_WRITE_COMMON_IMPL(DeviceWriteBusBitwidthIsMultipleOfDataBitwidth, DeviceWriteDataBitwidthIsMultipleOfBusBitwidth, true)
}

template <class BusDataT, class NPtr, class BusType>
inline typename space_enable_if<
		space_is_arithmetic<NPtr>::value || space_is_enum<NPtr>::value
	, eSpaceStatus
>::type
DeviceWriteRouted_single(BusType& bus, uintptr_t startAddress, const NPtr* data, unsigned long nb_elements) {
	#pragma HLS inline
	SPACE_DEVICE_WRITE_COMMON_IMPL(DeviceWriteBusBitwidthIsMultipleOfDataBitwidth_single, DeviceWriteDataBitwidthIsMultipleOfBusBitwidth_single, false)
}

#undef SPACE_DEVICE_WRITE_COMMON_IMPL

template <class BusDataT, class StruT, class BusType>
inline typename space_enable_if<
		space_is_class<StruT>::value
	, eSpaceStatus
>::type
DeviceWriteRouted(BusType& bus, uintptr_t startAddress, const StruT* data, unsigned long nb_elements) {
	#pragma HLS inline
	return DeviceWriteStruct<true, BusDataT>(bus, startAddress, data, nb_elements);
}

template <class BusDataT, class StruT, class BusType>
inline typename space_enable_if<
		space_is_class<StruT>::value
	, eSpaceStatus
>::type
DeviceWriteRouted_single(BusType& bus, uintptr_t startAddress, const StruT* data, unsigned long nb_elements) {
	#pragma HLS inline
	return DeviceWriteStruct<false, BusDataT>(bus, startAddress, data, nb_elements);
}

template <class BusDataT, class UniT, class BusType>
inline typename space_enable_if<
		space_is_union<UniT>::value
	, eSpaceStatus
>::type
DeviceWriteRouted(BusType& bus, uintptr_t startAddress, const UniT* data, unsigned long nb_elements) {
	const bool FALSE_BUT_DEPENDS_ON_DATA_TYPE = sizeof(UniT) < 0;

	// Compile error if trying to use this override.
	int cause_compile_error_PLEASE_SEE_REST_OF_THIS_LINE[("If this array declaration causes a compile error, it means a DeviceWrite is trying to is trying to transfer a union type. This is currently unsupported with Vivado HLS. It is possible to implement this by transferring the largest field of the union, however." && FALSE_BUT_DEPENDS_ON_DATA_TYPE) ? 1 : -1];
}

template <class BusDataT, class UniT, class BusType>
inline typename space_enable_if<
		space_is_union<UniT>::value
	, eSpaceStatus
>::type
DeviceWriteRouted_single(BusType& bus, uintptr_t startAddress, const UniT* data, unsigned long nb_elements) {
	const bool FALSE_BUT_DEPENDS_ON_DATA_TYPE = sizeof(UniT) < 0;

	// Compile error if trying to use this override.
	int cause_compile_error_PLEASE_SEE_REST_OF_THIS_LINE[("If this array declaration causes a compile error, it means a DeviceWrite is trying to is trying to transfer a union type. This is currently unsupported with Vivado HLS. It is possible to implement this by transferring the largest field of the union, however." && FALSE_BUT_DEPENDS_ON_DATA_TYPE) ? 1 : -1];
}

template<class BusDataT, class AnyPtrPtr, class BusType>
inline typename space_enable_if<
	space_is_pointer_or_array<AnyPtrPtr>::value &&
		!space_is_same<AnyPtrPtr, typename space_remove_pointer<AnyPtrPtr>::type>::value &&
		space_is_pointer_or_array<typename space_remove_pointer<AnyPtrPtr>::type>::value
	, eSpaceStatus
>::type
DeviceWriteRouted(BusType& bus, uintptr_t startAddress, AnyPtrPtr data, unsigned long nb_elements) {
	#pragma HLS inline recursive
	return DeviceWriteRouted<BusDataT>(bus, startAddress, *data, nb_elements);
}

#undef SPACE_EVALUATE

eSpaceStatus ModuleReadMSA_0(producer0* module, unsigned int data_size, unsigned long timeout, unsigned int* data, unsigned long nb_elements) {
	{
		#pragma HLS PROTOCOL fixed
	}
	eSpaceStatus status = SPACE_OK;
	unsigned int i;
	const unsigned long nb_iterations = nb_elements;

	bool has_sample = module->fifo_in_0.num_available() != 0;

	if (timeout == SPACE_WAIT_FOREVER || has_sample) {
		MR_loop0: for (i = 0; i < nb_iterations; i++) {
		ap_uint<32> temp;
		temp = module->fifo_in_0.read();
		GET_LONG(temp, *data);
			if (i < nb_iterations - 1) {
				data++;
			}
		}
	}
	else {
		status = SPACE_EMPTY;
	}
	{
		#pragma HLS PROTOCOL fixed
	}
	return status;
}

eSpaceStatus ModuleWriteMSA_0(producer0* module, unsigned int data_size, unsigned long timeout, const unsigned int* data, unsigned long nb_elements) {
	{
		#pragma HLS PROTOCOL fixed
	}
	eSpaceStatus status = SPACE_OK;
	unsigned int i;
	const unsigned long nb_iterations = nb_elements;

	bool has_free_slot = module->fifo_out_0.num_free() != 0;

	if (timeout == SPACE_WAIT_FOREVER || has_free_slot) {
		MW_loop1: for (i = 0; i < nb_iterations; i++) {
			#pragma HLS pipeline


		ap_uint<32> temp = 0;
		PUT_LONG(temp, *data);
		module->fifo_out_0.write(temp);
			if (i < nb_iterations - 1) {
				data++;
			}
		}
	}
	else {
		status = SPACE_FULL;
	}
	{
		#pragma HLS PROTOCOL fixed
	}
	return status;
}

template <class NPtr>
typename space_enable_if<
	!space_is_pointer_or_array<NPtr>::value &&
		(space_is_integral<NPtr>::value || space_is_enum<NPtr>::value),
	eSpaceStatus
>::type RegisterWrite_0(producer0* module, unsigned long register_id, const NPtr* data) {
	module->RegisterWriteDataPort_0.write(*data);
	module->RegisterWriteEnablePort_0.write(1);
	sc_core::wait();
	module->RegisterWriteEnablePort_0.write(0);
	return SPACE_OK;
}

template <class NPtr>
typename space_enable_if<
	!space_is_pointer_or_array<NPtr>::value &&
		space_is_floating_point<NPtr>::value &&
		space_is_floating_point_supported<NPtr>::value,
	eSpaceStatus
>::type RegisterWrite_0(producer0* module, unsigned long register_id, const NPtr* data) {
	const uint32_t DATA_BITWIDTH = 32;

	space_floating_point_and_integral_union<NPtr> space_float_reinterpreter;
	space_float_reinterpreter.space_f = *data;
	ap_uint<DATA_BITWIDTH> dataBufObj = space_float_reinterpreter.space_i;

	module->RegisterWriteDataPort_0.write(dataBufObj.to_uint());
	module->RegisterWriteEnablePort_0.write(1);
	sc_core::wait();
	module->RegisterWriteEnablePort_0.write(0);
	return SPACE_OK;
}


template <class AnyPtr>
	inline typename space_enable_if<
		space_is_pointer<AnyPtr>::value
			&& space_is_pointer_or_array<
				typename space_remove_pointer<AnyPtr>::type
			>::value,
		eSpaceStatus
	>::type
producer0::ModuleRead_0(unsigned int fifo_id, unsigned int data_width, unsigned long timeout, AnyPtr data, unsigned long nb_elements) {
	#pragma HLS inline recursive
	return ModuleRead_0(fifo_id, data_width, timeout, *data, nb_elements);
}

inline eSpaceStatus producer0::ModuleRead_0(unsigned int fifo_id, unsigned int data_width, unsigned long timeout, unsigned int* data, unsigned long nb_elements) {
	#pragma HLS inline recursive
	if (fifo_id == 0)
		return ModuleReadMSA_0(this, data_width, timeout, data, nb_elements);
	else
		return SPACE_ERROR;
}


template <class AnyPtr>
	inline typename space_enable_if<
		space_is_pointer<AnyPtr>::value
			&& space_is_pointer_or_array<
				typename space_remove_pointer<AnyPtr>::type
			>::value,
		eSpaceStatus
	>::type
producer0::ModuleWrite_0(unsigned int fifo_id, unsigned int data_width, unsigned long timeout, const AnyPtr data, unsigned long nb_elements) {
	#pragma HLS inline recursive
	return ModuleWrite_0(fifo_id, data_width, timeout, *data, nb_elements);
}

inline eSpaceStatus producer0::ModuleWrite_0(unsigned int fifo_id, unsigned int data_width, unsigned long timeout, const unsigned int* data, unsigned long nb_elements) {
	#pragma HLS inline recursive
	if (fifo_id == 0)
		return ModuleWriteMSA_0(this, data_width, timeout, data, nb_elements);
	else
		return SPACE_ERROR;
}


template <typename T>
inline typename space_enable_if<
	!space_is_array<T>::value &&
		space_is_pointer<T>::value
	, eSpaceStatus
>::type
producer0::DeviceRead_single(unsigned int destination_id, uintptr_t offset, T data, unsigned long nb_elements) {
	return SPACE_ERROR;
}

template <typename T>
inline typename space_enable_if<
	!space_is_array<T>::value &&
		space_is_pointer<T>::value
	, eSpaceStatus
>::type
producer0::DeviceRead(unsigned int destination_id, uintptr_t offset, T data, unsigned long nb_elements) {
	return SPACE_ERROR;
}

template <typename T, size_t DIM0>
inline eSpaceStatus
producer0::DeviceRead(unsigned int destination_id, uintptr_t offset, T (&data)[DIM0], unsigned long nb_elements) {
	return SPACE_ERROR;
}

template <typename T>
inline typename space_enable_if<
	!space_is_array<T>::value &&
		space_is_pointer<T>::value
	, eSpaceStatus
>::type
producer0::DeviceWrite_single(unsigned int destination_id, uintptr_t offset, const T data, unsigned long nb_elements) {
	if (destination_id == 8) {
		const uintptr_t DEST_BASE_ADDR = 0x10000000;
		return DeviceWriteRouted_single<int>(this->axi_master_0, DEST_BASE_ADDR + offset, data, nb_elements);
	} else
	return SPACE_ERROR;
}

template <typename T>
inline typename space_enable_if<
	!space_is_array<T>::value &&
		space_is_pointer<T>::value
	, eSpaceStatus
>::type
producer0::DeviceWrite(unsigned int destination_id, uintptr_t offset, const T data, unsigned long nb_elements) {
	if (destination_id == 8) {
		const uintptr_t DEST_BASE_ADDR = 0x10000000;
		return DeviceWriteRouted<int>(this->axi_master_0, DEST_BASE_ADDR + offset, data, nb_elements);
	} else
	return SPACE_ERROR;
}

template <typename T, size_t DIM0>
inline eSpaceStatus
producer0::DeviceWrite(unsigned int destination_id, uintptr_t offset, const T (&data)[DIM0], unsigned long nb_elements) {
	if (destination_id == 8) {
		const uintptr_t DEST_BASE_ADDR = 0x10000000;
		return DeviceWriteRouted<int>(this->axi_master_0, DEST_BASE_ADDR + offset, data, nb_elements);
	} else
	return SPACE_ERROR;
}

template <typename T>
eSpaceStatus producer0::RegisterRead(unsigned int register_file_id, unsigned long register_id, T* data) {
	// Compile error if sizeof(T) != 4.
	SPACE_STATIC_ASSERT("A RegisterRead is trying to transfer something that is not of size 4B. This is not supported.", sizeof(T) == 4);
	// Compile error if T is a pointer or array.
	SPACE_STATIC_ASSERT("A RegisterRead is trying to transfer a pointer or array. This is not supported.", !space_is_array<T>::value && !space_is_pointer<T>::value);
	// Compile error if T is not an integral, floating-point, or enum.
	SPACE_STATIC_ASSERT("A RegisterRead is trying to transfer a type that is not integral/enum or a floating-point. This is not supported.", space_is_integral<T>::value || space_is_enum<T>::value || space_is_floating_point<T>::value);
	return SPACE_ERROR;
}

template <typename T>
eSpaceStatus producer0::RegisterWrite(unsigned int register_file_id, unsigned long register_id, const T* data) {
	// Compile error if sizeof(T) != 4.
	SPACE_STATIC_ASSERT("A RegisterWrite is trying to transfer something that is not of size 4B. This is not supported.", sizeof(T) == 4);
	// Compile error if T is a pointer or array.
	SPACE_STATIC_ASSERT("A RegisterWrite is trying to transfer a pointer or array. This is not supported.", !space_is_array<T>::value && !space_is_pointer<T>::value);
	// Compile error if T is not an integral, floating-point, or enum.
	SPACE_STATIC_ASSERT("A RegisterWrite is trying to transfer a type that is not integral/enum or a floating-point. This is not supported.", space_is_integral<T>::value || space_is_enum<T>::value || space_is_floating_point<T>::value);
	if (register_file_id == 4 && register_id == 0) {
		return RegisterWrite_0(this, register_id, data);
	} else
	return SPACE_ERROR;
}

void producer0::MakeSignalsInactive() {
	RegisterWriteEnablePort_0.write(0);
	RegisterWriteDataPort_0.write(0);
}
