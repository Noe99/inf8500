-- ==============================================================
-- RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC
-- Version: 2018.3
-- Copyright (C) 1986-2018 Xilinx, Inc. All Rights Reserved.
-- 
-- ===========================================================

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity producer0 is
port (
    clock : IN STD_LOGIC;
    reset : IN STD_LOGIC;
    fifo_in_0_dout : IN STD_LOGIC_VECTOR (31 downto 0);
    fifo_in_0_empty_n : IN STD_LOGIC;
    fifo_in_0_read : OUT STD_LOGIC;
    fifo_out_0_din : OUT STD_LOGIC_VECTOR (31 downto 0);
    fifo_out_0_full_n : IN STD_LOGIC;
    fifo_out_0_write : OUT STD_LOGIC;
    RegisterWriteEnablePort_0 : OUT STD_LOGIC_VECTOR (0 downto 0) := "0";
    RegisterWriteDataPort_0 : OUT STD_LOGIC_VECTOR (31 downto 0) := "00000000000000000000000000000000";
    axi_master_0_req_din : OUT STD_LOGIC;
    axi_master_0_req_full_n : IN STD_LOGIC;
    axi_master_0_req_write : OUT STD_LOGIC;
    axi_master_0_rsp_empty_n : IN STD_LOGIC;
    axi_master_0_rsp_read : OUT STD_LOGIC;
    axi_master_0_address : OUT STD_LOGIC_VECTOR (31 downto 0);
    axi_master_0_datain : IN STD_LOGIC_VECTOR (31 downto 0);
    axi_master_0_dataout : OUT STD_LOGIC_VECTOR (31 downto 0);
    axi_master_0_size : OUT STD_LOGIC_VECTOR (31 downto 0) );
end;


architecture behav of producer0 is 
    attribute CORE_GENERATION_INFO : STRING;
    attribute CORE_GENERATION_INFO of behav : architecture is
    "producer0,hls_ip_2018_3,{HLS_INPUT_TYPE=sc,HLS_INPUT_FLOAT=0,HLS_INPUT_FIXED=0,HLS_INPUT_PART=xc7z020clg484-1,HLS_INPUT_CLOCK=10.000000,HLS_INPUT_ARCH=others,HLS_SYN_CLOCK=7.000000,HLS_SYN_LAT=10,HLS_SYN_TPT=none,HLS_SYN_MEM=0,HLS_SYN_DSP=0,HLS_SYN_FF=77,HLS_SYN_LUT=134,HLS_VERSION=2018_3}";
    constant ap_const_logic_1 : STD_LOGIC := '1';
    constant ap_const_lv1_0 : STD_LOGIC_VECTOR (0 downto 0) := "0";
    constant ap_const_lv32_0 : STD_LOGIC_VECTOR (31 downto 0) := "00000000000000000000000000000000";
    constant ap_const_logic_0 : STD_LOGIC := '0';
    constant ap_const_boolean_1 : BOOLEAN := true;

    signal grp_producer0_thread_fu_84_fifo_in_0_read : STD_LOGIC;
    signal grp_producer0_thread_fu_84_fifo_out_0_din : STD_LOGIC_VECTOR (31 downto 0);
    signal grp_producer0_thread_fu_84_fifo_out_0_write : STD_LOGIC;
    signal grp_producer0_thread_fu_84_RegisterWriteEnablePort_0 : STD_LOGIC_VECTOR (0 downto 0);
    signal grp_producer0_thread_fu_84_RegisterWriteEnablePort_0_ap_vld : STD_LOGIC;
    signal grp_producer0_thread_fu_84_RegisterWriteDataPort_0 : STD_LOGIC_VECTOR (31 downto 0);
    signal grp_producer0_thread_fu_84_RegisterWriteDataPort_0_ap_vld : STD_LOGIC;
    signal grp_producer0_thread_fu_84_axi_master_0_req_din : STD_LOGIC;
    signal grp_producer0_thread_fu_84_axi_master_0_req_write : STD_LOGIC;
    signal grp_producer0_thread_fu_84_axi_master_0_rsp_read : STD_LOGIC;
    signal grp_producer0_thread_fu_84_axi_master_0_address : STD_LOGIC_VECTOR (31 downto 0);
    signal grp_producer0_thread_fu_84_axi_master_0_dataout : STD_LOGIC_VECTOR (31 downto 0);
    signal grp_producer0_thread_fu_84_axi_master_0_size : STD_LOGIC_VECTOR (31 downto 0);

    component producer0_thread IS
    port (
        ap_clk : IN STD_LOGIC;
        ap_rst : IN STD_LOGIC;
        fifo_in_0_dout : IN STD_LOGIC_VECTOR (31 downto 0);
        fifo_in_0_empty_n : IN STD_LOGIC;
        fifo_in_0_read : OUT STD_LOGIC;
        fifo_out_0_din : OUT STD_LOGIC_VECTOR (31 downto 0);
        fifo_out_0_full_n : IN STD_LOGIC;
        fifo_out_0_write : OUT STD_LOGIC;
        RegisterWriteEnablePort_0 : OUT STD_LOGIC_VECTOR (0 downto 0);
        RegisterWriteEnablePort_0_ap_vld : OUT STD_LOGIC;
        RegisterWriteDataPort_0 : OUT STD_LOGIC_VECTOR (31 downto 0);
        RegisterWriteDataPort_0_ap_vld : OUT STD_LOGIC;
        axi_master_0_req_din : OUT STD_LOGIC;
        axi_master_0_req_full_n : IN STD_LOGIC;
        axi_master_0_req_write : OUT STD_LOGIC;
        axi_master_0_rsp_empty_n : IN STD_LOGIC;
        axi_master_0_rsp_read : OUT STD_LOGIC;
        axi_master_0_address : OUT STD_LOGIC_VECTOR (31 downto 0);
        axi_master_0_datain : IN STD_LOGIC_VECTOR (31 downto 0);
        axi_master_0_dataout : OUT STD_LOGIC_VECTOR (31 downto 0);
        axi_master_0_size : OUT STD_LOGIC_VECTOR (31 downto 0) );
    end component;



begin
    grp_producer0_thread_fu_84 : component producer0_thread
    port map (
        ap_clk => clock,
        ap_rst => reset,
        fifo_in_0_dout => fifo_in_0_dout,
        fifo_in_0_empty_n => fifo_in_0_empty_n,
        fifo_in_0_read => grp_producer0_thread_fu_84_fifo_in_0_read,
        fifo_out_0_din => grp_producer0_thread_fu_84_fifo_out_0_din,
        fifo_out_0_full_n => fifo_out_0_full_n,
        fifo_out_0_write => grp_producer0_thread_fu_84_fifo_out_0_write,
        RegisterWriteEnablePort_0 => grp_producer0_thread_fu_84_RegisterWriteEnablePort_0,
        RegisterWriteEnablePort_0_ap_vld => grp_producer0_thread_fu_84_RegisterWriteEnablePort_0_ap_vld,
        RegisterWriteDataPort_0 => grp_producer0_thread_fu_84_RegisterWriteDataPort_0,
        RegisterWriteDataPort_0_ap_vld => grp_producer0_thread_fu_84_RegisterWriteDataPort_0_ap_vld,
        axi_master_0_req_din => grp_producer0_thread_fu_84_axi_master_0_req_din,
        axi_master_0_req_full_n => axi_master_0_req_full_n,
        axi_master_0_req_write => grp_producer0_thread_fu_84_axi_master_0_req_write,
        axi_master_0_rsp_empty_n => axi_master_0_rsp_empty_n,
        axi_master_0_rsp_read => grp_producer0_thread_fu_84_axi_master_0_rsp_read,
        axi_master_0_address => grp_producer0_thread_fu_84_axi_master_0_address,
        axi_master_0_datain => axi_master_0_datain,
        axi_master_0_dataout => grp_producer0_thread_fu_84_axi_master_0_dataout,
        axi_master_0_size => grp_producer0_thread_fu_84_axi_master_0_size);





    RegisterWriteDataPort_0_assign_proc : process(clock)
    begin
        if (clock'event and clock =  '1') then
            if (reset = '1') then
                RegisterWriteDataPort_0 <= ap_const_lv32_0;
            else
                if ((grp_producer0_thread_fu_84_RegisterWriteDataPort_0_ap_vld = ap_const_logic_1)) then 
                    RegisterWriteDataPort_0 <= grp_producer0_thread_fu_84_RegisterWriteDataPort_0;
                end if; 
            end if;
        end if;
    end process;


    RegisterWriteEnablePort_0_assign_proc : process(clock)
    begin
        if (clock'event and clock =  '1') then
            if (reset = '1') then
                RegisterWriteEnablePort_0 <= ap_const_lv1_0;
            else
                if ((grp_producer0_thread_fu_84_RegisterWriteEnablePort_0_ap_vld = ap_const_logic_1)) then 
                    RegisterWriteEnablePort_0 <= grp_producer0_thread_fu_84_RegisterWriteEnablePort_0;
                end if; 
            end if;
        end if;
    end process;

    axi_master_0_address <= grp_producer0_thread_fu_84_axi_master_0_address;
    axi_master_0_dataout <= grp_producer0_thread_fu_84_axi_master_0_dataout;
    axi_master_0_req_din <= grp_producer0_thread_fu_84_axi_master_0_req_din;
    axi_master_0_req_write <= grp_producer0_thread_fu_84_axi_master_0_req_write;
    axi_master_0_rsp_read <= grp_producer0_thread_fu_84_axi_master_0_rsp_read;
    axi_master_0_size <= grp_producer0_thread_fu_84_axi_master_0_size;
    fifo_in_0_read <= grp_producer0_thread_fu_84_fifo_in_0_read;
    fifo_out_0_din <= grp_producer0_thread_fu_84_fifo_out_0_din;
    fifo_out_0_write <= grp_producer0_thread_fu_84_fifo_out_0_write;
end behav;
