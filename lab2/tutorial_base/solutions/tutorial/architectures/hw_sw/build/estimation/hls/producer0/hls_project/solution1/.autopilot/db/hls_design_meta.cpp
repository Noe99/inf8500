#include "hls_design_meta.h"
const Port_Property HLS_Design_Meta::port_props[]={
	Port_Property("clock", 1, hls_in, -1, "", "", 1),
	Port_Property("reset", 1, hls_in, -1, "", "", 1),
	Port_Property("fifo_in_0_dout", 32, hls_in, 2, "ap_fifo", "fifo_data", 1),
	Port_Property("fifo_in_0_empty_n", 1, hls_in, 2, "ap_fifo", "fifo_status", 1),
	Port_Property("fifo_in_0_read", 1, hls_out, 2, "ap_fifo", "fifo_update", 1),
	Port_Property("fifo_out_0_din", 32, hls_out, 3, "ap_fifo", "fifo_data", 1),
	Port_Property("fifo_out_0_full_n", 1, hls_in, 3, "ap_fifo", "fifo_status", 1),
	Port_Property("fifo_out_0_write", 1, hls_out, 3, "ap_fifo", "fifo_update", 1),
	Port_Property("RegisterWriteEnablePort_0", 1, hls_out, 4, "ap_vld", "out_data", 1),
	Port_Property("RegisterWriteDataPort_0", 32, hls_out, 5, "ap_vld", "out_data", 1),
	Port_Property("axi_master_0_req_din", 1, hls_out, 6, "ap_bus", "fifo_data", 1),
	Port_Property("axi_master_0_req_full_n", 1, hls_in, 6, "ap_bus", "fifo_status", 1),
	Port_Property("axi_master_0_req_write", 1, hls_out, 6, "ap_bus", "fifo_update", 1),
	Port_Property("axi_master_0_rsp_empty_n", 1, hls_in, 6, "ap_bus", "fifo_status", 1),
	Port_Property("axi_master_0_rsp_read", 1, hls_out, 6, "ap_bus", "fifo_update", 1),
	Port_Property("axi_master_0_address", 32, hls_out, 6, "ap_bus", "unknown", 1),
	Port_Property("axi_master_0_datain", 32, hls_in, 6, "ap_bus", "unknown", 1),
	Port_Property("axi_master_0_dataout", 32, hls_out, 6, "ap_bus", "unknown", 1),
	Port_Property("axi_master_0_size", 32, hls_out, 6, "ap_bus", "unknown", 1),
};
const char* HLS_Design_Meta::dut_name = "producer0::producer0";
