# ==============================================================
# File generated on Wed Oct 05 12:02:23 EDT 2022
# Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2018.3 (64-bit)
# SW Build 2405991 on Thu Dec  6 23:36:41 MST 2018
# IP Build 2404404 on Fri Dec  7 01:43:56 MST 2018
# Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
# ==============================================================
add_files producer0.cpp
set_part xc7z020clg484-1
create_clock -name default -period 10.0
set_clock_uncertainty 3.0 default
config_rtl -reset_level=high
