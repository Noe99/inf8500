// ==============================================================
// File generated on Wed Oct 05 12:02:23 EDT 2022
// Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2018.3 (64-bit)
// SW Build 2405991 on Thu Dec  6 23:36:41 MST 2018
// IP Build 2404404 on Fri Dec  7 01:43:56 MST 2018
// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// ==============================================================
`timescale 1 ns / 1 ps
module producer0_top (
m_axi_axi_master_0_AWID,
m_axi_axi_master_0_AWADDR,
m_axi_axi_master_0_AWLEN,
m_axi_axi_master_0_AWSIZE,
m_axi_axi_master_0_AWBURST,
m_axi_axi_master_0_AWLOCK,
m_axi_axi_master_0_AWCACHE,
m_axi_axi_master_0_AWPROT,
m_axi_axi_master_0_AWQOS,
m_axi_axi_master_0_AWUSER,
m_axi_axi_master_0_AWVALID,
m_axi_axi_master_0_AWREADY,
m_axi_axi_master_0_WDATA,
m_axi_axi_master_0_WSTRB,
m_axi_axi_master_0_WLAST,
m_axi_axi_master_0_WUSER,
m_axi_axi_master_0_WVALID,
m_axi_axi_master_0_WREADY,
m_axi_axi_master_0_BID,
m_axi_axi_master_0_BRESP,
m_axi_axi_master_0_BUSER,
m_axi_axi_master_0_BVALID,
m_axi_axi_master_0_BREADY,
m_axi_axi_master_0_ARID,
m_axi_axi_master_0_ARADDR,
m_axi_axi_master_0_ARLEN,
m_axi_axi_master_0_ARSIZE,
m_axi_axi_master_0_ARBURST,
m_axi_axi_master_0_ARLOCK,
m_axi_axi_master_0_ARCACHE,
m_axi_axi_master_0_ARPROT,
m_axi_axi_master_0_ARQOS,
m_axi_axi_master_0_ARUSER,
m_axi_axi_master_0_ARVALID,
m_axi_axi_master_0_ARREADY,
m_axi_axi_master_0_RID,
m_axi_axi_master_0_RDATA,
m_axi_axi_master_0_RRESP,
m_axi_axi_master_0_RLAST,
m_axi_axi_master_0_RUSER,
m_axi_axi_master_0_RVALID,
m_axi_axi_master_0_RREADY,
aresetn,
aclk,
fifo_in_0_dout,
fifo_in_0_empty_n,
fifo_in_0_read,
fifo_out_0_din,
fifo_out_0_full_n,
fifo_out_0_write,
RegisterWriteEnablePort_0,
RegisterWriteDataPort_0
);

parameter C_M_AXI_AXI_MASTER_0_ID_WIDTH = 1;
parameter C_M_AXI_AXI_MASTER_0_ADDR_WIDTH = 32;
parameter C_M_AXI_AXI_MASTER_0_DATA_WIDTH = 32;
parameter C_M_AXI_AXI_MASTER_0_AWUSER_WIDTH = 1;
parameter C_M_AXI_AXI_MASTER_0_ARUSER_WIDTH = 1;
parameter C_M_AXI_AXI_MASTER_0_WUSER_WIDTH = 1;
parameter C_M_AXI_AXI_MASTER_0_RUSER_WIDTH = 1;
parameter C_M_AXI_AXI_MASTER_0_BUSER_WIDTH = 1;
parameter C_M_AXI_AXI_MASTER_0_USER_DATA_WIDTH = 32;
parameter C_M_AXI_AXI_MASTER_0_TARGET_ADDR = 32'h00000000;
parameter C_M_AXI_AXI_MASTER_0_USER_VALUE = 1'b0;
parameter C_M_AXI_AXI_MASTER_0_PROT_VALUE = 3'b010;
parameter C_M_AXI_AXI_MASTER_0_CACHE_VALUE = 4'b0000;
parameter RESET_ACTIVE_LOW = 1;

output [C_M_AXI_AXI_MASTER_0_ID_WIDTH - 1:0] m_axi_axi_master_0_AWID ;
output [C_M_AXI_AXI_MASTER_0_ADDR_WIDTH - 1:0] m_axi_axi_master_0_AWADDR ;
output [8 - 1:0] m_axi_axi_master_0_AWLEN ;
output [3 - 1:0] m_axi_axi_master_0_AWSIZE ;
output [2 - 1:0] m_axi_axi_master_0_AWBURST ;
output [2 - 1:0] m_axi_axi_master_0_AWLOCK ;
output [4 - 1:0] m_axi_axi_master_0_AWCACHE ;
output [3 - 1:0] m_axi_axi_master_0_AWPROT ;
output [4 - 1:0] m_axi_axi_master_0_AWQOS ;
output [C_M_AXI_AXI_MASTER_0_AWUSER_WIDTH - 1:0] m_axi_axi_master_0_AWUSER ;
output m_axi_axi_master_0_AWVALID ;
input m_axi_axi_master_0_AWREADY ;
output [C_M_AXI_AXI_MASTER_0_DATA_WIDTH - 1:0] m_axi_axi_master_0_WDATA ;
output [C_M_AXI_AXI_MASTER_0_DATA_WIDTH/8 - 1:0] m_axi_axi_master_0_WSTRB ;
output m_axi_axi_master_0_WLAST ;
output [C_M_AXI_AXI_MASTER_0_WUSER_WIDTH - 1:0] m_axi_axi_master_0_WUSER ;
output m_axi_axi_master_0_WVALID ;
input m_axi_axi_master_0_WREADY ;
input [C_M_AXI_AXI_MASTER_0_ID_WIDTH - 1:0] m_axi_axi_master_0_BID ;
input [2 - 1:0] m_axi_axi_master_0_BRESP ;
input [C_M_AXI_AXI_MASTER_0_BUSER_WIDTH - 1:0] m_axi_axi_master_0_BUSER ;
input m_axi_axi_master_0_BVALID ;
output m_axi_axi_master_0_BREADY ;
output [C_M_AXI_AXI_MASTER_0_ID_WIDTH - 1:0] m_axi_axi_master_0_ARID ;
output [C_M_AXI_AXI_MASTER_0_ADDR_WIDTH - 1:0] m_axi_axi_master_0_ARADDR ;
output [8 - 1:0] m_axi_axi_master_0_ARLEN ;
output [3 - 1:0] m_axi_axi_master_0_ARSIZE ;
output [2 - 1:0] m_axi_axi_master_0_ARBURST ;
output [2 - 1:0] m_axi_axi_master_0_ARLOCK ;
output [4 - 1:0] m_axi_axi_master_0_ARCACHE ;
output [3 - 1:0] m_axi_axi_master_0_ARPROT ;
output [4 - 1:0] m_axi_axi_master_0_ARQOS ;
output [C_M_AXI_AXI_MASTER_0_ARUSER_WIDTH - 1:0] m_axi_axi_master_0_ARUSER ;
output m_axi_axi_master_0_ARVALID ;
input m_axi_axi_master_0_ARREADY ;
input [C_M_AXI_AXI_MASTER_0_ID_WIDTH - 1:0] m_axi_axi_master_0_RID ;
input [C_M_AXI_AXI_MASTER_0_DATA_WIDTH - 1:0] m_axi_axi_master_0_RDATA ;
input [2 - 1:0] m_axi_axi_master_0_RRESP ;
input m_axi_axi_master_0_RLAST ;
input [C_M_AXI_AXI_MASTER_0_RUSER_WIDTH - 1:0] m_axi_axi_master_0_RUSER ;
input m_axi_axi_master_0_RVALID ;
output m_axi_axi_master_0_RREADY ;

input aresetn ;

input aclk ;

input [32 - 1:0] fifo_in_0_dout ;
input fifo_in_0_empty_n ;
output fifo_in_0_read ;
output [32 - 1:0] fifo_out_0_din ;
input fifo_out_0_full_n ;
output fifo_out_0_write ;
output [1 - 1:0] RegisterWriteEnablePort_0 ;
output [32 - 1:0] RegisterWriteDataPort_0 ;


wire [C_M_AXI_AXI_MASTER_0_ID_WIDTH - 1:0] m_axi_axi_master_0_AWID;
wire [C_M_AXI_AXI_MASTER_0_ADDR_WIDTH - 1:0] m_axi_axi_master_0_AWADDR;
wire [8 - 1:0] m_axi_axi_master_0_AWLEN;
wire [3 - 1:0] m_axi_axi_master_0_AWSIZE;
wire [2 - 1:0] m_axi_axi_master_0_AWBURST;
wire [2 - 1:0] m_axi_axi_master_0_AWLOCK;
wire [4 - 1:0] m_axi_axi_master_0_AWCACHE;
wire [3 - 1:0] m_axi_axi_master_0_AWPROT;
wire [4 - 1:0] m_axi_axi_master_0_AWQOS;
wire [C_M_AXI_AXI_MASTER_0_AWUSER_WIDTH - 1:0] m_axi_axi_master_0_AWUSER;
wire m_axi_axi_master_0_AWVALID;
wire m_axi_axi_master_0_AWREADY;
wire [C_M_AXI_AXI_MASTER_0_DATA_WIDTH - 1:0] m_axi_axi_master_0_WDATA;
wire [C_M_AXI_AXI_MASTER_0_DATA_WIDTH/8 - 1:0] m_axi_axi_master_0_WSTRB;
wire m_axi_axi_master_0_WLAST;
wire [C_M_AXI_AXI_MASTER_0_WUSER_WIDTH - 1:0] m_axi_axi_master_0_WUSER;
wire m_axi_axi_master_0_WVALID;
wire m_axi_axi_master_0_WREADY;
wire [C_M_AXI_AXI_MASTER_0_ID_WIDTH - 1:0] m_axi_axi_master_0_BID;
wire [2 - 1:0] m_axi_axi_master_0_BRESP;
wire [C_M_AXI_AXI_MASTER_0_BUSER_WIDTH - 1:0] m_axi_axi_master_0_BUSER;
wire m_axi_axi_master_0_BVALID;
wire m_axi_axi_master_0_BREADY;
wire [C_M_AXI_AXI_MASTER_0_ID_WIDTH - 1:0] m_axi_axi_master_0_ARID;
wire [C_M_AXI_AXI_MASTER_0_ADDR_WIDTH - 1:0] m_axi_axi_master_0_ARADDR;
wire [8 - 1:0] m_axi_axi_master_0_ARLEN;
wire [3 - 1:0] m_axi_axi_master_0_ARSIZE;
wire [2 - 1:0] m_axi_axi_master_0_ARBURST;
wire [2 - 1:0] m_axi_axi_master_0_ARLOCK;
wire [4 - 1:0] m_axi_axi_master_0_ARCACHE;
wire [3 - 1:0] m_axi_axi_master_0_ARPROT;
wire [4 - 1:0] m_axi_axi_master_0_ARQOS;
wire [C_M_AXI_AXI_MASTER_0_ARUSER_WIDTH - 1:0] m_axi_axi_master_0_ARUSER;
wire m_axi_axi_master_0_ARVALID;
wire m_axi_axi_master_0_ARREADY;
wire [C_M_AXI_AXI_MASTER_0_ID_WIDTH - 1:0] m_axi_axi_master_0_RID;
wire [C_M_AXI_AXI_MASTER_0_DATA_WIDTH - 1:0] m_axi_axi_master_0_RDATA;
wire [2 - 1:0] m_axi_axi_master_0_RRESP;
wire m_axi_axi_master_0_RLAST;
wire [C_M_AXI_AXI_MASTER_0_RUSER_WIDTH - 1:0] m_axi_axi_master_0_RUSER;
wire m_axi_axi_master_0_RVALID;
wire m_axi_axi_master_0_RREADY;

wire aresetn;


wire [32 - 1:0] sig_producer0_axi_master_0_datain;
wire [32 - 1:0] sig_producer0_axi_master_0_dataout;
wire [32 - 1:0] sig_producer0_axi_master_0_address;
wire [32 - 1:0] sig_producer0_axi_master_0_size;
wire sig_producer0_axi_master_0_req_din;
wire sig_producer0_axi_master_0_req_full_n;
wire sig_producer0_axi_master_0_req_write;
wire sig_producer0_axi_master_0_rsp_empty_n;
wire sig_producer0_axi_master_0_rsp_read;

wire sig_producer0_reset;



producer0 producer0_U(
    .axi_master_0_datain(sig_producer0_axi_master_0_datain),
    .axi_master_0_dataout(sig_producer0_axi_master_0_dataout),
    .axi_master_0_address(sig_producer0_axi_master_0_address),
    .axi_master_0_size(sig_producer0_axi_master_0_size),
    .axi_master_0_req_din(sig_producer0_axi_master_0_req_din),
    .axi_master_0_req_full_n(sig_producer0_axi_master_0_req_full_n),
    .axi_master_0_req_write(sig_producer0_axi_master_0_req_write),
    .axi_master_0_rsp_empty_n(sig_producer0_axi_master_0_rsp_empty_n),
    .axi_master_0_rsp_read(sig_producer0_axi_master_0_rsp_read),
    .reset(sig_producer0_reset),
    .clock(aclk),
    .fifo_in_0_dout(fifo_in_0_dout),
    .fifo_in_0_empty_n(fifo_in_0_empty_n),
    .fifo_in_0_read(fifo_in_0_read),
    .fifo_out_0_din(fifo_out_0_din),
    .fifo_out_0_full_n(fifo_out_0_full_n),
    .fifo_out_0_write(fifo_out_0_write),
    .RegisterWriteEnablePort_0(RegisterWriteEnablePort_0),
    .RegisterWriteDataPort_0(RegisterWriteDataPort_0)
);

producer0_axi_master_0_if #(
    .C_ID_WIDTH(C_M_AXI_AXI_MASTER_0_ID_WIDTH),
    .C_ADDR_WIDTH(C_M_AXI_AXI_MASTER_0_ADDR_WIDTH),
    .C_DATA_WIDTH(C_M_AXI_AXI_MASTER_0_DATA_WIDTH),
    .C_AWUSER_WIDTH(C_M_AXI_AXI_MASTER_0_AWUSER_WIDTH),
    .C_ARUSER_WIDTH(C_M_AXI_AXI_MASTER_0_ARUSER_WIDTH),
    .C_WUSER_WIDTH(C_M_AXI_AXI_MASTER_0_WUSER_WIDTH),
    .C_RUSER_WIDTH(C_M_AXI_AXI_MASTER_0_RUSER_WIDTH),
    .C_BUSER_WIDTH(C_M_AXI_AXI_MASTER_0_BUSER_WIDTH),
    .C_USER_DATA_WIDTH(C_M_AXI_AXI_MASTER_0_USER_DATA_WIDTH),
    .C_TARGET_ADDR(C_M_AXI_AXI_MASTER_0_TARGET_ADDR),
    .C_USER_VALUE(C_M_AXI_AXI_MASTER_0_USER_VALUE),
    .C_PROT_VALUE(C_M_AXI_AXI_MASTER_0_PROT_VALUE),
    .C_CACHE_VALUE(C_M_AXI_AXI_MASTER_0_CACHE_VALUE))
producer0_axi_master_0_if_U(
    .ACLK(aclk),
    .ARESETN(aresetn),
    .USER_datain(sig_producer0_axi_master_0_datain),
    .USER_dataout(sig_producer0_axi_master_0_dataout),
    .USER_address(sig_producer0_axi_master_0_address),
    .USER_size(sig_producer0_axi_master_0_size),
    .USER_req_din(sig_producer0_axi_master_0_req_din),
    .USER_req_full_n(sig_producer0_axi_master_0_req_full_n),
    .USER_req_write(sig_producer0_axi_master_0_req_write),
    .USER_rsp_empty_n(sig_producer0_axi_master_0_rsp_empty_n),
    .USER_rsp_read(sig_producer0_axi_master_0_rsp_read),
    .AWID(m_axi_axi_master_0_AWID),
    .AWADDR(m_axi_axi_master_0_AWADDR),
    .AWLEN(m_axi_axi_master_0_AWLEN),
    .AWSIZE(m_axi_axi_master_0_AWSIZE),
    .AWBURST(m_axi_axi_master_0_AWBURST),
    .AWLOCK(m_axi_axi_master_0_AWLOCK),
    .AWCACHE(m_axi_axi_master_0_AWCACHE),
    .AWPROT(m_axi_axi_master_0_AWPROT),
    .AWQOS(m_axi_axi_master_0_AWQOS),
    .AWUSER(m_axi_axi_master_0_AWUSER),
    .AWVALID(m_axi_axi_master_0_AWVALID),
    .AWREADY(m_axi_axi_master_0_AWREADY),
    .WDATA(m_axi_axi_master_0_WDATA),
    .WSTRB(m_axi_axi_master_0_WSTRB),
    .WLAST(m_axi_axi_master_0_WLAST),
    .WUSER(m_axi_axi_master_0_WUSER),
    .WVALID(m_axi_axi_master_0_WVALID),
    .WREADY(m_axi_axi_master_0_WREADY),
    .BID(m_axi_axi_master_0_BID),
    .BRESP(m_axi_axi_master_0_BRESP),
    .BUSER(m_axi_axi_master_0_BUSER),
    .BVALID(m_axi_axi_master_0_BVALID),
    .BREADY(m_axi_axi_master_0_BREADY),
    .ARID(m_axi_axi_master_0_ARID),
    .ARADDR(m_axi_axi_master_0_ARADDR),
    .ARLEN(m_axi_axi_master_0_ARLEN),
    .ARSIZE(m_axi_axi_master_0_ARSIZE),
    .ARBURST(m_axi_axi_master_0_ARBURST),
    .ARLOCK(m_axi_axi_master_0_ARLOCK),
    .ARCACHE(m_axi_axi_master_0_ARCACHE),
    .ARPROT(m_axi_axi_master_0_ARPROT),
    .ARQOS(m_axi_axi_master_0_ARQOS),
    .ARUSER(m_axi_axi_master_0_ARUSER),
    .ARVALID(m_axi_axi_master_0_ARVALID),
    .ARREADY(m_axi_axi_master_0_ARREADY),
    .RID(m_axi_axi_master_0_RID),
    .RDATA(m_axi_axi_master_0_RDATA),
    .RRESP(m_axi_axi_master_0_RRESP),
    .RLAST(m_axi_axi_master_0_RLAST),
    .RUSER(m_axi_axi_master_0_RUSER),
    .RVALID(m_axi_axi_master_0_RVALID),
    .RREADY(m_axi_axi_master_0_RREADY));

producer0_reset_if #(
    .RESET_ACTIVE_LOW(RESET_ACTIVE_LOW))
reset_if_U(
    .dout(sig_producer0_reset),
    .din(aresetn));

endmodule
