////////////////////////////////////////////////////////////////////////////////
//
// Space Codesign System Inc. - (https://www.spacecodesign.com)
// Copyright 2005-2020. All rights reserved
//
// No authorization to modify or use this file for
// commercial purposes without prior written consent
// of its author(s)
//
////////////////////////////////////////////////////////////////////////////////

#ifndef SPACE_TYPE_TRAITS
#define SPACE_TYPE_TRAITS

//////////
// If you modify this header, please also change the other space_type_traits
// headers to keep things consistent.
//////////

///////////////////////////////////////////////
// Partial C++11 type_traits implementation since Vivado HLS doesn't have C++11 available.
// The compiler should be able to infer const values as if they were constexpr
// wherever possible.
// Prepend identifiers with "space_" to avoid name conflicts.
// TAKEN FROM IMPLEMENTATIONS IN:
// - https://en.cppreference.com/w/cpp/header/type_traits
// - https://github.com/llvm/llvm-project/blob/main/libcxx/include/type_traits

// __has_feature is a Clang extension ; if it doesn't exist, always reply false.
#ifndef __has_feature
#define __has_feature(f) 0
#endif

template<class T, T v>
struct space_integral_constant {
	static const T value = v;
};
typedef space_integral_constant<bool, true > space_true_type;
typedef space_integral_constant<bool, false> space_false_type;

template<class T> struct space_remove_cv                   {typedef T type;};
template<class T> struct space_remove_cv<const T>          {typedef T type;};
template<class T> struct space_remove_cv<volatile T>       {typedef T type;};
template<class T> struct space_remove_cv<const volatile T> {typedef T type;};

template<class T> struct space_is_const                : space_false_type { };
template<class T> struct space_is_const   <const T>    : space_true_type  { };

template<class T> struct space_is_volatile             : space_false_type { };
template<class T> struct space_is_volatile<volatile T> : space_true_type  { };

template<class T> struct space_remove_pointer                    {typedef T type;};
template<class T> struct space_remove_pointer<T*>                {typedef T type;};
template<class T> struct space_remove_pointer<T* const>          {typedef T type;};
template<class T> struct space_remove_pointer<T* volatile>       {typedef T type;};
template<class T> struct space_remove_pointer<T* const volatile> {typedef T type;};

template<class T> struct space_is_pointer                    : space_false_type { };
template<class T> struct space_is_pointer<T*               > : space_true_type  { };
template<class T> struct space_is_pointer<T* const         > : space_true_type  { };
template<class T> struct space_is_pointer<T* volatile      > : space_true_type  { };
template<class T> struct space_is_pointer<T* const volatile> : space_true_type  { };

template<class T> struct space_is_array                 : space_false_type { };
template<class T> struct space_is_array<T[]>            : space_true_type  { };
template<class T, size_t N> struct space_is_array<T[N]> : space_true_type  { };

template<class T> struct space_is_reference      : space_false_type { };
template<class T> struct space_is_reference<T&>  : space_true_type  { };

// space_is_union implementation
#if __has_feature(is_union) || defined(_LIBCPP_COMPILER_GCC)
// Implementation using __is_union
template<class T> struct space_is_union
    : public space_integral_constant<bool, __is_union(T)> { };
#else
// Manual implementation (that always returns false).
template<class T> struct space_is_union                   : public space_false_type  { };
template<class T> struct space_is_union<const T         > : public space_is_union<T> { };
template<class T> struct space_is_union<volatile T      > : public space_is_union<T> { };
template<class T> struct space_is_union<const volatile T> : public space_is_union<T> { };
#endif

// space_is_class implementation
#if __has_feature(is_class) || defined(_LIBCPP_COMPILER_GCC)
// Implementation using __is_class. This is better than the other one
// below for reasons described here: https://stackoverflow.com/a/57591546/2252948
template<class T> struct space_is_class : public space_integral_constant<bool, __is_class(T)> { };
#else
// Manual (imperfect, but pretty good) implementation
struct space_two { char space_lx[2]; };
namespace space_is_class_imp
{
template<class T> char space_test(int T::*);
template<class T> space_two space_test(...);
}
template<class T> struct space_is_class : public space_integral_constant<bool, sizeof(space_is_class_imp::space_test<T>(0)) == 1 && !space_is_union<T>::value> { };
#endif

template<class T> struct space_is_member_pointer_helper                  : space_false_type { };
template<class T, class U> struct space_is_member_pointer_helper<T U::*> : space_true_type  { };
template<class T> struct space_is_member_pointer : space_is_member_pointer_helper<typename space_remove_cv<T>::type> { };

template<class T, class U> struct space_is_same : space_false_type { };
template<class T> struct space_is_same<T, T>    : space_true_type  { };

template<class T> struct space_is_void : space_is_same<void, typename space_remove_cv<T>::type> { };

template<bool B, class T = void> struct space_enable_if { };
template<class T> struct space_enable_if<true, T> { typedef T type; };

template<class T> struct space_remove_all_extents {
	typedef T type;
};
template<class T> struct space_remove_all_extents<T[]> {
	typedef typename space_remove_all_extents<T>::type type;
};
template<class T, size_t N> struct space_remove_all_extents<T[N]> {
	typedef typename space_remove_all_extents<T>::type type;
};

// List of fundamental types: https://en.cppreference.com/w/cpp/types/numeric_limits/is_integer
template<class T> struct space_is_integral                     : public space_false_type     { };
template<class T> struct space_is_integral<const T           > : public space_is_integral<T> { };
template<class T> struct space_is_integral<volatile T        > : public space_is_integral<T> { };
template<class T> struct space_is_integral<const volatile T  > : public space_is_integral<T> { };
template<       > struct space_is_integral<bool              > : public space_true_type      { };
template<       > struct space_is_integral<char              > : public space_true_type      { };
template<       > struct space_is_integral<signed char       > : public space_true_type      { };
template<       > struct space_is_integral<unsigned char     > : public space_true_type      { };
template<       > struct space_is_integral<wchar_t           > : public space_true_type      { };
template<       > struct space_is_integral<short             > : public space_true_type      { };
template<       > struct space_is_integral<unsigned short    > : public space_true_type      { };
template<       > struct space_is_integral<int               > : public space_true_type      { };
template<       > struct space_is_integral<unsigned int      > : public space_true_type      { };
template<       > struct space_is_integral<long              > : public space_true_type      { };
template<       > struct space_is_integral<unsigned long     > : public space_true_type      { };
template<       > struct space_is_integral<long long         > : public space_true_type      { };
template<       > struct space_is_integral<unsigned long long> : public space_true_type      { };
template<       > struct space_is_integral<float             > : public space_false_type     { };
template<       > struct space_is_integral<double            > : public space_false_type     { };
template<       > struct space_is_integral<long double       > : public space_false_type     { };

template <class T> struct space_is_function
    : space_integral_constant<
        bool,
#ifdef __clang__
        __is_function(T)
#else
        !(space_is_reference<T>::value || space_is_const<const T>::value)
#endif
    > { };

template<class T> struct space_is_floating_point
	: space_integral_constant<
		bool,
		space_is_same<float      , typename space_remove_cv<T>::type>::value  ||
		space_is_same<double     , typename space_remove_cv<T>::type>::value  ||
		space_is_same<long double, typename space_remove_cv<T>::type>::value
	> { };

template<class T> struct space_is_arithmetic
	: space_integral_constant<
		bool,
		space_is_integral<T>::value ||
		space_is_floating_point<T>::value
	> { };

#if __has_feature(is_enum) || defined(_LIBCPP_COMPILER_GCC)
template<class T> struct space_is_enum : public space_integral_constant<bool, __is_enum(T)> { };
#else
template<class T> struct space_is_enum
    : space_integral_constant<bool, !space_is_void<T>::value             &&
                                    !space_is_integral<T>::value         &&
                                    !space_is_floating_point<T>::value   &&
                                    !space_is_array<T>::value            &&
                                    !space_is_pointer<T>::value          &&
                                    !space_is_reference<T>::value        &&
                                    !space_is_member_pointer<T>::value   &&
                                    !space_is_union<T>::value            &&
                                    !space_is_class<T>::value            &&
                                    !space_is_function<T>::value         > { };
#endif

///////////////////////////////////////////////

///////////////////////////////////////////////
// Defined by us ; not part of standard type_traits header.
template<class T> struct space_is_pointer_or_array                    : space_false_type { };
template<class T> struct space_is_pointer_or_array<T*               > : space_true_type  { };
template<class T> struct space_is_pointer_or_array<T* const         > : space_true_type  { };
template<class T> struct space_is_pointer_or_array<T* volatile      > : space_true_type  { };
template<class T> struct space_is_pointer_or_array<T* const volatile> : space_true_type  { };
template<class T> struct space_is_pointer_or_array<T[]              > : space_true_type  { };
template<class T, size_t N> struct space_is_pointer_or_array<T[N]   > : space_true_type  { };

/**
 * @warning If the type passed is a pointer-to-array, make sure the result
 * is what is expected.
 */
template<class T> struct space_remove_all_extents_and_pointers {
	typedef T type;
};
template<class T> struct space_remove_all_extents_and_pointers<T[]> {
	typedef typename space_remove_all_extents_and_pointers<T>::type type;
};
template<class T, size_t N> struct space_remove_all_extents_and_pointers<T[N]> {
	typedef typename space_remove_all_extents_and_pointers<T>::type type;
};
template<class T> struct space_remove_all_extents_and_pointers<T*> {
	typedef typename space_remove_all_extents_and_pointers<T>::type type;
};
template<class T> struct space_remove_all_extents_and_pointers<T* const> {
	typedef typename space_remove_all_extents_and_pointers<T>::type type;
};
template<class T> struct space_remove_all_extents_and_pointers<T* volatile> {
	typedef typename space_remove_all_extents_and_pointers<T>::type type;
};
template<class T> struct space_remove_all_extents_and_pointers<T* const volatile> {
	typedef typename space_remove_all_extents_and_pointers<T>::type type;
};

/**
 * space_integral_of_size<N>::type is an integral type of size N, if such an
 * integral type is found.
 */
template<size_t N> struct space_integral_of_size                   { };
template<>         struct space_integral_of_size<sizeof(uint8_t )> { typedef uint8_t  type; };
template<>         struct space_integral_of_size<sizeof(uint16_t)> { typedef uint16_t type; };
template<>         struct space_integral_of_size<sizeof(uint32_t)> { typedef uint32_t type; };
template<>         struct space_integral_of_size<sizeof(uint64_t)> { typedef uint64_t type; };

/**
 * space_have_integral_of_size<N>::value is true if we found an integral of that
 * size.
 */
template<size_t N> struct space_have_integral_of_size           : public space_false_type { };
template<> struct space_have_integral_of_size<sizeof(uint8_t )> : public space_true_type  { };
template<> struct space_have_integral_of_size<sizeof(uint16_t)> : public space_true_type  { };
template<> struct space_have_integral_of_size<sizeof(uint32_t)> : public space_true_type  { };
template<> struct space_have_integral_of_size<sizeof(uint64_t)> : public space_true_type  { };

/**
 * space_is_floating_point_supported<T>::value is true if T is a floating-point type for
 * which we can find an integral type of the same size.
 */
template<class T> struct space_is_floating_point_supported : public space_have_integral_of_size<sizeof(T)> { };

/**
 * If T is a floating-point type, then space_integral_from_floating_point<T>::type is an
 * integral type of same size as T if one is found, with the same cv-qualifiers.
 */
template<class T, class Enabler = void> struct space_integral_from_floating_point { };
template<class T> struct space_integral_from_floating_point<T, typename space_enable_if<space_is_floating_point<T>::value && space_is_floating_point_supported<T>::value && !space_is_const<T>::value && !space_is_volatile<T>::value>::type> { typedef                typename space_integral_of_size<sizeof(T)>::type type; };
template<class T> struct space_integral_from_floating_point<T, typename space_enable_if<space_is_floating_point<T>::value && space_is_floating_point_supported<T>::value &&  space_is_const<T>::value && !space_is_volatile<T>::value>::type> { typedef const          typename space_integral_of_size<sizeof(T)>::type type; };
template<class T> struct space_integral_from_floating_point<T, typename space_enable_if<space_is_floating_point<T>::value && space_is_floating_point_supported<T>::value && !space_is_const<T>::value &&  space_is_volatile<T>::value>::type> { typedef volatile       typename space_integral_of_size<sizeof(T)>::type type; };
template<class T> struct space_integral_from_floating_point<T, typename space_enable_if<space_is_floating_point<T>::value && space_is_floating_point_supported<T>::value &&  space_is_const<T>::value &&  space_is_volatile<T>::value>::type> { typedef const volatile typename space_integral_of_size<sizeof(T)>::type type; };

/**
 * Union used for transferring floating-point types to a device (e.g. the AXI).
 * For some reason, Vivado HLS does not support reinterpret_cast of a float to
 * an integer, even if that integer has the same size.
 */
template<class T> union space_floating_point_and_integral_union {
    T space_f;
    typename space_integral_from_floating_point<T>::type space_i;
};
///////////////////////////////////////////////

#endif // !SPACE_TYPE_TRAITS
