////////////////////////////////////////////////////////////////////////////////
//
// Space Codesign System Inc. - (https://www.spacecodesign.com)
// Copyright 2005-2021. All rights reserved
//
// No authorization to modify or use this file for
// commercial purposes without prior written consent
// of its author(s)
//
////////////////////////////////////////////////////////////////////////////////

#ifndef _SPACE_VIVADOHLS_INCLUDES_H_
#define _SPACE_VIVADOHLS_INCLUDES_H_

#include <ap_shift_reg.h>

#include "space_type_traits.h"

/**
 * Causes a compilation error if the condition can be determined false at
 * compile-time with a message displaying where this macro is used.
 *
 * Wherever this macro is used, please do not split that macro's message into
 * multiple lines: compilers often show just the first line of the piece of
 * code which causes the error. If the macro usage's message is split into
 * multiple lines, then that message would not be fully shown on the error
 * message.
 */
#define SPACE_STATIC_ASSERT(MSG, COND)										\
	do { int space_static_asserter[(MSG && (COND)) ? 1 : -1]; } while (false)

/**
 * A FSM which (with current implementation) can only advance to the next
 * state, circularly.
 */
template<int SPACE_N_ST> class space_one_hot_fsm : public ap_uint<SPACE_N_ST> {
public:
	explicit space_one_hot_fsm() : ap_uint<SPACE_N_ST>(1) {}
	/**
	 * Determines whether the FSM is at given state.
	 */
	inline __attribute__((always_inline)) bool is(int state) const {
		return (*this)[state];
	}
	/**
	 * Advances to next FSM state.
	 */
	inline __attribute__((always_inline)) void advance() {
		bool overflow = (*this)[SPACE_N_ST - 1];
		(*this) <<= 1;
		this->range(0, 0) = overflow;
	}
};

/**
 * A FSM which (with current implementation) can only advance to the next
 * state, circularly. This class is (currently) unused but is left as a
 * reference for us.
 *
 * @detail
 * This one is implemented using shift registers (SRL), but Vivado HLS seems to
 * think, when calling is() multiple times, that the calls to the code inside
 * is() have data dependencies between each other, and that therefore they must
 * be scheduled on a different cycle. There might be a
 * "#pragma HLS dependence variable=<stateVar> RAW intra off" that we could use
 * in some cases, but places that read the state also often need to change the
 * state as well, so we don't necessarily want to remove all dependencies
 * either.
 */
template<int SPACE_N_ST> class space_one_hot_fsm_srl
	: public ap_shift_reg<ap_uint<1>, SPACE_N_ST> {
private:
	typedef ap_uint<1> DataT;
	typedef ap_shift_reg<DataT, SPACE_N_ST> Base;
public:
	explicit space_one_hot_fsm_srl() : Base() {
		Base::shift(1);
	}
	/**
	 * Determines whether the FSM is at given state.
	 */
	inline __attribute__((always_inline)) DataT is(int state) const {
		return Base::read(state);
	}
	/**
	 * Advances to next FSM state.
	 */
	inline __attribute__((always_inline)) void advance() {
		DataT lastBit = Base::read();
		Base::shift(lastBit);
	}
};

/**
 * When accessing a pointer variable, Vivado HLS (2018.3) needs to know
 * whether that pointer points to an array or not. To determine this,
 * according to tests, it looks at how we access the pointer:
 *
 * If we do "ptr[idx]", then Vivado HLS assumes the pointer points to
 * an array (which are usually implemented as BRAMs). If we use that
 * syntax but it actually pointed to a non-array, then it seems the
 * last transfer of the DeviceRead/Write is wrong (RTL simulation
 * shows several "X" bytes for the last transfer).
 *
 * Otherwise, if we do "*ptr", then either:
 * 1) it assumes the pointer points to a non-array value, or
 * 2) if it sees this function is called for both arrays and
 * non-arrays, Vivado HLS seems to add a WHOLE LOT of logic to make
 * sure the code works with both array and non-array values.
 *
 * So must help Vivado HLS to optimize the code by using the template
 * function below.
 *
 * To do this, the template function below will do "*ptr" when IS_ARR
 * is false, and "ptr[currentElement]" when it is true. For this trick
 * to work, the calling function must have a template argument which
 * is directly used to determine the value of IS_ARR below. This will make
 * Vivado HLS see this calling function as if it had multiple separate
 * implementations (which means no single such implementation will be
 * called once with a pointer pointing to an array, and another time with
 * a pointer pointing to a non-array), and therefore Vivado HLS will not
 * add all that extra logic mentioned earlier.
 *
 * So basically, do this:
 *
 * template <bool IS_ARR, ...>
 * void myfunc(${my_data_type}* data) {
 *     // ...
 *     ${my_data_type}& d = space_get_ref_to_buf<IS_ARR>(data, dataIndex);
 *     // Use "d"...
 * }
 *
 * Note that, rather than using the template function below, we could
 * have also just written two separate functions, one with "*ptr", and
 * another with "ptr[currentElement]". That would have worked too, but
 * would have required us to generate the transfer code twice with
 * only that as a difference, which would make the code really hard on
 * the eyes :)
 */
template <bool IS_ARR, class DataT, class IdxT>
inline __attribute__((always_inline))
typename space_enable_if<
	IS_ARR == true
	, DataT&
>::type space_get_ref_to_buf(DataT* data, IdxT currentElement) {
	return data[currentElement];
}

/**
 * Implementation with "*ptr" syntax.
 */
template <bool IS_ARR, class DataT, class IdxT>
inline __attribute__((always_inline))
typename space_enable_if<
	IS_ARR == false
	, DataT&
>::type space_get_ref_to_buf(DataT* data, IdxT currentElement) {
	return *data;
}

#endif
