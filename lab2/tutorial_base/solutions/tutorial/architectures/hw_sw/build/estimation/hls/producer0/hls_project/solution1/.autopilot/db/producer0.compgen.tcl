# This script segment is generated automatically by AutoPilot

# clear list
if {${::AESL::PGuard_autoexp_gen}} {
    cg_default_interface_gen_dc_begin
    cg_default_interface_gen_bundle_begin
    AESL_LIB_XILADAPTER::native_axis_begin
}

# Adapter definition:
set PortName axi_master_0
set DataWd 32
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::axi_master_gen] == "::AESL_LIB_XILADAPTER::axi_master_gen"} {
eval "::AESL_LIB_XILADAPTER::axi_master_gen { \
    id 10 \
    name ${PortName} \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename AXI4M \
    op interface \
    metadata {} \
    latency 1 \
    data_wd ${DataWd} \
}"
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for '${PortName}'"
}
}


# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 6 \
    name fifo_in_0 \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_fifo_in_0 \
    op interface \
    ports { fifo_in_0_dout { I 32 vector } fifo_in_0_empty_n { I 1 bit } fifo_in_0_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 7 \
    name fifo_out_0 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_fifo_out_0 \
    op interface \
    ports { fifo_out_0_din { O 32 vector } fifo_out_0_full_n { I 1 bit } fifo_out_0_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 8 \
    name RegisterWriteEnablePort_0 \
    type other \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_RegisterWriteEnablePort_0 \
    op interface \
    ports { RegisterWriteEnablePort_0 { O 1 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 9 \
    name RegisterWriteDataPort_0 \
    type other \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_RegisterWriteDataPort_0 \
    op interface \
    ports { RegisterWriteDataPort_0 { O 32 vector } } \
} "
}


# Adapter definition:
set PortName clock
set DataWd 1 
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc cg_default_interface_gen_clock] == "cg_default_interface_gen_clock"} {
eval "cg_default_interface_gen_clock { \
    id -1 \
    name ${PortName} \
    reset_level 1 \
    sync_rst true \
    corename apif_clock \
    data_wd ${DataWd} \
    op interface \
}"
} else {
puts "@W \[IMPL-113\] Cannot find bus interface model in the library. Ignored generation of bus interface for '${PortName}'"
}
}


# Adapter definition:
set PortName reset
set DataWd 1 
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc cg_default_interface_gen_reset] == "cg_default_interface_gen_reset"} {
eval "cg_default_interface_gen_reset { \
    id -2 \
    name ${PortName} \
    reset_level 1 \
    sync_rst true \
    corename apif_reset \
    data_wd ${DataWd} \
    op interface \
}"
} else {
puts "@W \[IMPL-114\] Cannot find bus interface model in the library. Ignored generation of bus interface for '${PortName}'"
}
}



# merge
if {${::AESL::PGuard_autoexp_gen}} {
    cg_default_interface_gen_dc_end
    cg_default_interface_gen_bundle_end
    AESL_LIB_XILADAPTER::native_axis_end
}


