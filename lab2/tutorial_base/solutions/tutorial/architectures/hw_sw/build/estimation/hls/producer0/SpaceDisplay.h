///////////////////////////////////////////////////////////////////////////////
///
///         Space Codesign System Inc. - (http://www.spacecodesign.com)
///         (c) All Rights Reserved. 2017
///
///         No authorization to modify or use this file for
///         commercial purposes without prior written consent
///         of its author(s)
///
///////////////////////////////////////////////////////////////////////////////
#ifndef SPACE_DISPLAY_H
#define SPACE_DISPLAY_H

#define SpacePrint printf
#define SpaceMessageError printf
#define SpaceFlush fflush

#endif
