open_project hls_project
add_files producer0.cpp
open_solution "solution1"
set_part {xc7z020clg484-1}
create_clock -period 10.0
set_clock_uncertainty 3.0
config_rtl -reset_level high
csynth_design
exit