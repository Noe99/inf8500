library ieee;
use ieee.std_logic_1164.all;

entity producer0 is
	generic (
		AWIDTH : positive := 32;
		DWIDTH : positive := 32
	);
	port (
		-- AXI-Stream master to consumer0
		stream_master_0_tdata : out std_logic_vector(0 to 31);
		stream_master_0_tlast : out std_logic;
		stream_master_0_tready : in std_logic;
		stream_master_0_tvalid : out std_logic;

		-- AXI-Stream slave from consumer0
		stream_slave_0_tdata : in std_logic_vector(0 to 31);
		stream_slave_0_tlast : in std_logic;
		stream_slave_0_tready : out std_logic;
		stream_slave_0_tvalid : in std_logic;

		-- Register-based write interface for register 0 of register file register_file0
		register_write_0_enable : out std_logic;
		register_write_0_data : out std_logic_vector(0 to DWIDTH-1);

		-- AXI-Master bus interface #0 to device(s) ddr_mem
		axi_master_0_awid : out std_logic_vector(0 downto 0);
		axi_master_0_awaddr : out std_logic_vector(AWIDTH-1 downto 0);
		axi_master_0_awlen : out std_logic_vector(7 downto 0);
		axi_master_0_awsize : out std_logic_vector(2 downto 0);
		axi_master_0_awburst : out std_logic_vector(1 downto 0);
		axi_master_0_awlock : out std_logic_vector(1 downto 0);
		axi_master_0_awcache : out std_logic_vector(AWIDTH/8-1 downto 0);
		axi_master_0_awprot : out std_logic_vector(2 downto 0);
		axi_master_0_awqos : out std_logic_vector(AWIDTH/8-1 downto 0);
		axi_master_0_awuser : out std_logic_vector(0 downto 0);
		axi_master_0_awvalid : out std_logic;
		axi_master_0_awready : in std_logic;
		axi_master_0_wdata : out std_logic_vector(DWIDTH-1 downto 0);
		axi_master_0_wstrb : out std_logic_vector(DWIDTH/8-1 downto 0);
		axi_master_0_wlast : out std_logic;
		axi_master_0_wuser : out std_logic_vector(0 downto 0);
		axi_master_0_wvalid : out std_logic;
		axi_master_0_wready : in std_logic;
		axi_master_0_bid : in std_logic_vector(0 downto 0);
		axi_master_0_bresp : in std_logic_vector(1 downto 0);
		axi_master_0_buser : in std_logic_vector(0 downto 0);
		axi_master_0_bvalid : in std_logic;
		axi_master_0_bready : out std_logic;
		axi_master_0_arid : out std_logic_vector(0 downto 0);
		axi_master_0_araddr : out std_logic_vector(AWIDTH-1 downto 0);
		axi_master_0_arlen : out std_logic_vector(7 downto 0);
		axi_master_0_arsize : out std_logic_vector(2 downto 0);
		axi_master_0_arburst : out std_logic_vector(1 downto 0);
		axi_master_0_arlock : out std_logic_vector(1 downto 0);
		axi_master_0_arcache : out std_logic_vector(AWIDTH/8-1 downto 0);
		axi_master_0_arprot : out std_logic_vector(2 downto 0);
		axi_master_0_arqos : out std_logic_vector(AWIDTH/8-1 downto 0);
		axi_master_0_aruser : out std_logic_vector(0 downto 0);
		axi_master_0_arvalid : out std_logic;
		axi_master_0_arready : in std_logic;
		axi_master_0_rid : in std_logic_vector(0 downto 0);
		axi_master_0_rdata : in std_logic_vector(DWIDTH-1 downto 0);
		axi_master_0_rresp : in std_logic_vector(1 downto 0);
		axi_master_0_rlast : in std_logic;
		axi_master_0_ruser : in std_logic_vector(0 downto 0);
		axi_master_0_rvalid : in std_logic;
		axi_master_0_rready : out std_logic;

		-- clock, reset
		clock : in std_logic;
		reset : in std_logic
	);
end producer0;

architecture STRUCTURE of producer0 is
	component producer0_internal_top is
		generic (
			C_M_AXI_AXI_MASTER_0_PROT_VALUE : std_logic_vector := "000" ; 
			C_M_AXI_AXI_MASTER_0_CACHE_VALUE : std_logic_vector := "000" 
		);
		port (
			RegisterWriteEnablePort_0 : out std_logic_vector(0 to 0);
			RegisterWriteDataPort_0 : out std_logic_vector(31 downto 0);
			m_axi_axi_master_0_AWID : out std_logic_vector(0 to 0);
			m_axi_axi_master_0_AWADDR : out std_logic_vector(31 downto 0);
			m_axi_axi_master_0_AWLEN : out std_logic_vector(7 downto 0);
			m_axi_axi_master_0_AWSIZE : out std_logic_vector(2 downto 0);
			m_axi_axi_master_0_AWBURST : out std_logic_vector(1 downto 0);
			m_axi_axi_master_0_AWLOCK : out std_logic_vector(1 downto 0);
			m_axi_axi_master_0_AWCACHE : out std_logic_vector(3 downto 0);
			m_axi_axi_master_0_AWPROT : out std_logic_vector(2 downto 0);
			m_axi_axi_master_0_AWQOS : out std_logic_vector(3 downto 0);
			m_axi_axi_master_0_AWUSER : out std_logic_vector(0 to 0);
			m_axi_axi_master_0_AWVALID : out std_logic;
			m_axi_axi_master_0_AWREADY : in std_logic;
			m_axi_axi_master_0_WDATA : out std_logic_vector(31 downto 0);
			m_axi_axi_master_0_WSTRB : out std_logic_vector(3 downto 0);
			m_axi_axi_master_0_WLAST : out std_logic;
			m_axi_axi_master_0_WUSER : out std_logic_vector(0 to 0);
			m_axi_axi_master_0_WVALID : out std_logic;
			m_axi_axi_master_0_WREADY : in std_logic;
			m_axi_axi_master_0_BID : in std_logic_vector(0 to 0);
			m_axi_axi_master_0_BRESP : in std_logic_vector(1 downto 0);
			m_axi_axi_master_0_BUSER : in std_logic_vector(0 to 0);
			m_axi_axi_master_0_BVALID : in std_logic;
			m_axi_axi_master_0_BREADY : out std_logic;
			m_axi_axi_master_0_ARID : out std_logic_vector(0 to 0);
			m_axi_axi_master_0_ARADDR : out std_logic_vector(31 downto 0);
			m_axi_axi_master_0_ARLEN : out std_logic_vector(7 downto 0);
			m_axi_axi_master_0_ARSIZE : out std_logic_vector(2 downto 0);
			m_axi_axi_master_0_ARBURST : out std_logic_vector(1 downto 0);
			m_axi_axi_master_0_ARLOCK : out std_logic_vector(1 downto 0);
			m_axi_axi_master_0_ARCACHE : out std_logic_vector(3 downto 0);
			m_axi_axi_master_0_ARPROT : out std_logic_vector(2 downto 0);
			m_axi_axi_master_0_ARQOS : out std_logic_vector(3 downto 0);
			m_axi_axi_master_0_ARUSER : out std_logic_vector(0 to 0);
			m_axi_axi_master_0_ARVALID : out std_logic;
			m_axi_axi_master_0_ARREADY : in std_logic;
			m_axi_axi_master_0_RID : in std_logic_vector(0 to 0);
			m_axi_axi_master_0_RDATA : in std_logic_vector(31 downto 0);
			m_axi_axi_master_0_RRESP : in std_logic_vector(1 downto 0);
			m_axi_axi_master_0_RLAST : in std_logic;
			m_axi_axi_master_0_RUSER : in std_logic_vector(0 to 0);
			m_axi_axi_master_0_RVALID : in std_logic;
			m_axi_axi_master_0_RREADY : out std_logic;
			fifo_in_0_dout : in std_logic_vector(31 downto 0);
			fifo_in_0_empty_n : in std_logic;
			fifo_in_0_read : out std_logic;
			fifo_out_0_din : out std_logic_vector(31 downto 0);
			fifo_out_0_full_n : in std_logic;
			fifo_out_0_write : out std_logic;
			aclk : in std_logic;
			aresetn : in std_logic
		);
end component;

begin
	stream_master_0_tlast <= '1';
	producer0_internal_top_instance : producer0_internal_top
		generic map (
			C_M_AXI_AXI_MASTER_0_CACHE_VALUE => "0011",
			C_M_AXI_AXI_MASTER_0_PROT_VALUE => "000"
		)		port map (
			RegisterWriteDataPort_0 => register_write_0_data,
			RegisterWriteEnablePort_0(0) => register_write_0_enable,
			aclk => clock,
			aresetn => "not"(reset),
			fifo_in_0_dout => stream_slave_0_tdata,
			fifo_in_0_empty_n => stream_slave_0_tvalid,
			fifo_in_0_read => stream_slave_0_tready,
			fifo_out_0_din => stream_master_0_tdata,
			fifo_out_0_full_n => stream_master_0_tready,
			fifo_out_0_write => stream_master_0_tvalid,
			m_axi_axi_master_0_ARADDR => axi_master_0_araddr,
			m_axi_axi_master_0_ARBURST => axi_master_0_arburst,
			m_axi_axi_master_0_ARCACHE => axi_master_0_arcache,
			m_axi_axi_master_0_ARID => axi_master_0_arid,
			m_axi_axi_master_0_ARLEN => axi_master_0_arlen,
			m_axi_axi_master_0_ARLOCK => axi_master_0_arlock,
			m_axi_axi_master_0_ARPROT => axi_master_0_arprot,
			m_axi_axi_master_0_ARQOS => axi_master_0_arqos,
			m_axi_axi_master_0_ARREADY => axi_master_0_arready,
			m_axi_axi_master_0_ARSIZE => axi_master_0_arsize,
			m_axi_axi_master_0_ARUSER => axi_master_0_aruser,
			m_axi_axi_master_0_ARVALID => axi_master_0_arvalid,
			m_axi_axi_master_0_AWADDR => axi_master_0_awaddr,
			m_axi_axi_master_0_AWBURST => axi_master_0_awburst,
			m_axi_axi_master_0_AWCACHE => axi_master_0_awcache,
			m_axi_axi_master_0_AWID => axi_master_0_awid,
			m_axi_axi_master_0_AWLEN => axi_master_0_awlen,
			m_axi_axi_master_0_AWLOCK => axi_master_0_awlock,
			m_axi_axi_master_0_AWPROT => axi_master_0_awprot,
			m_axi_axi_master_0_AWQOS => axi_master_0_awqos,
			m_axi_axi_master_0_AWREADY => axi_master_0_awready,
			m_axi_axi_master_0_AWSIZE => axi_master_0_awsize,
			m_axi_axi_master_0_AWUSER => axi_master_0_awuser,
			m_axi_axi_master_0_AWVALID => axi_master_0_awvalid,
			m_axi_axi_master_0_BID => axi_master_0_bid,
			m_axi_axi_master_0_BREADY => axi_master_0_bready,
			m_axi_axi_master_0_BRESP => axi_master_0_bresp,
			m_axi_axi_master_0_BUSER => axi_master_0_buser,
			m_axi_axi_master_0_BVALID => axi_master_0_bvalid,
			m_axi_axi_master_0_RDATA => axi_master_0_rdata,
			m_axi_axi_master_0_RID => axi_master_0_rid,
			m_axi_axi_master_0_RLAST => axi_master_0_rlast,
			m_axi_axi_master_0_RREADY => axi_master_0_rready,
			m_axi_axi_master_0_RRESP => axi_master_0_rresp,
			m_axi_axi_master_0_RUSER => axi_master_0_ruser,
			m_axi_axi_master_0_RVALID => axi_master_0_rvalid,
			m_axi_axi_master_0_WDATA => axi_master_0_wdata,
			m_axi_axi_master_0_WLAST => axi_master_0_wlast,
			m_axi_axi_master_0_WREADY => axi_master_0_wready,
			m_axi_axi_master_0_WSTRB => axi_master_0_wstrb,
			m_axi_axi_master_0_WUSER => axi_master_0_wuser,
			m_axi_axi_master_0_WVALID => axi_master_0_wvalid
		);

end architecture STRUCTURE;