; ModuleID = '/home/julien/education/inf8500/lab2/tutorial_ju/solutions/tutorial/architectures/hw_sw/build/estimation/hls/producer0/hls_project/solution1/.autopilot/db/a.o.2.bc'
target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v64:64:64-v128:128:128-a0:0:64-s0:64:64-f80:128:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@producer0_ssdm_thread_M_thread = external global i1
@p_str9 = private unnamed_addr constant [11 x i8] c"fifo_out_0\00", align 1
@p_str8 = private unnamed_addr constant [10 x i8] c"fifo_in_0\00", align 1
@p_str7 = private unnamed_addr constant [14 x i8] c"\22ap_uint<32>\22\00", align 1
@p_str6 = private unnamed_addr constant [1 x i8] zeroinitializer, align 1
@p_str5 = private unnamed_addr constant [8 x i8] c"ap_fifo\00", align 1
@p_str4 = private unnamed_addr constant [7 x i8] c"\22bool\22\00", align 1
@p_str30 = private unnamed_addr constant [7 x i8] c"ap_bus\00", align 1
@p_str3 = private unnamed_addr constant [6 x i8] c"reset\00", align 1
@p_str29 = private unnamed_addr constant [6 x i8] c"AXI4M\00", align 1
@p_str23 = private unnamed_addr constant [12 x i8] c"hls_label_3\00", align 1
@p_str22 = private unnamed_addr constant [9 x i8] c"MW_loop1\00", align 1
@p_str21 = private unnamed_addr constant [12 x i8] c"hls_label_2\00", align 1
@p_str20 = private unnamed_addr constant [12 x i8] c"hls_label_1\00", align 1
@p_str2 = private unnamed_addr constant [6 x i8] c"clock\00", align 1
@p_str19 = private unnamed_addr constant [9 x i8] c"MR_loop0\00", align 1
@p_str18 = private unnamed_addr constant [12 x i8] c"hls_label_0\00", align 1
@p_str17 = private unnamed_addr constant [15 x i8] c"__ssdm_reset__\00", align 1
@p_str15 = private unnamed_addr constant [13 x i8] c"axi_master_0\00", align 1
@p_str14 = private unnamed_addr constant [6 x i8] c"\22int\22\00", align 1
@p_str13 = private unnamed_addr constant [24 x i8] c"RegisterWriteDataPort_0\00", align 1
@p_str12 = private unnamed_addr constant [19 x i8] c"\22sc_dt::sc_lv<32>\22\00", align 1
@p_str11 = private unnamed_addr constant [26 x i8] c"RegisterWriteEnablePort_0\00", align 1
@p_str10 = private unnamed_addr constant [18 x i8] c"\22sc_dt::sc_lv<1>\22\00", align 1
@p_str1 = private unnamed_addr constant [7 x i8] c"thread\00", align 1
@p_str = private unnamed_addr constant [12 x i8] c"producer0\00", align 1

declare void @llvm.dbg.value(metadata, i64, metadata) nounwind readnone

define weak i1 @_ssdm_op_WriteReq.ap_bus.i32P(i32*, i32) {
entry:
  ret i1 true
}

define weak void @_ssdm_op_Write.ap_fifo.volatile.i32P(i32*, i32) {
entry:
  %empty = call i32 @_autotb_FifoWrite_i32(i32* %0, i32 %1)
  ret void
}

define weak void @_ssdm_op_Write.ap_bus.volatile.i32P(i32*, i32) {
entry:
  store i32 %1, i32* %0
  ret void
}

define weak void @_ssdm_op_Write.ap_auto.volatile.i32P(i32*, i32) {
entry:
  store i32 %1, i32* %0
  ret void
}

define weak void @_ssdm_op_Write.ap_auto.volatile.i1P(i1*, i1) {
entry:
  store i1 %1, i1* %0
  ret void
}

define weak void @_ssdm_op_Wait(...) nounwind {
entry:
  ret void
}

define weak void @_ssdm_op_SpecTopModule(...) nounwind {
entry:
  ret void
}

define weak i32 @_ssdm_op_SpecStateEnd(...) nounwind {
entry:
  ret i32 0
}

define weak i32 @_ssdm_op_SpecStateBegin(...) nounwind {
entry:
  ret i32 0
}

define weak void @_ssdm_op_SpecSensitive(...) nounwind {
entry:
  ret void
}

define weak i32 @_ssdm_op_SpecRegionEnd(...) {
entry:
  ret i32 0
}

define weak i32 @_ssdm_op_SpecRegionBegin(...) {
entry:
  ret i32 0
}

define weak void @_ssdm_op_SpecProtocol(...) nounwind {
entry:
  ret void
}

define weak void @_ssdm_op_SpecProcessDef(...) nounwind {
entry:
  ret void
}

define weak void @_ssdm_op_SpecProcessDecl(...) nounwind {
entry:
  ret void
}

define weak void @_ssdm_op_SpecPort(...) nounwind {
entry:
  ret void
}

define weak void @_ssdm_op_SpecPipeline(...) nounwind {
entry:
  ret void
}

declare i32 @_ssdm_op_SpecLoopTripCount(...)

define weak void @_ssdm_op_SpecLoopName(...) nounwind {
entry:
  ret void
}

define weak void @_ssdm_op_SpecInterface(...) nounwind {
entry:
  ret void
}

define weak void @_ssdm_op_SpecIFCore(...) {
entry:
  ret void
}

define weak void @_ssdm_op_SpecBitsMap(...) {
entry:
  ret void
}

define weak i32 @_ssdm_op_Read.ap_fifo.volatile.i32P(i32*) {
entry:
  %empty = call i32 @_autotb_FifoRead_i32(i32* %0)
  ret i32 %empty
}

define weak i1 @_ssdm_op_NbWriteReq.ap_fifo.i32P(i32*, i32) {
entry:
  %empty = call i1 @_autotb_FifoCanWrite_i32(i32* %0)
  ret i1 %empty
}

define weak i1 @_ssdm_op_NbReadReq.ap_fifo.i32P(i32*, i32) {
entry:
  %empty = call i1 @_autotb_FifoCanRead_i32(i32* %0)
  ret i1 %empty
}

declare i32 @_autotb_FifoWrite_i32(i32*, i32)

declare i32 @_autotb_FifoRead_i32(i32*)

declare i1 @_autotb_FifoCanWrite_i32(i32*)

declare i1 @_autotb_FifoCanRead_i32(i32*)

define void @"producer0::thread"(i1* %clock, i1* %reset, i32* %fifo_in_0, i32* %fifo_out_0, i1* %RegisterWriteEnablePort_0, i32* %RegisterWriteDataPort_0, i32* %axi_master_0) noinline {
_ZN7_ap_sc_7sc_core4waitEi.exit:
  call void (...)* @_ssdm_op_SpecIFCore(i32* %axi_master_0, [1 x i8]* @p_str6, [6 x i8]* @p_str29, [1 x i8]* @p_str6, i32 -1, [1 x i8]* @p_str6, [1 x i8]* @p_str6, [1 x i8]* @p_str6, [1 x i8]* @p_str6, [1 x i8]* @p_str6)
  call void (...)* @_ssdm_op_SpecInterface(i32* %axi_master_0, [7 x i8]* @p_str30, i32 0, i32 0, [1 x i8]* @p_str6, i32 0, i32 0, [1 x i8]* @p_str6, [1 x i8]* @p_str6, [1 x i8]* @p_str6, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str6, [1 x i8]* @p_str6)
  call void (...)* @_ssdm_op_SpecBitsMap(i1* %clock), !map !132
  call void (...)* @_ssdm_op_SpecBitsMap(i1* %reset), !map !136
  call void (...)* @_ssdm_op_SpecBitsMap(i32* %fifo_in_0), !map !140
  call void (...)* @_ssdm_op_SpecBitsMap(i32* %fifo_out_0), !map !144
  call void (...)* @_ssdm_op_SpecBitsMap(i1* %RegisterWriteEnablePort_0), !map !148
  call void (...)* @_ssdm_op_SpecBitsMap(i32* %RegisterWriteDataPort_0), !map !152
  call void (...)* @_ssdm_op_SpecBitsMap(i32* %axi_master_0), !map !156
  call void @llvm.dbg.value(metadata !{i1* %clock}, i64 0, metadata !160), !dbg !1848
  call void @llvm.dbg.value(metadata !{i1* %reset}, i64 0, metadata !1849), !dbg !1848
  call void @llvm.dbg.value(metadata !{i32* %fifo_in_0}, i64 0, metadata !1850), !dbg !1848
  call void @llvm.dbg.value(metadata !{i32* %fifo_out_0}, i64 0, metadata !1866), !dbg !1848
  call void @llvm.dbg.value(metadata !{i1* %RegisterWriteEnablePort_0}, i64 0, metadata !1875), !dbg !1848
  call void @llvm.dbg.value(metadata !{i32* %RegisterWriteDataPort_0}, i64 0, metadata !1895), !dbg !1848
  call void @llvm.dbg.value(metadata !{i32* %axi_master_0}, i64 0, metadata !1910), !dbg !1848
  call void (...)* @_ssdm_op_SpecPort([12 x i8]* @p_str, i32 0, [7 x i8]* @p_str4, [6 x i8]* @p_str2, i32 0, i32 0, i1* %clock) nounwind, !dbg !1920
  call void (...)* @_ssdm_op_SpecPort([12 x i8]* @p_str, i32 0, [7 x i8]* @p_str4, [6 x i8]* @p_str3, i32 0, i32 0, i1* %reset) nounwind, !dbg !1922
  call void (...)* @_ssdm_op_SpecInterface(i32* %fifo_in_0, [8 x i8]* @p_str5, i32 0, i32 0, [1 x i8]* @p_str6, i32 0, i32 0, [1 x i8]* @p_str6, [1 x i8]* @p_str6, [1 x i8]* @p_str6, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str6, [1 x i8]* @p_str6) nounwind, !dbg !1923
  call void (...)* @_ssdm_op_SpecInterface(i32* %fifo_out_0, [8 x i8]* @p_str5, i32 0, i32 0, [1 x i8]* @p_str6, i32 0, i32 0, [1 x i8]* @p_str6, [1 x i8]* @p_str6, [1 x i8]* @p_str6, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str6, [1 x i8]* @p_str6) nounwind, !dbg !1924
  call void (...)* @_ssdm_op_SpecPort([12 x i8]* @p_str, i32 1, [18 x i8]* @p_str10, [26 x i8]* @p_str11, i32 0, i32 0, i1* %RegisterWriteEnablePort_0) nounwind, !dbg !1925
  call void (...)* @_ssdm_op_SpecPort([12 x i8]* @p_str, i32 1, [19 x i8]* @p_str12, [24 x i8]* @p_str13, i32 0, i32 0, i32* %RegisterWriteDataPort_0) nounwind, !dbg !1926
  call void (...)* @_ssdm_op_SpecPort([12 x i8]* @p_str, i32 7, [6 x i8]* @p_str14, [13 x i8]* @p_str15, i32 0, i32 0, i32* %axi_master_0) nounwind, !dbg !1927
  call void (...)* @_ssdm_op_SpecProcessDef([12 x i8]* @p_str, i32 2, [7 x i8]* @p_str1) nounwind, !dbg !1928
  %tmp = call i32 (...)* @_ssdm_op_SpecRegionBegin([15 x i8]* @p_str17), !dbg !1928
  call void (...)* @_ssdm_op_SpecProtocol(i32 1, [1 x i8]* @p_str6) nounwind, !dbg !1928
  %p_ssdm_reset_v = call i32 (...)* @_ssdm_op_SpecStateBegin(i32 0, i32 0, i32 1) nounwind, !dbg !1928
  call void @llvm.dbg.value(metadata !{i32 %p_ssdm_reset_v}, i64 0, metadata !1929), !dbg !1928
  call void @llvm.dbg.value(metadata !{i1* %clock}, i64 0, metadata !1930), !dbg !1934
  call void @llvm.dbg.value(metadata !{i1* %reset}, i64 0, metadata !1935), !dbg !1934
  call void @llvm.dbg.value(metadata !{i32* %fifo_in_0}, i64 0, metadata !1936), !dbg !1934
  call void @llvm.dbg.value(metadata !{i32* %fifo_out_0}, i64 0, metadata !1937), !dbg !1934
  call void @llvm.dbg.value(metadata !{i1* %RegisterWriteEnablePort_0}, i64 0, metadata !1938), !dbg !1934
  call void @llvm.dbg.value(metadata !{i32* %RegisterWriteDataPort_0}, i64 0, metadata !1939), !dbg !1934
  call void @llvm.dbg.value(metadata !{i32* %axi_master_0}, i64 0, metadata !1940), !dbg !1934
  call void @llvm.dbg.value(metadata !{i1* %RegisterWriteEnablePort_0}, i64 0, metadata !1941), !dbg !1963
  call void @_ssdm_op_Write.ap_auto.volatile.i1P(i1* %RegisterWriteEnablePort_0, i1 false), !dbg !1964
  call void @llvm.dbg.value(metadata !{i32* %RegisterWriteDataPort_0}, i64 0, metadata !1967), !dbg !1988
  call void @_ssdm_op_Write.ap_auto.volatile.i32P(i32* %RegisterWriteDataPort_0, i32 0), !dbg !1989
  call void (...)* @_ssdm_op_Wait(i32 1) nounwind, !dbg !1991
  call void (...)* @_ssdm_op_Wait(i32 1) nounwind, !dbg !1998
  %empty = call i32 (...)* @_ssdm_op_SpecStateEnd(i32 %p_ssdm_reset_v) nounwind, !dbg !2000
  %empty_3 = call i32 (...)* @_ssdm_op_SpecRegionEnd([15 x i8]* @p_str17, i32 %tmp), !dbg !2000
  %producer0_axi_mast = getelementptr inbounds i32* %axi_master_0, i64 67108864, !dbg !2001
  br label %"DeviceWriteBusBitwidthIsMultipleOfDataBitwidth_single<false, int, unsigned int, AXI4M_bus_port<int> >.exit", !dbg !2052

"DeviceWriteBusBitwidthIsMultipleOfDataBitwidth_single<false, int, unsigned int, AXI4M_bus_port<int> >.exit": ; preds = %"DeviceWriteBusBitwidthIsMultipleOfDataBitwidth_single<false, int, unsigned int, AXI4M_bus_port<int> >.exit", %_ZN7_ap_sc_7sc_core4waitEi.exit
  %tmp_V = phi i32 [ 240, %_ZN7_ap_sc_7sc_core4waitEi.exit ], [ %value, %"DeviceWriteBusBitwidthIsMultipleOfDataBitwidth_single<false, int, unsigned int, AXI4M_bus_port<int> >.exit" ]
  call void @llvm.dbg.value(metadata !{i1* %clock}, i64 0, metadata !2053), !dbg !2055
  call void @llvm.dbg.value(metadata !{i1* %reset}, i64 0, metadata !2056), !dbg !2055
  call void @llvm.dbg.value(metadata !{i32* %fifo_in_0}, i64 0, metadata !2057), !dbg !2055
  call void @llvm.dbg.value(metadata !{i32* %fifo_out_0}, i64 0, metadata !2058), !dbg !2055
  call void @llvm.dbg.value(metadata !{i1* %RegisterWriteEnablePort_0}, i64 0, metadata !2059), !dbg !2055
  call void @llvm.dbg.value(metadata !{i32* %RegisterWriteDataPort_0}, i64 0, metadata !2060), !dbg !2055
  call void @llvm.dbg.value(metadata !{i32* %axi_master_0}, i64 0, metadata !2061), !dbg !2055
  call void @llvm.dbg.value(metadata !{i1* %clock}, i64 0, metadata !2062), !dbg !2064
  call void @llvm.dbg.value(metadata !{i1* %reset}, i64 0, metadata !2065), !dbg !2064
  call void @llvm.dbg.value(metadata !{i32* %fifo_in_0}, i64 0, metadata !2066), !dbg !2064
  call void @llvm.dbg.value(metadata !{i32* %fifo_out_0}, i64 0, metadata !2067), !dbg !2064
  call void @llvm.dbg.value(metadata !{i1* %RegisterWriteEnablePort_0}, i64 0, metadata !2068), !dbg !2064
  call void @llvm.dbg.value(metadata !{i32* %RegisterWriteDataPort_0}, i64 0, metadata !2069), !dbg !2064
  call void @llvm.dbg.value(metadata !{i32* %axi_master_0}, i64 0, metadata !2070), !dbg !2064
  %producer0_axi_mast_1 = call i1 @_ssdm_op_WriteReq.ap_bus.i32P(i32* %producer0_axi_mast, i32 1), !dbg !2071
  call void @_ssdm_op_Write.ap_bus.volatile.i32P(i32* %producer0_axi_mast, i32 %tmp_V), !dbg !2071
  call void @llvm.dbg.value(metadata !{i1* %clock}, i64 0, metadata !2080), !dbg !2087
  call void @llvm.dbg.value(metadata !{i1* %reset}, i64 0, metadata !2088), !dbg !2087
  call void @llvm.dbg.value(metadata !{i32* %fifo_in_0}, i64 0, metadata !2089), !dbg !2087
  call void @llvm.dbg.value(metadata !{i32* %fifo_out_0}, i64 0, metadata !2090), !dbg !2087
  call void @llvm.dbg.value(metadata !{i1* %RegisterWriteEnablePort_0}, i64 0, metadata !2091), !dbg !2087
  call void @llvm.dbg.value(metadata !{i32* %RegisterWriteDataPort_0}, i64 0, metadata !2092), !dbg !2087
  call void @llvm.dbg.value(metadata !{i32* %axi_master_0}, i64 0, metadata !2093), !dbg !2087
  call void @llvm.dbg.value(metadata !{i1* %clock}, i64 0, metadata !2094), !dbg !2101
  call void @llvm.dbg.value(metadata !{i1* %reset}, i64 0, metadata !2102), !dbg !2101
  call void @llvm.dbg.value(metadata !{i32* %fifo_in_0}, i64 0, metadata !2103), !dbg !2101
  call void @llvm.dbg.value(metadata !{i32* %fifo_out_0}, i64 0, metadata !2104), !dbg !2101
  call void @llvm.dbg.value(metadata !{i1* %RegisterWriteEnablePort_0}, i64 0, metadata !2105), !dbg !2101
  call void @llvm.dbg.value(metadata !{i32* %RegisterWriteDataPort_0}, i64 0, metadata !2106), !dbg !2101
  call void @llvm.dbg.value(metadata !{i32* %axi_master_0}, i64 0, metadata !2107), !dbg !2101
  call void @llvm.dbg.value(metadata !{i1* %clock}, i64 0, metadata !2108), !dbg !2116
  call void @llvm.dbg.value(metadata !{i1* %reset}, i64 0, metadata !2117), !dbg !2116
  call void @llvm.dbg.value(metadata !{i32* %fifo_in_0}, i64 0, metadata !2118), !dbg !2116
  call void @llvm.dbg.value(metadata !{i32* %fifo_out_0}, i64 0, metadata !2119), !dbg !2116
  call void @llvm.dbg.value(metadata !{i1* %RegisterWriteEnablePort_0}, i64 0, metadata !2120), !dbg !2116
  call void @llvm.dbg.value(metadata !{i32* %RegisterWriteDataPort_0}, i64 0, metadata !2121), !dbg !2116
  call void @llvm.dbg.value(metadata !{i32* %axi_master_0}, i64 0, metadata !2122), !dbg !2116
  call void @llvm.dbg.value(metadata !{i32 %tmp_V}, i64 0, metadata !2123) nounwind, !dbg !2127
  call void @llvm.dbg.value(metadata !{i32 %tmp_V}, i64 0, metadata !2128) nounwind, !dbg !2131
  call void @llvm.dbg.value(metadata !{i32 %tmp_V}, i64 0, metadata !2132) nounwind, !dbg !2135
  call void @llvm.dbg.value(metadata !{i32 %tmp_V}, i64 0, metadata !2136) nounwind, !dbg !2139
  call void @llvm.dbg.value(metadata !{i32* %RegisterWriteDataPort_0}, i64 0, metadata !2140), !dbg !2143
  call void @llvm.dbg.value(metadata !{i32 %tmp_V}, i64 0, metadata !2144), !dbg !2146
  call void @_ssdm_op_Write.ap_auto.volatile.i32P(i32* %RegisterWriteDataPort_0, i32 %tmp_V), !dbg !2147
  call void @llvm.dbg.value(metadata !{i1* %RegisterWriteEnablePort_0}, i64 0, metadata !2148), !dbg !2153
  call void @_ssdm_op_Write.ap_auto.volatile.i1P(i1* %RegisterWriteEnablePort_0, i1 true), !dbg !2154
  call void (...)* @_ssdm_op_Wait(i32 1) nounwind, !dbg !2155
  call void @llvm.dbg.value(metadata !{i1* %RegisterWriteEnablePort_0}, i64 0, metadata !2157), !dbg !2162
  call void @_ssdm_op_Write.ap_auto.volatile.i1P(i1* %RegisterWriteEnablePort_0, i1 false), !dbg !2163
  call void @llvm.dbg.value(metadata !{i1* %clock}, i64 0, metadata !2164), !dbg !2168
  call void @llvm.dbg.value(metadata !{i1* %reset}, i64 0, metadata !2169), !dbg !2168
  call void @llvm.dbg.value(metadata !{i32* %fifo_in_0}, i64 0, metadata !2170), !dbg !2168
  call void @llvm.dbg.value(metadata !{i32* %fifo_out_0}, i64 0, metadata !2171), !dbg !2168
  call void @llvm.dbg.value(metadata !{i1* %RegisterWriteEnablePort_0}, i64 0, metadata !2172), !dbg !2168
  call void @llvm.dbg.value(metadata !{i32* %RegisterWriteDataPort_0}, i64 0, metadata !2173), !dbg !2168
  call void @llvm.dbg.value(metadata !{i32* %axi_master_0}, i64 0, metadata !2174), !dbg !2168
  call void @llvm.dbg.value(metadata !{i1* %clock}, i64 0, metadata !2175), !dbg !2180
  call void @llvm.dbg.value(metadata !{i1* %reset}, i64 0, metadata !2181), !dbg !2180
  call void @llvm.dbg.value(metadata !{i32* %fifo_in_0}, i64 0, metadata !2182), !dbg !2180
  call void @llvm.dbg.value(metadata !{i32* %fifo_out_0}, i64 0, metadata !2183), !dbg !2180
  call void @llvm.dbg.value(metadata !{i1* %RegisterWriteEnablePort_0}, i64 0, metadata !2184), !dbg !2180
  call void @llvm.dbg.value(metadata !{i32* %RegisterWriteDataPort_0}, i64 0, metadata !2185), !dbg !2180
  call void @llvm.dbg.value(metadata !{i32* %axi_master_0}, i64 0, metadata !2186), !dbg !2180
  call void @llvm.dbg.value(metadata !{i1* %clock}, i64 0, metadata !2187), !dbg !2194
  call void @llvm.dbg.value(metadata !{i1* %reset}, i64 0, metadata !2195), !dbg !2194
  call void @llvm.dbg.value(metadata !{i32* %fifo_in_0}, i64 0, metadata !2196), !dbg !2194
  call void @llvm.dbg.value(metadata !{i32* %fifo_out_0}, i64 0, metadata !2197), !dbg !2194
  call void @llvm.dbg.value(metadata !{i1* %RegisterWriteEnablePort_0}, i64 0, metadata !2198), !dbg !2194
  call void @llvm.dbg.value(metadata !{i32* %RegisterWriteDataPort_0}, i64 0, metadata !2199), !dbg !2194
  call void @llvm.dbg.value(metadata !{i32* %axi_master_0}, i64 0, metadata !2200), !dbg !2194
  %tmp_1 = call i32 (...)* @_ssdm_op_SpecRegionBegin([12 x i8]* @p_str21), !dbg !2201
  call void (...)* @_ssdm_op_SpecProtocol(i32 1, [1 x i8]* @p_str6) nounwind, !dbg !2204
  %empty_4 = call i32 (...)* @_ssdm_op_SpecRegionEnd([12 x i8]* @p_str21, i32 %tmp_1), !dbg !2205
  call void @llvm.dbg.value(metadata !{i32* %fifo_out_0}, i64 0, metadata !2206), !dbg !2223
  %empty_5 = call i1 @_ssdm_op_NbWriteReq.ap_fifo.i32P(i32* %fifo_out_0, i32 1), !dbg !2224
  call void (...)* @_ssdm_op_SpecLoopName([9 x i8]* @p_str22) nounwind, !dbg !2226
  %tmp_2 = call i32 (...)* @_ssdm_op_SpecRegionBegin([9 x i8]* @p_str22), !dbg !2226
  call void (...)* @_ssdm_op_SpecPipeline(i32 -1, i32 1, i32 1, i32 0, [1 x i8]* @p_str6) nounwind, !dbg !2230
  call void @llvm.dbg.value(metadata !{i32* %fifo_out_0}, i64 0, metadata !2231), !dbg !2247
  call void @_ssdm_op_Write.ap_fifo.volatile.i32P(i32* %fifo_out_0, i32 undef), !dbg !2248
  %empty_6 = call i32 (...)* @_ssdm_op_SpecRegionEnd([9 x i8]* @p_str22, i32 %tmp_2), !dbg !2250
  %tmp_3 = call i32 (...)* @_ssdm_op_SpecRegionBegin([12 x i8]* @p_str23), !dbg !2251
  call void (...)* @_ssdm_op_SpecProtocol(i32 1, [1 x i8]* @p_str6) nounwind, !dbg !2253
  %empty_7 = call i32 (...)* @_ssdm_op_SpecRegionEnd([12 x i8]* @p_str23, i32 %tmp_3), !dbg !2254
  call void @llvm.dbg.value(metadata !{i1* %clock}, i64 0, metadata !2255), !dbg !2259
  call void @llvm.dbg.value(metadata !{i1* %reset}, i64 0, metadata !2260), !dbg !2259
  call void @llvm.dbg.value(metadata !{i32* %fifo_in_0}, i64 0, metadata !2261), !dbg !2259
  call void @llvm.dbg.value(metadata !{i32* %fifo_out_0}, i64 0, metadata !2262), !dbg !2259
  call void @llvm.dbg.value(metadata !{i1* %RegisterWriteEnablePort_0}, i64 0, metadata !2263), !dbg !2259
  call void @llvm.dbg.value(metadata !{i32* %RegisterWriteDataPort_0}, i64 0, metadata !2264), !dbg !2259
  call void @llvm.dbg.value(metadata !{i32* %axi_master_0}, i64 0, metadata !2265), !dbg !2259
  call void @llvm.dbg.value(metadata !{i1* %clock}, i64 0, metadata !2266), !dbg !2271
  call void @llvm.dbg.value(metadata !{i1* %reset}, i64 0, metadata !2272), !dbg !2271
  call void @llvm.dbg.value(metadata !{i32* %fifo_in_0}, i64 0, metadata !2273), !dbg !2271
  call void @llvm.dbg.value(metadata !{i32* %fifo_out_0}, i64 0, metadata !2274), !dbg !2271
  call void @llvm.dbg.value(metadata !{i1* %RegisterWriteEnablePort_0}, i64 0, metadata !2275), !dbg !2271
  call void @llvm.dbg.value(metadata !{i32* %RegisterWriteDataPort_0}, i64 0, metadata !2276), !dbg !2271
  call void @llvm.dbg.value(metadata !{i32* %axi_master_0}, i64 0, metadata !2277), !dbg !2271
  call void @llvm.dbg.value(metadata !{i1* %clock}, i64 0, metadata !2278), !dbg !2285
  call void @llvm.dbg.value(metadata !{i1* %reset}, i64 0, metadata !2286), !dbg !2285
  call void @llvm.dbg.value(metadata !{i32* %fifo_in_0}, i64 0, metadata !2287), !dbg !2285
  call void @llvm.dbg.value(metadata !{i32* %fifo_out_0}, i64 0, metadata !2288), !dbg !2285
  call void @llvm.dbg.value(metadata !{i1* %RegisterWriteEnablePort_0}, i64 0, metadata !2289), !dbg !2285
  call void @llvm.dbg.value(metadata !{i32* %RegisterWriteDataPort_0}, i64 0, metadata !2290), !dbg !2285
  call void @llvm.dbg.value(metadata !{i32* %axi_master_0}, i64 0, metadata !2291), !dbg !2285
  %tmp_4 = call i32 (...)* @_ssdm_op_SpecRegionBegin([12 x i8]* @p_str18), !dbg !2292
  call void (...)* @_ssdm_op_SpecProtocol(i32 1, [1 x i8]* @p_str6) nounwind, !dbg !2295
  %empty_8 = call i32 (...)* @_ssdm_op_SpecRegionEnd([12 x i8]* @p_str18, i32 %tmp_4), !dbg !2296
  call void @llvm.dbg.value(metadata !{i32* %fifo_in_0}, i64 0, metadata !2297), !dbg !2307
  %empty_9 = call i1 @_ssdm_op_NbReadReq.ap_fifo.i32P(i32* %fifo_in_0, i32 1), !dbg !2308
  call void (...)* @_ssdm_op_SpecLoopName([9 x i8]* @p_str19) nounwind, !dbg !2310
  call void @llvm.dbg.value(metadata !{i32* %fifo_in_0}, i64 0, metadata !2314), !dbg !2327
  %val_V_0 = call i32 @_ssdm_op_Read.ap_fifo.volatile.i32P(i32* %fifo_in_0), !dbg !2328
  call void @llvm.dbg.value(metadata !{i32 %val_V_0}, i64 0, metadata !2330), !dbg !2328
  %tmp_5 = call i32 (...)* @_ssdm_op_SpecRegionBegin([12 x i8]* @p_str20), !dbg !2332
  call void (...)* @_ssdm_op_SpecProtocol(i32 1, [1 x i8]* @p_str6) nounwind, !dbg !2334
  %empty_10 = call i32 (...)* @_ssdm_op_SpecRegionEnd([12 x i8]* @p_str20, i32 %tmp_5), !dbg !2335
  %value = add i32 %tmp_V, 1, !dbg !2336
  call void @llvm.dbg.value(metadata !{i32 %value}, i64 0, metadata !2337), !dbg !2336
  call void @llvm.dbg.value(metadata !{i32 %value}, i64 0, metadata !2338), !dbg !2339
  br label %"DeviceWriteBusBitwidthIsMultipleOfDataBitwidth_single<false, int, unsigned int, AXI4M_bus_port<int> >.exit", !dbg !2340
}

define void @"producer0::producer0"(i1* %clock, i1* %reset, i32* %fifo_in_0, i32* %fifo_out_0, i1* %RegisterWriteEnablePort_0, i32* %RegisterWriteDataPort_0, i32* %axi_master_0) noinline {
  call void (...)* @_ssdm_op_SpecBitsMap(i1* %clock), !map !132
  call void (...)* @_ssdm_op_SpecBitsMap(i1* %reset), !map !136
  call void (...)* @_ssdm_op_SpecBitsMap(i32* %fifo_in_0), !map !140
  call void (...)* @_ssdm_op_SpecBitsMap(i32* %fifo_out_0), !map !144
  call void (...)* @_ssdm_op_SpecBitsMap(i1* %RegisterWriteEnablePort_0), !map !148
  call void (...)* @_ssdm_op_SpecBitsMap(i32* %RegisterWriteDataPort_0), !map !152
  call void (...)* @_ssdm_op_SpecBitsMap(i32* %axi_master_0), !map !156
  call void @llvm.dbg.value(metadata !{i1* %clock}, i64 0, metadata !2341), !dbg !2344
  call void @llvm.dbg.value(metadata !{i1* %reset}, i64 0, metadata !2345), !dbg !2344
  call void @llvm.dbg.value(metadata !{i32* %fifo_in_0}, i64 0, metadata !2346), !dbg !2344
  call void @llvm.dbg.value(metadata !{i32* %fifo_out_0}, i64 0, metadata !2347), !dbg !2344
  call void @llvm.dbg.value(metadata !{i1* %RegisterWriteEnablePort_0}, i64 0, metadata !2348), !dbg !2344
  call void @llvm.dbg.value(metadata !{i32* %RegisterWriteDataPort_0}, i64 0, metadata !2349), !dbg !2344
  call void @llvm.dbg.value(metadata !{i32* %axi_master_0}, i64 0, metadata !2350), !dbg !2344
  call void @llvm.dbg.value(metadata !{i32* %fifo_in_0}, i64 0, metadata !2351), !dbg !2357
  call void @llvm.dbg.value(metadata !{i32* %fifo_in_0}, i64 0, metadata !2358), !dbg !2362
  call void @llvm.dbg.value(metadata !{i32* %fifo_in_0}, i64 0, metadata !2363), !dbg !2370
  call void @llvm.dbg.value(metadata !{i32* %fifo_in_0}, i64 0, metadata !2371), !dbg !2375
  call void (...)* @_ssdm_op_SpecInterface(i32* %fifo_in_0, [8 x i8]* @p_str5, i32 0, i32 0, [1 x i8]* @p_str6, i32 0, i32 0, [1 x i8]* @p_str6, [1 x i8]* @p_str6, [1 x i8]* @p_str6, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str6, [1 x i8]* @p_str6) nounwind, !dbg !2376
  call void (...)* @_ssdm_op_SpecPort([1 x i8]* @p_str6, i32 4, [1 x i8]* @p_str6, [1 x i8]* @p_str6, i32 0, i32 0, i32* %fifo_in_0) nounwind, !dbg !2378
  call void @llvm.dbg.value(metadata !{i32* %fifo_out_0}, i64 0, metadata !2379), !dbg !2384
  call void @llvm.dbg.value(metadata !{i32* %fifo_out_0}, i64 0, metadata !2385), !dbg !2389
  call void @llvm.dbg.value(metadata !{i32* %fifo_out_0}, i64 0, metadata !2390), !dbg !2397
  call void @llvm.dbg.value(metadata !{i32* %fifo_out_0}, i64 0, metadata !2398), !dbg !2402
  call void (...)* @_ssdm_op_SpecInterface(i32* %fifo_out_0, [8 x i8]* @p_str5, i32 0, i32 0, [1 x i8]* @p_str6, i32 0, i32 0, [1 x i8]* @p_str6, [1 x i8]* @p_str6, [1 x i8]* @p_str6, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str6, [1 x i8]* @p_str6) nounwind, !dbg !2403
  call void (...)* @_ssdm_op_SpecPort([1 x i8]* @p_str6, i32 5, [1 x i8]* @p_str6, [1 x i8]* @p_str6, i32 0, i32 0, i32* %fifo_out_0) nounwind, !dbg !2405
  call void @llvm.dbg.value(metadata !{i32* %axi_master_0}, i64 0, metadata !2406), !dbg !2410
  call void @llvm.dbg.value(metadata !{i32* %axi_master_0}, i64 0, metadata !2411), !dbg !2414
  call void (...)* @_ssdm_op_SpecInterface(i32* %axi_master_0, [7 x i8]* @p_str30, i32 0, i32 0, [1 x i8]* @p_str6, i32 0, i32 0, [1 x i8]* @p_str6, [1 x i8]* @p_str6, [1 x i8]* @p_str6, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str6, [1 x i8]* @p_str6) nounwind, !dbg !2416
  call void (...)* @_ssdm_op_SpecPort([1 x i8]* @p_str6, i32 7, [1 x i8]* @p_str6, [1 x i8]* @p_str6, i32 0, i32 0, i32* %axi_master_0) nounwind, !dbg !2424
  call void (...)* @_ssdm_op_SpecIFCore(i32* %axi_master_0, [1 x i8]* @p_str6, [6 x i8]* @p_str29, [1 x i8]* @p_str6, i32 -1, [1 x i8]* @p_str6, [1 x i8]* @p_str6, [1 x i8]* @p_str6, [1 x i8]* @p_str6, [1 x i8]* @p_str6), !dbg !2425
  call void (...)* @_ssdm_op_SpecTopModule([12 x i8]* @p_str, [12 x i8]* @p_str) nounwind, !dbg !2427
  %producer0_ssdm_s = load i1* @producer0_ssdm_thread_M_thread, align 1, !dbg !2429
  br i1 %producer0_ssdm_s, label %1, label %2, !dbg !2429

; <label>:1                                       ; preds = %0
  call void @"producer0::thread"(i1* %clock, i1* %reset, i32* %fifo_in_0, i32* %fifo_out_0, i1* %RegisterWriteEnablePort_0, i32* %RegisterWriteDataPort_0, i32* %axi_master_0), !dbg !2429
  unreachable

; <label>:2                                       ; preds = %0
  call void (...)* @_ssdm_op_SpecProcessDecl([12 x i8]* @p_str, i32 2, [7 x i8]* @p_str1) nounwind, !dbg !2429
  call void (...)* @_ssdm_op_SpecSensitive([7 x i8]* @p_str1, [6 x i8]* @p_str2, i1* %clock, i32 1) nounwind, !dbg !2430
  call void (...)* @_ssdm_op_SpecSensitive([7 x i8]* @p_str1, [6 x i8]* @p_str3, i1* %reset, i32 3) nounwind, !dbg !2431
  call void (...)* @_ssdm_op_SpecPort([12 x i8]* @p_str, i32 0, [7 x i8]* @p_str4, [6 x i8]* @p_str2, i32 0, i32 0, i1* %clock) nounwind, !dbg !2432
  call void (...)* @_ssdm_op_SpecPort([12 x i8]* @p_str, i32 0, [7 x i8]* @p_str4, [6 x i8]* @p_str3, i32 0, i32 0, i1* %reset) nounwind, !dbg !2433
  call void (...)* @_ssdm_op_SpecInterface(i32* %fifo_in_0, [8 x i8]* @p_str5, i32 0, i32 0, [1 x i8]* @p_str6, i32 0, i32 0, [1 x i8]* @p_str6, [1 x i8]* @p_str6, [1 x i8]* @p_str6, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str6, [1 x i8]* @p_str6) nounwind, !dbg !2434
  call void (...)* @_ssdm_op_SpecPort([12 x i8]* @p_str, i32 4, [14 x i8]* @p_str7, [10 x i8]* @p_str8, i32 0, i32 0, i32* %fifo_in_0) nounwind, !dbg !2435
  call void (...)* @_ssdm_op_SpecInterface(i32* %fifo_out_0, [8 x i8]* @p_str5, i32 0, i32 0, [1 x i8]* @p_str6, i32 0, i32 0, [1 x i8]* @p_str6, [1 x i8]* @p_str6, [1 x i8]* @p_str6, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str6, [1 x i8]* @p_str6) nounwind, !dbg !2436
  call void (...)* @_ssdm_op_SpecPort([12 x i8]* @p_str, i32 5, [14 x i8]* @p_str7, [11 x i8]* @p_str9, i32 0, i32 0, i32* %fifo_out_0) nounwind, !dbg !2437
  call void (...)* @_ssdm_op_SpecPort([12 x i8]* @p_str, i32 1, [18 x i8]* @p_str10, [26 x i8]* @p_str11, i32 0, i32 0, i1* %RegisterWriteEnablePort_0) nounwind, !dbg !2438
  call void (...)* @_ssdm_op_SpecPort([12 x i8]* @p_str, i32 1, [19 x i8]* @p_str12, [24 x i8]* @p_str13, i32 0, i32 0, i32* %RegisterWriteDataPort_0) nounwind, !dbg !2439
  call void (...)* @_ssdm_op_SpecPort([12 x i8]* @p_str, i32 7, [6 x i8]* @p_str14, [13 x i8]* @p_str15, i32 0, i32 0, i32* %axi_master_0) nounwind, !dbg !2440
  ret void, !dbg !2441
}

!opencl.kernels = !{!0, !0, !7, !13, !19, !25, !13, !27, !27, !30, !13, !33, !35, !37, !27, !27, !30, !13, !40, !42, !43, !45, !47, !49, !51, !51, !13, !54, !54, !13, !13, !55, !13, !58, !13, !13, !61, !13, !13, !13, !13, !55, !63, !69, !73, !77, !77, !78, !79, !82, !88, !88, !91, !91, !95, !13, !51, !78, !97, !100, !100, !13, !13, !13, !104, !104, !13, !13, !13, !13, !13, !13, !13, !13, !13, !13, !13, !13, !13, !13, !13, !13, !13, !13, !13, !13, !13, !13, !13, !13, !104, !104, !13, !13, !13, !104, !104, !108, !108, !13, !13, !110, !113, !110, !117, !118, !13, !13, !13, !13, !13}
!hls.encrypted.func = !{}
!llvm.map.gv = !{!120, !127}

!0 = metadata !{null, metadata !1, metadata !2, metadata !3, metadata !4, metadata !5, metadata !6}
!1 = metadata !{metadata !"kernel_arg_addr_space", i32 0, i32 0}
!2 = metadata !{metadata !"kernel_arg_access_qual", metadata !"none", metadata !"none"}
!3 = metadata !{metadata !"kernel_arg_type", metadata !"uint", metadata !"uint"}
!4 = metadata !{metadata !"kernel_arg_type_qual", metadata !"", metadata !""}
!5 = metadata !{metadata !"kernel_arg_name", metadata !"module_id", metadata !"data_width"}
!6 = metadata !{metadata !"reqd_work_group_size", i32 1, i32 1, i32 1}
!7 = metadata !{null, metadata !8, metadata !9, metadata !10, metadata !11, metadata !12, metadata !6}
!8 = metadata !{metadata !"kernel_arg_addr_space", i32 0}
!9 = metadata !{metadata !"kernel_arg_access_qual", metadata !"none"}
!10 = metadata !{metadata !"kernel_arg_type", metadata !"sc_core::sc_module_name"}
!11 = metadata !{metadata !"kernel_arg_type_qual", metadata !""}
!12 = metadata !{metadata !"kernel_arg_name", metadata !"name"}
!13 = metadata !{null, metadata !14, metadata !15, metadata !16, metadata !17, metadata !18, metadata !6}
!14 = metadata !{metadata !"kernel_arg_addr_space"}
!15 = metadata !{metadata !"kernel_arg_access_qual"}
!16 = metadata !{metadata !"kernel_arg_type"}
!17 = metadata !{metadata !"kernel_arg_type_qual"}
!18 = metadata !{metadata !"kernel_arg_name"}
!19 = metadata !{null, metadata !20, metadata !21, metadata !22, metadata !23, metadata !24, metadata !6}
!20 = metadata !{metadata !"kernel_arg_addr_space", i32 1, i32 0, i32 0, i32 1, i32 0}
!21 = metadata !{metadata !"kernel_arg_access_qual", metadata !"none", metadata !"none", metadata !"none", metadata !"none", metadata !"none"}
!22 = metadata !{metadata !"kernel_arg_type", metadata !"struct producer0*", metadata !"uint", metadata !"ulong", metadata !"uint*", metadata !"ulong"}
!23 = metadata !{metadata !"kernel_arg_type_qual", metadata !"", metadata !"", metadata !"", metadata !"", metadata !""}
!24 = metadata !{metadata !"kernel_arg_name", metadata !"module", metadata !"data_size", metadata !"timeout", metadata !"data", metadata !"nb_elements"}
!25 = metadata !{null, metadata !20, metadata !21, metadata !22, metadata !26, metadata !24, metadata !6}
!26 = metadata !{metadata !"kernel_arg_type_qual", metadata !"", metadata !"", metadata !"", metadata !"const", metadata !""}
!27 = metadata !{null, metadata !8, metadata !9, metadata !28, metadata !11, metadata !29, metadata !6}
!28 = metadata !{metadata !"kernel_arg_type", metadata !"int"}
!29 = metadata !{metadata !"kernel_arg_name", metadata !"v"}
!30 = metadata !{null, metadata !8, metadata !9, metadata !28, metadata !31, metadata !32, metadata !6}
!31 = metadata !{metadata !"kernel_arg_type_qual", metadata !"const"}
!32 = metadata !{metadata !"kernel_arg_name", metadata !"op"}
!33 = metadata !{null, metadata !8, metadata !9, metadata !34, metadata !11, metadata !29, metadata !6}
!34 = metadata !{metadata !"kernel_arg_type", metadata !"const struct _ap_sc_::sc_dt::sc_lv<32> &"}
!35 = metadata !{null, metadata !8, metadata !9, metadata !34, metadata !11, metadata !36, metadata !6}
!36 = metadata !{metadata !"kernel_arg_name", metadata !"v2"}
!37 = metadata !{null, metadata !1, metadata !2, metadata !38, metadata !4, metadata !39, metadata !6}
!38 = metadata !{metadata !"kernel_arg_type", metadata !"volatile struct _ap_sc_::sc_dt::sc_lv<32> &", metadata !"const struct _ap_sc_::sc_dt::sc_lv<32> &"}
!39 = metadata !{metadata !"kernel_arg_name", metadata !"P", metadata !"val"}
!40 = metadata !{null, metadata !8, metadata !9, metadata !41, metadata !11, metadata !29, metadata !6}
!41 = metadata !{metadata !"kernel_arg_type", metadata !"const struct _ap_sc_::sc_dt::sc_lv<1> &"}
!42 = metadata !{null, metadata !8, metadata !9, metadata !41, metadata !11, metadata !36, metadata !6}
!43 = metadata !{null, metadata !1, metadata !2, metadata !44, metadata !4, metadata !39, metadata !6}
!44 = metadata !{metadata !"kernel_arg_type", metadata !"volatile struct _ap_sc_::sc_dt::sc_lv<1> &", metadata !"const struct _ap_sc_::sc_dt::sc_lv<1> &"}
!45 = metadata !{null, metadata !8, metadata !9, metadata !46, metadata !11, metadata !29, metadata !6}
!46 = metadata !{metadata !"kernel_arg_type", metadata !"const struct ap_uint<32> &"}
!47 = metadata !{null, metadata !8, metadata !9, metadata !46, metadata !11, metadata !48, metadata !6}
!48 = metadata !{metadata !"kernel_arg_name", metadata !"t"}
!49 = metadata !{null, metadata !1, metadata !2, metadata !50, metadata !4, metadata !39, metadata !6}
!50 = metadata !{metadata !"kernel_arg_type", metadata !"volatile struct ap_uint<32> &", metadata !"const struct ap_uint<32> &"}
!51 = metadata !{null, metadata !8, metadata !9, metadata !52, metadata !11, metadata !53, metadata !6}
!52 = metadata !{metadata !"kernel_arg_type", metadata !"uint"}
!53 = metadata !{metadata !"kernel_arg_name", metadata !"val"}
!54 = metadata !{null, metadata !8, metadata !9, metadata !28, metadata !11, metadata !53, metadata !6}
!55 = metadata !{null, metadata !8, metadata !9, metadata !56, metadata !11, metadata !57, metadata !6}
!56 = metadata !{metadata !"kernel_arg_type", metadata !"volatile const struct ap_uint<32> &"}
!57 = metadata !{metadata !"kernel_arg_name", metadata !"P"}
!58 = metadata !{null, metadata !8, metadata !9, metadata !59, metadata !11, metadata !60, metadata !6}
!59 = metadata !{metadata !"kernel_arg_type", metadata !"const ap_uint<32> &"}
!60 = metadata !{metadata !"kernel_arg_name", metadata !"op2"}
!61 = metadata !{null, metadata !8, metadata !9, metadata !62, metadata !11, metadata !57, metadata !6}
!62 = metadata !{metadata !"kernel_arg_type", metadata !"volatile struct ap_uint<32> &"}
!63 = metadata !{null, metadata !64, metadata !65, metadata !66, metadata !67, metadata !68, metadata !6}
!64 = metadata !{metadata !"kernel_arg_addr_space", i32 0, i32 0, i32 0}
!65 = metadata !{metadata !"kernel_arg_access_qual", metadata !"none", metadata !"none", metadata !"none"}
!66 = metadata !{metadata !"kernel_arg_type", metadata !"uint", metadata !"uint32_t", metadata !"const uint &"}
!67 = metadata !{metadata !"kernel_arg_type_qual", metadata !"", metadata !"", metadata !""}
!68 = metadata !{metadata !"kernel_arg_name", metadata !"register_file_id", metadata !"register_id", metadata !"data"}
!69 = metadata !{null, metadata !70, metadata !65, metadata !71, metadata !72, metadata !68, metadata !6}
!70 = metadata !{metadata !"kernel_arg_addr_space", i32 0, i32 0, i32 1}
!71 = metadata !{metadata !"kernel_arg_type", metadata !"uint", metadata !"ulong", metadata !"uint*"}
!72 = metadata !{metadata !"kernel_arg_type_qual", metadata !"", metadata !"", metadata !"const"}
!73 = metadata !{null, metadata !74, metadata !65, metadata !75, metadata !72, metadata !76, metadata !6}
!74 = metadata !{metadata !"kernel_arg_addr_space", i32 1, i32 0, i32 1}
!75 = metadata !{metadata !"kernel_arg_type", metadata !"struct producer0*", metadata !"ulong", metadata !"uint*"}
!76 = metadata !{metadata !"kernel_arg_name", metadata !"module", metadata !"register_id", metadata !"data"}
!77 = metadata !{null, metadata !8, metadata !9, metadata !52, metadata !11, metadata !29, metadata !6}
!78 = metadata !{null, metadata !8, metadata !9, metadata !52, metadata !31, metadata !32, metadata !6}
!79 = metadata !{null, metadata !64, metadata !65, metadata !80, metadata !67, metadata !81, metadata !6}
!80 = metadata !{metadata !"kernel_arg_type", metadata !"uint", metadata !"uintptr_t", metadata !"const uint &"}
!81 = metadata !{metadata !"kernel_arg_name", metadata !"destination_id", metadata !"offset", metadata !"data"}
!82 = metadata !{null, metadata !83, metadata !84, metadata !85, metadata !86, metadata !87, metadata !6}
!83 = metadata !{metadata !"kernel_arg_addr_space", i32 0, i32 0, i32 1, i32 0}
!84 = metadata !{metadata !"kernel_arg_access_qual", metadata !"none", metadata !"none", metadata !"none", metadata !"none"}
!85 = metadata !{metadata !"kernel_arg_type", metadata !"uint", metadata !"uintptr_t", metadata !"uint*", metadata !"ulong"}
!86 = metadata !{metadata !"kernel_arg_type_qual", metadata !"", metadata !"", metadata !"const", metadata !""}
!87 = metadata !{metadata !"kernel_arg_name", metadata !"destination_id", metadata !"offset", metadata !"data", metadata !"nb_elements"}
!88 = metadata !{null, metadata !83, metadata !84, metadata !89, metadata !86, metadata !90, metadata !6}
!89 = metadata !{metadata !"kernel_arg_type", metadata !"class AXI4M_bus_port<int> &", metadata !"uintptr_t", metadata !"uint*", metadata !"ulong"}
!90 = metadata !{metadata !"kernel_arg_name", metadata !"bus", metadata !"startAddress", metadata !"data", metadata !"nb_elements"}
!91 = metadata !{null, metadata !92, metadata !2, metadata !93, metadata !4, metadata !94, metadata !6}
!92 = metadata !{metadata !"kernel_arg_addr_space", i32 0, i32 1}
!93 = metadata !{metadata !"kernel_arg_type", metadata !"uint", metadata !"int*"}
!94 = metadata !{metadata !"kernel_arg_name", metadata !"addr", metadata !"data"}
!95 = metadata !{null, metadata !1, metadata !2, metadata !96, metadata !4, metadata !39, metadata !6}
!96 = metadata !{metadata !"kernel_arg_type", metadata !"volatile int &", metadata !"const int &"}
!97 = metadata !{null, metadata !1, metadata !2, metadata !98, metadata !4, metadata !99, metadata !6}
!98 = metadata !{metadata !"kernel_arg_type", metadata !"int", metadata !"int"}
!99 = metadata !{metadata !"kernel_arg_name", metadata !"Hi", metadata !"Lo"}
!100 = metadata !{null, metadata !101, metadata !65, metadata !102, metadata !67, metadata !103, metadata !6}
!101 = metadata !{metadata !"kernel_arg_addr_space", i32 1, i32 0, i32 0}
!102 = metadata !{metadata !"kernel_arg_type", metadata !"ref_type*", metadata !"int", metadata !"int"}
!103 = metadata !{metadata !"kernel_arg_name", metadata !"bv", metadata !"h", metadata !"l"}
!104 = metadata !{null, metadata !105, metadata !9, metadata !106, metadata !31, metadata !107, metadata !6}
!105 = metadata !{metadata !"kernel_arg_addr_space", i32 1}
!106 = metadata !{metadata !"kernel_arg_type", metadata !"char*"}
!107 = metadata !{metadata !"kernel_arg_name", metadata !"name_"}
!108 = metadata !{null, metadata !105, metadata !9, metadata !106, metadata !31, metadata !109, metadata !6}
!109 = metadata !{metadata !"kernel_arg_name", metadata !""}
!110 = metadata !{null, metadata !64, metadata !65, metadata !111, metadata !67, metadata !112, metadata !6}
!111 = metadata !{metadata !"kernel_arg_type", metadata !"uint", metadata !"uint", metadata !"uint32_t"}
!112 = metadata !{metadata !"kernel_arg_name", metadata !"fifo_id", metadata !"data_width", metadata !"timeout"}
!113 = metadata !{null, metadata !114, metadata !21, metadata !115, metadata !23, metadata !116, metadata !6}
!114 = metadata !{metadata !"kernel_arg_addr_space", i32 0, i32 0, i32 0, i32 1, i32 0}
!115 = metadata !{metadata !"kernel_arg_type", metadata !"uint", metadata !"uint", metadata !"ulong", metadata !"uint*", metadata !"ulong"}
!116 = metadata !{metadata !"kernel_arg_name", metadata !"fifo_id", metadata !"data_width", metadata !"timeout", metadata !"data", metadata !"nb_elements"}
!117 = metadata !{null, metadata !114, metadata !21, metadata !115, metadata !26, metadata !116, metadata !6}
!118 = metadata !{null, metadata !8, metadata !9, metadata !28, metadata !11, metadata !119, metadata !6}
!119 = metadata !{metadata !"kernel_arg_name", metadata !"n"}
!120 = metadata !{metadata !121, null}
!121 = metadata !{metadata !122}
!122 = metadata !{i32 0, i32 31, metadata !123}
!123 = metadata !{metadata !124}
!124 = metadata !{metadata !"llvm.global_ctors.0", metadata !125, metadata !"", i32 0, i32 31}
!125 = metadata !{metadata !126}
!126 = metadata !{i32 0, i32 0, i32 1}
!127 = metadata !{metadata !128, i1* @producer0_ssdm_thread_M_thread}
!128 = metadata !{metadata !129}
!129 = metadata !{i32 0, i32 0, metadata !130}
!130 = metadata !{metadata !131}
!131 = metadata !{metadata !"producer0::__ssdm_thread_M_thread", metadata !125, metadata !"bool", i32 0, i32 0}
!132 = metadata !{metadata !133}
!133 = metadata !{i32 0, i32 0, metadata !134}
!134 = metadata !{metadata !135}
!135 = metadata !{metadata !"producer0.clock.m_if.Val", metadata !125, metadata !"bool", i32 0, i32 0}
!136 = metadata !{metadata !137}
!137 = metadata !{i32 0, i32 0, metadata !138}
!138 = metadata !{metadata !139}
!139 = metadata !{metadata !"producer0.reset.m_if.Val", metadata !125, metadata !"bool", i32 0, i32 0}
!140 = metadata !{metadata !141}
!141 = metadata !{i32 0, i32 31, metadata !142}
!142 = metadata !{metadata !143}
!143 = metadata !{metadata !"producer0.fifo_in_0.m_if.Val.V", metadata !125, metadata !"uint32", i32 0, i32 31}
!144 = metadata !{metadata !145}
!145 = metadata !{i32 0, i32 31, metadata !146}
!146 = metadata !{metadata !147}
!147 = metadata !{metadata !"producer0.fifo_out_0.m_if.Val.V", metadata !125, metadata !"uint32", i32 0, i32 31}
!148 = metadata !{metadata !149}
!149 = metadata !{i32 0, i32 0, metadata !150}
!150 = metadata !{metadata !151}
!151 = metadata !{metadata !"producer0.RegisterWriteEnablePort_0.m_if.Val.V", metadata !125, metadata !"uint1", i32 0, i32 0}
!152 = metadata !{metadata !153}
!153 = metadata !{i32 0, i32 31, metadata !154}
!154 = metadata !{metadata !155}
!155 = metadata !{metadata !"producer0.RegisterWriteDataPort_0.m_if.Val.V", metadata !125, metadata !"uint32", i32 0, i32 31}
!156 = metadata !{metadata !157}
!157 = metadata !{i32 0, i32 31, metadata !158}
!158 = metadata !{metadata !159}
!159 = metadata !{metadata !"producer0.axi_master_0.m_if.Val", metadata !125, metadata !"int", i32 0, i32 31}
!160 = metadata !{i32 790531, metadata !161, metadata !"producer0.clock.m_if.Val", null, i32 78, metadata !1839, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!161 = metadata !{i32 786689, metadata !162, metadata !"this", metadata !163, i32 16777294, metadata !1838, i32 64, i32 0} ; [ DW_TAG_arg_variable ]
!162 = metadata !{i32 786478, i32 0, null, metadata !"thread", metadata !"thread", metadata !"_ZN11producer06threadEv", metadata !163, i32 78, metadata !164, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, metadata !1837, metadata !186, i32 79} ; [ DW_TAG_subprogram ]
!163 = metadata !{i32 786473, metadata !"producer0.cpp", metadata !"/home/julien/education/inf8500/lab2/tutorial_ju/solutions/tutorial/architectures/hw_sw/build/estimation/hls/producer0", null} ; [ DW_TAG_file_type ]
!164 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !165, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!165 = metadata !{null, metadata !166}
!166 = metadata !{i32 786447, i32 0, metadata !"", i32 0, i32 0, i64 64, i64 64, i64 0, i32 64, metadata !167} ; [ DW_TAG_pointer_type ]
!167 = metadata !{i32 786434, null, metadata !"producer0", metadata !168, i32 46, i64 192, i64 32, i32 0, i32 0, null, metadata !169, i32 0, null, null} ; [ DW_TAG_class_type ]
!168 = metadata !{i32 786473, metadata !"./producer0.h", metadata !"/home/julien/education/inf8500/lab2/tutorial_ju/solutions/tutorial/architectures/hw_sw/build/estimation/hls/producer0", null} ; [ DW_TAG_file_type ]
!169 = metadata !{metadata !170, metadata !289, metadata !290, metadata !884, metadata !954, metadata !1504, metadata !1701, metadata !1793, metadata !1807, metadata !1810, metadata !1811, metadata !1814, metadata !1815, metadata !1816, metadata !1821, metadata !1826, metadata !1831, metadata !1835, metadata !1836, metadata !1837}
!170 = metadata !{i32 786445, metadata !167, metadata !"clock", metadata !168, i32 49, i64 8, i64 8, i64 0, i32 0, metadata !171} ; [ DW_TAG_member ]
!171 = metadata !{i32 786434, metadata !172, metadata !"sc_in<bool>", metadata !174, i32 357, i64 8, i64 8, i32 0, i32 0, null, metadata !175, i32 0, null, metadata !220} ; [ DW_TAG_class_type ]
!172 = metadata !{i32 786489, metadata !173, metadata !"sc_core", metadata !174, i32 163} ; [ DW_TAG_namespace ]
!173 = metadata !{i32 786489, null, metadata !"_ap_sc_", metadata !174, i32 160} ; [ DW_TAG_namespace ]
!174 = metadata !{i32 786473, metadata !"/tools/Xilinx/Vivado/2018.3/common/technology/autopilot/ap_sysc/ap_sc_core.h", metadata !"/home/julien/education/inf8500/lab2/tutorial_ju/solutions/tutorial/architectures/hw_sw/build/estimation/hls/producer0", null} ; [ DW_TAG_file_type ]
!175 = metadata !{metadata !176, metadata !255, metadata !263, metadata !264, metadata !268, metadata !271, metadata !274, metadata !277}
!176 = metadata !{i32 786460, metadata !171, null, metadata !174, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !177} ; [ DW_TAG_inheritance ]
!177 = metadata !{i32 786434, metadata !172, metadata !"sc_port_b<_ap_sc_::sc_core::sc_signal_in_if<bool> >", metadata !174, i32 268, i64 8, i64 8, i32 0, i32 0, null, metadata !178, i32 0, null, metadata !253} ; [ DW_TAG_class_type ]
!178 = metadata !{metadata !179, metadata !188, metadata !222, metadata !226, metadata !232, metadata !236, metadata !237, metadata !243, metadata !244, metadata !248, metadata !249}
!179 = metadata !{i32 786460, metadata !177, null, metadata !174, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !180} ; [ DW_TAG_inheritance ]
!180 = metadata !{i32 786434, metadata !172, metadata !"sc_port_base", metadata !174, i32 265, i64 8, i64 8, i32 0, i32 0, null, metadata !181, i32 0, null, null} ; [ DW_TAG_class_type ]
!181 = metadata !{metadata !182}
!182 = metadata !{i32 786478, i32 0, metadata !180, metadata !"sc_port_base", metadata !"sc_port_base", metadata !"", metadata !174, i32 265, metadata !183, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 265} ; [ DW_TAG_subprogram ]
!183 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !184, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!184 = metadata !{null, metadata !185}
!185 = metadata !{i32 786447, i32 0, metadata !"", i32 0, i32 0, i64 64, i64 64, i64 0, i32 64, metadata !180} ; [ DW_TAG_pointer_type ]
!186 = metadata !{metadata !187}
!187 = metadata !{i32 786468}                     ; [ DW_TAG_base_type ]
!188 = metadata !{i32 786445, metadata !177, metadata !"m_if", metadata !174, i32 270, i64 8, i64 8, i64 0, i32 0, metadata !189} ; [ DW_TAG_member ]
!189 = metadata !{i32 786434, metadata !172, metadata !"sc_signal_in_if<bool>", metadata !174, i32 172, i64 8, i64 8, i32 0, i32 0, null, metadata !190, i32 0, null, metadata !220} ; [ DW_TAG_class_type ]
!190 = metadata !{metadata !191, metadata !198, metadata !201, metadata !205, metadata !208, metadata !213, metadata !217}
!191 = metadata !{i32 786460, metadata !189, null, metadata !174, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !192} ; [ DW_TAG_inheritance ]
!192 = metadata !{i32 786434, metadata !172, metadata !"sc_interface", metadata !174, i32 165, i64 8, i64 8, i32 0, i32 0, null, metadata !193, i32 0, null, null} ; [ DW_TAG_class_type ]
!193 = metadata !{metadata !194}
!194 = metadata !{i32 786478, i32 0, metadata !192, metadata !"sc_interface", metadata !"sc_interface", metadata !"", metadata !174, i32 165, metadata !195, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 165} ; [ DW_TAG_subprogram ]
!195 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !196, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!196 = metadata !{null, metadata !197}
!197 = metadata !{i32 786447, i32 0, metadata !"", i32 0, i32 0, i64 64, i64 64, i64 0, i32 64, metadata !192} ; [ DW_TAG_pointer_type ]
!198 = metadata !{i32 786445, metadata !189, metadata !"Val", metadata !174, i32 174, i64 8, i64 8, i64 0, i32 0, metadata !199} ; [ DW_TAG_member ]
!199 = metadata !{i32 786485, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !200} ; [ DW_TAG_volatile_type ]
!200 = metadata !{i32 786468, null, metadata !"bool", null, i32 0, i64 8, i64 8, i64 0, i32 0, i32 2} ; [ DW_TAG_base_type ]
!201 = metadata !{i32 786478, i32 0, metadata !189, metadata !"sc_signal_in_if", metadata !"sc_signal_in_if", metadata !"", metadata !174, i32 176, metadata !202, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 176} ; [ DW_TAG_subprogram ]
!202 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !203, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!203 = metadata !{null, metadata !204}
!204 = metadata !{i32 786447, i32 0, metadata !"", i32 0, i32 0, i64 64, i64 64, i64 0, i32 64, metadata !189} ; [ DW_TAG_pointer_type ]
!205 = metadata !{i32 786478, i32 0, metadata !189, metadata !"read", metadata !"read", metadata !"_ZN7_ap_sc_7sc_core15sc_signal_in_ifIbE4readEv", metadata !174, i32 180, metadata !206, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 180} ; [ DW_TAG_subprogram ]
!206 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !207, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!207 = metadata !{metadata !200, metadata !204}
!208 = metadata !{i32 786478, i32 0, metadata !189, metadata !"read", metadata !"read", metadata !"_ZNK7_ap_sc_7sc_core15sc_signal_in_ifIbE4readEv", metadata !174, i32 181, metadata !209, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 181} ; [ DW_TAG_subprogram ]
!209 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !210, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!210 = metadata !{metadata !200, metadata !211}
!211 = metadata !{i32 786447, i32 0, metadata !"", i32 0, i32 0, i64 64, i64 64, i64 0, i32 64, metadata !212} ; [ DW_TAG_pointer_type ]
!212 = metadata !{i32 786470, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !189} ; [ DW_TAG_const_type ]
!213 = metadata !{i32 786478, i32 0, metadata !189, metadata !"operator const _Bool", metadata !"operator const _Bool", metadata !"_ZN7_ap_sc_7sc_core15sc_signal_in_ifIbEcvKbEv", metadata !174, i32 187, metadata !214, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 187} ; [ DW_TAG_subprogram ]
!214 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !215, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!215 = metadata !{metadata !216, metadata !204}
!216 = metadata !{i32 786470, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !200} ; [ DW_TAG_const_type ]
!217 = metadata !{i32 786478, i32 0, metadata !189, metadata !"operator const _Bool", metadata !"operator const _Bool", metadata !"_ZNK7_ap_sc_7sc_core15sc_signal_in_ifIbEcvKbEv", metadata !174, i32 188, metadata !218, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 188} ; [ DW_TAG_subprogram ]
!218 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !219, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!219 = metadata !{metadata !216, metadata !211}
!220 = metadata !{metadata !221}
!221 = metadata !{i32 786479, null, metadata !"T", metadata !200, null, i32 0, i32 0} ; [ DW_TAG_template_type_parameter ]
!222 = metadata !{i32 786478, i32 0, metadata !177, metadata !"sc_port_b", metadata !"sc_port_b", metadata !"", metadata !174, i32 272, metadata !223, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 272} ; [ DW_TAG_subprogram ]
!223 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !224, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!224 = metadata !{null, metadata !225}
!225 = metadata !{i32 786447, i32 0, metadata !"", i32 0, i32 0, i64 64, i64 64, i64 0, i32 64, metadata !177} ; [ DW_TAG_pointer_type ]
!226 = metadata !{i32 786478, i32 0, metadata !177, metadata !"sc_port_b", metadata !"sc_port_b", metadata !"", metadata !174, i32 273, metadata !227, i1 false, i1 false, i32 0, i32 0, null, i32 384, i1 false, null, null, i32 0, metadata !186, i32 273} ; [ DW_TAG_subprogram ]
!227 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !228, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!228 = metadata !{null, metadata !225, metadata !229}
!229 = metadata !{i32 786447, null, metadata !"", null, i32 0, i64 64, i64 64, i64 0, i32 0, metadata !230} ; [ DW_TAG_pointer_type ]
!230 = metadata !{i32 786470, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !231} ; [ DW_TAG_const_type ]
!231 = metadata !{i32 786468, null, metadata !"char", null, i32 0, i64 8, i64 8, i64 0, i32 0, i32 6} ; [ DW_TAG_base_type ]
!232 = metadata !{i32 786478, i32 0, metadata !177, metadata !"bind", metadata !"bind", metadata !"_ZN7_ap_sc_7sc_core9sc_port_bINS0_15sc_signal_in_ifIbEEE4bindERS3_", metadata !174, i32 277, metadata !233, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 277} ; [ DW_TAG_subprogram ]
!233 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !234, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!234 = metadata !{null, metadata !225, metadata !235}
!235 = metadata !{i32 786448, null, null, null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !189} ; [ DW_TAG_reference_type ]
!236 = metadata !{i32 786478, i32 0, metadata !177, metadata !"operator()", metadata !"operator()", metadata !"_ZN7_ap_sc_7sc_core9sc_port_bINS0_15sc_signal_in_ifIbEEEclERS3_", metadata !174, i32 280, metadata !233, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 280} ; [ DW_TAG_subprogram ]
!237 = metadata !{i32 786478, i32 0, metadata !177, metadata !"bind", metadata !"bind", metadata !"_ZN7_ap_sc_7sc_core9sc_port_bINS0_15sc_signal_in_ifIbEEE4bindERNS0_15sc_prim_channelE", metadata !174, i32 281, metadata !238, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 281} ; [ DW_TAG_subprogram ]
!238 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !239, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!239 = metadata !{null, metadata !225, metadata !240}
!240 = metadata !{i32 786448, null, null, null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !241} ; [ DW_TAG_reference_type ]
!241 = metadata !{i32 786434, metadata !172, metadata !"sc_prim_channel", metadata !174, i32 166, i64 8, i64 8, i32 0, i32 0, null, metadata !242, i32 0, null, null} ; [ DW_TAG_class_type ]
!242 = metadata !{i32 0}
!243 = metadata !{i32 786478, i32 0, metadata !177, metadata !"operator()", metadata !"operator()", metadata !"_ZN7_ap_sc_7sc_core9sc_port_bINS0_15sc_signal_in_ifIbEEEclERNS0_15sc_prim_channelE", metadata !174, i32 284, metadata !238, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 284} ; [ DW_TAG_subprogram ]
!244 = metadata !{i32 786478, i32 0, metadata !177, metadata !"bind", metadata !"bind", metadata !"_ZN7_ap_sc_7sc_core9sc_port_bINS0_15sc_signal_in_ifIbEEE4bindERS4_", metadata !174, i32 285, metadata !245, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 285} ; [ DW_TAG_subprogram ]
!245 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !246, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!246 = metadata !{null, metadata !225, metadata !247}
!247 = metadata !{i32 786448, null, null, null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !177} ; [ DW_TAG_reference_type ]
!248 = metadata !{i32 786478, i32 0, metadata !177, metadata !"operator()", metadata !"operator()", metadata !"_ZN7_ap_sc_7sc_core9sc_port_bINS0_15sc_signal_in_ifIbEEEclERS4_", metadata !174, i32 286, metadata !245, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 286} ; [ DW_TAG_subprogram ]
!249 = metadata !{i32 786478, i32 0, metadata !177, metadata !"operator->", metadata !"operator->", metadata !"_ZN7_ap_sc_7sc_core9sc_port_bINS0_15sc_signal_in_ifIbEEEptEv", metadata !174, i32 288, metadata !250, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 288} ; [ DW_TAG_subprogram ]
!250 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !251, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!251 = metadata !{metadata !252, metadata !225}
!252 = metadata !{i32 786447, null, metadata !"", null, i32 0, i64 64, i64 64, i64 0, i32 0, metadata !189} ; [ DW_TAG_pointer_type ]
!253 = metadata !{metadata !254}
!254 = metadata !{i32 786479, null, metadata !"IF", metadata !189, null, i32 0, i32 0} ; [ DW_TAG_template_type_parameter ]
!255 = metadata !{i32 786478, i32 0, metadata !171, metadata !"pos", metadata !"pos", metadata !"_ZNK7_ap_sc_7sc_core5sc_inIbE3posEv", metadata !174, i32 365, metadata !256, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 365} ; [ DW_TAG_subprogram ]
!256 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !257, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!257 = metadata !{metadata !258, metadata !261}
!258 = metadata !{i32 786448, null, null, null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !259} ; [ DW_TAG_reference_type ]
!259 = metadata !{i32 786454, metadata !172, metadata !"sc_event_finder", metadata !174, i32 353, i64 0, i64 0, i64 0, i32 0, metadata !260} ; [ DW_TAG_typedef ]
!260 = metadata !{i32 786447, null, metadata !"", null, i32 0, i64 64, i64 64, i64 0, i32 0, null} ; [ DW_TAG_pointer_type ]
!261 = metadata !{i32 786447, i32 0, metadata !"", i32 0, i32 0, i64 64, i64 64, i64 0, i32 64, metadata !262} ; [ DW_TAG_pointer_type ]
!262 = metadata !{i32 786470, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !171} ; [ DW_TAG_const_type ]
!263 = metadata !{i32 786478, i32 0, metadata !171, metadata !"neg", metadata !"neg", metadata !"_ZNK7_ap_sc_7sc_core5sc_inIbE3negEv", metadata !174, i32 366, metadata !256, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 366} ; [ DW_TAG_subprogram ]
!264 = metadata !{i32 786478, i32 0, metadata !171, metadata !"sc_in", metadata !"sc_in", metadata !"", metadata !174, i32 368, metadata !265, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 368} ; [ DW_TAG_subprogram ]
!265 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !266, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!266 = metadata !{null, metadata !267}
!267 = metadata !{i32 786447, i32 0, metadata !"", i32 0, i32 0, i64 64, i64 64, i64 0, i32 64, metadata !171} ; [ DW_TAG_pointer_type ]
!268 = metadata !{i32 786478, i32 0, metadata !171, metadata !"sc_in", metadata !"sc_in", metadata !"", metadata !174, i32 369, metadata !269, i1 false, i1 false, i32 0, i32 0, null, i32 384, i1 false, null, null, i32 0, metadata !186, i32 369} ; [ DW_TAG_subprogram ]
!269 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !270, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!270 = metadata !{null, metadata !267, metadata !229}
!271 = metadata !{i32 786478, i32 0, metadata !171, metadata !"read", metadata !"read", metadata !"_ZN7_ap_sc_7sc_core5sc_inIbE4readEv", metadata !174, i32 372, metadata !272, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 372} ; [ DW_TAG_subprogram ]
!272 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !273, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!273 = metadata !{metadata !200, metadata !267}
!274 = metadata !{i32 786478, i32 0, metadata !171, metadata !"operator const _Bool", metadata !"operator const _Bool", metadata !"_ZN7_ap_sc_7sc_core5sc_inIbEcvKbEv", metadata !174, i32 373, metadata !275, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 373} ; [ DW_TAG_subprogram ]
!275 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !276, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!276 = metadata !{metadata !216, metadata !267}
!277 = metadata !{i32 786478, i32 0, metadata !171, metadata !"delayed", metadata !"delayed", metadata !"_ZNK7_ap_sc_7sc_core5sc_inIbE7delayedEv", metadata !174, i32 376, metadata !278, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 376} ; [ DW_TAG_subprogram ]
!278 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !279, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!279 = metadata !{metadata !280, metadata !261}
!280 = metadata !{i32 786448, null, null, null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !281} ; [ DW_TAG_reference_type ]
!281 = metadata !{i32 786470, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !282} ; [ DW_TAG_const_type ]
!282 = metadata !{i32 786434, metadata !172, metadata !"sc_signal_bool_deval", metadata !174, i32 255, i64 8, i64 8, i32 0, i32 0, null, metadata !283, i32 0, null, null} ; [ DW_TAG_class_type ]
!283 = metadata !{metadata !284}
!284 = metadata !{i32 786478, i32 0, metadata !282, metadata !"operator==", metadata !"operator==", metadata !"_ZNK7_ap_sc_7sc_core20sc_signal_bool_devaleqEb", metadata !174, i32 257, metadata !285, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 257} ; [ DW_TAG_subprogram ]
!285 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !286, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!286 = metadata !{metadata !287, metadata !288, metadata !200}
!287 = metadata !{i32 786448, null, null, null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !282} ; [ DW_TAG_reference_type ]
!288 = metadata !{i32 786447, i32 0, metadata !"", i32 0, i32 0, i64 64, i64 64, i64 0, i32 64, metadata !281} ; [ DW_TAG_pointer_type ]
!289 = metadata !{i32 786445, metadata !167, metadata !"reset", metadata !168, i32 50, i64 8, i64 8, i64 8, i32 0, metadata !171} ; [ DW_TAG_member ]
!290 = metadata !{i32 786445, metadata !167, metadata !"fifo_in_0", metadata !168, i32 51, i64 32, i64 32, i64 32, i32 0, metadata !291} ; [ DW_TAG_member ]
!291 = metadata !{i32 786434, metadata !172, metadata !"sc_fifo_in<ap_uint<32> >", metadata !174, i32 477, i64 32, i64 32, i32 0, i32 0, null, metadata !292, i32 0, null, metadata !836} ; [ DW_TAG_class_type ]
!292 = metadata !{metadata !293, metadata !865, metadata !869, metadata !872, metadata !875, metadata !878, metadata !881}
!293 = metadata !{i32 786460, metadata !291, null, metadata !174, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !294} ; [ DW_TAG_inheritance ]
!294 = metadata !{i32 786434, metadata !172, metadata !"sc_port_b<_ap_sc_::sc_core::sc_fifo_in_if<ap_uint<32> > >", metadata !174, i32 268, i64 32, i64 32, i32 0, i32 0, null, metadata !295, i32 0, null, metadata !863} ; [ DW_TAG_class_type ]
!295 = metadata !{metadata !296, metadata !297, metadata !838, metadata !842, metadata !845, metadata !849, metadata !850, metadata !853, metadata !854, metadata !858, metadata !859}
!296 = metadata !{i32 786460, metadata !294, null, metadata !174, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !180} ; [ DW_TAG_inheritance ]
!297 = metadata !{i32 786445, metadata !294, metadata !"m_if", metadata !174, i32 270, i64 32, i64 32, i64 0, i32 0, metadata !298} ; [ DW_TAG_member ]
!298 = metadata !{i32 786434, metadata !172, metadata !"sc_fifo_in_if<ap_uint<32> >", metadata !174, i32 212, i64 32, i64 32, i32 0, i32 0, null, metadata !299, i32 0, null, metadata !836} ; [ DW_TAG_class_type ]
!299 = metadata !{metadata !300, metadata !301, metadata !818, metadata !822, metadata !825, metadata !828, metadata !831}
!300 = metadata !{i32 786460, metadata !298, null, metadata !174, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !192} ; [ DW_TAG_inheritance ]
!301 = metadata !{i32 786445, metadata !298, metadata !"Val", metadata !174, i32 214, i64 32, i64 32, i64 0, i32 0, metadata !302} ; [ DW_TAG_member ]
!302 = metadata !{i32 786485, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !303} ; [ DW_TAG_volatile_type ]
!303 = metadata !{i32 786434, null, metadata !"ap_uint<32>", metadata !304, i32 194, i64 32, i64 32, i32 0, i32 0, null, metadata !305, i32 0, null, metadata !817} ; [ DW_TAG_class_type ]
!304 = metadata !{i32 786473, metadata !"/tools/Xilinx/Vivado/2018.3/common/technology/autopilot/ap_int.h", metadata !"/home/julien/education/inf8500/lab2/tutorial_ju/solutions/tutorial/architectures/hw_sw/build/estimation/hls/producer0", null} ; [ DW_TAG_file_type ]
!305 = metadata !{metadata !306, metadata !734, metadata !738, metadata !744, metadata !749, metadata !752, metadata !755, metadata !758, metadata !761, metadata !764, metadata !767, metadata !770, metadata !773, metadata !776, metadata !779, metadata !782, metadata !785, metadata !788, metadata !791, metadata !794, metadata !797, metadata !800, metadata !803, metadata !807, metadata !810, metadata !814}
!306 = metadata !{i32 786460, metadata !303, null, metadata !304, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !307} ; [ DW_TAG_inheritance ]
!307 = metadata !{i32 786434, null, metadata !"ap_int_base<32, false>", metadata !308, i32 150, i64 32, i64 32, i32 0, i32 0, null, metadata !309, i32 0, null, metadata !702} ; [ DW_TAG_class_type ]
!308 = metadata !{i32 786473, metadata !"/tools/Xilinx/Vivado/2018.3/common/technology/autopilot/ap_int_base.h", metadata !"/home/julien/education/inf8500/lab2/tutorial_ju/solutions/tutorial/architectures/hw_sw/build/estimation/hls/producer0", null} ; [ DW_TAG_file_type ]
!309 = metadata !{metadata !310, metadata !324, metadata !328, metadata !336, metadata !342, metadata !345, metadata !348, metadata !353, metadata !358, metadata !363, metadata !368, metadata !372, metadata !377, metadata !382, metadata !387, metadata !393, metadata !399, metadata !404, metadata !408, metadata !412, metadata !415, metadata !418, metadata !422, metadata !425, metadata !428, metadata !429, metadata !433, metadata !436, metadata !439, metadata !442, metadata !445, metadata !448, metadata !451, metadata !454, metadata !457, metadata !460, metadata !463, metadata !466, metadata !469, metadata !472, metadata !481, metadata !484, metadata !487, metadata !490, metadata !493, metadata !496, metadata !499, metadata !502, metadata !505, metadata !508, metadata !511, metadata !514, metadata !517, metadata !520, metadata !523, metadata !527, metadata !528, metadata !529, metadata !530, metadata !533, metadata !534, metadata !537, metadata !540, metadata !541, metadata !544, metadata !545, metadata !546, metadata !547, metadata !548, metadata !549, metadata !550, metadata !551, metadata !552, metadata !560, metadata !561, metadata !564, metadata !580, metadata !581, metadata !582, metadata !704, metadata !707, metadata !710, metadata !713, metadata !714, metadata !715, metadata !719, metadata !720, metadata !721, metadata !722, metadata !725, metadata !726, metadata !727, metadata !728, metadata !729, metadata !730, metadata !731}
!310 = metadata !{i32 786460, metadata !307, null, metadata !308, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !311} ; [ DW_TAG_inheritance ]
!311 = metadata !{i32 786434, null, metadata !"ssdm_int<32 + 1024 * 0, false>", metadata !312, i32 66, i64 32, i64 32, i32 0, i32 0, null, metadata !313, i32 0, null, metadata !320} ; [ DW_TAG_class_type ]
!312 = metadata !{i32 786473, metadata !"/tools/Xilinx/Vivado/2018.3/common/technology/autopilot/etc/autopilot_dt.def", metadata !"/home/julien/education/inf8500/lab2/tutorial_ju/solutions/tutorial/architectures/hw_sw/build/estimation/hls/producer0", null} ; [ DW_TAG_file_type ]
!313 = metadata !{metadata !314, metadata !316}
!314 = metadata !{i32 786445, metadata !311, metadata !"V", metadata !312, i32 66, i64 32, i64 32, i64 0, i32 0, metadata !315} ; [ DW_TAG_member ]
!315 = metadata !{i32 786468, null, metadata !"uint32", null, i32 0, i64 32, i64 32, i64 0, i32 0, i32 7} ; [ DW_TAG_base_type ]
!316 = metadata !{i32 786478, i32 0, metadata !311, metadata !"ssdm_int", metadata !"ssdm_int", metadata !"", metadata !312, i32 66, metadata !317, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 66} ; [ DW_TAG_subprogram ]
!317 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !318, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!318 = metadata !{null, metadata !319}
!319 = metadata !{i32 786447, i32 0, metadata !"", i32 0, i32 0, i64 64, i64 64, i64 0, i32 64, metadata !311} ; [ DW_TAG_pointer_type ]
!320 = metadata !{metadata !321, metadata !323}
!321 = metadata !{i32 786480, null, metadata !"_AP_N", metadata !322, i64 32, null, i32 0, i32 0} ; [ DW_TAG_template_value_parameter ]
!322 = metadata !{i32 786468, null, metadata !"int", null, i32 0, i64 32, i64 32, i64 0, i32 0, i32 5} ; [ DW_TAG_base_type ]
!323 = metadata !{i32 786480, null, metadata !"_AP_S", metadata !200, i64 0, null, i32 0, i32 0} ; [ DW_TAG_template_value_parameter ]
!324 = metadata !{i32 786478, i32 0, metadata !307, metadata !"ap_int_base", metadata !"ap_int_base", metadata !"", metadata !308, i32 206, metadata !325, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 206} ; [ DW_TAG_subprogram ]
!325 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !326, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!326 = metadata !{null, metadata !327}
!327 = metadata !{i32 786447, i32 0, metadata !"", i32 0, i32 0, i64 64, i64 64, i64 0, i32 64, metadata !307} ; [ DW_TAG_pointer_type ]
!328 = metadata !{i32 786478, i32 0, metadata !307, metadata !"ap_int_base<32, false>", metadata !"ap_int_base<32, false>", metadata !"", metadata !308, i32 216, metadata !329, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, metadata !333, i32 0, metadata !186, i32 216} ; [ DW_TAG_subprogram ]
!329 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !330, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!330 = metadata !{null, metadata !327, metadata !331}
!331 = metadata !{i32 786448, null, null, null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !332} ; [ DW_TAG_reference_type ]
!332 = metadata !{i32 786470, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !307} ; [ DW_TAG_const_type ]
!333 = metadata !{metadata !334, metadata !335}
!334 = metadata !{i32 786480, null, metadata !"_AP_W2", metadata !322, i64 32, null, i32 0, i32 0} ; [ DW_TAG_template_value_parameter ]
!335 = metadata !{i32 786480, null, metadata !"_AP_S2", metadata !200, i64 0, null, i32 0, i32 0} ; [ DW_TAG_template_value_parameter ]
!336 = metadata !{i32 786478, i32 0, metadata !307, metadata !"ap_int_base<32, false>", metadata !"ap_int_base<32, false>", metadata !"", metadata !308, i32 222, metadata !337, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, metadata !333, i32 0, metadata !186, i32 222} ; [ DW_TAG_subprogram ]
!337 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !338, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!338 = metadata !{null, metadata !327, metadata !339}
!339 = metadata !{i32 786448, null, null, null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !340} ; [ DW_TAG_reference_type ]
!340 = metadata !{i32 786470, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !341} ; [ DW_TAG_const_type ]
!341 = metadata !{i32 786485, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !307} ; [ DW_TAG_volatile_type ]
!342 = metadata !{i32 786478, i32 0, metadata !307, metadata !"ap_int_base", metadata !"ap_int_base", metadata !"", metadata !308, i32 239, metadata !343, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 239} ; [ DW_TAG_subprogram ]
!343 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !344, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!344 = metadata !{null, metadata !327, metadata !216}
!345 = metadata !{i32 786478, i32 0, metadata !307, metadata !"ap_int_base", metadata !"ap_int_base", metadata !"", metadata !308, i32 240, metadata !346, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 240} ; [ DW_TAG_subprogram ]
!346 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !347, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!347 = metadata !{null, metadata !327, metadata !230}
!348 = metadata !{i32 786478, i32 0, metadata !307, metadata !"ap_int_base", metadata !"ap_int_base", metadata !"", metadata !308, i32 241, metadata !349, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 241} ; [ DW_TAG_subprogram ]
!349 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !350, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!350 = metadata !{null, metadata !327, metadata !351}
!351 = metadata !{i32 786470, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !352} ; [ DW_TAG_const_type ]
!352 = metadata !{i32 786468, null, metadata !"signed char", null, i32 0, i64 8, i64 8, i64 0, i32 0, i32 6} ; [ DW_TAG_base_type ]
!353 = metadata !{i32 786478, i32 0, metadata !307, metadata !"ap_int_base", metadata !"ap_int_base", metadata !"", metadata !308, i32 242, metadata !354, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 242} ; [ DW_TAG_subprogram ]
!354 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !355, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!355 = metadata !{null, metadata !327, metadata !356}
!356 = metadata !{i32 786470, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !357} ; [ DW_TAG_const_type ]
!357 = metadata !{i32 786468, null, metadata !"unsigned char", null, i32 0, i64 8, i64 8, i64 0, i32 0, i32 8} ; [ DW_TAG_base_type ]
!358 = metadata !{i32 786478, i32 0, metadata !307, metadata !"ap_int_base", metadata !"ap_int_base", metadata !"", metadata !308, i32 243, metadata !359, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 243} ; [ DW_TAG_subprogram ]
!359 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !360, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!360 = metadata !{null, metadata !327, metadata !361}
!361 = metadata !{i32 786470, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !362} ; [ DW_TAG_const_type ]
!362 = metadata !{i32 786468, null, metadata !"short", null, i32 0, i64 16, i64 16, i64 0, i32 0, i32 5} ; [ DW_TAG_base_type ]
!363 = metadata !{i32 786478, i32 0, metadata !307, metadata !"ap_int_base", metadata !"ap_int_base", metadata !"", metadata !308, i32 244, metadata !364, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 244} ; [ DW_TAG_subprogram ]
!364 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !365, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!365 = metadata !{null, metadata !327, metadata !366}
!366 = metadata !{i32 786470, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !367} ; [ DW_TAG_const_type ]
!367 = metadata !{i32 786468, null, metadata !"unsigned short", null, i32 0, i64 16, i64 16, i64 0, i32 0, i32 7} ; [ DW_TAG_base_type ]
!368 = metadata !{i32 786478, i32 0, metadata !307, metadata !"ap_int_base", metadata !"ap_int_base", metadata !"", metadata !308, i32 245, metadata !369, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 245} ; [ DW_TAG_subprogram ]
!369 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !370, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!370 = metadata !{null, metadata !327, metadata !371}
!371 = metadata !{i32 786470, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !322} ; [ DW_TAG_const_type ]
!372 = metadata !{i32 786478, i32 0, metadata !307, metadata !"ap_int_base", metadata !"ap_int_base", metadata !"", metadata !308, i32 246, metadata !373, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 246} ; [ DW_TAG_subprogram ]
!373 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !374, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!374 = metadata !{null, metadata !327, metadata !375}
!375 = metadata !{i32 786470, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !376} ; [ DW_TAG_const_type ]
!376 = metadata !{i32 786468, null, metadata !"unsigned int", null, i32 0, i64 32, i64 32, i64 0, i32 0, i32 7} ; [ DW_TAG_base_type ]
!377 = metadata !{i32 786478, i32 0, metadata !307, metadata !"ap_int_base", metadata !"ap_int_base", metadata !"", metadata !308, i32 247, metadata !378, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 247} ; [ DW_TAG_subprogram ]
!378 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !379, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!379 = metadata !{null, metadata !327, metadata !380}
!380 = metadata !{i32 786470, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !381} ; [ DW_TAG_const_type ]
!381 = metadata !{i32 786468, null, metadata !"long int", null, i32 0, i64 64, i64 64, i64 0, i32 0, i32 5} ; [ DW_TAG_base_type ]
!382 = metadata !{i32 786478, i32 0, metadata !307, metadata !"ap_int_base", metadata !"ap_int_base", metadata !"", metadata !308, i32 248, metadata !383, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 248} ; [ DW_TAG_subprogram ]
!383 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !384, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!384 = metadata !{null, metadata !327, metadata !385}
!385 = metadata !{i32 786470, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !386} ; [ DW_TAG_const_type ]
!386 = metadata !{i32 786468, null, metadata !"long unsigned int", null, i32 0, i64 64, i64 64, i64 0, i32 0, i32 7} ; [ DW_TAG_base_type ]
!387 = metadata !{i32 786478, i32 0, metadata !307, metadata !"ap_int_base", metadata !"ap_int_base", metadata !"", metadata !308, i32 249, metadata !388, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 249} ; [ DW_TAG_subprogram ]
!388 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !389, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!389 = metadata !{null, metadata !327, metadata !390}
!390 = metadata !{i32 786470, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !391} ; [ DW_TAG_const_type ]
!391 = metadata !{i32 786454, null, metadata !"ap_slong", metadata !308, i32 232, i64 0, i64 0, i64 0, i32 0, metadata !392} ; [ DW_TAG_typedef ]
!392 = metadata !{i32 786468, null, metadata !"long long int", null, i32 0, i64 64, i64 64, i64 0, i32 0, i32 5} ; [ DW_TAG_base_type ]
!393 = metadata !{i32 786478, i32 0, metadata !307, metadata !"ap_int_base", metadata !"ap_int_base", metadata !"", metadata !308, i32 250, metadata !394, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 250} ; [ DW_TAG_subprogram ]
!394 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !395, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!395 = metadata !{null, metadata !327, metadata !396}
!396 = metadata !{i32 786470, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !397} ; [ DW_TAG_const_type ]
!397 = metadata !{i32 786454, null, metadata !"ap_ulong", metadata !308, i32 233, i64 0, i64 0, i64 0, i32 0, metadata !398} ; [ DW_TAG_typedef ]
!398 = metadata !{i32 786468, null, metadata !"long long unsigned int", null, i32 0, i64 64, i64 64, i64 0, i32 0, i32 7} ; [ DW_TAG_base_type ]
!399 = metadata !{i32 786478, i32 0, metadata !307, metadata !"ap_int_base", metadata !"ap_int_base", metadata !"", metadata !308, i32 255, metadata !400, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 255} ; [ DW_TAG_subprogram ]
!400 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !401, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!401 = metadata !{null, metadata !327, metadata !402}
!402 = metadata !{i32 786454, null, metadata !"half", metadata !308, i32 600, i64 0, i64 0, i64 0, i32 0, metadata !403} ; [ DW_TAG_typedef ]
!403 = metadata !{i32 786468, null, metadata !"half", null, i32 0, i64 16, i64 16, i64 0, i32 0, i32 4} ; [ DW_TAG_base_type ]
!404 = metadata !{i32 786478, i32 0, metadata !307, metadata !"ap_int_base", metadata !"ap_int_base", metadata !"", metadata !308, i32 261, metadata !405, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 261} ; [ DW_TAG_subprogram ]
!405 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !406, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!406 = metadata !{null, metadata !327, metadata !407}
!407 = metadata !{i32 786468, null, metadata !"float", null, i32 0, i64 32, i64 32, i64 0, i32 0, i32 4} ; [ DW_TAG_base_type ]
!408 = metadata !{i32 786478, i32 0, metadata !307, metadata !"ap_int_base", metadata !"ap_int_base", metadata !"", metadata !308, i32 309, metadata !409, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 309} ; [ DW_TAG_subprogram ]
!409 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !410, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!410 = metadata !{null, metadata !327, metadata !411}
!411 = metadata !{i32 786468, null, metadata !"double", null, i32 0, i64 64, i64 64, i64 0, i32 0, i32 4} ; [ DW_TAG_base_type ]
!412 = metadata !{i32 786478, i32 0, metadata !307, metadata !"ap_int_base", metadata !"ap_int_base", metadata !"", metadata !308, i32 393, metadata !413, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 393} ; [ DW_TAG_subprogram ]
!413 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !414, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!414 = metadata !{null, metadata !327, metadata !229}
!415 = metadata !{i32 786478, i32 0, metadata !307, metadata !"ap_int_base", metadata !"ap_int_base", metadata !"", metadata !308, i32 399, metadata !416, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 399} ; [ DW_TAG_subprogram ]
!416 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !417, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!417 = metadata !{null, metadata !327, metadata !229, metadata !352}
!418 = metadata !{i32 786478, i32 0, metadata !307, metadata !"read", metadata !"read", metadata !"_ZNV11ap_int_baseILi32ELb0EE4readEv", metadata !308, i32 421, metadata !419, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 421} ; [ DW_TAG_subprogram ]
!419 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !420, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!420 = metadata !{metadata !307, metadata !421}
!421 = metadata !{i32 786447, i32 0, metadata !"", i32 0, i32 0, i64 64, i64 64, i64 0, i32 64, metadata !341} ; [ DW_TAG_pointer_type ]
!422 = metadata !{i32 786478, i32 0, metadata !307, metadata !"write", metadata !"write", metadata !"_ZNV11ap_int_baseILi32ELb0EE5writeERKS0_", metadata !308, i32 428, metadata !423, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 428} ; [ DW_TAG_subprogram ]
!423 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !424, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!424 = metadata !{null, metadata !421, metadata !331}
!425 = metadata !{i32 786478, i32 0, metadata !307, metadata !"operator=", metadata !"operator=", metadata !"_ZNV11ap_int_baseILi32ELb0EEaSERVKS0_", metadata !308, i32 440, metadata !426, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 440} ; [ DW_TAG_subprogram ]
!426 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !427, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!427 = metadata !{null, metadata !421, metadata !339}
!428 = metadata !{i32 786478, i32 0, metadata !307, metadata !"operator=", metadata !"operator=", metadata !"_ZNV11ap_int_baseILi32ELb0EEaSERKS0_", metadata !308, i32 450, metadata !423, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 450} ; [ DW_TAG_subprogram ]
!429 = metadata !{i32 786478, i32 0, metadata !307, metadata !"operator=", metadata !"operator=", metadata !"_ZN11ap_int_baseILi32ELb0EEaSERVKS0_", metadata !308, i32 467, metadata !430, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 467} ; [ DW_TAG_subprogram ]
!430 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !431, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!431 = metadata !{metadata !432, metadata !327, metadata !339}
!432 = metadata !{i32 786448, null, null, null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !307} ; [ DW_TAG_reference_type ]
!433 = metadata !{i32 786478, i32 0, metadata !307, metadata !"operator=", metadata !"operator=", metadata !"_ZN11ap_int_baseILi32ELb0EEaSERKS0_", metadata !308, i32 472, metadata !434, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 472} ; [ DW_TAG_subprogram ]
!434 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !435, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!435 = metadata !{metadata !432, metadata !327, metadata !331}
!436 = metadata !{i32 786478, i32 0, metadata !307, metadata !"operator=", metadata !"operator=", metadata !"_ZN11ap_int_baseILi32ELb0EEaSEb", metadata !308, i32 484, metadata !437, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 484} ; [ DW_TAG_subprogram ]
!437 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !438, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!438 = metadata !{metadata !432, metadata !327, metadata !200}
!439 = metadata !{i32 786478, i32 0, metadata !307, metadata !"operator=", metadata !"operator=", metadata !"_ZN11ap_int_baseILi32ELb0EEaSEc", metadata !308, i32 485, metadata !440, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 485} ; [ DW_TAG_subprogram ]
!440 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !441, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!441 = metadata !{metadata !432, metadata !327, metadata !231}
!442 = metadata !{i32 786478, i32 0, metadata !307, metadata !"operator=", metadata !"operator=", metadata !"_ZN11ap_int_baseILi32ELb0EEaSEa", metadata !308, i32 486, metadata !443, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 486} ; [ DW_TAG_subprogram ]
!443 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !444, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!444 = metadata !{metadata !432, metadata !327, metadata !352}
!445 = metadata !{i32 786478, i32 0, metadata !307, metadata !"operator=", metadata !"operator=", metadata !"_ZN11ap_int_baseILi32ELb0EEaSEh", metadata !308, i32 487, metadata !446, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 487} ; [ DW_TAG_subprogram ]
!446 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !447, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!447 = metadata !{metadata !432, metadata !327, metadata !357}
!448 = metadata !{i32 786478, i32 0, metadata !307, metadata !"operator=", metadata !"operator=", metadata !"_ZN11ap_int_baseILi32ELb0EEaSEs", metadata !308, i32 488, metadata !449, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 488} ; [ DW_TAG_subprogram ]
!449 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !450, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!450 = metadata !{metadata !432, metadata !327, metadata !362}
!451 = metadata !{i32 786478, i32 0, metadata !307, metadata !"operator=", metadata !"operator=", metadata !"_ZN11ap_int_baseILi32ELb0EEaSEt", metadata !308, i32 489, metadata !452, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 489} ; [ DW_TAG_subprogram ]
!452 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !453, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!453 = metadata !{metadata !432, metadata !327, metadata !367}
!454 = metadata !{i32 786478, i32 0, metadata !307, metadata !"operator=", metadata !"operator=", metadata !"_ZN11ap_int_baseILi32ELb0EEaSEi", metadata !308, i32 490, metadata !455, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 490} ; [ DW_TAG_subprogram ]
!455 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !456, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!456 = metadata !{metadata !432, metadata !327, metadata !322}
!457 = metadata !{i32 786478, i32 0, metadata !307, metadata !"operator=", metadata !"operator=", metadata !"_ZN11ap_int_baseILi32ELb0EEaSEj", metadata !308, i32 491, metadata !458, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 491} ; [ DW_TAG_subprogram ]
!458 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !459, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!459 = metadata !{metadata !432, metadata !327, metadata !376}
!460 = metadata !{i32 786478, i32 0, metadata !307, metadata !"operator=", metadata !"operator=", metadata !"_ZN11ap_int_baseILi32ELb0EEaSEl", metadata !308, i32 492, metadata !461, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 492} ; [ DW_TAG_subprogram ]
!461 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !462, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!462 = metadata !{metadata !432, metadata !327, metadata !381}
!463 = metadata !{i32 786478, i32 0, metadata !307, metadata !"operator=", metadata !"operator=", metadata !"_ZN11ap_int_baseILi32ELb0EEaSEm", metadata !308, i32 493, metadata !464, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 493} ; [ DW_TAG_subprogram ]
!464 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !465, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!465 = metadata !{metadata !432, metadata !327, metadata !386}
!466 = metadata !{i32 786478, i32 0, metadata !307, metadata !"operator=", metadata !"operator=", metadata !"_ZN11ap_int_baseILi32ELb0EEaSEx", metadata !308, i32 494, metadata !467, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 494} ; [ DW_TAG_subprogram ]
!467 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !468, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!468 = metadata !{metadata !432, metadata !327, metadata !391}
!469 = metadata !{i32 786478, i32 0, metadata !307, metadata !"operator=", metadata !"operator=", metadata !"_ZN11ap_int_baseILi32ELb0EEaSEy", metadata !308, i32 495, metadata !470, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 495} ; [ DW_TAG_subprogram ]
!470 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !471, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!471 = metadata !{metadata !432, metadata !327, metadata !397}
!472 = metadata !{i32 786478, i32 0, metadata !307, metadata !"operator unsigned long long", metadata !"operator unsigned long long", metadata !"_ZNK11ap_int_baseILi32ELb0EEcvyEv", metadata !308, i32 546, metadata !473, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 546} ; [ DW_TAG_subprogram ]
!473 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !474, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!474 = metadata !{metadata !475, metadata !480}
!475 = metadata !{i32 786454, metadata !307, metadata !"RetType", metadata !308, i32 160, i64 0, i64 0, i64 0, i32 0, metadata !476} ; [ DW_TAG_typedef ]
!476 = metadata !{i32 786454, metadata !477, metadata !"Type", metadata !308, i32 93, i64 0, i64 0, i64 0, i32 0, metadata !397} ; [ DW_TAG_typedef ]
!477 = metadata !{i32 786434, null, metadata !"retval<8, false>", metadata !308, i32 92, i64 8, i64 8, i32 0, i32 0, null, metadata !242, i32 0, null, metadata !478} ; [ DW_TAG_class_type ]
!478 = metadata !{metadata !479, metadata !323}
!479 = metadata !{i32 786480, null, metadata !"_AP_N", metadata !322, i64 8, null, i32 0, i32 0} ; [ DW_TAG_template_value_parameter ]
!480 = metadata !{i32 786447, i32 0, metadata !"", i32 0, i32 0, i64 64, i64 64, i64 0, i32 64, metadata !332} ; [ DW_TAG_pointer_type ]
!481 = metadata !{i32 786478, i32 0, metadata !307, metadata !"to_bool", metadata !"to_bool", metadata !"_ZNK11ap_int_baseILi32ELb0EE7to_boolEv", metadata !308, i32 551, metadata !482, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 551} ; [ DW_TAG_subprogram ]
!482 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !483, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!483 = metadata !{metadata !200, metadata !480}
!484 = metadata !{i32 786478, i32 0, metadata !307, metadata !"to_char", metadata !"to_char", metadata !"_ZNK11ap_int_baseILi32ELb0EE7to_charEv", metadata !308, i32 552, metadata !485, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 552} ; [ DW_TAG_subprogram ]
!485 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !486, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!486 = metadata !{metadata !231, metadata !480}
!487 = metadata !{i32 786478, i32 0, metadata !307, metadata !"to_schar", metadata !"to_schar", metadata !"_ZNK11ap_int_baseILi32ELb0EE8to_scharEv", metadata !308, i32 553, metadata !488, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 553} ; [ DW_TAG_subprogram ]
!488 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !489, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!489 = metadata !{metadata !352, metadata !480}
!490 = metadata !{i32 786478, i32 0, metadata !307, metadata !"to_uchar", metadata !"to_uchar", metadata !"_ZNK11ap_int_baseILi32ELb0EE8to_ucharEv", metadata !308, i32 554, metadata !491, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 554} ; [ DW_TAG_subprogram ]
!491 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !492, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!492 = metadata !{metadata !357, metadata !480}
!493 = metadata !{i32 786478, i32 0, metadata !307, metadata !"to_short", metadata !"to_short", metadata !"_ZNK11ap_int_baseILi32ELb0EE8to_shortEv", metadata !308, i32 555, metadata !494, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 555} ; [ DW_TAG_subprogram ]
!494 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !495, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!495 = metadata !{metadata !362, metadata !480}
!496 = metadata !{i32 786478, i32 0, metadata !307, metadata !"to_ushort", metadata !"to_ushort", metadata !"_ZNK11ap_int_baseILi32ELb0EE9to_ushortEv", metadata !308, i32 556, metadata !497, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 556} ; [ DW_TAG_subprogram ]
!497 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !498, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!498 = metadata !{metadata !367, metadata !480}
!499 = metadata !{i32 786478, i32 0, metadata !307, metadata !"to_int", metadata !"to_int", metadata !"_ZNK11ap_int_baseILi32ELb0EE6to_intEv", metadata !308, i32 557, metadata !500, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 557} ; [ DW_TAG_subprogram ]
!500 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !501, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!501 = metadata !{metadata !322, metadata !480}
!502 = metadata !{i32 786478, i32 0, metadata !307, metadata !"to_uint", metadata !"to_uint", metadata !"_ZNK11ap_int_baseILi32ELb0EE7to_uintEv", metadata !308, i32 558, metadata !503, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 558} ; [ DW_TAG_subprogram ]
!503 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !504, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!504 = metadata !{metadata !376, metadata !480}
!505 = metadata !{i32 786478, i32 0, metadata !307, metadata !"to_long", metadata !"to_long", metadata !"_ZNK11ap_int_baseILi32ELb0EE7to_longEv", metadata !308, i32 559, metadata !506, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 559} ; [ DW_TAG_subprogram ]
!506 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !507, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!507 = metadata !{metadata !381, metadata !480}
!508 = metadata !{i32 786478, i32 0, metadata !307, metadata !"to_ulong", metadata !"to_ulong", metadata !"_ZNK11ap_int_baseILi32ELb0EE8to_ulongEv", metadata !308, i32 560, metadata !509, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 560} ; [ DW_TAG_subprogram ]
!509 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !510, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!510 = metadata !{metadata !386, metadata !480}
!511 = metadata !{i32 786478, i32 0, metadata !307, metadata !"to_int64", metadata !"to_int64", metadata !"_ZNK11ap_int_baseILi32ELb0EE8to_int64Ev", metadata !308, i32 561, metadata !512, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 561} ; [ DW_TAG_subprogram ]
!512 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !513, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!513 = metadata !{metadata !391, metadata !480}
!514 = metadata !{i32 786478, i32 0, metadata !307, metadata !"to_uint64", metadata !"to_uint64", metadata !"_ZNK11ap_int_baseILi32ELb0EE9to_uint64Ev", metadata !308, i32 562, metadata !515, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 562} ; [ DW_TAG_subprogram ]
!515 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !516, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!516 = metadata !{metadata !397, metadata !480}
!517 = metadata !{i32 786478, i32 0, metadata !307, metadata !"to_float", metadata !"to_float", metadata !"_ZNK11ap_int_baseILi32ELb0EE8to_floatEv", metadata !308, i32 563, metadata !518, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 563} ; [ DW_TAG_subprogram ]
!518 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !519, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!519 = metadata !{metadata !407, metadata !480}
!520 = metadata !{i32 786478, i32 0, metadata !307, metadata !"to_double", metadata !"to_double", metadata !"_ZNK11ap_int_baseILi32ELb0EE9to_doubleEv", metadata !308, i32 564, metadata !521, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 564} ; [ DW_TAG_subprogram ]
!521 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !522, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!522 = metadata !{metadata !411, metadata !480}
!523 = metadata !{i32 786478, i32 0, metadata !307, metadata !"length", metadata !"length", metadata !"_ZNVK11ap_int_baseILi32ELb0EE6lengthEv", metadata !308, i32 588, metadata !524, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 588} ; [ DW_TAG_subprogram ]
!524 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !525, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!525 = metadata !{metadata !322, metadata !526}
!526 = metadata !{i32 786447, i32 0, metadata !"", i32 0, i32 0, i64 64, i64 64, i64 0, i32 64, metadata !340} ; [ DW_TAG_pointer_type ]
!527 = metadata !{i32 786478, i32 0, metadata !307, metadata !"iszero", metadata !"iszero", metadata !"_ZNK11ap_int_baseILi32ELb0EE6iszeroEv", metadata !308, i32 591, metadata !482, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 591} ; [ DW_TAG_subprogram ]
!528 = metadata !{i32 786478, i32 0, metadata !307, metadata !"is_zero", metadata !"is_zero", metadata !"_ZNK11ap_int_baseILi32ELb0EE7is_zeroEv", metadata !308, i32 594, metadata !482, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 594} ; [ DW_TAG_subprogram ]
!529 = metadata !{i32 786478, i32 0, metadata !307, metadata !"sign", metadata !"sign", metadata !"_ZNK11ap_int_baseILi32ELb0EE4signEv", metadata !308, i32 597, metadata !482, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 597} ; [ DW_TAG_subprogram ]
!530 = metadata !{i32 786478, i32 0, metadata !307, metadata !"clear", metadata !"clear", metadata !"_ZN11ap_int_baseILi32ELb0EE5clearEi", metadata !308, i32 606, metadata !531, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 606} ; [ DW_TAG_subprogram ]
!531 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !532, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!532 = metadata !{null, metadata !327, metadata !322}
!533 = metadata !{i32 786478, i32 0, metadata !307, metadata !"invert", metadata !"invert", metadata !"_ZN11ap_int_baseILi32ELb0EE6invertEi", metadata !308, i32 612, metadata !531, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 612} ; [ DW_TAG_subprogram ]
!534 = metadata !{i32 786478, i32 0, metadata !307, metadata !"test", metadata !"test", metadata !"_ZNK11ap_int_baseILi32ELb0EE4testEi", metadata !308, i32 621, metadata !535, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 621} ; [ DW_TAG_subprogram ]
!535 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !536, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!536 = metadata !{metadata !200, metadata !480, metadata !322}
!537 = metadata !{i32 786478, i32 0, metadata !307, metadata !"get", metadata !"get", metadata !"_ZN11ap_int_baseILi32ELb0EE3getEv", metadata !308, i32 627, metadata !538, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 627} ; [ DW_TAG_subprogram ]
!538 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !539, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!539 = metadata !{metadata !432, metadata !327}
!540 = metadata !{i32 786478, i32 0, metadata !307, metadata !"set", metadata !"set", metadata !"_ZN11ap_int_baseILi32ELb0EE3setEi", metadata !308, i32 630, metadata !531, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 630} ; [ DW_TAG_subprogram ]
!541 = metadata !{i32 786478, i32 0, metadata !307, metadata !"set", metadata !"set", metadata !"_ZN11ap_int_baseILi32ELb0EE3setEib", metadata !308, i32 636, metadata !542, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 636} ; [ DW_TAG_subprogram ]
!542 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !543, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!543 = metadata !{null, metadata !327, metadata !322, metadata !200}
!544 = metadata !{i32 786478, i32 0, metadata !307, metadata !"lrotate", metadata !"lrotate", metadata !"_ZN11ap_int_baseILi32ELb0EE7lrotateEi", metadata !308, i32 643, metadata !455, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 643} ; [ DW_TAG_subprogram ]
!545 = metadata !{i32 786478, i32 0, metadata !307, metadata !"rrotate", metadata !"rrotate", metadata !"_ZN11ap_int_baseILi32ELb0EE7rrotateEi", metadata !308, i32 658, metadata !455, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 658} ; [ DW_TAG_subprogram ]
!546 = metadata !{i32 786478, i32 0, metadata !307, metadata !"reverse", metadata !"reverse", metadata !"_ZN11ap_int_baseILi32ELb0EE7reverseEv", metadata !308, i32 673, metadata !538, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 673} ; [ DW_TAG_subprogram ]
!547 = metadata !{i32 786478, i32 0, metadata !307, metadata !"set_bit", metadata !"set_bit", metadata !"_ZN11ap_int_baseILi32ELb0EE7set_bitEib", metadata !308, i32 679, metadata !542, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 679} ; [ DW_TAG_subprogram ]
!548 = metadata !{i32 786478, i32 0, metadata !307, metadata !"get_bit", metadata !"get_bit", metadata !"_ZNK11ap_int_baseILi32ELb0EE7get_bitEi", metadata !308, i32 684, metadata !535, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 684} ; [ DW_TAG_subprogram ]
!549 = metadata !{i32 786478, i32 0, metadata !307, metadata !"b_not", metadata !"b_not", metadata !"_ZN11ap_int_baseILi32ELb0EE5b_notEv", metadata !308, i32 689, metadata !325, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 689} ; [ DW_TAG_subprogram ]
!550 = metadata !{i32 786478, i32 0, metadata !307, metadata !"operator++", metadata !"operator++", metadata !"_ZN11ap_int_baseILi32ELb0EEppEv", metadata !308, i32 727, metadata !538, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 727} ; [ DW_TAG_subprogram ]
!551 = metadata !{i32 786478, i32 0, metadata !307, metadata !"operator--", metadata !"operator--", metadata !"_ZN11ap_int_baseILi32ELb0EEmmEv", metadata !308, i32 731, metadata !538, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 731} ; [ DW_TAG_subprogram ]
!552 = metadata !{i32 786478, i32 0, metadata !307, metadata !"operator++", metadata !"operator++", metadata !"_ZN11ap_int_baseILi32ELb0EEppEi", metadata !308, i32 739, metadata !553, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 739} ; [ DW_TAG_subprogram ]
!553 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !554, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!554 = metadata !{metadata !555, metadata !327, metadata !322}
!555 = metadata !{i32 786470, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !556} ; [ DW_TAG_const_type ]
!556 = metadata !{i32 786454, metadata !557, metadata !"arg1", metadata !308, i32 198, i64 0, i64 0, i64 0, i32 0, metadata !558} ; [ DW_TAG_typedef ]
!557 = metadata !{i32 786434, metadata !307, metadata !"RType<32, false>", metadata !308, i32 165, i64 8, i64 8, i32 0, i32 0, null, metadata !242, i32 0, null, metadata !333} ; [ DW_TAG_class_type ]
!558 = metadata !{i32 786454, metadata !559, metadata !"type", metadata !308, i32 147, i64 0, i64 0, i64 0, i32 0, metadata !303} ; [ DW_TAG_typedef ]
!559 = metadata !{i32 786434, null, metadata !"_ap_int_factory<32, false>", metadata !308, i32 147, i64 8, i64 8, i32 0, i32 0, null, metadata !242, i32 0, null, metadata !333} ; [ DW_TAG_class_type ]
!560 = metadata !{i32 786478, i32 0, metadata !307, metadata !"operator--", metadata !"operator--", metadata !"_ZN11ap_int_baseILi32ELb0EEmmEi", metadata !308, i32 744, metadata !553, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 744} ; [ DW_TAG_subprogram ]
!561 = metadata !{i32 786478, i32 0, metadata !307, metadata !"operator+", metadata !"operator+", metadata !"_ZNK11ap_int_baseILi32ELb0EEpsEv", metadata !308, i32 753, metadata !562, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 753} ; [ DW_TAG_subprogram ]
!562 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !563, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!563 = metadata !{metadata !556, metadata !480}
!564 = metadata !{i32 786478, i32 0, metadata !307, metadata !"operator-", metadata !"operator-", metadata !"_ZNK11ap_int_baseILi32ELb0EEngEv", metadata !308, i32 756, metadata !565, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 756} ; [ DW_TAG_subprogram ]
!565 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !566, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!566 = metadata !{metadata !567, metadata !480}
!567 = metadata !{i32 786454, metadata !568, metadata !"minus", metadata !308, i32 194, i64 0, i64 0, i64 0, i32 0, metadata !571} ; [ DW_TAG_typedef ]
!568 = metadata !{i32 786434, metadata !307, metadata !"RType<1, false>", metadata !308, i32 165, i64 8, i64 8, i32 0, i32 0, null, metadata !242, i32 0, null, metadata !569} ; [ DW_TAG_class_type ]
!569 = metadata !{metadata !570, metadata !335}
!570 = metadata !{i32 786480, null, metadata !"_AP_W2", metadata !322, i64 1, null, i32 0, i32 0} ; [ DW_TAG_template_value_parameter ]
!571 = metadata !{i32 786454, metadata !572, metadata !"type", metadata !308, i32 145, i64 0, i64 0, i64 0, i32 0, metadata !576} ; [ DW_TAG_typedef ]
!572 = metadata !{i32 786434, null, metadata !"_ap_int_factory<33, true>", metadata !308, i32 145, i64 8, i64 8, i32 0, i32 0, null, metadata !242, i32 0, null, metadata !573} ; [ DW_TAG_class_type ]
!573 = metadata !{metadata !574, metadata !575}
!574 = metadata !{i32 786480, null, metadata !"_AP_W2", metadata !322, i64 33, null, i32 0, i32 0} ; [ DW_TAG_template_value_parameter ]
!575 = metadata !{i32 786480, null, metadata !"_AP_S2", metadata !200, i64 1, null, i32 0, i32 0} ; [ DW_TAG_template_value_parameter ]
!576 = metadata !{i32 786434, null, metadata !"ap_int<33>", metadata !577, i32 183, i64 0, i64 0, i32 0, i32 4, null, null, i32 0, null, metadata !578} ; [ DW_TAG_class_type ]
!577 = metadata !{i32 786473, metadata !"/tools/Xilinx/Vivado/2018.3/common/technology/autopilot/ap_decl.h", metadata !"/home/julien/education/inf8500/lab2/tutorial_ju/solutions/tutorial/architectures/hw_sw/build/estimation/hls/producer0", null} ; [ DW_TAG_file_type ]
!578 = metadata !{metadata !579}
!579 = metadata !{i32 786480, null, metadata !"_AP_W", metadata !322, i64 33, null, i32 0, i32 0} ; [ DW_TAG_template_value_parameter ]
!580 = metadata !{i32 786478, i32 0, metadata !307, metadata !"operator!", metadata !"operator!", metadata !"_ZNK11ap_int_baseILi32ELb0EEntEv", metadata !308, i32 763, metadata !482, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 763} ; [ DW_TAG_subprogram ]
!581 = metadata !{i32 786478, i32 0, metadata !307, metadata !"operator~", metadata !"operator~", metadata !"_ZNK11ap_int_baseILi32ELb0EEcoEv", metadata !308, i32 769, metadata !562, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 769} ; [ DW_TAG_subprogram ]
!582 = metadata !{i32 786478, i32 0, metadata !307, metadata !"range", metadata !"range", metadata !"_ZN11ap_int_baseILi32ELb0EE5rangeEii", metadata !308, i32 907, metadata !583, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 907} ; [ DW_TAG_subprogram ]
!583 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !584, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!584 = metadata !{metadata !585, metadata !327, metadata !322, metadata !322}
!585 = metadata !{i32 786434, null, metadata !"ap_range_ref<32, false>", metadata !586, i32 335, i64 128, i64 64, i32 0, i32 0, null, metadata !587, i32 0, null, metadata !702} ; [ DW_TAG_class_type ]
!586 = metadata !{i32 786473, metadata !"/tools/Xilinx/Vivado/2018.3/common/technology/autopilot/ap_int_ref.h", metadata !"/home/julien/education/inf8500/lab2/tutorial_ju/solutions/tutorial/architectures/hw_sw/build/estimation/hls/producer0", null} ; [ DW_TAG_file_type ]
!587 = metadata !{metadata !588, metadata !591, metadata !592, metadata !593, metadata !599, metadata !603, metadata !608, metadata !612, metadata !615, metadata !619, metadata !622, metadata !625, metadata !628, metadata !631, metadata !634, metadata !637, metadata !640, metadata !643, metadata !646, metadata !649, metadata !652, metadata !655, metadata !658, metadata !661, metadata !664, metadata !667, metadata !675, metadata !676, metadata !679, metadata !680, metadata !683, metadata !686, metadata !689, metadata !692, metadata !693, metadata !696, metadata !697, metadata !698}
!588 = metadata !{i32 786445, metadata !585, metadata !"d_bv", metadata !586, i32 340, i64 64, i64 64, i64 0, i32 0, metadata !589} ; [ DW_TAG_member ]
!589 = metadata !{i32 786448, null, null, null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !590} ; [ DW_TAG_reference_type ]
!590 = metadata !{i32 786454, metadata !585, metadata !"ref_type", metadata !586, i32 339, i64 0, i64 0, i64 0, i32 0, metadata !307} ; [ DW_TAG_typedef ]
!591 = metadata !{i32 786445, metadata !585, metadata !"l_index", metadata !586, i32 341, i64 32, i64 32, i64 64, i32 0, metadata !322} ; [ DW_TAG_member ]
!592 = metadata !{i32 786445, metadata !585, metadata !"h_index", metadata !586, i32 342, i64 32, i64 32, i64 96, i32 0, metadata !322} ; [ DW_TAG_member ]
!593 = metadata !{i32 786478, i32 0, metadata !585, metadata !"ap_range_ref", metadata !"ap_range_ref", metadata !"", metadata !586, i32 345, metadata !594, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 345} ; [ DW_TAG_subprogram ]
!594 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !595, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!595 = metadata !{null, metadata !596, metadata !597}
!596 = metadata !{i32 786447, i32 0, metadata !"", i32 0, i32 0, i64 64, i64 64, i64 0, i32 64, metadata !585} ; [ DW_TAG_pointer_type ]
!597 = metadata !{i32 786448, null, null, null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !598} ; [ DW_TAG_reference_type ]
!598 = metadata !{i32 786470, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !585} ; [ DW_TAG_const_type ]
!599 = metadata !{i32 786478, i32 0, metadata !585, metadata !"ap_range_ref", metadata !"ap_range_ref", metadata !"", metadata !586, i32 348, metadata !600, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 348} ; [ DW_TAG_subprogram ]
!600 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !601, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!601 = metadata !{null, metadata !596, metadata !602, metadata !322, metadata !322}
!602 = metadata !{i32 786447, null, metadata !"", null, i32 0, i64 64, i64 64, i64 0, i32 0, metadata !590} ; [ DW_TAG_pointer_type ]
!603 = metadata !{i32 786478, i32 0, metadata !585, metadata !"ap_range_ref", metadata !"ap_range_ref", metadata !"", metadata !586, i32 351, metadata !604, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 351} ; [ DW_TAG_subprogram ]
!604 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !605, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!605 = metadata !{null, metadata !596, metadata !606, metadata !322, metadata !322}
!606 = metadata !{i32 786447, null, metadata !"", null, i32 0, i64 64, i64 64, i64 0, i32 0, metadata !607} ; [ DW_TAG_pointer_type ]
!607 = metadata !{i32 786470, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !590} ; [ DW_TAG_const_type ]
!608 = metadata !{i32 786478, i32 0, metadata !585, metadata !"operator ap_int_base", metadata !"operator ap_int_base", metadata !"_ZNK12ap_range_refILi32ELb0EEcv11ap_int_baseILi32ELb0EEEv", metadata !586, i32 354, metadata !609, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 354} ; [ DW_TAG_subprogram ]
!609 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !610, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!610 = metadata !{metadata !307, metadata !611}
!611 = metadata !{i32 786447, i32 0, metadata !"", i32 0, i32 0, i64 64, i64 64, i64 0, i32 64, metadata !598} ; [ DW_TAG_pointer_type ]
!612 = metadata !{i32 786478, i32 0, metadata !585, metadata !"operator unsigned long long", metadata !"operator unsigned long long", metadata !"_ZNK12ap_range_refILi32ELb0EEcvyEv", metadata !586, i32 360, metadata !613, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 360} ; [ DW_TAG_subprogram ]
!613 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !614, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!614 = metadata !{metadata !397, metadata !611}
!615 = metadata !{i32 786478, i32 0, metadata !585, metadata !"operator=", metadata !"operator=", metadata !"_ZN12ap_range_refILi32ELb0EEaSEb", metadata !586, i32 384, metadata !616, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 384} ; [ DW_TAG_subprogram ]
!616 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !617, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!617 = metadata !{metadata !618, metadata !596, metadata !200}
!618 = metadata !{i32 786448, null, null, null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !585} ; [ DW_TAG_reference_type ]
!619 = metadata !{i32 786478, i32 0, metadata !585, metadata !"operator=", metadata !"operator=", metadata !"_ZN12ap_range_refILi32ELb0EEaSEc", metadata !586, i32 385, metadata !620, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 385} ; [ DW_TAG_subprogram ]
!620 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !621, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!621 = metadata !{metadata !618, metadata !596, metadata !231}
!622 = metadata !{i32 786478, i32 0, metadata !585, metadata !"operator=", metadata !"operator=", metadata !"_ZN12ap_range_refILi32ELb0EEaSEa", metadata !586, i32 386, metadata !623, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 386} ; [ DW_TAG_subprogram ]
!623 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !624, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!624 = metadata !{metadata !618, metadata !596, metadata !352}
!625 = metadata !{i32 786478, i32 0, metadata !585, metadata !"operator=", metadata !"operator=", metadata !"_ZN12ap_range_refILi32ELb0EEaSEh", metadata !586, i32 387, metadata !626, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 387} ; [ DW_TAG_subprogram ]
!626 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !627, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!627 = metadata !{metadata !618, metadata !596, metadata !357}
!628 = metadata !{i32 786478, i32 0, metadata !585, metadata !"operator=", metadata !"operator=", metadata !"_ZN12ap_range_refILi32ELb0EEaSEs", metadata !586, i32 388, metadata !629, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 388} ; [ DW_TAG_subprogram ]
!629 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !630, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!630 = metadata !{metadata !618, metadata !596, metadata !362}
!631 = metadata !{i32 786478, i32 0, metadata !585, metadata !"operator=", metadata !"operator=", metadata !"_ZN12ap_range_refILi32ELb0EEaSEt", metadata !586, i32 389, metadata !632, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 389} ; [ DW_TAG_subprogram ]
!632 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !633, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!633 = metadata !{metadata !618, metadata !596, metadata !367}
!634 = metadata !{i32 786478, i32 0, metadata !585, metadata !"operator=", metadata !"operator=", metadata !"_ZN12ap_range_refILi32ELb0EEaSEi", metadata !586, i32 390, metadata !635, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 390} ; [ DW_TAG_subprogram ]
!635 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !636, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!636 = metadata !{metadata !618, metadata !596, metadata !322}
!637 = metadata !{i32 786478, i32 0, metadata !585, metadata !"operator=", metadata !"operator=", metadata !"_ZN12ap_range_refILi32ELb0EEaSEj", metadata !586, i32 391, metadata !638, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 391} ; [ DW_TAG_subprogram ]
!638 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !639, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!639 = metadata !{metadata !618, metadata !596, metadata !376}
!640 = metadata !{i32 786478, i32 0, metadata !585, metadata !"operator=", metadata !"operator=", metadata !"_ZN12ap_range_refILi32ELb0EEaSEl", metadata !586, i32 392, metadata !641, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 392} ; [ DW_TAG_subprogram ]
!641 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !642, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!642 = metadata !{metadata !618, metadata !596, metadata !381}
!643 = metadata !{i32 786478, i32 0, metadata !585, metadata !"operator=", metadata !"operator=", metadata !"_ZN12ap_range_refILi32ELb0EEaSEm", metadata !586, i32 393, metadata !644, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 393} ; [ DW_TAG_subprogram ]
!644 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !645, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!645 = metadata !{metadata !618, metadata !596, metadata !386}
!646 = metadata !{i32 786478, i32 0, metadata !585, metadata !"operator=", metadata !"operator=", metadata !"_ZN12ap_range_refILi32ELb0EEaSEx", metadata !586, i32 394, metadata !647, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 394} ; [ DW_TAG_subprogram ]
!647 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !648, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!648 = metadata !{metadata !618, metadata !596, metadata !391}
!649 = metadata !{i32 786478, i32 0, metadata !585, metadata !"operator=", metadata !"operator=", metadata !"_ZN12ap_range_refILi32ELb0EEaSEy", metadata !586, i32 395, metadata !650, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 395} ; [ DW_TAG_subprogram ]
!650 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !651, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!651 = metadata !{metadata !618, metadata !596, metadata !397}
!652 = metadata !{i32 786478, i32 0, metadata !585, metadata !"operator=", metadata !"operator=", metadata !"_ZN12ap_range_refILi32ELb0EEaSEDh", metadata !586, i32 396, metadata !653, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 396} ; [ DW_TAG_subprogram ]
!653 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !654, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!654 = metadata !{metadata !618, metadata !596, metadata !402}
!655 = metadata !{i32 786478, i32 0, metadata !585, metadata !"operator=", metadata !"operator=", metadata !"_ZN12ap_range_refILi32ELb0EEaSEf", metadata !586, i32 397, metadata !656, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 397} ; [ DW_TAG_subprogram ]
!656 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !657, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!657 = metadata !{metadata !618, metadata !596, metadata !407}
!658 = metadata !{i32 786478, i32 0, metadata !585, metadata !"operator=", metadata !"operator=", metadata !"_ZN12ap_range_refILi32ELb0EEaSEd", metadata !586, i32 398, metadata !659, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 398} ; [ DW_TAG_subprogram ]
!659 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !660, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!660 = metadata !{metadata !618, metadata !596, metadata !411}
!661 = metadata !{i32 786478, i32 0, metadata !585, metadata !"operator=", metadata !"operator=", metadata !"_ZN12ap_range_refILi32ELb0EEaSEPKc", metadata !586, i32 403, metadata !662, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 403} ; [ DW_TAG_subprogram ]
!662 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !663, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!663 = metadata !{metadata !618, metadata !596, metadata !229}
!664 = metadata !{i32 786478, i32 0, metadata !585, metadata !"operator=", metadata !"operator=", metadata !"_ZN12ap_range_refILi32ELb0EEaSERKS0_", metadata !586, i32 420, metadata !665, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 420} ; [ DW_TAG_subprogram ]
!665 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !666, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!666 = metadata !{metadata !618, metadata !596, metadata !597}
!667 = metadata !{i32 786478, i32 0, metadata !585, metadata !"operator,", metadata !"operator,", metadata !"_ZN12ap_range_refILi32ELb0EEcmER11ap_int_baseILi32ELb0EE", metadata !586, i32 488, metadata !668, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 488} ; [ DW_TAG_subprogram ]
!668 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !669, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!669 = metadata !{metadata !670, metadata !596, metadata !432}
!670 = metadata !{i32 786434, null, metadata !"ap_concat_ref<32, ap_range_ref<32, false>, 32, ap_int_base<32, false> >", metadata !586, i32 73, i64 0, i64 0, i32 0, i32 4, null, null, i32 0, null, metadata !671} ; [ DW_TAG_class_type ]
!671 = metadata !{metadata !672, metadata !673, metadata !334, metadata !674}
!672 = metadata !{i32 786480, null, metadata !"_AP_W1", metadata !322, i64 32, null, i32 0, i32 0} ; [ DW_TAG_template_value_parameter ]
!673 = metadata !{i32 786479, null, metadata !"_AP_T1", metadata !585, null, i32 0, i32 0} ; [ DW_TAG_template_type_parameter ]
!674 = metadata !{i32 786479, null, metadata !"_AP_T2", metadata !307, null, i32 0, i32 0} ; [ DW_TAG_template_type_parameter ]
!675 = metadata !{i32 786478, i32 0, metadata !585, metadata !"get", metadata !"get", metadata !"_ZNK12ap_range_refILi32ELb0EE3getEv", metadata !586, i32 644, metadata !609, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 644} ; [ DW_TAG_subprogram ]
!676 = metadata !{i32 786478, i32 0, metadata !585, metadata !"length", metadata !"length", metadata !"_ZNK12ap_range_refILi32ELb0EE6lengthEv", metadata !586, i32 655, metadata !677, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 655} ; [ DW_TAG_subprogram ]
!677 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !678, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!678 = metadata !{metadata !322, metadata !611}
!679 = metadata !{i32 786478, i32 0, metadata !585, metadata !"to_int", metadata !"to_int", metadata !"_ZNK12ap_range_refILi32ELb0EE6to_intEv", metadata !586, i32 659, metadata !677, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 659} ; [ DW_TAG_subprogram ]
!680 = metadata !{i32 786478, i32 0, metadata !585, metadata !"to_uint", metadata !"to_uint", metadata !"_ZNK12ap_range_refILi32ELb0EE7to_uintEv", metadata !586, i32 663, metadata !681, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 663} ; [ DW_TAG_subprogram ]
!681 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !682, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!682 = metadata !{metadata !376, metadata !611}
!683 = metadata !{i32 786478, i32 0, metadata !585, metadata !"to_long", metadata !"to_long", metadata !"_ZNK12ap_range_refILi32ELb0EE7to_longEv", metadata !586, i32 667, metadata !684, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 667} ; [ DW_TAG_subprogram ]
!684 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !685, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!685 = metadata !{metadata !381, metadata !611}
!686 = metadata !{i32 786478, i32 0, metadata !585, metadata !"to_ulong", metadata !"to_ulong", metadata !"_ZNK12ap_range_refILi32ELb0EE8to_ulongEv", metadata !586, i32 671, metadata !687, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 671} ; [ DW_TAG_subprogram ]
!687 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !688, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!688 = metadata !{metadata !386, metadata !611}
!689 = metadata !{i32 786478, i32 0, metadata !585, metadata !"to_int64", metadata !"to_int64", metadata !"_ZNK12ap_range_refILi32ELb0EE8to_int64Ev", metadata !586, i32 675, metadata !690, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 675} ; [ DW_TAG_subprogram ]
!690 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !691, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!691 = metadata !{metadata !391, metadata !611}
!692 = metadata !{i32 786478, i32 0, metadata !585, metadata !"to_uint64", metadata !"to_uint64", metadata !"_ZNK12ap_range_refILi32ELb0EE9to_uint64Ev", metadata !586, i32 679, metadata !613, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 679} ; [ DW_TAG_subprogram ]
!693 = metadata !{i32 786478, i32 0, metadata !585, metadata !"and_reduce", metadata !"and_reduce", metadata !"_ZNK12ap_range_refILi32ELb0EE10and_reduceEv", metadata !586, i32 683, metadata !694, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 683} ; [ DW_TAG_subprogram ]
!694 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !695, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!695 = metadata !{metadata !200, metadata !611}
!696 = metadata !{i32 786478, i32 0, metadata !585, metadata !"or_reduce", metadata !"or_reduce", metadata !"_ZNK12ap_range_refILi32ELb0EE9or_reduceEv", metadata !586, i32 697, metadata !694, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 697} ; [ DW_TAG_subprogram ]
!697 = metadata !{i32 786478, i32 0, metadata !585, metadata !"xor_reduce", metadata !"xor_reduce", metadata !"_ZNK12ap_range_refILi32ELb0EE10xor_reduceEv", metadata !586, i32 711, metadata !694, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 711} ; [ DW_TAG_subprogram ]
!698 = metadata !{i32 786478, i32 0, metadata !585, metadata !"to_string", metadata !"to_string", metadata !"_ZNK12ap_range_refILi32ELb0EE9to_stringEa", metadata !586, i32 732, metadata !699, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 732} ; [ DW_TAG_subprogram ]
!699 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !700, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!700 = metadata !{metadata !701, metadata !611, metadata !352}
!701 = metadata !{i32 786447, null, metadata !"", null, i32 0, i64 64, i64 64, i64 0, i32 0, metadata !231} ; [ DW_TAG_pointer_type ]
!702 = metadata !{metadata !703, metadata !323}
!703 = metadata !{i32 786480, null, metadata !"_AP_W", metadata !322, i64 32, null, i32 0, i32 0} ; [ DW_TAG_template_value_parameter ]
!704 = metadata !{i32 786478, i32 0, metadata !307, metadata !"range", metadata !"range", metadata !"_ZNK11ap_int_baseILi32ELb0EE5rangeEii", metadata !308, i32 914, metadata !705, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 914} ; [ DW_TAG_subprogram ]
!705 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !706, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!706 = metadata !{metadata !585, metadata !480, metadata !322, metadata !322}
!707 = metadata !{i32 786478, i32 0, metadata !307, metadata !"range", metadata !"range", metadata !"_ZN11ap_int_baseILi32ELb0EE5rangeEv", metadata !308, i32 938, metadata !708, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 938} ; [ DW_TAG_subprogram ]
!708 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !709, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!709 = metadata !{metadata !585, metadata !327}
!710 = metadata !{i32 786478, i32 0, metadata !307, metadata !"range", metadata !"range", metadata !"_ZNK11ap_int_baseILi32ELb0EE5rangeEv", metadata !308, i32 942, metadata !711, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 942} ; [ DW_TAG_subprogram ]
!711 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !712, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!712 = metadata !{metadata !585, metadata !480}
!713 = metadata !{i32 786478, i32 0, metadata !307, metadata !"operator()", metadata !"operator()", metadata !"_ZN11ap_int_baseILi32ELb0EEclEii", metadata !308, i32 946, metadata !583, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 946} ; [ DW_TAG_subprogram ]
!714 = metadata !{i32 786478, i32 0, metadata !307, metadata !"operator()", metadata !"operator()", metadata !"_ZNK11ap_int_baseILi32ELb0EEclEii", metadata !308, i32 950, metadata !705, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 950} ; [ DW_TAG_subprogram ]
!715 = metadata !{i32 786478, i32 0, metadata !307, metadata !"operator[]", metadata !"operator[]", metadata !"_ZN11ap_int_baseILi32ELb0EEixEi", metadata !308, i32 988, metadata !716, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 988} ; [ DW_TAG_subprogram ]
!716 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !717, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!717 = metadata !{metadata !718, metadata !327, metadata !322}
!718 = metadata !{i32 786434, null, metadata !"ap_bit_ref<32, false>", metadata !577, i32 192, i64 0, i64 0, i32 0, i32 4, null, null, i32 0, null, metadata !702} ; [ DW_TAG_class_type ]
!719 = metadata !{i32 786478, i32 0, metadata !307, metadata !"operator[]", metadata !"operator[]", metadata !"_ZNK11ap_int_baseILi32ELb0EEixEi", metadata !308, i32 1004, metadata !535, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 1004} ; [ DW_TAG_subprogram ]
!720 = metadata !{i32 786478, i32 0, metadata !307, metadata !"bit", metadata !"bit", metadata !"_ZN11ap_int_baseILi32ELb0EE3bitEi", metadata !308, i32 1017, metadata !716, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 1017} ; [ DW_TAG_subprogram ]
!721 = metadata !{i32 786478, i32 0, metadata !307, metadata !"bit", metadata !"bit", metadata !"_ZNK11ap_int_baseILi32ELb0EE3bitEi", metadata !308, i32 1032, metadata !535, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 1032} ; [ DW_TAG_subprogram ]
!722 = metadata !{i32 786478, i32 0, metadata !307, metadata !"countLeadingZeros", metadata !"countLeadingZeros", metadata !"_ZN11ap_int_baseILi32ELb0EE17countLeadingZerosEv", metadata !308, i32 1055, metadata !723, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 1055} ; [ DW_TAG_subprogram ]
!723 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !724, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!724 = metadata !{metadata !322, metadata !327}
!725 = metadata !{i32 786478, i32 0, metadata !307, metadata !"and_reduce", metadata !"and_reduce", metadata !"_ZNK11ap_int_baseILi32ELb0EE10and_reduceEv", metadata !308, i32 1275, metadata !482, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 1275} ; [ DW_TAG_subprogram ]
!726 = metadata !{i32 786478, i32 0, metadata !307, metadata !"nand_reduce", metadata !"nand_reduce", metadata !"_ZNK11ap_int_baseILi32ELb0EE11nand_reduceEv", metadata !308, i32 1276, metadata !482, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 1276} ; [ DW_TAG_subprogram ]
!727 = metadata !{i32 786478, i32 0, metadata !307, metadata !"or_reduce", metadata !"or_reduce", metadata !"_ZNK11ap_int_baseILi32ELb0EE9or_reduceEv", metadata !308, i32 1277, metadata !482, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 1277} ; [ DW_TAG_subprogram ]
!728 = metadata !{i32 786478, i32 0, metadata !307, metadata !"nor_reduce", metadata !"nor_reduce", metadata !"_ZNK11ap_int_baseILi32ELb0EE10nor_reduceEv", metadata !308, i32 1278, metadata !482, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 1278} ; [ DW_TAG_subprogram ]
!729 = metadata !{i32 786478, i32 0, metadata !307, metadata !"xor_reduce", metadata !"xor_reduce", metadata !"_ZNK11ap_int_baseILi32ELb0EE10xor_reduceEv", metadata !308, i32 1279, metadata !482, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 1279} ; [ DW_TAG_subprogram ]
!730 = metadata !{i32 786478, i32 0, metadata !307, metadata !"xnor_reduce", metadata !"xnor_reduce", metadata !"_ZNK11ap_int_baseILi32ELb0EE11xnor_reduceEv", metadata !308, i32 1280, metadata !482, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 1280} ; [ DW_TAG_subprogram ]
!731 = metadata !{i32 786478, i32 0, metadata !307, metadata !"to_string", metadata !"to_string", metadata !"_ZNK11ap_int_baseILi32ELb0EE9to_stringEab", metadata !308, i32 1295, metadata !732, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 1295} ; [ DW_TAG_subprogram ]
!732 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !733, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!733 = metadata !{metadata !701, metadata !480, metadata !352, metadata !200}
!734 = metadata !{i32 786478, i32 0, metadata !303, metadata !"ap_uint", metadata !"ap_uint", metadata !"", metadata !304, i32 197, metadata !735, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 197} ; [ DW_TAG_subprogram ]
!735 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !736, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!736 = metadata !{null, metadata !737}
!737 = metadata !{i32 786447, i32 0, metadata !"", i32 0, i32 0, i64 64, i64 64, i64 0, i32 64, metadata !303} ; [ DW_TAG_pointer_type ]
!738 = metadata !{i32 786478, i32 0, metadata !303, metadata !"ap_uint<32>", metadata !"ap_uint<32>", metadata !"", metadata !304, i32 199, metadata !739, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, metadata !743, i32 0, metadata !186, i32 199} ; [ DW_TAG_subprogram ]
!739 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !740, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!740 = metadata !{null, metadata !737, metadata !741}
!741 = metadata !{i32 786448, null, null, null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !742} ; [ DW_TAG_reference_type ]
!742 = metadata !{i32 786470, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !303} ; [ DW_TAG_const_type ]
!743 = metadata !{metadata !334}
!744 = metadata !{i32 786478, i32 0, metadata !303, metadata !"ap_uint<32>", metadata !"ap_uint<32>", metadata !"", metadata !304, i32 209, metadata !745, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, metadata !743, i32 0, metadata !186, i32 209} ; [ DW_TAG_subprogram ]
!745 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !746, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!746 = metadata !{null, metadata !737, metadata !747}
!747 = metadata !{i32 786448, null, null, null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !748} ; [ DW_TAG_reference_type ]
!748 = metadata !{i32 786470, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !302} ; [ DW_TAG_const_type ]
!749 = metadata !{i32 786478, i32 0, metadata !303, metadata !"ap_uint<32, false>", metadata !"ap_uint<32, false>", metadata !"", metadata !304, i32 253, metadata !750, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, metadata !333, i32 0, metadata !186, i32 253} ; [ DW_TAG_subprogram ]
!750 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !751, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!751 = metadata !{null, metadata !737, metadata !331}
!752 = metadata !{i32 786478, i32 0, metadata !303, metadata !"ap_uint", metadata !"ap_uint", metadata !"", metadata !304, i32 277, metadata !753, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 277} ; [ DW_TAG_subprogram ]
!753 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !754, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!754 = metadata !{null, metadata !737, metadata !200}
!755 = metadata !{i32 786478, i32 0, metadata !303, metadata !"ap_uint", metadata !"ap_uint", metadata !"", metadata !304, i32 278, metadata !756, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 278} ; [ DW_TAG_subprogram ]
!756 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !757, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!757 = metadata !{null, metadata !737, metadata !231}
!758 = metadata !{i32 786478, i32 0, metadata !303, metadata !"ap_uint", metadata !"ap_uint", metadata !"", metadata !304, i32 279, metadata !759, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 279} ; [ DW_TAG_subprogram ]
!759 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !760, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!760 = metadata !{null, metadata !737, metadata !352}
!761 = metadata !{i32 786478, i32 0, metadata !303, metadata !"ap_uint", metadata !"ap_uint", metadata !"", metadata !304, i32 280, metadata !762, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 280} ; [ DW_TAG_subprogram ]
!762 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !763, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!763 = metadata !{null, metadata !737, metadata !357}
!764 = metadata !{i32 786478, i32 0, metadata !303, metadata !"ap_uint", metadata !"ap_uint", metadata !"", metadata !304, i32 281, metadata !765, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 281} ; [ DW_TAG_subprogram ]
!765 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !766, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!766 = metadata !{null, metadata !737, metadata !362}
!767 = metadata !{i32 786478, i32 0, metadata !303, metadata !"ap_uint", metadata !"ap_uint", metadata !"", metadata !304, i32 282, metadata !768, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 282} ; [ DW_TAG_subprogram ]
!768 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !769, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!769 = metadata !{null, metadata !737, metadata !367}
!770 = metadata !{i32 786478, i32 0, metadata !303, metadata !"ap_uint", metadata !"ap_uint", metadata !"", metadata !304, i32 283, metadata !771, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 283} ; [ DW_TAG_subprogram ]
!771 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !772, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!772 = metadata !{null, metadata !737, metadata !322}
!773 = metadata !{i32 786478, i32 0, metadata !303, metadata !"ap_uint", metadata !"ap_uint", metadata !"", metadata !304, i32 284, metadata !774, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 284} ; [ DW_TAG_subprogram ]
!774 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !775, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!775 = metadata !{null, metadata !737, metadata !376}
!776 = metadata !{i32 786478, i32 0, metadata !303, metadata !"ap_uint", metadata !"ap_uint", metadata !"", metadata !304, i32 285, metadata !777, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 285} ; [ DW_TAG_subprogram ]
!777 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !778, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!778 = metadata !{null, metadata !737, metadata !381}
!779 = metadata !{i32 786478, i32 0, metadata !303, metadata !"ap_uint", metadata !"ap_uint", metadata !"", metadata !304, i32 286, metadata !780, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 286} ; [ DW_TAG_subprogram ]
!780 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !781, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!781 = metadata !{null, metadata !737, metadata !386}
!782 = metadata !{i32 786478, i32 0, metadata !303, metadata !"ap_uint", metadata !"ap_uint", metadata !"", metadata !304, i32 287, metadata !783, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 287} ; [ DW_TAG_subprogram ]
!783 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !784, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!784 = metadata !{null, metadata !737, metadata !391}
!785 = metadata !{i32 786478, i32 0, metadata !303, metadata !"ap_uint", metadata !"ap_uint", metadata !"", metadata !304, i32 288, metadata !786, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 288} ; [ DW_TAG_subprogram ]
!786 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !787, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!787 = metadata !{null, metadata !737, metadata !397}
!788 = metadata !{i32 786478, i32 0, metadata !303, metadata !"ap_uint", metadata !"ap_uint", metadata !"", metadata !304, i32 290, metadata !789, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 290} ; [ DW_TAG_subprogram ]
!789 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !790, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!790 = metadata !{null, metadata !737, metadata !411}
!791 = metadata !{i32 786478, i32 0, metadata !303, metadata !"ap_uint", metadata !"ap_uint", metadata !"", metadata !304, i32 291, metadata !792, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 291} ; [ DW_TAG_subprogram ]
!792 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !793, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!793 = metadata !{null, metadata !737, metadata !407}
!794 = metadata !{i32 786478, i32 0, metadata !303, metadata !"ap_uint", metadata !"ap_uint", metadata !"", metadata !304, i32 292, metadata !795, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 292} ; [ DW_TAG_subprogram ]
!795 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !796, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!796 = metadata !{null, metadata !737, metadata !402}
!797 = metadata !{i32 786478, i32 0, metadata !303, metadata !"ap_uint", metadata !"ap_uint", metadata !"", metadata !304, i32 295, metadata !798, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 295} ; [ DW_TAG_subprogram ]
!798 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !799, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!799 = metadata !{null, metadata !737, metadata !229}
!800 = metadata !{i32 786478, i32 0, metadata !303, metadata !"ap_uint", metadata !"ap_uint", metadata !"", metadata !304, i32 297, metadata !801, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 297} ; [ DW_TAG_subprogram ]
!801 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !802, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!802 = metadata !{null, metadata !737, metadata !229, metadata !352}
!803 = metadata !{i32 786478, i32 0, metadata !303, metadata !"operator=", metadata !"operator=", metadata !"_ZN7ap_uintILi32EEaSERKS0_", metadata !304, i32 302, metadata !804, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 302} ; [ DW_TAG_subprogram ]
!804 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !805, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!805 = metadata !{metadata !806, metadata !737, metadata !741}
!806 = metadata !{i32 786448, null, null, null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !303} ; [ DW_TAG_reference_type ]
!807 = metadata !{i32 786478, i32 0, metadata !303, metadata !"operator=", metadata !"operator=", metadata !"_ZN7ap_uintILi32EEaSERVKS0_", metadata !304, i32 308, metadata !808, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 308} ; [ DW_TAG_subprogram ]
!808 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !809, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!809 = metadata !{metadata !806, metadata !737, metadata !747}
!810 = metadata !{i32 786478, i32 0, metadata !303, metadata !"operator=", metadata !"operator=", metadata !"_ZNV7ap_uintILi32EEaSERKS0_", metadata !304, i32 314, metadata !811, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 314} ; [ DW_TAG_subprogram ]
!811 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !812, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!812 = metadata !{null, metadata !813, metadata !741}
!813 = metadata !{i32 786447, i32 0, metadata !"", i32 0, i32 0, i64 64, i64 64, i64 0, i32 64, metadata !302} ; [ DW_TAG_pointer_type ]
!814 = metadata !{i32 786478, i32 0, metadata !303, metadata !"operator=", metadata !"operator=", metadata !"_ZNV7ap_uintILi32EEaSERVKS0_", metadata !304, i32 316, metadata !815, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 316} ; [ DW_TAG_subprogram ]
!815 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !816, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!816 = metadata !{null, metadata !813, metadata !747}
!817 = metadata !{metadata !703}
!818 = metadata !{i32 786478, i32 0, metadata !298, metadata !"sc_fifo_in_if", metadata !"sc_fifo_in_if", metadata !"", metadata !174, i32 216, metadata !819, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 216} ; [ DW_TAG_subprogram ]
!819 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !820, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!820 = metadata !{null, metadata !821, metadata !229}
!821 = metadata !{i32 786447, i32 0, metadata !"", i32 0, i32 0, i64 64, i64 64, i64 0, i32 64, metadata !298} ; [ DW_TAG_pointer_type ]
!822 = metadata !{i32 786478, i32 0, metadata !298, metadata !"read", metadata !"read", metadata !"_ZN7_ap_sc_7sc_core13sc_fifo_in_ifI7ap_uintILi32EEE4readEv", metadata !174, i32 221, metadata !823, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 221} ; [ DW_TAG_subprogram ]
!823 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !824, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!824 = metadata !{metadata !303, metadata !821}
!825 = metadata !{i32 786478, i32 0, metadata !298, metadata !"read", metadata !"read", metadata !"_ZN7_ap_sc_7sc_core13sc_fifo_in_ifI7ap_uintILi32EEE4readERS3_", metadata !174, i32 223, metadata !826, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 223} ; [ DW_TAG_subprogram ]
!826 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !827, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!827 = metadata !{null, metadata !821, metadata !806}
!828 = metadata !{i32 786478, i32 0, metadata !298, metadata !"nb_read", metadata !"nb_read", metadata !"_ZN7_ap_sc_7sc_core13sc_fifo_in_ifI7ap_uintILi32EEE7nb_readERS3_", metadata !174, i32 225, metadata !829, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 225} ; [ DW_TAG_subprogram ]
!829 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !830, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!830 = metadata !{metadata !200, metadata !821, metadata !806}
!831 = metadata !{i32 786478, i32 0, metadata !298, metadata !"num_available", metadata !"num_available", metadata !"_ZNK7_ap_sc_7sc_core13sc_fifo_in_ifI7ap_uintILi32EEE13num_availableEv", metadata !174, i32 228, metadata !832, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 228} ; [ DW_TAG_subprogram ]
!832 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !833, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!833 = metadata !{metadata !200, metadata !834}
!834 = metadata !{i32 786447, i32 0, metadata !"", i32 0, i32 0, i64 64, i64 64, i64 0, i32 64, metadata !835} ; [ DW_TAG_pointer_type ]
!835 = metadata !{i32 786470, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !298} ; [ DW_TAG_const_type ]
!836 = metadata !{metadata !837}
!837 = metadata !{i32 786479, null, metadata !"T", metadata !303, null, i32 0, i32 0} ; [ DW_TAG_template_type_parameter ]
!838 = metadata !{i32 786478, i32 0, metadata !294, metadata !"sc_port_b", metadata !"sc_port_b", metadata !"", metadata !174, i32 272, metadata !839, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 272} ; [ DW_TAG_subprogram ]
!839 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !840, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!840 = metadata !{null, metadata !841}
!841 = metadata !{i32 786447, i32 0, metadata !"", i32 0, i32 0, i64 64, i64 64, i64 0, i32 64, metadata !294} ; [ DW_TAG_pointer_type ]
!842 = metadata !{i32 786478, i32 0, metadata !294, metadata !"sc_port_b", metadata !"sc_port_b", metadata !"", metadata !174, i32 273, metadata !843, i1 false, i1 false, i32 0, i32 0, null, i32 384, i1 false, null, null, i32 0, metadata !186, i32 273} ; [ DW_TAG_subprogram ]
!843 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !844, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!844 = metadata !{null, metadata !841, metadata !229}
!845 = metadata !{i32 786478, i32 0, metadata !294, metadata !"bind", metadata !"bind", metadata !"_ZN7_ap_sc_7sc_core9sc_port_bINS0_13sc_fifo_in_ifI7ap_uintILi32EEEEE4bindERS5_", metadata !174, i32 277, metadata !846, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 277} ; [ DW_TAG_subprogram ]
!846 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !847, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!847 = metadata !{null, metadata !841, metadata !848}
!848 = metadata !{i32 786448, null, null, null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !298} ; [ DW_TAG_reference_type ]
!849 = metadata !{i32 786478, i32 0, metadata !294, metadata !"operator()", metadata !"operator()", metadata !"_ZN7_ap_sc_7sc_core9sc_port_bINS0_13sc_fifo_in_ifI7ap_uintILi32EEEEEclERS5_", metadata !174, i32 280, metadata !846, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 280} ; [ DW_TAG_subprogram ]
!850 = metadata !{i32 786478, i32 0, metadata !294, metadata !"bind", metadata !"bind", metadata !"_ZN7_ap_sc_7sc_core9sc_port_bINS0_13sc_fifo_in_ifI7ap_uintILi32EEEEE4bindERNS0_15sc_prim_channelE", metadata !174, i32 281, metadata !851, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 281} ; [ DW_TAG_subprogram ]
!851 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !852, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!852 = metadata !{null, metadata !841, metadata !240}
!853 = metadata !{i32 786478, i32 0, metadata !294, metadata !"operator()", metadata !"operator()", metadata !"_ZN7_ap_sc_7sc_core9sc_port_bINS0_13sc_fifo_in_ifI7ap_uintILi32EEEEEclERNS0_15sc_prim_channelE", metadata !174, i32 284, metadata !851, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 284} ; [ DW_TAG_subprogram ]
!854 = metadata !{i32 786478, i32 0, metadata !294, metadata !"bind", metadata !"bind", metadata !"_ZN7_ap_sc_7sc_core9sc_port_bINS0_13sc_fifo_in_ifI7ap_uintILi32EEEEE4bindERS6_", metadata !174, i32 285, metadata !855, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 285} ; [ DW_TAG_subprogram ]
!855 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !856, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!856 = metadata !{null, metadata !841, metadata !857}
!857 = metadata !{i32 786448, null, null, null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !294} ; [ DW_TAG_reference_type ]
!858 = metadata !{i32 786478, i32 0, metadata !294, metadata !"operator()", metadata !"operator()", metadata !"_ZN7_ap_sc_7sc_core9sc_port_bINS0_13sc_fifo_in_ifI7ap_uintILi32EEEEEclERS6_", metadata !174, i32 286, metadata !855, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 286} ; [ DW_TAG_subprogram ]
!859 = metadata !{i32 786478, i32 0, metadata !294, metadata !"operator->", metadata !"operator->", metadata !"_ZN7_ap_sc_7sc_core9sc_port_bINS0_13sc_fifo_in_ifI7ap_uintILi32EEEEEptEv", metadata !174, i32 288, metadata !860, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 288} ; [ DW_TAG_subprogram ]
!860 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !861, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!861 = metadata !{metadata !862, metadata !841}
!862 = metadata !{i32 786447, null, metadata !"", null, i32 0, i64 64, i64 64, i64 0, i32 0, metadata !298} ; [ DW_TAG_pointer_type ]
!863 = metadata !{metadata !864}
!864 = metadata !{i32 786479, null, metadata !"IF", metadata !298, null, i32 0, i32 0} ; [ DW_TAG_template_type_parameter ]
!865 = metadata !{i32 786478, i32 0, metadata !291, metadata !"sc_fifo_in", metadata !"sc_fifo_in", metadata !"", metadata !174, i32 480, metadata !866, i1 false, i1 false, i32 0, i32 0, null, i32 384, i1 false, null, null, i32 0, metadata !186, i32 480} ; [ DW_TAG_subprogram ]
!866 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !867, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!867 = metadata !{null, metadata !868}
!868 = metadata !{i32 786447, i32 0, metadata !"", i32 0, i32 0, i64 64, i64 64, i64 0, i32 64, metadata !291} ; [ DW_TAG_pointer_type ]
!869 = metadata !{i32 786478, i32 0, metadata !291, metadata !"sc_fifo_in", metadata !"sc_fifo_in", metadata !"", metadata !174, i32 481, metadata !870, i1 false, i1 false, i32 0, i32 0, null, i32 384, i1 false, null, null, i32 0, metadata !186, i32 481} ; [ DW_TAG_subprogram ]
!870 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !871, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!871 = metadata !{null, metadata !868, metadata !229}
!872 = metadata !{i32 786478, i32 0, metadata !291, metadata !"read", metadata !"read", metadata !"_ZN7_ap_sc_7sc_core10sc_fifo_inI7ap_uintILi32EEE4readEv", metadata !174, i32 482, metadata !873, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 482} ; [ DW_TAG_subprogram ]
!873 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !874, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!874 = metadata !{metadata !303, metadata !868}
!875 = metadata !{i32 786478, i32 0, metadata !291, metadata !"read", metadata !"read", metadata !"_ZN7_ap_sc_7sc_core10sc_fifo_inI7ap_uintILi32EEE4readERS3_", metadata !174, i32 483, metadata !876, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 483} ; [ DW_TAG_subprogram ]
!876 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !877, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!877 = metadata !{null, metadata !868, metadata !806}
!878 = metadata !{i32 786478, i32 0, metadata !291, metadata !"nb_read", metadata !"nb_read", metadata !"_ZN7_ap_sc_7sc_core10sc_fifo_inI7ap_uintILi32EEE7nb_readERS3_", metadata !174, i32 484, metadata !879, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 484} ; [ DW_TAG_subprogram ]
!879 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !880, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!880 = metadata !{metadata !200, metadata !868, metadata !806}
!881 = metadata !{i32 786478, i32 0, metadata !291, metadata !"num_available", metadata !"num_available", metadata !"_ZN7_ap_sc_7sc_core10sc_fifo_inI7ap_uintILi32EEE13num_availableEv", metadata !174, i32 485, metadata !882, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 485} ; [ DW_TAG_subprogram ]
!882 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !883, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!883 = metadata !{metadata !200, metadata !868}
!884 = metadata !{i32 786445, metadata !167, metadata !"fifo_out_0", metadata !168, i32 52, i64 32, i64 32, i64 64, i32 0, metadata !885} ; [ DW_TAG_member ]
!885 = metadata !{i32 786434, metadata !172, metadata !"sc_fifo_out<ap_uint<32> >", metadata !174, i32 489, i64 32, i64 32, i32 0, i32 0, null, metadata !886, i32 0, null, metadata !836} ; [ DW_TAG_class_type ]
!886 = metadata !{metadata !887, metadata !938, metadata !942, metadata !945, metadata !948, metadata !951}
!887 = metadata !{i32 786460, metadata !885, null, metadata !174, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !888} ; [ DW_TAG_inheritance ]
!888 = metadata !{i32 786434, metadata !172, metadata !"sc_port_b<_ap_sc_::sc_core::sc_fifo_out_if<ap_uint<32> > >", metadata !174, i32 268, i64 32, i64 32, i32 0, i32 0, null, metadata !889, i32 0, null, metadata !936} ; [ DW_TAG_class_type ]
!889 = metadata !{metadata !890, metadata !891, metadata !911, metadata !915, metadata !918, metadata !922, metadata !923, metadata !926, metadata !927, metadata !931, metadata !932}
!890 = metadata !{i32 786460, metadata !888, null, metadata !174, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !180} ; [ DW_TAG_inheritance ]
!891 = metadata !{i32 786445, metadata !888, metadata !"m_if", metadata !174, i32 270, i64 32, i64 32, i64 0, i32 0, metadata !892} ; [ DW_TAG_member ]
!892 = metadata !{i32 786434, metadata !172, metadata !"sc_fifo_out_if<ap_uint<32> >", metadata !174, i32 235, i64 32, i64 32, i32 0, i32 0, null, metadata !893, i32 0, null, metadata !836} ; [ DW_TAG_class_type ]
!893 = metadata !{metadata !894, metadata !895, metadata !896, metadata !900, metadata !903, metadata !906}
!894 = metadata !{i32 786460, metadata !892, null, metadata !174, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !192} ; [ DW_TAG_inheritance ]
!895 = metadata !{i32 786445, metadata !892, metadata !"Val", metadata !174, i32 237, i64 32, i64 32, i64 0, i32 0, metadata !302} ; [ DW_TAG_member ]
!896 = metadata !{i32 786478, i32 0, metadata !892, metadata !"sc_fifo_out_if", metadata !"sc_fifo_out_if", metadata !"", metadata !174, i32 239, metadata !897, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 239} ; [ DW_TAG_subprogram ]
!897 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !898, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!898 = metadata !{null, metadata !899, metadata !229}
!899 = metadata !{i32 786447, i32 0, metadata !"", i32 0, i32 0, i64 64, i64 64, i64 0, i32 64, metadata !892} ; [ DW_TAG_pointer_type ]
!900 = metadata !{i32 786478, i32 0, metadata !892, metadata !"write", metadata !"write", metadata !"_ZN7_ap_sc_7sc_core14sc_fifo_out_ifI7ap_uintILi32EEE5writeERKS3_", metadata !174, i32 244, metadata !901, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 244} ; [ DW_TAG_subprogram ]
!901 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !902, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!902 = metadata !{null, metadata !899, metadata !741}
!903 = metadata !{i32 786478, i32 0, metadata !892, metadata !"nb_write", metadata !"nb_write", metadata !"_ZN7_ap_sc_7sc_core14sc_fifo_out_ifI7ap_uintILi32EEE8nb_writeERKS3_", metadata !174, i32 246, metadata !904, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 246} ; [ DW_TAG_subprogram ]
!904 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !905, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!905 = metadata !{metadata !200, metadata !899, metadata !741}
!906 = metadata !{i32 786478, i32 0, metadata !892, metadata !"num_free", metadata !"num_free", metadata !"_ZNK7_ap_sc_7sc_core14sc_fifo_out_ifI7ap_uintILi32EEE8num_freeEv", metadata !174, i32 249, metadata !907, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 249} ; [ DW_TAG_subprogram ]
!907 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !908, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!908 = metadata !{metadata !200, metadata !909}
!909 = metadata !{i32 786447, i32 0, metadata !"", i32 0, i32 0, i64 64, i64 64, i64 0, i32 64, metadata !910} ; [ DW_TAG_pointer_type ]
!910 = metadata !{i32 786470, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !892} ; [ DW_TAG_const_type ]
!911 = metadata !{i32 786478, i32 0, metadata !888, metadata !"sc_port_b", metadata !"sc_port_b", metadata !"", metadata !174, i32 272, metadata !912, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 272} ; [ DW_TAG_subprogram ]
!912 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !913, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!913 = metadata !{null, metadata !914}
!914 = metadata !{i32 786447, i32 0, metadata !"", i32 0, i32 0, i64 64, i64 64, i64 0, i32 64, metadata !888} ; [ DW_TAG_pointer_type ]
!915 = metadata !{i32 786478, i32 0, metadata !888, metadata !"sc_port_b", metadata !"sc_port_b", metadata !"", metadata !174, i32 273, metadata !916, i1 false, i1 false, i32 0, i32 0, null, i32 384, i1 false, null, null, i32 0, metadata !186, i32 273} ; [ DW_TAG_subprogram ]
!916 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !917, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!917 = metadata !{null, metadata !914, metadata !229}
!918 = metadata !{i32 786478, i32 0, metadata !888, metadata !"bind", metadata !"bind", metadata !"_ZN7_ap_sc_7sc_core9sc_port_bINS0_14sc_fifo_out_ifI7ap_uintILi32EEEEE4bindERS5_", metadata !174, i32 277, metadata !919, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 277} ; [ DW_TAG_subprogram ]
!919 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !920, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!920 = metadata !{null, metadata !914, metadata !921}
!921 = metadata !{i32 786448, null, null, null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !892} ; [ DW_TAG_reference_type ]
!922 = metadata !{i32 786478, i32 0, metadata !888, metadata !"operator()", metadata !"operator()", metadata !"_ZN7_ap_sc_7sc_core9sc_port_bINS0_14sc_fifo_out_ifI7ap_uintILi32EEEEEclERS5_", metadata !174, i32 280, metadata !919, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 280} ; [ DW_TAG_subprogram ]
!923 = metadata !{i32 786478, i32 0, metadata !888, metadata !"bind", metadata !"bind", metadata !"_ZN7_ap_sc_7sc_core9sc_port_bINS0_14sc_fifo_out_ifI7ap_uintILi32EEEEE4bindERNS0_15sc_prim_channelE", metadata !174, i32 281, metadata !924, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 281} ; [ DW_TAG_subprogram ]
!924 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !925, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!925 = metadata !{null, metadata !914, metadata !240}
!926 = metadata !{i32 786478, i32 0, metadata !888, metadata !"operator()", metadata !"operator()", metadata !"_ZN7_ap_sc_7sc_core9sc_port_bINS0_14sc_fifo_out_ifI7ap_uintILi32EEEEEclERNS0_15sc_prim_channelE", metadata !174, i32 284, metadata !924, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 284} ; [ DW_TAG_subprogram ]
!927 = metadata !{i32 786478, i32 0, metadata !888, metadata !"bind", metadata !"bind", metadata !"_ZN7_ap_sc_7sc_core9sc_port_bINS0_14sc_fifo_out_ifI7ap_uintILi32EEEEE4bindERS6_", metadata !174, i32 285, metadata !928, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 285} ; [ DW_TAG_subprogram ]
!928 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !929, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!929 = metadata !{null, metadata !914, metadata !930}
!930 = metadata !{i32 786448, null, null, null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !888} ; [ DW_TAG_reference_type ]
!931 = metadata !{i32 786478, i32 0, metadata !888, metadata !"operator()", metadata !"operator()", metadata !"_ZN7_ap_sc_7sc_core9sc_port_bINS0_14sc_fifo_out_ifI7ap_uintILi32EEEEEclERS6_", metadata !174, i32 286, metadata !928, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 286} ; [ DW_TAG_subprogram ]
!932 = metadata !{i32 786478, i32 0, metadata !888, metadata !"operator->", metadata !"operator->", metadata !"_ZN7_ap_sc_7sc_core9sc_port_bINS0_14sc_fifo_out_ifI7ap_uintILi32EEEEEptEv", metadata !174, i32 288, metadata !933, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 288} ; [ DW_TAG_subprogram ]
!933 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !934, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!934 = metadata !{metadata !935, metadata !914}
!935 = metadata !{i32 786447, null, metadata !"", null, i32 0, i64 64, i64 64, i64 0, i32 0, metadata !892} ; [ DW_TAG_pointer_type ]
!936 = metadata !{metadata !937}
!937 = metadata !{i32 786479, null, metadata !"IF", metadata !892, null, i32 0, i32 0} ; [ DW_TAG_template_type_parameter ]
!938 = metadata !{i32 786478, i32 0, metadata !885, metadata !"sc_fifo_out", metadata !"sc_fifo_out", metadata !"", metadata !174, i32 492, metadata !939, i1 false, i1 false, i32 0, i32 0, null, i32 384, i1 false, null, null, i32 0, metadata !186, i32 492} ; [ DW_TAG_subprogram ]
!939 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !940, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!940 = metadata !{null, metadata !941}
!941 = metadata !{i32 786447, i32 0, metadata !"", i32 0, i32 0, i64 64, i64 64, i64 0, i32 64, metadata !885} ; [ DW_TAG_pointer_type ]
!942 = metadata !{i32 786478, i32 0, metadata !885, metadata !"sc_fifo_out", metadata !"sc_fifo_out", metadata !"", metadata !174, i32 493, metadata !943, i1 false, i1 false, i32 0, i32 0, null, i32 384, i1 false, null, null, i32 0, metadata !186, i32 493} ; [ DW_TAG_subprogram ]
!943 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !944, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!944 = metadata !{null, metadata !941, metadata !229}
!945 = metadata !{i32 786478, i32 0, metadata !885, metadata !"write", metadata !"write", metadata !"_ZN7_ap_sc_7sc_core11sc_fifo_outI7ap_uintILi32EEE5writeERKS3_", metadata !174, i32 494, metadata !946, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 494} ; [ DW_TAG_subprogram ]
!946 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !947, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!947 = metadata !{null, metadata !941, metadata !741}
!948 = metadata !{i32 786478, i32 0, metadata !885, metadata !"nb_write", metadata !"nb_write", metadata !"_ZN7_ap_sc_7sc_core11sc_fifo_outI7ap_uintILi32EEE8nb_writeERKS3_", metadata !174, i32 495, metadata !949, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 495} ; [ DW_TAG_subprogram ]
!949 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !950, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!950 = metadata !{metadata !200, metadata !941, metadata !741}
!951 = metadata !{i32 786478, i32 0, metadata !885, metadata !"num_free", metadata !"num_free", metadata !"_ZN7_ap_sc_7sc_core11sc_fifo_outI7ap_uintILi32EEE8num_freeEv", metadata !174, i32 496, metadata !952, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 496} ; [ DW_TAG_subprogram ]
!952 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !953, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!953 = metadata !{metadata !200, metadata !941}
!954 = metadata !{i32 786445, metadata !167, metadata !"RegisterWriteEnablePort_0", metadata !168, i32 53, i64 8, i64 8, i64 96, i32 0, metadata !955} ; [ DW_TAG_member ]
!955 = metadata !{i32 786434, metadata !172, metadata !"sc_out<_ap_sc_::sc_dt::sc_lv<1> >", metadata !174, i32 430, i64 8, i64 8, i32 0, i32 0, null, metadata !956, i32 0, null, metadata !1495} ; [ DW_TAG_class_type ]
!956 = metadata !{metadata !957, metadata !1497, metadata !1501}
!957 = metadata !{i32 786460, metadata !955, null, metadata !174, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !958} ; [ DW_TAG_inheritance ]
!958 = metadata !{i32 786434, metadata !172, metadata !"sc_inout<_ap_sc_::sc_dt::sc_lv<1> >", metadata !174, i32 409, i64 8, i64 8, i32 0, i32 0, null, metadata !959, i32 0, null, metadata !1495} ; [ DW_TAG_class_type ]
!959 = metadata !{metadata !960, metadata !1471, metadata !1475, metadata !1478, metadata !1481, metadata !1485, metadata !1492}
!960 = metadata !{i32 786460, metadata !958, null, metadata !174, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !961} ; [ DW_TAG_inheritance ]
!961 = metadata !{i32 786434, metadata !172, metadata !"sc_port_b<_ap_sc_::sc_core::sc_signal_inout_if<_ap_sc_::sc_dt::sc_lv<1> > >", metadata !174, i32 268, i64 8, i64 8, i32 0, i32 0, null, metadata !962, i32 0, null, metadata !1469} ; [ DW_TAG_class_type ]
!962 = metadata !{metadata !963, metadata !964, metadata !1445, metadata !1449, metadata !1452, metadata !1455, metadata !1456, metadata !1459, metadata !1460, metadata !1464, metadata !1465}
!963 = metadata !{i32 786460, metadata !961, null, metadata !174, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !180} ; [ DW_TAG_inheritance ]
!964 = metadata !{i32 786445, metadata !961, metadata !"m_if", metadata !174, i32 270, i64 8, i64 8, i64 0, i32 0, metadata !965} ; [ DW_TAG_member ]
!965 = metadata !{i32 786434, metadata !172, metadata !"sc_signal_inout_if<_ap_sc_::sc_dt::sc_lv<1> >", metadata !174, i32 193, i64 8, i64 8, i32 0, i32 0, null, metadata !966, i32 0, null, metadata !1433} ; [ DW_TAG_class_type ]
!966 = metadata !{metadata !967, metadata !1435, metadata !1439}
!967 = metadata !{i32 786460, metadata !965, null, metadata !174, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !968} ; [ DW_TAG_inheritance ]
!968 = metadata !{i32 786434, metadata !172, metadata !"sc_signal_in_if<_ap_sc_::sc_dt::sc_lv<1> >", metadata !174, i32 172, i64 8, i64 8, i32 0, i32 0, null, metadata !969, i32 0, null, metadata !1433} ; [ DW_TAG_class_type ]
!969 = metadata !{metadata !970, metadata !971, metadata !1415, metadata !1419, metadata !1422, metadata !1427, metadata !1430}
!970 = metadata !{i32 786460, metadata !968, null, metadata !174, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !192} ; [ DW_TAG_inheritance ]
!971 = metadata !{i32 786445, metadata !968, metadata !"Val", metadata !174, i32 174, i64 8, i64 8, i64 0, i32 0, metadata !972} ; [ DW_TAG_member ]
!972 = metadata !{i32 786485, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !973} ; [ DW_TAG_volatile_type ]
!973 = metadata !{i32 786434, metadata !974, metadata !"sc_lv<1>", metadata !976, i32 880, i64 8, i64 8, i32 0, i32 0, null, metadata !977, i32 0, null, metadata !1413} ; [ DW_TAG_class_type ]
!974 = metadata !{i32 786489, metadata !975, metadata !"sc_dt", metadata !976, i32 67} ; [ DW_TAG_namespace ]
!975 = metadata !{i32 786489, null, metadata !"_ap_sc_", metadata !976, i32 64} ; [ DW_TAG_namespace ]
!976 = metadata !{i32 786473, metadata !"/tools/Xilinx/Vivado/2018.3/common/technology/autopilot/ap_sysc/ap_sc_dt.h", metadata !"/home/julien/education/inf8500/lab2/tutorial_ju/solutions/tutorial/architectures/hw_sw/build/estimation/hls/producer0", null} ; [ DW_TAG_file_type ]
!977 = metadata !{metadata !978, metadata !1327, metadata !1331, metadata !1337, metadata !1342, metadata !1348, metadata !1352, metadata !1358, metadata !1361, metadata !1364, metadata !1367, metadata !1370, metadata !1373, metadata !1376, metadata !1379, metadata !1382, metadata !1385, metadata !1388, metadata !1391, metadata !1394, metadata !1397, metadata !1401, metadata !1406, metadata !1410}
!978 = metadata !{i32 786460, metadata !973, null, metadata !976, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !979} ; [ DW_TAG_inheritance ]
!979 = metadata !{i32 786434, null, metadata !"ap_int_base<1, false>", metadata !308, i32 150, i64 8, i64 8, i32 0, i32 0, null, metadata !980, i32 0, null, metadata !1296} ; [ DW_TAG_class_type ]
!980 = metadata !{metadata !981, metadata !992, metadata !996, metadata !1001, metadata !1007, metadata !1010, metadata !1013, metadata !1016, metadata !1019, metadata !1022, metadata !1025, metadata !1028, metadata !1031, metadata !1034, metadata !1037, metadata !1040, metadata !1043, metadata !1046, metadata !1049, metadata !1052, metadata !1055, metadata !1058, metadata !1062, metadata !1065, metadata !1068, metadata !1069, metadata !1073, metadata !1076, metadata !1079, metadata !1082, metadata !1085, metadata !1088, metadata !1091, metadata !1094, metadata !1097, metadata !1100, metadata !1103, metadata !1106, metadata !1109, metadata !1112, metadata !1117, metadata !1120, metadata !1123, metadata !1126, metadata !1129, metadata !1132, metadata !1135, metadata !1138, metadata !1141, metadata !1144, metadata !1147, metadata !1150, metadata !1153, metadata !1156, metadata !1159, metadata !1163, metadata !1164, metadata !1165, metadata !1166, metadata !1169, metadata !1170, metadata !1173, metadata !1176, metadata !1177, metadata !1180, metadata !1181, metadata !1182, metadata !1183, metadata !1184, metadata !1185, metadata !1186, metadata !1187, metadata !1188, metadata !1275, metadata !1276, metadata !1279, metadata !1290, metadata !1291, metadata !1292, metadata !1297, metadata !1300, metadata !1303, metadata !1306, metadata !1307, metadata !1308, metadata !1312, metadata !1313, metadata !1314, metadata !1315, metadata !1318, metadata !1319, metadata !1320, metadata !1321, metadata !1322, metadata !1323, metadata !1324}
!981 = metadata !{i32 786460, metadata !979, null, metadata !308, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !982} ; [ DW_TAG_inheritance ]
!982 = metadata !{i32 786434, null, metadata !"ssdm_int<1 + 1024 * 0, false>", metadata !312, i32 4, i64 8, i64 8, i32 0, i32 0, null, metadata !983, i32 0, null, metadata !990} ; [ DW_TAG_class_type ]
!983 = metadata !{metadata !984, metadata !986}
!984 = metadata !{i32 786445, metadata !982, metadata !"V", metadata !312, i32 4, i64 1, i64 1, i64 0, i32 0, metadata !985} ; [ DW_TAG_member ]
!985 = metadata !{i32 786468, null, metadata !"uint1", null, i32 0, i64 1, i64 1, i64 0, i32 0, i32 7} ; [ DW_TAG_base_type ]
!986 = metadata !{i32 786478, i32 0, metadata !982, metadata !"ssdm_int", metadata !"ssdm_int", metadata !"", metadata !312, i32 4, metadata !987, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 4} ; [ DW_TAG_subprogram ]
!987 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !988, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!988 = metadata !{null, metadata !989}
!989 = metadata !{i32 786447, i32 0, metadata !"", i32 0, i32 0, i64 64, i64 64, i64 0, i32 64, metadata !982} ; [ DW_TAG_pointer_type ]
!990 = metadata !{metadata !991, metadata !323}
!991 = metadata !{i32 786480, null, metadata !"_AP_N", metadata !322, i64 1, null, i32 0, i32 0} ; [ DW_TAG_template_value_parameter ]
!992 = metadata !{i32 786478, i32 0, metadata !979, metadata !"ap_int_base", metadata !"ap_int_base", metadata !"", metadata !308, i32 206, metadata !993, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 206} ; [ DW_TAG_subprogram ]
!993 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !994, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!994 = metadata !{null, metadata !995}
!995 = metadata !{i32 786447, i32 0, metadata !"", i32 0, i32 0, i64 64, i64 64, i64 0, i32 64, metadata !979} ; [ DW_TAG_pointer_type ]
!996 = metadata !{i32 786478, i32 0, metadata !979, metadata !"ap_int_base<1, false>", metadata !"ap_int_base<1, false>", metadata !"", metadata !308, i32 216, metadata !997, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, metadata !569, i32 0, metadata !186, i32 216} ; [ DW_TAG_subprogram ]
!997 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !998, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!998 = metadata !{null, metadata !995, metadata !999}
!999 = metadata !{i32 786448, null, null, null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !1000} ; [ DW_TAG_reference_type ]
!1000 = metadata !{i32 786470, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !979} ; [ DW_TAG_const_type ]
!1001 = metadata !{i32 786478, i32 0, metadata !979, metadata !"ap_int_base<1, false>", metadata !"ap_int_base<1, false>", metadata !"", metadata !308, i32 222, metadata !1002, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, metadata !569, i32 0, metadata !186, i32 222} ; [ DW_TAG_subprogram ]
!1002 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1003, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1003 = metadata !{null, metadata !995, metadata !1004}
!1004 = metadata !{i32 786448, null, null, null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !1005} ; [ DW_TAG_reference_type ]
!1005 = metadata !{i32 786470, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !1006} ; [ DW_TAG_const_type ]
!1006 = metadata !{i32 786485, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !979} ; [ DW_TAG_volatile_type ]
!1007 = metadata !{i32 786478, i32 0, metadata !979, metadata !"ap_int_base", metadata !"ap_int_base", metadata !"", metadata !308, i32 239, metadata !1008, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 239} ; [ DW_TAG_subprogram ]
!1008 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1009, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1009 = metadata !{null, metadata !995, metadata !216}
!1010 = metadata !{i32 786478, i32 0, metadata !979, metadata !"ap_int_base", metadata !"ap_int_base", metadata !"", metadata !308, i32 240, metadata !1011, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 240} ; [ DW_TAG_subprogram ]
!1011 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1012, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1012 = metadata !{null, metadata !995, metadata !230}
!1013 = metadata !{i32 786478, i32 0, metadata !979, metadata !"ap_int_base", metadata !"ap_int_base", metadata !"", metadata !308, i32 241, metadata !1014, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 241} ; [ DW_TAG_subprogram ]
!1014 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1015, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1015 = metadata !{null, metadata !995, metadata !351}
!1016 = metadata !{i32 786478, i32 0, metadata !979, metadata !"ap_int_base", metadata !"ap_int_base", metadata !"", metadata !308, i32 242, metadata !1017, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 242} ; [ DW_TAG_subprogram ]
!1017 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1018, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1018 = metadata !{null, metadata !995, metadata !356}
!1019 = metadata !{i32 786478, i32 0, metadata !979, metadata !"ap_int_base", metadata !"ap_int_base", metadata !"", metadata !308, i32 243, metadata !1020, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 243} ; [ DW_TAG_subprogram ]
!1020 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1021, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1021 = metadata !{null, metadata !995, metadata !361}
!1022 = metadata !{i32 786478, i32 0, metadata !979, metadata !"ap_int_base", metadata !"ap_int_base", metadata !"", metadata !308, i32 244, metadata !1023, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 244} ; [ DW_TAG_subprogram ]
!1023 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1024, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1024 = metadata !{null, metadata !995, metadata !366}
!1025 = metadata !{i32 786478, i32 0, metadata !979, metadata !"ap_int_base", metadata !"ap_int_base", metadata !"", metadata !308, i32 245, metadata !1026, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 245} ; [ DW_TAG_subprogram ]
!1026 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1027, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1027 = metadata !{null, metadata !995, metadata !371}
!1028 = metadata !{i32 786478, i32 0, metadata !979, metadata !"ap_int_base", metadata !"ap_int_base", metadata !"", metadata !308, i32 246, metadata !1029, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 246} ; [ DW_TAG_subprogram ]
!1029 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1030, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1030 = metadata !{null, metadata !995, metadata !375}
!1031 = metadata !{i32 786478, i32 0, metadata !979, metadata !"ap_int_base", metadata !"ap_int_base", metadata !"", metadata !308, i32 247, metadata !1032, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 247} ; [ DW_TAG_subprogram ]
!1032 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1033, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1033 = metadata !{null, metadata !995, metadata !380}
!1034 = metadata !{i32 786478, i32 0, metadata !979, metadata !"ap_int_base", metadata !"ap_int_base", metadata !"", metadata !308, i32 248, metadata !1035, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 248} ; [ DW_TAG_subprogram ]
!1035 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1036, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1036 = metadata !{null, metadata !995, metadata !385}
!1037 = metadata !{i32 786478, i32 0, metadata !979, metadata !"ap_int_base", metadata !"ap_int_base", metadata !"", metadata !308, i32 249, metadata !1038, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 249} ; [ DW_TAG_subprogram ]
!1038 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1039, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1039 = metadata !{null, metadata !995, metadata !390}
!1040 = metadata !{i32 786478, i32 0, metadata !979, metadata !"ap_int_base", metadata !"ap_int_base", metadata !"", metadata !308, i32 250, metadata !1041, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 250} ; [ DW_TAG_subprogram ]
!1041 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1042, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1042 = metadata !{null, metadata !995, metadata !396}
!1043 = metadata !{i32 786478, i32 0, metadata !979, metadata !"ap_int_base", metadata !"ap_int_base", metadata !"", metadata !308, i32 255, metadata !1044, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 255} ; [ DW_TAG_subprogram ]
!1044 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1045, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1045 = metadata !{null, metadata !995, metadata !402}
!1046 = metadata !{i32 786478, i32 0, metadata !979, metadata !"ap_int_base", metadata !"ap_int_base", metadata !"", metadata !308, i32 261, metadata !1047, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 261} ; [ DW_TAG_subprogram ]
!1047 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1048, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1048 = metadata !{null, metadata !995, metadata !407}
!1049 = metadata !{i32 786478, i32 0, metadata !979, metadata !"ap_int_base", metadata !"ap_int_base", metadata !"", metadata !308, i32 309, metadata !1050, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 309} ; [ DW_TAG_subprogram ]
!1050 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1051, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1051 = metadata !{null, metadata !995, metadata !411}
!1052 = metadata !{i32 786478, i32 0, metadata !979, metadata !"ap_int_base", metadata !"ap_int_base", metadata !"", metadata !308, i32 393, metadata !1053, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 393} ; [ DW_TAG_subprogram ]
!1053 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1054, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1054 = metadata !{null, metadata !995, metadata !229}
!1055 = metadata !{i32 786478, i32 0, metadata !979, metadata !"ap_int_base", metadata !"ap_int_base", metadata !"", metadata !308, i32 399, metadata !1056, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 399} ; [ DW_TAG_subprogram ]
!1056 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1057, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1057 = metadata !{null, metadata !995, metadata !229, metadata !352}
!1058 = metadata !{i32 786478, i32 0, metadata !979, metadata !"read", metadata !"read", metadata !"_ZNV11ap_int_baseILi1ELb0EE4readEv", metadata !308, i32 421, metadata !1059, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 421} ; [ DW_TAG_subprogram ]
!1059 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1060, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1060 = metadata !{metadata !979, metadata !1061}
!1061 = metadata !{i32 786447, i32 0, metadata !"", i32 0, i32 0, i64 64, i64 64, i64 0, i32 64, metadata !1006} ; [ DW_TAG_pointer_type ]
!1062 = metadata !{i32 786478, i32 0, metadata !979, metadata !"write", metadata !"write", metadata !"_ZNV11ap_int_baseILi1ELb0EE5writeERKS0_", metadata !308, i32 428, metadata !1063, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 428} ; [ DW_TAG_subprogram ]
!1063 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1064, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1064 = metadata !{null, metadata !1061, metadata !999}
!1065 = metadata !{i32 786478, i32 0, metadata !979, metadata !"operator=", metadata !"operator=", metadata !"_ZNV11ap_int_baseILi1ELb0EEaSERVKS0_", metadata !308, i32 440, metadata !1066, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 440} ; [ DW_TAG_subprogram ]
!1066 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1067, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1067 = metadata !{null, metadata !1061, metadata !1004}
!1068 = metadata !{i32 786478, i32 0, metadata !979, metadata !"operator=", metadata !"operator=", metadata !"_ZNV11ap_int_baseILi1ELb0EEaSERKS0_", metadata !308, i32 450, metadata !1063, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 450} ; [ DW_TAG_subprogram ]
!1069 = metadata !{i32 786478, i32 0, metadata !979, metadata !"operator=", metadata !"operator=", metadata !"_ZN11ap_int_baseILi1ELb0EEaSERVKS0_", metadata !308, i32 467, metadata !1070, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 467} ; [ DW_TAG_subprogram ]
!1070 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1071, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1071 = metadata !{metadata !1072, metadata !995, metadata !1004}
!1072 = metadata !{i32 786448, null, null, null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !979} ; [ DW_TAG_reference_type ]
!1073 = metadata !{i32 786478, i32 0, metadata !979, metadata !"operator=", metadata !"operator=", metadata !"_ZN11ap_int_baseILi1ELb0EEaSERKS0_", metadata !308, i32 472, metadata !1074, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 472} ; [ DW_TAG_subprogram ]
!1074 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1075, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1075 = metadata !{metadata !1072, metadata !995, metadata !999}
!1076 = metadata !{i32 786478, i32 0, metadata !979, metadata !"operator=", metadata !"operator=", metadata !"_ZN11ap_int_baseILi1ELb0EEaSEb", metadata !308, i32 484, metadata !1077, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 484} ; [ DW_TAG_subprogram ]
!1077 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1078, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1078 = metadata !{metadata !1072, metadata !995, metadata !200}
!1079 = metadata !{i32 786478, i32 0, metadata !979, metadata !"operator=", metadata !"operator=", metadata !"_ZN11ap_int_baseILi1ELb0EEaSEc", metadata !308, i32 485, metadata !1080, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 485} ; [ DW_TAG_subprogram ]
!1080 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1081, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1081 = metadata !{metadata !1072, metadata !995, metadata !231}
!1082 = metadata !{i32 786478, i32 0, metadata !979, metadata !"operator=", metadata !"operator=", metadata !"_ZN11ap_int_baseILi1ELb0EEaSEa", metadata !308, i32 486, metadata !1083, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 486} ; [ DW_TAG_subprogram ]
!1083 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1084, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1084 = metadata !{metadata !1072, metadata !995, metadata !352}
!1085 = metadata !{i32 786478, i32 0, metadata !979, metadata !"operator=", metadata !"operator=", metadata !"_ZN11ap_int_baseILi1ELb0EEaSEh", metadata !308, i32 487, metadata !1086, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 487} ; [ DW_TAG_subprogram ]
!1086 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1087, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1087 = metadata !{metadata !1072, metadata !995, metadata !357}
!1088 = metadata !{i32 786478, i32 0, metadata !979, metadata !"operator=", metadata !"operator=", metadata !"_ZN11ap_int_baseILi1ELb0EEaSEs", metadata !308, i32 488, metadata !1089, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 488} ; [ DW_TAG_subprogram ]
!1089 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1090, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1090 = metadata !{metadata !1072, metadata !995, metadata !362}
!1091 = metadata !{i32 786478, i32 0, metadata !979, metadata !"operator=", metadata !"operator=", metadata !"_ZN11ap_int_baseILi1ELb0EEaSEt", metadata !308, i32 489, metadata !1092, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 489} ; [ DW_TAG_subprogram ]
!1092 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1093, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1093 = metadata !{metadata !1072, metadata !995, metadata !367}
!1094 = metadata !{i32 786478, i32 0, metadata !979, metadata !"operator=", metadata !"operator=", metadata !"_ZN11ap_int_baseILi1ELb0EEaSEi", metadata !308, i32 490, metadata !1095, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 490} ; [ DW_TAG_subprogram ]
!1095 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1096, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1096 = metadata !{metadata !1072, metadata !995, metadata !322}
!1097 = metadata !{i32 786478, i32 0, metadata !979, metadata !"operator=", metadata !"operator=", metadata !"_ZN11ap_int_baseILi1ELb0EEaSEj", metadata !308, i32 491, metadata !1098, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 491} ; [ DW_TAG_subprogram ]
!1098 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1099, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1099 = metadata !{metadata !1072, metadata !995, metadata !376}
!1100 = metadata !{i32 786478, i32 0, metadata !979, metadata !"operator=", metadata !"operator=", metadata !"_ZN11ap_int_baseILi1ELb0EEaSEl", metadata !308, i32 492, metadata !1101, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 492} ; [ DW_TAG_subprogram ]
!1101 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1102, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1102 = metadata !{metadata !1072, metadata !995, metadata !381}
!1103 = metadata !{i32 786478, i32 0, metadata !979, metadata !"operator=", metadata !"operator=", metadata !"_ZN11ap_int_baseILi1ELb0EEaSEm", metadata !308, i32 493, metadata !1104, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 493} ; [ DW_TAG_subprogram ]
!1104 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1105, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1105 = metadata !{metadata !1072, metadata !995, metadata !386}
!1106 = metadata !{i32 786478, i32 0, metadata !979, metadata !"operator=", metadata !"operator=", metadata !"_ZN11ap_int_baseILi1ELb0EEaSEx", metadata !308, i32 494, metadata !1107, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 494} ; [ DW_TAG_subprogram ]
!1107 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1108, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1108 = metadata !{metadata !1072, metadata !995, metadata !391}
!1109 = metadata !{i32 786478, i32 0, metadata !979, metadata !"operator=", metadata !"operator=", metadata !"_ZN11ap_int_baseILi1ELb0EEaSEy", metadata !308, i32 495, metadata !1110, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 495} ; [ DW_TAG_subprogram ]
!1110 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1111, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1111 = metadata !{metadata !1072, metadata !995, metadata !397}
!1112 = metadata !{i32 786478, i32 0, metadata !979, metadata !"operator unsigned long long", metadata !"operator unsigned long long", metadata !"_ZNK11ap_int_baseILi1ELb0EEcvyEv", metadata !308, i32 546, metadata !1113, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 546} ; [ DW_TAG_subprogram ]
!1113 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1114, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1114 = metadata !{metadata !1115, metadata !1116}
!1115 = metadata !{i32 786454, metadata !979, metadata !"RetType", metadata !308, i32 160, i64 0, i64 0, i64 0, i32 0, metadata !476} ; [ DW_TAG_typedef ]
!1116 = metadata !{i32 786447, i32 0, metadata !"", i32 0, i32 0, i64 64, i64 64, i64 0, i32 64, metadata !1000} ; [ DW_TAG_pointer_type ]
!1117 = metadata !{i32 786478, i32 0, metadata !979, metadata !"to_bool", metadata !"to_bool", metadata !"_ZNK11ap_int_baseILi1ELb0EE7to_boolEv", metadata !308, i32 551, metadata !1118, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 551} ; [ DW_TAG_subprogram ]
!1118 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1119, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1119 = metadata !{metadata !200, metadata !1116}
!1120 = metadata !{i32 786478, i32 0, metadata !979, metadata !"to_char", metadata !"to_char", metadata !"_ZNK11ap_int_baseILi1ELb0EE7to_charEv", metadata !308, i32 552, metadata !1121, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 552} ; [ DW_TAG_subprogram ]
!1121 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1122, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1122 = metadata !{metadata !231, metadata !1116}
!1123 = metadata !{i32 786478, i32 0, metadata !979, metadata !"to_schar", metadata !"to_schar", metadata !"_ZNK11ap_int_baseILi1ELb0EE8to_scharEv", metadata !308, i32 553, metadata !1124, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 553} ; [ DW_TAG_subprogram ]
!1124 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1125, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1125 = metadata !{metadata !352, metadata !1116}
!1126 = metadata !{i32 786478, i32 0, metadata !979, metadata !"to_uchar", metadata !"to_uchar", metadata !"_ZNK11ap_int_baseILi1ELb0EE8to_ucharEv", metadata !308, i32 554, metadata !1127, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 554} ; [ DW_TAG_subprogram ]
!1127 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1128, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1128 = metadata !{metadata !357, metadata !1116}
!1129 = metadata !{i32 786478, i32 0, metadata !979, metadata !"to_short", metadata !"to_short", metadata !"_ZNK11ap_int_baseILi1ELb0EE8to_shortEv", metadata !308, i32 555, metadata !1130, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 555} ; [ DW_TAG_subprogram ]
!1130 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1131, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1131 = metadata !{metadata !362, metadata !1116}
!1132 = metadata !{i32 786478, i32 0, metadata !979, metadata !"to_ushort", metadata !"to_ushort", metadata !"_ZNK11ap_int_baseILi1ELb0EE9to_ushortEv", metadata !308, i32 556, metadata !1133, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 556} ; [ DW_TAG_subprogram ]
!1133 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1134, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1134 = metadata !{metadata !367, metadata !1116}
!1135 = metadata !{i32 786478, i32 0, metadata !979, metadata !"to_int", metadata !"to_int", metadata !"_ZNK11ap_int_baseILi1ELb0EE6to_intEv", metadata !308, i32 557, metadata !1136, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 557} ; [ DW_TAG_subprogram ]
!1136 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1137, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1137 = metadata !{metadata !322, metadata !1116}
!1138 = metadata !{i32 786478, i32 0, metadata !979, metadata !"to_uint", metadata !"to_uint", metadata !"_ZNK11ap_int_baseILi1ELb0EE7to_uintEv", metadata !308, i32 558, metadata !1139, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 558} ; [ DW_TAG_subprogram ]
!1139 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1140, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1140 = metadata !{metadata !376, metadata !1116}
!1141 = metadata !{i32 786478, i32 0, metadata !979, metadata !"to_long", metadata !"to_long", metadata !"_ZNK11ap_int_baseILi1ELb0EE7to_longEv", metadata !308, i32 559, metadata !1142, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 559} ; [ DW_TAG_subprogram ]
!1142 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1143, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1143 = metadata !{metadata !381, metadata !1116}
!1144 = metadata !{i32 786478, i32 0, metadata !979, metadata !"to_ulong", metadata !"to_ulong", metadata !"_ZNK11ap_int_baseILi1ELb0EE8to_ulongEv", metadata !308, i32 560, metadata !1145, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 560} ; [ DW_TAG_subprogram ]
!1145 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1146, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1146 = metadata !{metadata !386, metadata !1116}
!1147 = metadata !{i32 786478, i32 0, metadata !979, metadata !"to_int64", metadata !"to_int64", metadata !"_ZNK11ap_int_baseILi1ELb0EE8to_int64Ev", metadata !308, i32 561, metadata !1148, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 561} ; [ DW_TAG_subprogram ]
!1148 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1149, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1149 = metadata !{metadata !391, metadata !1116}
!1150 = metadata !{i32 786478, i32 0, metadata !979, metadata !"to_uint64", metadata !"to_uint64", metadata !"_ZNK11ap_int_baseILi1ELb0EE9to_uint64Ev", metadata !308, i32 562, metadata !1151, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 562} ; [ DW_TAG_subprogram ]
!1151 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1152, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1152 = metadata !{metadata !397, metadata !1116}
!1153 = metadata !{i32 786478, i32 0, metadata !979, metadata !"to_float", metadata !"to_float", metadata !"_ZNK11ap_int_baseILi1ELb0EE8to_floatEv", metadata !308, i32 563, metadata !1154, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 563} ; [ DW_TAG_subprogram ]
!1154 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1155, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1155 = metadata !{metadata !407, metadata !1116}
!1156 = metadata !{i32 786478, i32 0, metadata !979, metadata !"to_double", metadata !"to_double", metadata !"_ZNK11ap_int_baseILi1ELb0EE9to_doubleEv", metadata !308, i32 564, metadata !1157, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 564} ; [ DW_TAG_subprogram ]
!1157 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1158, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1158 = metadata !{metadata !411, metadata !1116}
!1159 = metadata !{i32 786478, i32 0, metadata !979, metadata !"length", metadata !"length", metadata !"_ZNVK11ap_int_baseILi1ELb0EE6lengthEv", metadata !308, i32 588, metadata !1160, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 588} ; [ DW_TAG_subprogram ]
!1160 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1161, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1161 = metadata !{metadata !322, metadata !1162}
!1162 = metadata !{i32 786447, i32 0, metadata !"", i32 0, i32 0, i64 64, i64 64, i64 0, i32 64, metadata !1005} ; [ DW_TAG_pointer_type ]
!1163 = metadata !{i32 786478, i32 0, metadata !979, metadata !"iszero", metadata !"iszero", metadata !"_ZNK11ap_int_baseILi1ELb0EE6iszeroEv", metadata !308, i32 591, metadata !1118, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 591} ; [ DW_TAG_subprogram ]
!1164 = metadata !{i32 786478, i32 0, metadata !979, metadata !"is_zero", metadata !"is_zero", metadata !"_ZNK11ap_int_baseILi1ELb0EE7is_zeroEv", metadata !308, i32 594, metadata !1118, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 594} ; [ DW_TAG_subprogram ]
!1165 = metadata !{i32 786478, i32 0, metadata !979, metadata !"sign", metadata !"sign", metadata !"_ZNK11ap_int_baseILi1ELb0EE4signEv", metadata !308, i32 597, metadata !1118, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 597} ; [ DW_TAG_subprogram ]
!1166 = metadata !{i32 786478, i32 0, metadata !979, metadata !"clear", metadata !"clear", metadata !"_ZN11ap_int_baseILi1ELb0EE5clearEi", metadata !308, i32 606, metadata !1167, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 606} ; [ DW_TAG_subprogram ]
!1167 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1168, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1168 = metadata !{null, metadata !995, metadata !322}
!1169 = metadata !{i32 786478, i32 0, metadata !979, metadata !"invert", metadata !"invert", metadata !"_ZN11ap_int_baseILi1ELb0EE6invertEi", metadata !308, i32 612, metadata !1167, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 612} ; [ DW_TAG_subprogram ]
!1170 = metadata !{i32 786478, i32 0, metadata !979, metadata !"test", metadata !"test", metadata !"_ZNK11ap_int_baseILi1ELb0EE4testEi", metadata !308, i32 621, metadata !1171, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 621} ; [ DW_TAG_subprogram ]
!1171 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1172, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1172 = metadata !{metadata !200, metadata !1116, metadata !322}
!1173 = metadata !{i32 786478, i32 0, metadata !979, metadata !"get", metadata !"get", metadata !"_ZN11ap_int_baseILi1ELb0EE3getEv", metadata !308, i32 627, metadata !1174, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 627} ; [ DW_TAG_subprogram ]
!1174 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1175, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1175 = metadata !{metadata !1072, metadata !995}
!1176 = metadata !{i32 786478, i32 0, metadata !979, metadata !"set", metadata !"set", metadata !"_ZN11ap_int_baseILi1ELb0EE3setEi", metadata !308, i32 630, metadata !1167, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 630} ; [ DW_TAG_subprogram ]
!1177 = metadata !{i32 786478, i32 0, metadata !979, metadata !"set", metadata !"set", metadata !"_ZN11ap_int_baseILi1ELb0EE3setEib", metadata !308, i32 636, metadata !1178, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 636} ; [ DW_TAG_subprogram ]
!1178 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1179, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1179 = metadata !{null, metadata !995, metadata !322, metadata !200}
!1180 = metadata !{i32 786478, i32 0, metadata !979, metadata !"lrotate", metadata !"lrotate", metadata !"_ZN11ap_int_baseILi1ELb0EE7lrotateEi", metadata !308, i32 643, metadata !1095, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 643} ; [ DW_TAG_subprogram ]
!1181 = metadata !{i32 786478, i32 0, metadata !979, metadata !"rrotate", metadata !"rrotate", metadata !"_ZN11ap_int_baseILi1ELb0EE7rrotateEi", metadata !308, i32 658, metadata !1095, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 658} ; [ DW_TAG_subprogram ]
!1182 = metadata !{i32 786478, i32 0, metadata !979, metadata !"reverse", metadata !"reverse", metadata !"_ZN11ap_int_baseILi1ELb0EE7reverseEv", metadata !308, i32 673, metadata !1174, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 673} ; [ DW_TAG_subprogram ]
!1183 = metadata !{i32 786478, i32 0, metadata !979, metadata !"set_bit", metadata !"set_bit", metadata !"_ZN11ap_int_baseILi1ELb0EE7set_bitEib", metadata !308, i32 679, metadata !1178, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 679} ; [ DW_TAG_subprogram ]
!1184 = metadata !{i32 786478, i32 0, metadata !979, metadata !"get_bit", metadata !"get_bit", metadata !"_ZNK11ap_int_baseILi1ELb0EE7get_bitEi", metadata !308, i32 684, metadata !1171, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 684} ; [ DW_TAG_subprogram ]
!1185 = metadata !{i32 786478, i32 0, metadata !979, metadata !"b_not", metadata !"b_not", metadata !"_ZN11ap_int_baseILi1ELb0EE5b_notEv", metadata !308, i32 689, metadata !993, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 689} ; [ DW_TAG_subprogram ]
!1186 = metadata !{i32 786478, i32 0, metadata !979, metadata !"operator++", metadata !"operator++", metadata !"_ZN11ap_int_baseILi1ELb0EEppEv", metadata !308, i32 727, metadata !1174, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 727} ; [ DW_TAG_subprogram ]
!1187 = metadata !{i32 786478, i32 0, metadata !979, metadata !"operator--", metadata !"operator--", metadata !"_ZN11ap_int_baseILi1ELb0EEmmEv", metadata !308, i32 731, metadata !1174, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 731} ; [ DW_TAG_subprogram ]
!1188 = metadata !{i32 786478, i32 0, metadata !979, metadata !"operator++", metadata !"operator++", metadata !"_ZN11ap_int_baseILi1ELb0EEppEi", metadata !308, i32 739, metadata !1189, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 739} ; [ DW_TAG_subprogram ]
!1189 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1190, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1190 = metadata !{metadata !1191, metadata !995, metadata !322}
!1191 = metadata !{i32 786470, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !1192} ; [ DW_TAG_const_type ]
!1192 = metadata !{i32 786454, metadata !1193, metadata !"arg1", metadata !308, i32 198, i64 0, i64 0, i64 0, i32 0, metadata !1194} ; [ DW_TAG_typedef ]
!1193 = metadata !{i32 786434, metadata !979, metadata !"RType<1, false>", metadata !308, i32 165, i64 8, i64 8, i32 0, i32 0, null, metadata !242, i32 0, null, metadata !569} ; [ DW_TAG_class_type ]
!1194 = metadata !{i32 786454, metadata !1195, metadata !"type", metadata !308, i32 147, i64 0, i64 0, i64 0, i32 0, metadata !1196} ; [ DW_TAG_typedef ]
!1195 = metadata !{i32 786434, null, metadata !"_ap_int_factory<1, false>", metadata !308, i32 147, i64 8, i64 8, i32 0, i32 0, null, metadata !242, i32 0, null, metadata !569} ; [ DW_TAG_class_type ]
!1196 = metadata !{i32 786434, null, metadata !"ap_uint<1>", metadata !304, i32 194, i64 8, i64 8, i32 0, i32 0, null, metadata !1197, i32 0, null, metadata !1273} ; [ DW_TAG_class_type ]
!1197 = metadata !{metadata !1198, metadata !1199, metadata !1203, metadata !1206, metadata !1209, metadata !1212, metadata !1215, metadata !1218, metadata !1221, metadata !1224, metadata !1227, metadata !1230, metadata !1233, metadata !1236, metadata !1239, metadata !1242, metadata !1245, metadata !1248, metadata !1251, metadata !1254, metadata !1260, metadata !1266, metadata !1270}
!1198 = metadata !{i32 786460, metadata !1196, null, metadata !304, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !979} ; [ DW_TAG_inheritance ]
!1199 = metadata !{i32 786478, i32 0, metadata !1196, metadata !"ap_uint", metadata !"ap_uint", metadata !"", metadata !304, i32 197, metadata !1200, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 197} ; [ DW_TAG_subprogram ]
!1200 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1201, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1201 = metadata !{null, metadata !1202}
!1202 = metadata !{i32 786447, i32 0, metadata !"", i32 0, i32 0, i64 64, i64 64, i64 0, i32 64, metadata !1196} ; [ DW_TAG_pointer_type ]
!1203 = metadata !{i32 786478, i32 0, metadata !1196, metadata !"ap_uint", metadata !"ap_uint", metadata !"", metadata !304, i32 277, metadata !1204, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 277} ; [ DW_TAG_subprogram ]
!1204 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1205, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1205 = metadata !{null, metadata !1202, metadata !200}
!1206 = metadata !{i32 786478, i32 0, metadata !1196, metadata !"ap_uint", metadata !"ap_uint", metadata !"", metadata !304, i32 278, metadata !1207, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 278} ; [ DW_TAG_subprogram ]
!1207 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1208, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1208 = metadata !{null, metadata !1202, metadata !231}
!1209 = metadata !{i32 786478, i32 0, metadata !1196, metadata !"ap_uint", metadata !"ap_uint", metadata !"", metadata !304, i32 279, metadata !1210, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 279} ; [ DW_TAG_subprogram ]
!1210 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1211, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1211 = metadata !{null, metadata !1202, metadata !352}
!1212 = metadata !{i32 786478, i32 0, metadata !1196, metadata !"ap_uint", metadata !"ap_uint", metadata !"", metadata !304, i32 280, metadata !1213, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 280} ; [ DW_TAG_subprogram ]
!1213 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1214, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1214 = metadata !{null, metadata !1202, metadata !357}
!1215 = metadata !{i32 786478, i32 0, metadata !1196, metadata !"ap_uint", metadata !"ap_uint", metadata !"", metadata !304, i32 281, metadata !1216, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 281} ; [ DW_TAG_subprogram ]
!1216 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1217, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1217 = metadata !{null, metadata !1202, metadata !362}
!1218 = metadata !{i32 786478, i32 0, metadata !1196, metadata !"ap_uint", metadata !"ap_uint", metadata !"", metadata !304, i32 282, metadata !1219, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 282} ; [ DW_TAG_subprogram ]
!1219 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1220, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1220 = metadata !{null, metadata !1202, metadata !367}
!1221 = metadata !{i32 786478, i32 0, metadata !1196, metadata !"ap_uint", metadata !"ap_uint", metadata !"", metadata !304, i32 283, metadata !1222, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 283} ; [ DW_TAG_subprogram ]
!1222 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1223, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1223 = metadata !{null, metadata !1202, metadata !322}
!1224 = metadata !{i32 786478, i32 0, metadata !1196, metadata !"ap_uint", metadata !"ap_uint", metadata !"", metadata !304, i32 284, metadata !1225, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 284} ; [ DW_TAG_subprogram ]
!1225 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1226, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1226 = metadata !{null, metadata !1202, metadata !376}
!1227 = metadata !{i32 786478, i32 0, metadata !1196, metadata !"ap_uint", metadata !"ap_uint", metadata !"", metadata !304, i32 285, metadata !1228, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 285} ; [ DW_TAG_subprogram ]
!1228 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1229, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1229 = metadata !{null, metadata !1202, metadata !381}
!1230 = metadata !{i32 786478, i32 0, metadata !1196, metadata !"ap_uint", metadata !"ap_uint", metadata !"", metadata !304, i32 286, metadata !1231, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 286} ; [ DW_TAG_subprogram ]
!1231 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1232, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1232 = metadata !{null, metadata !1202, metadata !386}
!1233 = metadata !{i32 786478, i32 0, metadata !1196, metadata !"ap_uint", metadata !"ap_uint", metadata !"", metadata !304, i32 287, metadata !1234, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 287} ; [ DW_TAG_subprogram ]
!1234 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1235, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1235 = metadata !{null, metadata !1202, metadata !391}
!1236 = metadata !{i32 786478, i32 0, metadata !1196, metadata !"ap_uint", metadata !"ap_uint", metadata !"", metadata !304, i32 288, metadata !1237, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 288} ; [ DW_TAG_subprogram ]
!1237 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1238, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1238 = metadata !{null, metadata !1202, metadata !397}
!1239 = metadata !{i32 786478, i32 0, metadata !1196, metadata !"ap_uint", metadata !"ap_uint", metadata !"", metadata !304, i32 290, metadata !1240, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 290} ; [ DW_TAG_subprogram ]
!1240 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1241, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1241 = metadata !{null, metadata !1202, metadata !411}
!1242 = metadata !{i32 786478, i32 0, metadata !1196, metadata !"ap_uint", metadata !"ap_uint", metadata !"", metadata !304, i32 291, metadata !1243, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 291} ; [ DW_TAG_subprogram ]
!1243 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1244, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1244 = metadata !{null, metadata !1202, metadata !407}
!1245 = metadata !{i32 786478, i32 0, metadata !1196, metadata !"ap_uint", metadata !"ap_uint", metadata !"", metadata !304, i32 292, metadata !1246, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 292} ; [ DW_TAG_subprogram ]
!1246 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1247, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1247 = metadata !{null, metadata !1202, metadata !402}
!1248 = metadata !{i32 786478, i32 0, metadata !1196, metadata !"ap_uint", metadata !"ap_uint", metadata !"", metadata !304, i32 295, metadata !1249, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 295} ; [ DW_TAG_subprogram ]
!1249 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1250, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1250 = metadata !{null, metadata !1202, metadata !229}
!1251 = metadata !{i32 786478, i32 0, metadata !1196, metadata !"ap_uint", metadata !"ap_uint", metadata !"", metadata !304, i32 297, metadata !1252, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 297} ; [ DW_TAG_subprogram ]
!1252 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1253, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1253 = metadata !{null, metadata !1202, metadata !229, metadata !352}
!1254 = metadata !{i32 786478, i32 0, metadata !1196, metadata !"operator=", metadata !"operator=", metadata !"_ZN7ap_uintILi1EEaSERKS0_", metadata !304, i32 302, metadata !1255, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 302} ; [ DW_TAG_subprogram ]
!1255 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1256, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1256 = metadata !{metadata !1257, metadata !1202, metadata !1258}
!1257 = metadata !{i32 786448, null, null, null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !1196} ; [ DW_TAG_reference_type ]
!1258 = metadata !{i32 786448, null, null, null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !1259} ; [ DW_TAG_reference_type ]
!1259 = metadata !{i32 786470, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !1196} ; [ DW_TAG_const_type ]
!1260 = metadata !{i32 786478, i32 0, metadata !1196, metadata !"operator=", metadata !"operator=", metadata !"_ZN7ap_uintILi1EEaSERVKS0_", metadata !304, i32 308, metadata !1261, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 308} ; [ DW_TAG_subprogram ]
!1261 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1262, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1262 = metadata !{metadata !1257, metadata !1202, metadata !1263}
!1263 = metadata !{i32 786448, null, null, null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !1264} ; [ DW_TAG_reference_type ]
!1264 = metadata !{i32 786470, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !1265} ; [ DW_TAG_const_type ]
!1265 = metadata !{i32 786485, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !1196} ; [ DW_TAG_volatile_type ]
!1266 = metadata !{i32 786478, i32 0, metadata !1196, metadata !"operator=", metadata !"operator=", metadata !"_ZNV7ap_uintILi1EEaSERKS0_", metadata !304, i32 314, metadata !1267, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 314} ; [ DW_TAG_subprogram ]
!1267 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1268, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1268 = metadata !{null, metadata !1269, metadata !1258}
!1269 = metadata !{i32 786447, i32 0, metadata !"", i32 0, i32 0, i64 64, i64 64, i64 0, i32 64, metadata !1265} ; [ DW_TAG_pointer_type ]
!1270 = metadata !{i32 786478, i32 0, metadata !1196, metadata !"operator=", metadata !"operator=", metadata !"_ZNV7ap_uintILi1EEaSERVKS0_", metadata !304, i32 316, metadata !1271, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 316} ; [ DW_TAG_subprogram ]
!1271 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1272, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1272 = metadata !{null, metadata !1269, metadata !1263}
!1273 = metadata !{metadata !1274}
!1274 = metadata !{i32 786480, null, metadata !"_AP_W", metadata !322, i64 1, null, i32 0, i32 0} ; [ DW_TAG_template_value_parameter ]
!1275 = metadata !{i32 786478, i32 0, metadata !979, metadata !"operator--", metadata !"operator--", metadata !"_ZN11ap_int_baseILi1ELb0EEmmEi", metadata !308, i32 744, metadata !1189, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 744} ; [ DW_TAG_subprogram ]
!1276 = metadata !{i32 786478, i32 0, metadata !979, metadata !"operator+", metadata !"operator+", metadata !"_ZNK11ap_int_baseILi1ELb0EEpsEv", metadata !308, i32 753, metadata !1277, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 753} ; [ DW_TAG_subprogram ]
!1277 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1278, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1278 = metadata !{metadata !1192, metadata !1116}
!1279 = metadata !{i32 786478, i32 0, metadata !979, metadata !"operator-", metadata !"operator-", metadata !"_ZNK11ap_int_baseILi1ELb0EEngEv", metadata !308, i32 756, metadata !1280, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 756} ; [ DW_TAG_subprogram ]
!1280 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1281, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1281 = metadata !{metadata !1282, metadata !1116}
!1282 = metadata !{i32 786454, metadata !1193, metadata !"minus", metadata !308, i32 194, i64 0, i64 0, i64 0, i32 0, metadata !1283} ; [ DW_TAG_typedef ]
!1283 = metadata !{i32 786454, metadata !1284, metadata !"type", metadata !308, i32 145, i64 0, i64 0, i64 0, i32 0, metadata !1287} ; [ DW_TAG_typedef ]
!1284 = metadata !{i32 786434, null, metadata !"_ap_int_factory<2, true>", metadata !308, i32 145, i64 8, i64 8, i32 0, i32 0, null, metadata !242, i32 0, null, metadata !1285} ; [ DW_TAG_class_type ]
!1285 = metadata !{metadata !1286, metadata !575}
!1286 = metadata !{i32 786480, null, metadata !"_AP_W2", metadata !322, i64 2, null, i32 0, i32 0} ; [ DW_TAG_template_value_parameter ]
!1287 = metadata !{i32 786434, null, metadata !"ap_int<2>", metadata !577, i32 183, i64 0, i64 0, i32 0, i32 4, null, null, i32 0, null, metadata !1288} ; [ DW_TAG_class_type ]
!1288 = metadata !{metadata !1289}
!1289 = metadata !{i32 786480, null, metadata !"_AP_W", metadata !322, i64 2, null, i32 0, i32 0} ; [ DW_TAG_template_value_parameter ]
!1290 = metadata !{i32 786478, i32 0, metadata !979, metadata !"operator!", metadata !"operator!", metadata !"_ZNK11ap_int_baseILi1ELb0EEntEv", metadata !308, i32 763, metadata !1118, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 763} ; [ DW_TAG_subprogram ]
!1291 = metadata !{i32 786478, i32 0, metadata !979, metadata !"operator~", metadata !"operator~", metadata !"_ZNK11ap_int_baseILi1ELb0EEcoEv", metadata !308, i32 769, metadata !1277, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 769} ; [ DW_TAG_subprogram ]
!1292 = metadata !{i32 786478, i32 0, metadata !979, metadata !"range", metadata !"range", metadata !"_ZN11ap_int_baseILi1ELb0EE5rangeEii", metadata !308, i32 907, metadata !1293, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 907} ; [ DW_TAG_subprogram ]
!1293 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1294, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1294 = metadata !{metadata !1295, metadata !995, metadata !322, metadata !322}
!1295 = metadata !{i32 786434, null, metadata !"ap_range_ref<1, false>", metadata !577, i32 189, i64 0, i64 0, i32 0, i32 4, null, null, i32 0, null, metadata !1296} ; [ DW_TAG_class_type ]
!1296 = metadata !{metadata !1274, metadata !323}
!1297 = metadata !{i32 786478, i32 0, metadata !979, metadata !"range", metadata !"range", metadata !"_ZNK11ap_int_baseILi1ELb0EE5rangeEii", metadata !308, i32 914, metadata !1298, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 914} ; [ DW_TAG_subprogram ]
!1298 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1299, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1299 = metadata !{metadata !1295, metadata !1116, metadata !322, metadata !322}
!1300 = metadata !{i32 786478, i32 0, metadata !979, metadata !"range", metadata !"range", metadata !"_ZN11ap_int_baseILi1ELb0EE5rangeEv", metadata !308, i32 938, metadata !1301, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 938} ; [ DW_TAG_subprogram ]
!1301 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1302, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1302 = metadata !{metadata !1295, metadata !995}
!1303 = metadata !{i32 786478, i32 0, metadata !979, metadata !"range", metadata !"range", metadata !"_ZNK11ap_int_baseILi1ELb0EE5rangeEv", metadata !308, i32 942, metadata !1304, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 942} ; [ DW_TAG_subprogram ]
!1304 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1305, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1305 = metadata !{metadata !1295, metadata !1116}
!1306 = metadata !{i32 786478, i32 0, metadata !979, metadata !"operator()", metadata !"operator()", metadata !"_ZN11ap_int_baseILi1ELb0EEclEii", metadata !308, i32 946, metadata !1293, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 946} ; [ DW_TAG_subprogram ]
!1307 = metadata !{i32 786478, i32 0, metadata !979, metadata !"operator()", metadata !"operator()", metadata !"_ZNK11ap_int_baseILi1ELb0EEclEii", metadata !308, i32 950, metadata !1298, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 950} ; [ DW_TAG_subprogram ]
!1308 = metadata !{i32 786478, i32 0, metadata !979, metadata !"operator[]", metadata !"operator[]", metadata !"_ZN11ap_int_baseILi1ELb0EEixEi", metadata !308, i32 988, metadata !1309, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 988} ; [ DW_TAG_subprogram ]
!1309 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1310, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1310 = metadata !{metadata !1311, metadata !995, metadata !322}
!1311 = metadata !{i32 786434, null, metadata !"ap_bit_ref<1, false>", metadata !577, i32 192, i64 0, i64 0, i32 0, i32 4, null, null, i32 0, null, metadata !1296} ; [ DW_TAG_class_type ]
!1312 = metadata !{i32 786478, i32 0, metadata !979, metadata !"operator[]", metadata !"operator[]", metadata !"_ZNK11ap_int_baseILi1ELb0EEixEi", metadata !308, i32 1004, metadata !1171, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 1004} ; [ DW_TAG_subprogram ]
!1313 = metadata !{i32 786478, i32 0, metadata !979, metadata !"bit", metadata !"bit", metadata !"_ZN11ap_int_baseILi1ELb0EE3bitEi", metadata !308, i32 1017, metadata !1309, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 1017} ; [ DW_TAG_subprogram ]
!1314 = metadata !{i32 786478, i32 0, metadata !979, metadata !"bit", metadata !"bit", metadata !"_ZNK11ap_int_baseILi1ELb0EE3bitEi", metadata !308, i32 1032, metadata !1171, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 1032} ; [ DW_TAG_subprogram ]
!1315 = metadata !{i32 786478, i32 0, metadata !979, metadata !"countLeadingZeros", metadata !"countLeadingZeros", metadata !"_ZN11ap_int_baseILi1ELb0EE17countLeadingZerosEv", metadata !308, i32 1055, metadata !1316, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 1055} ; [ DW_TAG_subprogram ]
!1316 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1317, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1317 = metadata !{metadata !322, metadata !995}
!1318 = metadata !{i32 786478, i32 0, metadata !979, metadata !"and_reduce", metadata !"and_reduce", metadata !"_ZNK11ap_int_baseILi1ELb0EE10and_reduceEv", metadata !308, i32 1275, metadata !1118, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 1275} ; [ DW_TAG_subprogram ]
!1319 = metadata !{i32 786478, i32 0, metadata !979, metadata !"nand_reduce", metadata !"nand_reduce", metadata !"_ZNK11ap_int_baseILi1ELb0EE11nand_reduceEv", metadata !308, i32 1276, metadata !1118, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 1276} ; [ DW_TAG_subprogram ]
!1320 = metadata !{i32 786478, i32 0, metadata !979, metadata !"or_reduce", metadata !"or_reduce", metadata !"_ZNK11ap_int_baseILi1ELb0EE9or_reduceEv", metadata !308, i32 1277, metadata !1118, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 1277} ; [ DW_TAG_subprogram ]
!1321 = metadata !{i32 786478, i32 0, metadata !979, metadata !"nor_reduce", metadata !"nor_reduce", metadata !"_ZNK11ap_int_baseILi1ELb0EE10nor_reduceEv", metadata !308, i32 1278, metadata !1118, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 1278} ; [ DW_TAG_subprogram ]
!1322 = metadata !{i32 786478, i32 0, metadata !979, metadata !"xor_reduce", metadata !"xor_reduce", metadata !"_ZNK11ap_int_baseILi1ELb0EE10xor_reduceEv", metadata !308, i32 1279, metadata !1118, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 1279} ; [ DW_TAG_subprogram ]
!1323 = metadata !{i32 786478, i32 0, metadata !979, metadata !"xnor_reduce", metadata !"xnor_reduce", metadata !"_ZNK11ap_int_baseILi1ELb0EE11xnor_reduceEv", metadata !308, i32 1280, metadata !1118, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 1280} ; [ DW_TAG_subprogram ]
!1324 = metadata !{i32 786478, i32 0, metadata !979, metadata !"to_string", metadata !"to_string", metadata !"_ZNK11ap_int_baseILi1ELb0EE9to_stringEab", metadata !308, i32 1295, metadata !1325, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 1295} ; [ DW_TAG_subprogram ]
!1325 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1326, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1326 = metadata !{metadata !701, metadata !1116, metadata !352, metadata !200}
!1327 = metadata !{i32 786478, i32 0, metadata !973, metadata !"sc_lv", metadata !"sc_lv", metadata !"", metadata !976, i32 883, metadata !1328, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 883} ; [ DW_TAG_subprogram ]
!1328 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1329, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1329 = metadata !{null, metadata !1330}
!1330 = metadata !{i32 786447, i32 0, metadata !"", i32 0, i32 0, i64 64, i64 64, i64 0, i32 64, metadata !973} ; [ DW_TAG_pointer_type ]
!1331 = metadata !{i32 786478, i32 0, metadata !973, metadata !"sc_lv", metadata !"sc_lv", metadata !"", metadata !976, i32 889, metadata !1332, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 889} ; [ DW_TAG_subprogram ]
!1332 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1333, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1333 = metadata !{null, metadata !1330, metadata !1334}
!1334 = metadata !{i32 786448, null, null, null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !1335} ; [ DW_TAG_reference_type ]
!1335 = metadata !{i32 786470, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !1336} ; [ DW_TAG_const_type ]
!1336 = metadata !{i32 786454, metadata !973, metadata !"sc_lv_base", metadata !976, i32 881, i64 0, i64 0, i64 0, i32 0, metadata !979} ; [ DW_TAG_typedef ]
!1337 = metadata !{i32 786478, i32 0, metadata !973, metadata !"sc_lv", metadata !"sc_lv", metadata !"", metadata !976, i32 890, metadata !1338, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 890} ; [ DW_TAG_subprogram ]
!1338 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1339, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1339 = metadata !{null, metadata !1330, metadata !1340}
!1340 = metadata !{i32 786448, null, null, null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !1341} ; [ DW_TAG_reference_type ]
!1341 = metadata !{i32 786485, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !1336} ; [ DW_TAG_volatile_type ]
!1342 = metadata !{i32 786478, i32 0, metadata !973, metadata !"sc_lv<1, false>", metadata !"sc_lv<1, false>", metadata !"", metadata !976, i32 895, metadata !1343, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, metadata !1345, i32 0, metadata !186, i32 895} ; [ DW_TAG_subprogram ]
!1343 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1344, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1344 = metadata !{null, metadata !1330, metadata !999}
!1345 = metadata !{metadata !1346, metadata !1347}
!1346 = metadata !{i32 786480, null, metadata !"_SC_W2", metadata !322, i64 1, null, i32 0, i32 0} ; [ DW_TAG_template_value_parameter ]
!1347 = metadata !{i32 786480, null, metadata !"_SC_S2", metadata !200, i64 0, null, i32 0, i32 0} ; [ DW_TAG_template_value_parameter ]
!1348 = metadata !{i32 786478, i32 0, metadata !973, metadata !"sc_lv<false>", metadata !"sc_lv<false>", metadata !"", metadata !976, i32 898, metadata !1349, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, metadata !1351, i32 0, metadata !186, i32 898} ; [ DW_TAG_subprogram ]
!1349 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1350, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1350 = metadata !{null, metadata !1330, metadata !1072}
!1351 = metadata !{metadata !1347}
!1352 = metadata !{i32 786478, i32 0, metadata !973, metadata !"sc_lv<1>", metadata !"sc_lv<1>", metadata !"", metadata !976, i32 920, metadata !1353, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, metadata !1357, i32 0, metadata !186, i32 920} ; [ DW_TAG_subprogram ]
!1353 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1354, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1354 = metadata !{null, metadata !1330, metadata !1355}
!1355 = metadata !{i32 786448, null, null, null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !1356} ; [ DW_TAG_reference_type ]
!1356 = metadata !{i32 786470, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !973} ; [ DW_TAG_const_type ]
!1357 = metadata !{metadata !1346}
!1358 = metadata !{i32 786478, i32 0, metadata !973, metadata !"sc_lv", metadata !"sc_lv", metadata !"", metadata !976, i32 949, metadata !1359, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 949} ; [ DW_TAG_subprogram ]
!1359 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1360, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1360 = metadata !{null, metadata !1330, metadata !200}
!1361 = metadata !{i32 786478, i32 0, metadata !973, metadata !"sc_lv", metadata !"sc_lv", metadata !"", metadata !976, i32 950, metadata !1362, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 950} ; [ DW_TAG_subprogram ]
!1362 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1363, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1363 = metadata !{null, metadata !1330, metadata !352}
!1364 = metadata !{i32 786478, i32 0, metadata !973, metadata !"sc_lv", metadata !"sc_lv", metadata !"", metadata !976, i32 951, metadata !1365, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 951} ; [ DW_TAG_subprogram ]
!1365 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1366, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1366 = metadata !{null, metadata !1330, metadata !357}
!1367 = metadata !{i32 786478, i32 0, metadata !973, metadata !"sc_lv", metadata !"sc_lv", metadata !"", metadata !976, i32 952, metadata !1368, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 952} ; [ DW_TAG_subprogram ]
!1368 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1369, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1369 = metadata !{null, metadata !1330, metadata !362}
!1370 = metadata !{i32 786478, i32 0, metadata !973, metadata !"sc_lv", metadata !"sc_lv", metadata !"", metadata !976, i32 953, metadata !1371, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 953} ; [ DW_TAG_subprogram ]
!1371 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1372, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1372 = metadata !{null, metadata !1330, metadata !367}
!1373 = metadata !{i32 786478, i32 0, metadata !973, metadata !"sc_lv", metadata !"sc_lv", metadata !"", metadata !976, i32 954, metadata !1374, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 954} ; [ DW_TAG_subprogram ]
!1374 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1375, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1375 = metadata !{null, metadata !1330, metadata !322}
!1376 = metadata !{i32 786478, i32 0, metadata !973, metadata !"sc_lv", metadata !"sc_lv", metadata !"", metadata !976, i32 955, metadata !1377, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 955} ; [ DW_TAG_subprogram ]
!1377 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1378, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1378 = metadata !{null, metadata !1330, metadata !376}
!1379 = metadata !{i32 786478, i32 0, metadata !973, metadata !"sc_lv", metadata !"sc_lv", metadata !"", metadata !976, i32 956, metadata !1380, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 956} ; [ DW_TAG_subprogram ]
!1380 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1381, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1381 = metadata !{null, metadata !1330, metadata !381}
!1382 = metadata !{i32 786478, i32 0, metadata !973, metadata !"sc_lv", metadata !"sc_lv", metadata !"", metadata !976, i32 957, metadata !1383, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 957} ; [ DW_TAG_subprogram ]
!1383 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1384, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1384 = metadata !{null, metadata !1330, metadata !386}
!1385 = metadata !{i32 786478, i32 0, metadata !973, metadata !"sc_lv", metadata !"sc_lv", metadata !"", metadata !976, i32 958, metadata !1386, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 958} ; [ DW_TAG_subprogram ]
!1386 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1387, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1387 = metadata !{null, metadata !1330, metadata !391}
!1388 = metadata !{i32 786478, i32 0, metadata !973, metadata !"sc_lv", metadata !"sc_lv", metadata !"", metadata !976, i32 959, metadata !1389, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 959} ; [ DW_TAG_subprogram ]
!1389 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1390, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1390 = metadata !{null, metadata !1330, metadata !397}
!1391 = metadata !{i32 786478, i32 0, metadata !973, metadata !"sc_lv", metadata !"sc_lv", metadata !"", metadata !976, i32 960, metadata !1392, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 960} ; [ DW_TAG_subprogram ]
!1392 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1393, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1393 = metadata !{null, metadata !1330, metadata !411}
!1394 = metadata !{i32 786478, i32 0, metadata !973, metadata !"sc_lv", metadata !"sc_lv", metadata !"", metadata !976, i32 961, metadata !1395, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 961} ; [ DW_TAG_subprogram ]
!1395 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1396, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1396 = metadata !{null, metadata !1330, metadata !229}
!1397 = metadata !{i32 786478, i32 0, metadata !973, metadata !"operator=", metadata !"operator=", metadata !"_ZNV7_ap_sc_5sc_dt5sc_lvILi1EEaSERKS2_", metadata !976, i32 975, metadata !1398, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 975} ; [ DW_TAG_subprogram ]
!1398 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1399, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1399 = metadata !{null, metadata !1400, metadata !1355}
!1400 = metadata !{i32 786447, i32 0, metadata !"", i32 0, i32 0, i64 64, i64 64, i64 0, i32 64, metadata !972} ; [ DW_TAG_pointer_type ]
!1401 = metadata !{i32 786478, i32 0, metadata !973, metadata !"operator=", metadata !"operator=", metadata !"_ZNV7_ap_sc_5sc_dt5sc_lvILi1EEaSERVKS2_", metadata !976, i32 978, metadata !1402, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 978} ; [ DW_TAG_subprogram ]
!1402 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1403, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1403 = metadata !{null, metadata !1400, metadata !1404}
!1404 = metadata !{i32 786448, null, null, null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !1405} ; [ DW_TAG_reference_type ]
!1405 = metadata !{i32 786470, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !972} ; [ DW_TAG_const_type ]
!1406 = metadata !{i32 786478, i32 0, metadata !973, metadata !"operator=", metadata !"operator=", metadata !"_ZN7_ap_sc_5sc_dt5sc_lvILi1EEaSERVKS2_", metadata !976, i32 984, metadata !1407, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 984} ; [ DW_TAG_subprogram ]
!1407 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1408, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1408 = metadata !{metadata !1409, metadata !1330, metadata !1404}
!1409 = metadata !{i32 786448, null, null, null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !973} ; [ DW_TAG_reference_type ]
!1410 = metadata !{i32 786478, i32 0, metadata !973, metadata !"operator=", metadata !"operator=", metadata !"_ZN7_ap_sc_5sc_dt5sc_lvILi1EEaSERKS2_", metadata !976, i32 988, metadata !1411, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 988} ; [ DW_TAG_subprogram ]
!1411 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1412, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1412 = metadata !{metadata !1409, metadata !1330, metadata !1355}
!1413 = metadata !{metadata !1414}
!1414 = metadata !{i32 786480, null, metadata !"_SC_W", metadata !322, i64 1, null, i32 0, i32 0} ; [ DW_TAG_template_value_parameter ]
!1415 = metadata !{i32 786478, i32 0, metadata !968, metadata !"sc_signal_in_if", metadata !"sc_signal_in_if", metadata !"", metadata !174, i32 176, metadata !1416, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 176} ; [ DW_TAG_subprogram ]
!1416 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1417, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1417 = metadata !{null, metadata !1418}
!1418 = metadata !{i32 786447, i32 0, metadata !"", i32 0, i32 0, i64 64, i64 64, i64 0, i32 64, metadata !968} ; [ DW_TAG_pointer_type ]
!1419 = metadata !{i32 786478, i32 0, metadata !968, metadata !"read", metadata !"read", metadata !"_ZN7_ap_sc_7sc_core15sc_signal_in_ifINS_5sc_dt5sc_lvILi1EEEE4readEv", metadata !174, i32 180, metadata !1420, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 180} ; [ DW_TAG_subprogram ]
!1420 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1421, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1421 = metadata !{metadata !973, metadata !1418}
!1422 = metadata !{i32 786478, i32 0, metadata !968, metadata !"read", metadata !"read", metadata !"_ZNK7_ap_sc_7sc_core15sc_signal_in_ifINS_5sc_dt5sc_lvILi1EEEE4readEv", metadata !174, i32 181, metadata !1423, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 181} ; [ DW_TAG_subprogram ]
!1423 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1424, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1424 = metadata !{metadata !973, metadata !1425}
!1425 = metadata !{i32 786447, i32 0, metadata !"", i32 0, i32 0, i64 64, i64 64, i64 0, i32 64, metadata !1426} ; [ DW_TAG_pointer_type ]
!1426 = metadata !{i32 786470, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !968} ; [ DW_TAG_const_type ]
!1427 = metadata !{i32 786478, i32 0, metadata !968, metadata !"operator sc_lv", metadata !"operator sc_lv", metadata !"_ZN7_ap_sc_7sc_core15sc_signal_in_ifINS_5sc_dt5sc_lvILi1EEEEcvKS4_Ev", metadata !174, i32 187, metadata !1428, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 187} ; [ DW_TAG_subprogram ]
!1428 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1429, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1429 = metadata !{metadata !1356, metadata !1418}
!1430 = metadata !{i32 786478, i32 0, metadata !968, metadata !"operator sc_lv", metadata !"operator sc_lv", metadata !"_ZNK7_ap_sc_7sc_core15sc_signal_in_ifINS_5sc_dt5sc_lvILi1EEEEcvKS4_Ev", metadata !174, i32 188, metadata !1431, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 188} ; [ DW_TAG_subprogram ]
!1431 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1432, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1432 = metadata !{metadata !1356, metadata !1425}
!1433 = metadata !{metadata !1434}
!1434 = metadata !{i32 786479, null, metadata !"T", metadata !973, null, i32 0, i32 0} ; [ DW_TAG_template_type_parameter ]
!1435 = metadata !{i32 786478, i32 0, metadata !965, metadata !"sc_signal_inout_if", metadata !"sc_signal_inout_if", metadata !"", metadata !174, i32 197, metadata !1436, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 197} ; [ DW_TAG_subprogram ]
!1436 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1437, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1437 = metadata !{null, metadata !1438}
!1438 = metadata !{i32 786447, i32 0, metadata !"", i32 0, i32 0, i64 64, i64 64, i64 0, i32 64, metadata !965} ; [ DW_TAG_pointer_type ]
!1439 = metadata !{i32 786478, i32 0, metadata !965, metadata !"operator=", metadata !"operator=", metadata !"_ZN7_ap_sc_7sc_core18sc_signal_inout_ifINS_5sc_dt5sc_lvILi1EEEEaSERKS5_", metadata !174, i32 199, metadata !1440, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 199} ; [ DW_TAG_subprogram ]
!1440 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1441, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1441 = metadata !{metadata !1442, metadata !1438, metadata !1443}
!1442 = metadata !{i32 786448, null, null, null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !965} ; [ DW_TAG_reference_type ]
!1443 = metadata !{i32 786448, null, null, null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !1444} ; [ DW_TAG_reference_type ]
!1444 = metadata !{i32 786470, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !965} ; [ DW_TAG_const_type ]
!1445 = metadata !{i32 786478, i32 0, metadata !961, metadata !"sc_port_b", metadata !"sc_port_b", metadata !"", metadata !174, i32 272, metadata !1446, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 272} ; [ DW_TAG_subprogram ]
!1446 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1447, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1447 = metadata !{null, metadata !1448}
!1448 = metadata !{i32 786447, i32 0, metadata !"", i32 0, i32 0, i64 64, i64 64, i64 0, i32 64, metadata !961} ; [ DW_TAG_pointer_type ]
!1449 = metadata !{i32 786478, i32 0, metadata !961, metadata !"sc_port_b", metadata !"sc_port_b", metadata !"", metadata !174, i32 273, metadata !1450, i1 false, i1 false, i32 0, i32 0, null, i32 384, i1 false, null, null, i32 0, metadata !186, i32 273} ; [ DW_TAG_subprogram ]
!1450 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1451, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1451 = metadata !{null, metadata !1448, metadata !229}
!1452 = metadata !{i32 786478, i32 0, metadata !961, metadata !"bind", metadata !"bind", metadata !"_ZN7_ap_sc_7sc_core9sc_port_bINS0_18sc_signal_inout_ifINS_5sc_dt5sc_lvILi1EEEEEE4bindERS6_", metadata !174, i32 277, metadata !1453, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 277} ; [ DW_TAG_subprogram ]
!1453 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1454, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1454 = metadata !{null, metadata !1448, metadata !1442}
!1455 = metadata !{i32 786478, i32 0, metadata !961, metadata !"operator()", metadata !"operator()", metadata !"_ZN7_ap_sc_7sc_core9sc_port_bINS0_18sc_signal_inout_ifINS_5sc_dt5sc_lvILi1EEEEEEclERS6_", metadata !174, i32 280, metadata !1453, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 280} ; [ DW_TAG_subprogram ]
!1456 = metadata !{i32 786478, i32 0, metadata !961, metadata !"bind", metadata !"bind", metadata !"_ZN7_ap_sc_7sc_core9sc_port_bINS0_18sc_signal_inout_ifINS_5sc_dt5sc_lvILi1EEEEEE4bindERNS0_15sc_prim_channelE", metadata !174, i32 281, metadata !1457, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 281} ; [ DW_TAG_subprogram ]
!1457 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1458, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1458 = metadata !{null, metadata !1448, metadata !240}
!1459 = metadata !{i32 786478, i32 0, metadata !961, metadata !"operator()", metadata !"operator()", metadata !"_ZN7_ap_sc_7sc_core9sc_port_bINS0_18sc_signal_inout_ifINS_5sc_dt5sc_lvILi1EEEEEEclERNS0_15sc_prim_channelE", metadata !174, i32 284, metadata !1457, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 284} ; [ DW_TAG_subprogram ]
!1460 = metadata !{i32 786478, i32 0, metadata !961, metadata !"bind", metadata !"bind", metadata !"_ZN7_ap_sc_7sc_core9sc_port_bINS0_18sc_signal_inout_ifINS_5sc_dt5sc_lvILi1EEEEEE4bindERS7_", metadata !174, i32 285, metadata !1461, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 285} ; [ DW_TAG_subprogram ]
!1461 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1462, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1462 = metadata !{null, metadata !1448, metadata !1463}
!1463 = metadata !{i32 786448, null, null, null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !961} ; [ DW_TAG_reference_type ]
!1464 = metadata !{i32 786478, i32 0, metadata !961, metadata !"operator()", metadata !"operator()", metadata !"_ZN7_ap_sc_7sc_core9sc_port_bINS0_18sc_signal_inout_ifINS_5sc_dt5sc_lvILi1EEEEEEclERS7_", metadata !174, i32 286, metadata !1461, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 286} ; [ DW_TAG_subprogram ]
!1465 = metadata !{i32 786478, i32 0, metadata !961, metadata !"operator->", metadata !"operator->", metadata !"_ZN7_ap_sc_7sc_core9sc_port_bINS0_18sc_signal_inout_ifINS_5sc_dt5sc_lvILi1EEEEEEptEv", metadata !174, i32 288, metadata !1466, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 288} ; [ DW_TAG_subprogram ]
!1466 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1467, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1467 = metadata !{metadata !1468, metadata !1448}
!1468 = metadata !{i32 786447, null, metadata !"", null, i32 0, i64 64, i64 64, i64 0, i32 0, metadata !965} ; [ DW_TAG_pointer_type ]
!1469 = metadata !{metadata !1470}
!1470 = metadata !{i32 786479, null, metadata !"IF", metadata !965, null, i32 0, i32 0} ; [ DW_TAG_template_type_parameter ]
!1471 = metadata !{i32 786478, i32 0, metadata !958, metadata !"sc_inout", metadata !"sc_inout", metadata !"", metadata !174, i32 413, metadata !1472, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 413} ; [ DW_TAG_subprogram ]
!1472 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1473, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1473 = metadata !{null, metadata !1474}
!1474 = metadata !{i32 786447, i32 0, metadata !"", i32 0, i32 0, i64 64, i64 64, i64 0, i32 64, metadata !958} ; [ DW_TAG_pointer_type ]
!1475 = metadata !{i32 786478, i32 0, metadata !958, metadata !"sc_inout", metadata !"sc_inout", metadata !"", metadata !174, i32 414, metadata !1476, i1 false, i1 false, i32 0, i32 0, null, i32 384, i1 false, null, null, i32 0, metadata !186, i32 414} ; [ DW_TAG_subprogram ]
!1476 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1477, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1477 = metadata !{null, metadata !1474, metadata !229}
!1478 = metadata !{i32 786478, i32 0, metadata !958, metadata !"write", metadata !"write", metadata !"_ZN7_ap_sc_7sc_core8sc_inoutINS_5sc_dt5sc_lvILi1EEEE5writeERKS4_", metadata !174, i32 417, metadata !1479, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 417} ; [ DW_TAG_subprogram ]
!1479 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1480, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1480 = metadata !{null, metadata !1474, metadata !1355}
!1481 = metadata !{i32 786478, i32 0, metadata !958, metadata !"read", metadata !"read", metadata !"_ZN7_ap_sc_7sc_core8sc_inoutINS_5sc_dt5sc_lvILi1EEEE4readEv", metadata !174, i32 421, metadata !1482, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 421} ; [ DW_TAG_subprogram ]
!1482 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1483, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1483 = metadata !{metadata !1484, metadata !1474}
!1484 = metadata !{i32 786454, metadata !958, metadata !"data_type", metadata !174, i32 411, i64 0, i64 0, i64 0, i32 0, metadata !973} ; [ DW_TAG_typedef ]
!1485 = metadata !{i32 786478, i32 0, metadata !958, metadata !"operator const struct _ap_sc_::sc_dt::sc_lv<1> &", metadata !"operator const struct _ap_sc_::sc_dt::sc_lv<1> &", metadata !"_ZNK7_ap_sc_7sc_core8sc_inoutINS_5sc_dt5sc_lvILi1EEEEcvRKS4_Ev", metadata !174, i32 422, metadata !1486, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 422} ; [ DW_TAG_subprogram ]
!1486 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1487, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1487 = metadata !{metadata !1488, metadata !1490}
!1488 = metadata !{i32 786448, null, null, null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !1489} ; [ DW_TAG_reference_type ]
!1489 = metadata !{i32 786470, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !1484} ; [ DW_TAG_const_type ]
!1490 = metadata !{i32 786447, i32 0, metadata !"", i32 0, i32 0, i64 64, i64 64, i64 0, i32 64, metadata !1491} ; [ DW_TAG_pointer_type ]
!1491 = metadata !{i32 786470, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !958} ; [ DW_TAG_const_type ]
!1492 = metadata !{i32 786478, i32 0, metadata !958, metadata !"operator sc_lv", metadata !"operator sc_lv", metadata !"_ZN7_ap_sc_7sc_core8sc_inoutINS_5sc_dt5sc_lvILi1EEEEcvKS4_Ev", metadata !174, i32 425, metadata !1493, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 425} ; [ DW_TAG_subprogram ]
!1493 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1494, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1494 = metadata !{metadata !1489, metadata !1474}
!1495 = metadata !{metadata !1496}
!1496 = metadata !{i32 786479, null, metadata !"_T", metadata !973, null, i32 0, i32 0} ; [ DW_TAG_template_type_parameter ]
!1497 = metadata !{i32 786478, i32 0, metadata !955, metadata !"sc_out", metadata !"sc_out", metadata !"", metadata !174, i32 433, metadata !1498, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 433} ; [ DW_TAG_subprogram ]
!1498 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1499, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1499 = metadata !{null, metadata !1500}
!1500 = metadata !{i32 786447, i32 0, metadata !"", i32 0, i32 0, i64 64, i64 64, i64 0, i32 64, metadata !955} ; [ DW_TAG_pointer_type ]
!1501 = metadata !{i32 786478, i32 0, metadata !955, metadata !"sc_out", metadata !"sc_out", metadata !"", metadata !174, i32 434, metadata !1502, i1 false, i1 false, i32 0, i32 0, null, i32 384, i1 false, null, null, i32 0, metadata !186, i32 434} ; [ DW_TAG_subprogram ]
!1502 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1503, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1503 = metadata !{null, metadata !1500, metadata !229}
!1504 = metadata !{i32 786445, metadata !167, metadata !"RegisterWriteDataPort_0", metadata !168, i32 54, i64 32, i64 32, i64 128, i32 0, metadata !1505} ; [ DW_TAG_member ]
!1505 = metadata !{i32 786434, metadata !172, metadata !"sc_out<_ap_sc_::sc_dt::sc_lv<32> >", metadata !174, i32 430, i64 32, i64 32, i32 0, i32 0, null, metadata !1506, i32 0, null, metadata !1692} ; [ DW_TAG_class_type ]
!1506 = metadata !{metadata !1507, metadata !1694, metadata !1698}
!1507 = metadata !{i32 786460, metadata !1505, null, metadata !174, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !1508} ; [ DW_TAG_inheritance ]
!1508 = metadata !{i32 786434, metadata !172, metadata !"sc_inout<_ap_sc_::sc_dt::sc_lv<32> >", metadata !174, i32 409, i64 32, i64 32, i32 0, i32 0, null, metadata !1509, i32 0, null, metadata !1692} ; [ DW_TAG_class_type ]
!1509 = metadata !{metadata !1510, metadata !1668, metadata !1672, metadata !1675, metadata !1678, metadata !1682, metadata !1689}
!1510 = metadata !{i32 786460, metadata !1508, null, metadata !174, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !1511} ; [ DW_TAG_inheritance ]
!1511 = metadata !{i32 786434, metadata !172, metadata !"sc_port_b<_ap_sc_::sc_core::sc_signal_inout_if<_ap_sc_::sc_dt::sc_lv<32> > >", metadata !174, i32 268, i64 32, i64 32, i32 0, i32 0, null, metadata !1512, i32 0, null, metadata !1666} ; [ DW_TAG_class_type ]
!1512 = metadata !{metadata !1513, metadata !1514, metadata !1642, metadata !1646, metadata !1649, metadata !1652, metadata !1653, metadata !1656, metadata !1657, metadata !1661, metadata !1662}
!1513 = metadata !{i32 786460, metadata !1511, null, metadata !174, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !180} ; [ DW_TAG_inheritance ]
!1514 = metadata !{i32 786445, metadata !1511, metadata !"m_if", metadata !174, i32 270, i64 32, i64 32, i64 0, i32 0, metadata !1515} ; [ DW_TAG_member ]
!1515 = metadata !{i32 786434, metadata !172, metadata !"sc_signal_inout_if<_ap_sc_::sc_dt::sc_lv<32> >", metadata !174, i32 193, i64 32, i64 32, i32 0, i32 0, null, metadata !1516, i32 0, null, metadata !1630} ; [ DW_TAG_class_type ]
!1516 = metadata !{metadata !1517, metadata !1632, metadata !1636}
!1517 = metadata !{i32 786460, metadata !1515, null, metadata !174, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !1518} ; [ DW_TAG_inheritance ]
!1518 = metadata !{i32 786434, metadata !172, metadata !"sc_signal_in_if<_ap_sc_::sc_dt::sc_lv<32> >", metadata !174, i32 172, i64 32, i64 32, i32 0, i32 0, null, metadata !1519, i32 0, null, metadata !1630} ; [ DW_TAG_class_type ]
!1519 = metadata !{metadata !1520, metadata !1521, metadata !1612, metadata !1616, metadata !1619, metadata !1624, metadata !1627}
!1520 = metadata !{i32 786460, metadata !1518, null, metadata !174, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !192} ; [ DW_TAG_inheritance ]
!1521 = metadata !{i32 786445, metadata !1518, metadata !"Val", metadata !174, i32 174, i64 32, i64 32, i64 0, i32 0, metadata !1522} ; [ DW_TAG_member ]
!1522 = metadata !{i32 786485, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !1523} ; [ DW_TAG_volatile_type ]
!1523 = metadata !{i32 786434, metadata !974, metadata !"sc_lv<32>", metadata !976, i32 880, i64 32, i64 32, i32 0, i32 0, null, metadata !1524, i32 0, null, metadata !1610} ; [ DW_TAG_class_type ]
!1524 = metadata !{metadata !1525, metadata !1526, metadata !1530, metadata !1536, metadata !1541, metadata !1546, metadata !1549, metadata !1555, metadata !1558, metadata !1561, metadata !1564, metadata !1567, metadata !1570, metadata !1573, metadata !1576, metadata !1579, metadata !1582, metadata !1585, metadata !1588, metadata !1591, metadata !1594, metadata !1598, metadata !1603, metadata !1607}
!1525 = metadata !{i32 786460, metadata !1523, null, metadata !976, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !307} ; [ DW_TAG_inheritance ]
!1526 = metadata !{i32 786478, i32 0, metadata !1523, metadata !"sc_lv", metadata !"sc_lv", metadata !"", metadata !976, i32 883, metadata !1527, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 883} ; [ DW_TAG_subprogram ]
!1527 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1528, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1528 = metadata !{null, metadata !1529}
!1529 = metadata !{i32 786447, i32 0, metadata !"", i32 0, i32 0, i64 64, i64 64, i64 0, i32 64, metadata !1523} ; [ DW_TAG_pointer_type ]
!1530 = metadata !{i32 786478, i32 0, metadata !1523, metadata !"sc_lv", metadata !"sc_lv", metadata !"", metadata !976, i32 889, metadata !1531, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 889} ; [ DW_TAG_subprogram ]
!1531 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1532, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1532 = metadata !{null, metadata !1529, metadata !1533}
!1533 = metadata !{i32 786448, null, null, null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !1534} ; [ DW_TAG_reference_type ]
!1534 = metadata !{i32 786470, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !1535} ; [ DW_TAG_const_type ]
!1535 = metadata !{i32 786454, metadata !1523, metadata !"sc_lv_base", metadata !976, i32 881, i64 0, i64 0, i64 0, i32 0, metadata !307} ; [ DW_TAG_typedef ]
!1536 = metadata !{i32 786478, i32 0, metadata !1523, metadata !"sc_lv", metadata !"sc_lv", metadata !"", metadata !976, i32 890, metadata !1537, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 890} ; [ DW_TAG_subprogram ]
!1537 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1538, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1538 = metadata !{null, metadata !1529, metadata !1539}
!1539 = metadata !{i32 786448, null, null, null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !1540} ; [ DW_TAG_reference_type ]
!1540 = metadata !{i32 786485, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !1535} ; [ DW_TAG_volatile_type ]
!1541 = metadata !{i32 786478, i32 0, metadata !1523, metadata !"sc_lv<32, false>", metadata !"sc_lv<32, false>", metadata !"", metadata !976, i32 895, metadata !1542, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, metadata !1544, i32 0, metadata !186, i32 895} ; [ DW_TAG_subprogram ]
!1542 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1543, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1543 = metadata !{null, metadata !1529, metadata !331}
!1544 = metadata !{metadata !1545, metadata !1347}
!1545 = metadata !{i32 786480, null, metadata !"_SC_W2", metadata !322, i64 32, null, i32 0, i32 0} ; [ DW_TAG_template_value_parameter ]
!1546 = metadata !{i32 786478, i32 0, metadata !1523, metadata !"sc_lv<false>", metadata !"sc_lv<false>", metadata !"", metadata !976, i32 898, metadata !1547, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, metadata !1351, i32 0, metadata !186, i32 898} ; [ DW_TAG_subprogram ]
!1547 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1548, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1548 = metadata !{null, metadata !1529, metadata !432}
!1549 = metadata !{i32 786478, i32 0, metadata !1523, metadata !"sc_lv<32>", metadata !"sc_lv<32>", metadata !"", metadata !976, i32 920, metadata !1550, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, metadata !1554, i32 0, metadata !186, i32 920} ; [ DW_TAG_subprogram ]
!1550 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1551, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1551 = metadata !{null, metadata !1529, metadata !1552}
!1552 = metadata !{i32 786448, null, null, null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !1553} ; [ DW_TAG_reference_type ]
!1553 = metadata !{i32 786470, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !1523} ; [ DW_TAG_const_type ]
!1554 = metadata !{metadata !1545}
!1555 = metadata !{i32 786478, i32 0, metadata !1523, metadata !"sc_lv", metadata !"sc_lv", metadata !"", metadata !976, i32 949, metadata !1556, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 949} ; [ DW_TAG_subprogram ]
!1556 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1557, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1557 = metadata !{null, metadata !1529, metadata !200}
!1558 = metadata !{i32 786478, i32 0, metadata !1523, metadata !"sc_lv", metadata !"sc_lv", metadata !"", metadata !976, i32 950, metadata !1559, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 950} ; [ DW_TAG_subprogram ]
!1559 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1560, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1560 = metadata !{null, metadata !1529, metadata !352}
!1561 = metadata !{i32 786478, i32 0, metadata !1523, metadata !"sc_lv", metadata !"sc_lv", metadata !"", metadata !976, i32 951, metadata !1562, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 951} ; [ DW_TAG_subprogram ]
!1562 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1563, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1563 = metadata !{null, metadata !1529, metadata !357}
!1564 = metadata !{i32 786478, i32 0, metadata !1523, metadata !"sc_lv", metadata !"sc_lv", metadata !"", metadata !976, i32 952, metadata !1565, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 952} ; [ DW_TAG_subprogram ]
!1565 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1566, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1566 = metadata !{null, metadata !1529, metadata !362}
!1567 = metadata !{i32 786478, i32 0, metadata !1523, metadata !"sc_lv", metadata !"sc_lv", metadata !"", metadata !976, i32 953, metadata !1568, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 953} ; [ DW_TAG_subprogram ]
!1568 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1569, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1569 = metadata !{null, metadata !1529, metadata !367}
!1570 = metadata !{i32 786478, i32 0, metadata !1523, metadata !"sc_lv", metadata !"sc_lv", metadata !"", metadata !976, i32 954, metadata !1571, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 954} ; [ DW_TAG_subprogram ]
!1571 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1572, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1572 = metadata !{null, metadata !1529, metadata !322}
!1573 = metadata !{i32 786478, i32 0, metadata !1523, metadata !"sc_lv", metadata !"sc_lv", metadata !"", metadata !976, i32 955, metadata !1574, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 955} ; [ DW_TAG_subprogram ]
!1574 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1575, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1575 = metadata !{null, metadata !1529, metadata !376}
!1576 = metadata !{i32 786478, i32 0, metadata !1523, metadata !"sc_lv", metadata !"sc_lv", metadata !"", metadata !976, i32 956, metadata !1577, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 956} ; [ DW_TAG_subprogram ]
!1577 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1578, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1578 = metadata !{null, metadata !1529, metadata !381}
!1579 = metadata !{i32 786478, i32 0, metadata !1523, metadata !"sc_lv", metadata !"sc_lv", metadata !"", metadata !976, i32 957, metadata !1580, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 957} ; [ DW_TAG_subprogram ]
!1580 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1581, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1581 = metadata !{null, metadata !1529, metadata !386}
!1582 = metadata !{i32 786478, i32 0, metadata !1523, metadata !"sc_lv", metadata !"sc_lv", metadata !"", metadata !976, i32 958, metadata !1583, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 958} ; [ DW_TAG_subprogram ]
!1583 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1584, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1584 = metadata !{null, metadata !1529, metadata !391}
!1585 = metadata !{i32 786478, i32 0, metadata !1523, metadata !"sc_lv", metadata !"sc_lv", metadata !"", metadata !976, i32 959, metadata !1586, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 959} ; [ DW_TAG_subprogram ]
!1586 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1587, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1587 = metadata !{null, metadata !1529, metadata !397}
!1588 = metadata !{i32 786478, i32 0, metadata !1523, metadata !"sc_lv", metadata !"sc_lv", metadata !"", metadata !976, i32 960, metadata !1589, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 960} ; [ DW_TAG_subprogram ]
!1589 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1590, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1590 = metadata !{null, metadata !1529, metadata !411}
!1591 = metadata !{i32 786478, i32 0, metadata !1523, metadata !"sc_lv", metadata !"sc_lv", metadata !"", metadata !976, i32 961, metadata !1592, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 961} ; [ DW_TAG_subprogram ]
!1592 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1593, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1593 = metadata !{null, metadata !1529, metadata !229}
!1594 = metadata !{i32 786478, i32 0, metadata !1523, metadata !"operator=", metadata !"operator=", metadata !"_ZNV7_ap_sc_5sc_dt5sc_lvILi32EEaSERKS2_", metadata !976, i32 975, metadata !1595, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 975} ; [ DW_TAG_subprogram ]
!1595 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1596, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1596 = metadata !{null, metadata !1597, metadata !1552}
!1597 = metadata !{i32 786447, i32 0, metadata !"", i32 0, i32 0, i64 64, i64 64, i64 0, i32 64, metadata !1522} ; [ DW_TAG_pointer_type ]
!1598 = metadata !{i32 786478, i32 0, metadata !1523, metadata !"operator=", metadata !"operator=", metadata !"_ZNV7_ap_sc_5sc_dt5sc_lvILi32EEaSERVKS2_", metadata !976, i32 978, metadata !1599, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 978} ; [ DW_TAG_subprogram ]
!1599 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1600, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1600 = metadata !{null, metadata !1597, metadata !1601}
!1601 = metadata !{i32 786448, null, null, null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !1602} ; [ DW_TAG_reference_type ]
!1602 = metadata !{i32 786470, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !1522} ; [ DW_TAG_const_type ]
!1603 = metadata !{i32 786478, i32 0, metadata !1523, metadata !"operator=", metadata !"operator=", metadata !"_ZN7_ap_sc_5sc_dt5sc_lvILi32EEaSERVKS2_", metadata !976, i32 984, metadata !1604, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 984} ; [ DW_TAG_subprogram ]
!1604 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1605, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1605 = metadata !{metadata !1606, metadata !1529, metadata !1601}
!1606 = metadata !{i32 786448, null, null, null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !1523} ; [ DW_TAG_reference_type ]
!1607 = metadata !{i32 786478, i32 0, metadata !1523, metadata !"operator=", metadata !"operator=", metadata !"_ZN7_ap_sc_5sc_dt5sc_lvILi32EEaSERKS2_", metadata !976, i32 988, metadata !1608, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 988} ; [ DW_TAG_subprogram ]
!1608 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1609, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1609 = metadata !{metadata !1606, metadata !1529, metadata !1552}
!1610 = metadata !{metadata !1611}
!1611 = metadata !{i32 786480, null, metadata !"_SC_W", metadata !322, i64 32, null, i32 0, i32 0} ; [ DW_TAG_template_value_parameter ]
!1612 = metadata !{i32 786478, i32 0, metadata !1518, metadata !"sc_signal_in_if", metadata !"sc_signal_in_if", metadata !"", metadata !174, i32 176, metadata !1613, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 176} ; [ DW_TAG_subprogram ]
!1613 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1614, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1614 = metadata !{null, metadata !1615}
!1615 = metadata !{i32 786447, i32 0, metadata !"", i32 0, i32 0, i64 64, i64 64, i64 0, i32 64, metadata !1518} ; [ DW_TAG_pointer_type ]
!1616 = metadata !{i32 786478, i32 0, metadata !1518, metadata !"read", metadata !"read", metadata !"_ZN7_ap_sc_7sc_core15sc_signal_in_ifINS_5sc_dt5sc_lvILi32EEEE4readEv", metadata !174, i32 180, metadata !1617, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 180} ; [ DW_TAG_subprogram ]
!1617 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1618, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1618 = metadata !{metadata !1523, metadata !1615}
!1619 = metadata !{i32 786478, i32 0, metadata !1518, metadata !"read", metadata !"read", metadata !"_ZNK7_ap_sc_7sc_core15sc_signal_in_ifINS_5sc_dt5sc_lvILi32EEEE4readEv", metadata !174, i32 181, metadata !1620, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 181} ; [ DW_TAG_subprogram ]
!1620 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1621, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1621 = metadata !{metadata !1523, metadata !1622}
!1622 = metadata !{i32 786447, i32 0, metadata !"", i32 0, i32 0, i64 64, i64 64, i64 0, i32 64, metadata !1623} ; [ DW_TAG_pointer_type ]
!1623 = metadata !{i32 786470, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !1518} ; [ DW_TAG_const_type ]
!1624 = metadata !{i32 786478, i32 0, metadata !1518, metadata !"operator sc_lv", metadata !"operator sc_lv", metadata !"_ZN7_ap_sc_7sc_core15sc_signal_in_ifINS_5sc_dt5sc_lvILi32EEEEcvKS4_Ev", metadata !174, i32 187, metadata !1625, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 187} ; [ DW_TAG_subprogram ]
!1625 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1626, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1626 = metadata !{metadata !1553, metadata !1615}
!1627 = metadata !{i32 786478, i32 0, metadata !1518, metadata !"operator sc_lv", metadata !"operator sc_lv", metadata !"_ZNK7_ap_sc_7sc_core15sc_signal_in_ifINS_5sc_dt5sc_lvILi32EEEEcvKS4_Ev", metadata !174, i32 188, metadata !1628, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 188} ; [ DW_TAG_subprogram ]
!1628 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1629, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1629 = metadata !{metadata !1553, metadata !1622}
!1630 = metadata !{metadata !1631}
!1631 = metadata !{i32 786479, null, metadata !"T", metadata !1523, null, i32 0, i32 0} ; [ DW_TAG_template_type_parameter ]
!1632 = metadata !{i32 786478, i32 0, metadata !1515, metadata !"sc_signal_inout_if", metadata !"sc_signal_inout_if", metadata !"", metadata !174, i32 197, metadata !1633, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 197} ; [ DW_TAG_subprogram ]
!1633 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1634, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1634 = metadata !{null, metadata !1635}
!1635 = metadata !{i32 786447, i32 0, metadata !"", i32 0, i32 0, i64 64, i64 64, i64 0, i32 64, metadata !1515} ; [ DW_TAG_pointer_type ]
!1636 = metadata !{i32 786478, i32 0, metadata !1515, metadata !"operator=", metadata !"operator=", metadata !"_ZN7_ap_sc_7sc_core18sc_signal_inout_ifINS_5sc_dt5sc_lvILi32EEEEaSERKS5_", metadata !174, i32 199, metadata !1637, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 199} ; [ DW_TAG_subprogram ]
!1637 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1638, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1638 = metadata !{metadata !1639, metadata !1635, metadata !1640}
!1639 = metadata !{i32 786448, null, null, null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !1515} ; [ DW_TAG_reference_type ]
!1640 = metadata !{i32 786448, null, null, null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !1641} ; [ DW_TAG_reference_type ]
!1641 = metadata !{i32 786470, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !1515} ; [ DW_TAG_const_type ]
!1642 = metadata !{i32 786478, i32 0, metadata !1511, metadata !"sc_port_b", metadata !"sc_port_b", metadata !"", metadata !174, i32 272, metadata !1643, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 272} ; [ DW_TAG_subprogram ]
!1643 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1644, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1644 = metadata !{null, metadata !1645}
!1645 = metadata !{i32 786447, i32 0, metadata !"", i32 0, i32 0, i64 64, i64 64, i64 0, i32 64, metadata !1511} ; [ DW_TAG_pointer_type ]
!1646 = metadata !{i32 786478, i32 0, metadata !1511, metadata !"sc_port_b", metadata !"sc_port_b", metadata !"", metadata !174, i32 273, metadata !1647, i1 false, i1 false, i32 0, i32 0, null, i32 384, i1 false, null, null, i32 0, metadata !186, i32 273} ; [ DW_TAG_subprogram ]
!1647 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1648, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1648 = metadata !{null, metadata !1645, metadata !229}
!1649 = metadata !{i32 786478, i32 0, metadata !1511, metadata !"bind", metadata !"bind", metadata !"_ZN7_ap_sc_7sc_core9sc_port_bINS0_18sc_signal_inout_ifINS_5sc_dt5sc_lvILi32EEEEEE4bindERS6_", metadata !174, i32 277, metadata !1650, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 277} ; [ DW_TAG_subprogram ]
!1650 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1651, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1651 = metadata !{null, metadata !1645, metadata !1639}
!1652 = metadata !{i32 786478, i32 0, metadata !1511, metadata !"operator()", metadata !"operator()", metadata !"_ZN7_ap_sc_7sc_core9sc_port_bINS0_18sc_signal_inout_ifINS_5sc_dt5sc_lvILi32EEEEEEclERS6_", metadata !174, i32 280, metadata !1650, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 280} ; [ DW_TAG_subprogram ]
!1653 = metadata !{i32 786478, i32 0, metadata !1511, metadata !"bind", metadata !"bind", metadata !"_ZN7_ap_sc_7sc_core9sc_port_bINS0_18sc_signal_inout_ifINS_5sc_dt5sc_lvILi32EEEEEE4bindERNS0_15sc_prim_channelE", metadata !174, i32 281, metadata !1654, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 281} ; [ DW_TAG_subprogram ]
!1654 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1655, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1655 = metadata !{null, metadata !1645, metadata !240}
!1656 = metadata !{i32 786478, i32 0, metadata !1511, metadata !"operator()", metadata !"operator()", metadata !"_ZN7_ap_sc_7sc_core9sc_port_bINS0_18sc_signal_inout_ifINS_5sc_dt5sc_lvILi32EEEEEEclERNS0_15sc_prim_channelE", metadata !174, i32 284, metadata !1654, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 284} ; [ DW_TAG_subprogram ]
!1657 = metadata !{i32 786478, i32 0, metadata !1511, metadata !"bind", metadata !"bind", metadata !"_ZN7_ap_sc_7sc_core9sc_port_bINS0_18sc_signal_inout_ifINS_5sc_dt5sc_lvILi32EEEEEE4bindERS7_", metadata !174, i32 285, metadata !1658, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 285} ; [ DW_TAG_subprogram ]
!1658 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1659, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1659 = metadata !{null, metadata !1645, metadata !1660}
!1660 = metadata !{i32 786448, null, null, null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !1511} ; [ DW_TAG_reference_type ]
!1661 = metadata !{i32 786478, i32 0, metadata !1511, metadata !"operator()", metadata !"operator()", metadata !"_ZN7_ap_sc_7sc_core9sc_port_bINS0_18sc_signal_inout_ifINS_5sc_dt5sc_lvILi32EEEEEEclERS7_", metadata !174, i32 286, metadata !1658, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 286} ; [ DW_TAG_subprogram ]
!1662 = metadata !{i32 786478, i32 0, metadata !1511, metadata !"operator->", metadata !"operator->", metadata !"_ZN7_ap_sc_7sc_core9sc_port_bINS0_18sc_signal_inout_ifINS_5sc_dt5sc_lvILi32EEEEEEptEv", metadata !174, i32 288, metadata !1663, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 288} ; [ DW_TAG_subprogram ]
!1663 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1664, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1664 = metadata !{metadata !1665, metadata !1645}
!1665 = metadata !{i32 786447, null, metadata !"", null, i32 0, i64 64, i64 64, i64 0, i32 0, metadata !1515} ; [ DW_TAG_pointer_type ]
!1666 = metadata !{metadata !1667}
!1667 = metadata !{i32 786479, null, metadata !"IF", metadata !1515, null, i32 0, i32 0} ; [ DW_TAG_template_type_parameter ]
!1668 = metadata !{i32 786478, i32 0, metadata !1508, metadata !"sc_inout", metadata !"sc_inout", metadata !"", metadata !174, i32 413, metadata !1669, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 413} ; [ DW_TAG_subprogram ]
!1669 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1670, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1670 = metadata !{null, metadata !1671}
!1671 = metadata !{i32 786447, i32 0, metadata !"", i32 0, i32 0, i64 64, i64 64, i64 0, i32 64, metadata !1508} ; [ DW_TAG_pointer_type ]
!1672 = metadata !{i32 786478, i32 0, metadata !1508, metadata !"sc_inout", metadata !"sc_inout", metadata !"", metadata !174, i32 414, metadata !1673, i1 false, i1 false, i32 0, i32 0, null, i32 384, i1 false, null, null, i32 0, metadata !186, i32 414} ; [ DW_TAG_subprogram ]
!1673 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1674, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1674 = metadata !{null, metadata !1671, metadata !229}
!1675 = metadata !{i32 786478, i32 0, metadata !1508, metadata !"write", metadata !"write", metadata !"_ZN7_ap_sc_7sc_core8sc_inoutINS_5sc_dt5sc_lvILi32EEEE5writeERKS4_", metadata !174, i32 417, metadata !1676, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 417} ; [ DW_TAG_subprogram ]
!1676 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1677, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1677 = metadata !{null, metadata !1671, metadata !1552}
!1678 = metadata !{i32 786478, i32 0, metadata !1508, metadata !"read", metadata !"read", metadata !"_ZN7_ap_sc_7sc_core8sc_inoutINS_5sc_dt5sc_lvILi32EEEE4readEv", metadata !174, i32 421, metadata !1679, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 421} ; [ DW_TAG_subprogram ]
!1679 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1680, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1680 = metadata !{metadata !1681, metadata !1671}
!1681 = metadata !{i32 786454, metadata !1508, metadata !"data_type", metadata !174, i32 411, i64 0, i64 0, i64 0, i32 0, metadata !1523} ; [ DW_TAG_typedef ]
!1682 = metadata !{i32 786478, i32 0, metadata !1508, metadata !"operator const struct _ap_sc_::sc_dt::sc_lv<32> &", metadata !"operator const struct _ap_sc_::sc_dt::sc_lv<32> &", metadata !"_ZNK7_ap_sc_7sc_core8sc_inoutINS_5sc_dt5sc_lvILi32EEEEcvRKS4_Ev", metadata !174, i32 422, metadata !1683, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 422} ; [ DW_TAG_subprogram ]
!1683 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1684, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1684 = metadata !{metadata !1685, metadata !1687}
!1685 = metadata !{i32 786448, null, null, null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !1686} ; [ DW_TAG_reference_type ]
!1686 = metadata !{i32 786470, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !1681} ; [ DW_TAG_const_type ]
!1687 = metadata !{i32 786447, i32 0, metadata !"", i32 0, i32 0, i64 64, i64 64, i64 0, i32 64, metadata !1688} ; [ DW_TAG_pointer_type ]
!1688 = metadata !{i32 786470, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !1508} ; [ DW_TAG_const_type ]
!1689 = metadata !{i32 786478, i32 0, metadata !1508, metadata !"operator sc_lv", metadata !"operator sc_lv", metadata !"_ZN7_ap_sc_7sc_core8sc_inoutINS_5sc_dt5sc_lvILi32EEEEcvKS4_Ev", metadata !174, i32 425, metadata !1690, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 425} ; [ DW_TAG_subprogram ]
!1690 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1691, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1691 = metadata !{metadata !1686, metadata !1671}
!1692 = metadata !{metadata !1693}
!1693 = metadata !{i32 786479, null, metadata !"_T", metadata !1523, null, i32 0, i32 0} ; [ DW_TAG_template_type_parameter ]
!1694 = metadata !{i32 786478, i32 0, metadata !1505, metadata !"sc_out", metadata !"sc_out", metadata !"", metadata !174, i32 433, metadata !1695, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 433} ; [ DW_TAG_subprogram ]
!1695 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1696, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1696 = metadata !{null, metadata !1697}
!1697 = metadata !{i32 786447, i32 0, metadata !"", i32 0, i32 0, i64 64, i64 64, i64 0, i32 64, metadata !1505} ; [ DW_TAG_pointer_type ]
!1698 = metadata !{i32 786478, i32 0, metadata !1505, metadata !"sc_out", metadata !"sc_out", metadata !"", metadata !174, i32 434, metadata !1699, i1 false, i1 false, i32 0, i32 0, null, i32 384, i1 false, null, null, i32 0, metadata !186, i32 434} ; [ DW_TAG_subprogram ]
!1699 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1700, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1700 = metadata !{null, metadata !1697, metadata !229}
!1701 = metadata !{i32 786445, metadata !167, metadata !"axi_master_0", metadata !168, i32 55, i64 32, i64 32, i64 160, i32 0, metadata !1702} ; [ DW_TAG_member ]
!1702 = metadata !{i32 786434, null, metadata !"AXI4M_bus_port<int>", metadata !1703, i32 6, i64 32, i64 32, i32 0, i32 0, null, metadata !1704, i32 0, null, metadata !1739} ; [ DW_TAG_class_type ]
!1703 = metadata !{i32 786473, metadata !"/tools/Xilinx/Vivado/2018.3/common/technology/autopilot/ap_sysc/AXI4_if.h", metadata !"/home/julien/education/inf8500/lab2/tutorial_ju/solutions/tutorial/architectures/hw_sw/build/estimation/hls/producer0", null} ; [ DW_TAG_file_type ]
!1704 = metadata !{metadata !1705, metadata !1767, metadata !1771, metadata !1774, metadata !1775, metadata !1780, metadata !1783, metadata !1786, metadata !1789}
!1705 = metadata !{i32 786460, metadata !1702, null, metadata !1703, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !1706} ; [ DW_TAG_inheritance ]
!1706 = metadata !{i32 786434, metadata !172, metadata !"sc_port_b<hls_bus_if<int> >", metadata !174, i32 268, i64 32, i64 32, i32 0, i32 0, null, metadata !1707, i32 0, null, metadata !1765} ; [ DW_TAG_class_type ]
!1707 = metadata !{metadata !1708, metadata !1709, metadata !1741, metadata !1745, metadata !1748, metadata !1752, metadata !1753, metadata !1756, metadata !1757, metadata !1761, metadata !1762}
!1708 = metadata !{i32 786460, metadata !1706, null, metadata !174, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !180} ; [ DW_TAG_inheritance ]
!1709 = metadata !{i32 786445, metadata !1706, metadata !"m_if", metadata !174, i32 270, i64 32, i64 32, i64 0, i32 0, metadata !1710} ; [ DW_TAG_member ]
!1710 = metadata !{i32 786434, null, metadata !"hls_bus_if<int>", metadata !1711, i32 64, i64 32, i64 32, i32 0, i32 0, null, metadata !1712, i32 0, null, metadata !1739} ; [ DW_TAG_class_type ]
!1711 = metadata !{i32 786473, metadata !"/tools/Xilinx/Vivado/2018.3/common/technology/autopilot/ap_sysc/hls_bus_if.h", metadata !"/home/julien/education/inf8500/lab2/tutorial_ju/solutions/tutorial/architectures/hw_sw/build/estimation/hls/producer0", null} ; [ DW_TAG_file_type ]
!1712 = metadata !{metadata !1713, metadata !1714, metadata !1716, metadata !1720, metadata !1724, metadata !1729, metadata !1733, metadata !1736}
!1713 = metadata !{i32 786460, metadata !1710, null, metadata !1711, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !192} ; [ DW_TAG_inheritance ]
!1714 = metadata !{i32 786445, metadata !1710, metadata !"Val", metadata !1711, i32 67, i64 32, i64 32, i64 0, i32 0, metadata !1715} ; [ DW_TAG_member ]
!1715 = metadata !{i32 786485, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !322} ; [ DW_TAG_volatile_type ]
!1716 = metadata !{i32 786478, i32 0, metadata !1710, metadata !"hls_bus_if", metadata !"hls_bus_if", metadata !"", metadata !1711, i32 69, metadata !1717, i1 false, i1 false, i32 0, i32 0, null, i32 384, i1 false, null, null, i32 0, metadata !186, i32 69} ; [ DW_TAG_subprogram ]
!1717 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1718, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1718 = metadata !{null, metadata !1719, metadata !229}
!1719 = metadata !{i32 786447, i32 0, metadata !"", i32 0, i32 0, i64 64, i64 64, i64 0, i32 64, metadata !1710} ; [ DW_TAG_pointer_type ]
!1720 = metadata !{i32 786478, i32 0, metadata !1710, metadata !"operator->", metadata !"operator->", metadata !"_ZN10hls_bus_ifIiEptEv", metadata !1711, i32 75, metadata !1721, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 75} ; [ DW_TAG_subprogram ]
!1721 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1722, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1722 = metadata !{metadata !1723, metadata !1719}
!1723 = metadata !{i32 786447, null, metadata !"", null, i32 0, i64 64, i64 64, i64 0, i32 0, metadata !1710} ; [ DW_TAG_pointer_type ]
!1724 = metadata !{i32 786478, i32 0, metadata !1710, metadata !"read", metadata !"read", metadata !"_ZNV10hls_bus_ifIiE4readEj", metadata !1711, i32 86, metadata !1725, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 86} ; [ DW_TAG_subprogram ]
!1725 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1726, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1726 = metadata !{metadata !322, metadata !1727, metadata !376}
!1727 = metadata !{i32 786447, i32 0, metadata !"", i32 0, i32 0, i64 64, i64 64, i64 0, i32 64, metadata !1728} ; [ DW_TAG_pointer_type ]
!1728 = metadata !{i32 786485, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !1710} ; [ DW_TAG_volatile_type ]
!1729 = metadata !{i32 786478, i32 0, metadata !1710, metadata !"burst_read", metadata !"burst_read", metadata !"_ZNV10hls_bus_ifIiE10burst_readEjiPi", metadata !1711, i32 92, metadata !1730, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 92} ; [ DW_TAG_subprogram ]
!1730 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1731, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1731 = metadata !{null, metadata !1727, metadata !376, metadata !322, metadata !1732}
!1732 = metadata !{i32 786447, null, metadata !"", null, i32 0, i64 64, i64 64, i64 0, i32 0, metadata !322} ; [ DW_TAG_pointer_type ]
!1733 = metadata !{i32 786478, i32 0, metadata !1710, metadata !"write", metadata !"write", metadata !"_ZNV10hls_bus_ifIiE5writeEjPi", metadata !1711, i32 96, metadata !1734, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 96} ; [ DW_TAG_subprogram ]
!1734 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1735, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1735 = metadata !{null, metadata !1727, metadata !376, metadata !1732}
!1736 = metadata !{i32 786478, i32 0, metadata !1710, metadata !"burst_write", metadata !"burst_write", metadata !"_ZN10hls_bus_ifIiE11burst_writeEjiPi", metadata !1711, i32 101, metadata !1737, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 101} ; [ DW_TAG_subprogram ]
!1737 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1738, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1738 = metadata !{null, metadata !1719, metadata !376, metadata !322, metadata !1732}
!1739 = metadata !{metadata !1740}
!1740 = metadata !{i32 786479, null, metadata !"_VHLS_DT", metadata !322, null, i32 0, i32 0} ; [ DW_TAG_template_type_parameter ]
!1741 = metadata !{i32 786478, i32 0, metadata !1706, metadata !"sc_port_b", metadata !"sc_port_b", metadata !"", metadata !174, i32 272, metadata !1742, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 272} ; [ DW_TAG_subprogram ]
!1742 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1743, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1743 = metadata !{null, metadata !1744}
!1744 = metadata !{i32 786447, i32 0, metadata !"", i32 0, i32 0, i64 64, i64 64, i64 0, i32 64, metadata !1706} ; [ DW_TAG_pointer_type ]
!1745 = metadata !{i32 786478, i32 0, metadata !1706, metadata !"sc_port_b", metadata !"sc_port_b", metadata !"", metadata !174, i32 273, metadata !1746, i1 false, i1 false, i32 0, i32 0, null, i32 384, i1 false, null, null, i32 0, metadata !186, i32 273} ; [ DW_TAG_subprogram ]
!1746 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1747, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1747 = metadata !{null, metadata !1744, metadata !229}
!1748 = metadata !{i32 786478, i32 0, metadata !1706, metadata !"bind", metadata !"bind", metadata !"_ZN7_ap_sc_7sc_core9sc_port_bI10hls_bus_ifIiEE4bindERS3_", metadata !174, i32 277, metadata !1749, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 277} ; [ DW_TAG_subprogram ]
!1749 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1750, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1750 = metadata !{null, metadata !1744, metadata !1751}
!1751 = metadata !{i32 786448, null, null, null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !1710} ; [ DW_TAG_reference_type ]
!1752 = metadata !{i32 786478, i32 0, metadata !1706, metadata !"operator()", metadata !"operator()", metadata !"_ZN7_ap_sc_7sc_core9sc_port_bI10hls_bus_ifIiEEclERS3_", metadata !174, i32 280, metadata !1749, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 280} ; [ DW_TAG_subprogram ]
!1753 = metadata !{i32 786478, i32 0, metadata !1706, metadata !"bind", metadata !"bind", metadata !"_ZN7_ap_sc_7sc_core9sc_port_bI10hls_bus_ifIiEE4bindERNS0_15sc_prim_channelE", metadata !174, i32 281, metadata !1754, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 281} ; [ DW_TAG_subprogram ]
!1754 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1755, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1755 = metadata !{null, metadata !1744, metadata !240}
!1756 = metadata !{i32 786478, i32 0, metadata !1706, metadata !"operator()", metadata !"operator()", metadata !"_ZN7_ap_sc_7sc_core9sc_port_bI10hls_bus_ifIiEEclERNS0_15sc_prim_channelE", metadata !174, i32 284, metadata !1754, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 284} ; [ DW_TAG_subprogram ]
!1757 = metadata !{i32 786478, i32 0, metadata !1706, metadata !"bind", metadata !"bind", metadata !"_ZN7_ap_sc_7sc_core9sc_port_bI10hls_bus_ifIiEE4bindERS4_", metadata !174, i32 285, metadata !1758, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 285} ; [ DW_TAG_subprogram ]
!1758 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1759, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1759 = metadata !{null, metadata !1744, metadata !1760}
!1760 = metadata !{i32 786448, null, null, null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !1706} ; [ DW_TAG_reference_type ]
!1761 = metadata !{i32 786478, i32 0, metadata !1706, metadata !"operator()", metadata !"operator()", metadata !"_ZN7_ap_sc_7sc_core9sc_port_bI10hls_bus_ifIiEEclERS4_", metadata !174, i32 286, metadata !1758, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 286} ; [ DW_TAG_subprogram ]
!1762 = metadata !{i32 786478, i32 0, metadata !1706, metadata !"operator->", metadata !"operator->", metadata !"_ZN7_ap_sc_7sc_core9sc_port_bI10hls_bus_ifIiEEptEv", metadata !174, i32 288, metadata !1763, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 288} ; [ DW_TAG_subprogram ]
!1763 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1764, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1764 = metadata !{metadata !1723, metadata !1744}
!1765 = metadata !{metadata !1766}
!1766 = metadata !{i32 786479, null, metadata !"IF", metadata !1710, null, i32 0, i32 0} ; [ DW_TAG_template_type_parameter ]
!1767 = metadata !{i32 786478, i32 0, metadata !1702, metadata !"AXI4M_bus_port", metadata !"AXI4M_bus_port", metadata !"", metadata !1703, i32 12, metadata !1768, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 12} ; [ DW_TAG_subprogram ]
!1768 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1769, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1769 = metadata !{null, metadata !1770}
!1770 = metadata !{i32 786447, i32 0, metadata !"", i32 0, i32 0, i64 64, i64 64, i64 0, i32 64, metadata !1702} ; [ DW_TAG_pointer_type ]
!1771 = metadata !{i32 786478, i32 0, metadata !1702, metadata !"AXI4M_bus_port", metadata !"AXI4M_bus_port", metadata !"", metadata !1703, i32 15, metadata !1772, i1 false, i1 false, i32 0, i32 0, null, i32 384, i1 false, null, null, i32 0, metadata !186, i32 15} ; [ DW_TAG_subprogram ]
!1772 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1773, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1773 = metadata !{null, metadata !1770, metadata !229}
!1774 = metadata !{i32 786478, i32 0, metadata !1702, metadata !"reset", metadata !"reset", metadata !"_ZN14AXI4M_bus_portIiE5resetEv", metadata !1703, i32 19, metadata !1768, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 19} ; [ DW_TAG_subprogram ]
!1775 = metadata !{i32 786478, i32 0, metadata !1702, metadata !"read", metadata !"read", metadata !"_ZNV14AXI4M_bus_portIiE4readEj", metadata !1703, i32 21, metadata !1776, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 21} ; [ DW_TAG_subprogram ]
!1776 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1777, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1777 = metadata !{metadata !322, metadata !1778, metadata !376}
!1778 = metadata !{i32 786447, i32 0, metadata !"", i32 0, i32 0, i64 64, i64 64, i64 0, i32 64, metadata !1779} ; [ DW_TAG_pointer_type ]
!1779 = metadata !{i32 786485, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !1702} ; [ DW_TAG_volatile_type ]
!1780 = metadata !{i32 786478, i32 0, metadata !1702, metadata !"burst_read", metadata !"burst_read", metadata !"_ZNV14AXI4M_bus_portIiE10burst_readEjiPi", metadata !1703, i32 25, metadata !1781, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 25} ; [ DW_TAG_subprogram ]
!1781 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1782, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1782 = metadata !{null, metadata !1778, metadata !376, metadata !322, metadata !1732}
!1783 = metadata !{i32 786478, i32 0, metadata !1702, metadata !"write", metadata !"write", metadata !"_ZNV14AXI4M_bus_portIiE5writeEjPi", metadata !1703, i32 29, metadata !1784, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 29} ; [ DW_TAG_subprogram ]
!1784 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1785, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1785 = metadata !{null, metadata !1778, metadata !376, metadata !1732}
!1786 = metadata !{i32 786478, i32 0, metadata !1702, metadata !"burst_write", metadata !"burst_write", metadata !"_ZN14AXI4M_bus_portIiE11burst_writeEjiPi", metadata !1703, i32 33, metadata !1787, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 33} ; [ DW_TAG_subprogram ]
!1787 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1788, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1788 = metadata !{null, metadata !1770, metadata !376, metadata !322, metadata !1732}
!1789 = metadata !{i32 786478, i32 0, metadata !1702, metadata !"operator->", metadata !"operator->", metadata !"_ZN14AXI4M_bus_portIiEptEv", metadata !1703, i32 38, metadata !1790, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 38} ; [ DW_TAG_subprogram ]
!1790 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1791, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1791 = metadata !{metadata !1792, metadata !1770}
!1792 = metadata !{i32 786447, null, metadata !"", null, i32 0, i64 64, i64 64, i64 0, i32 0, metadata !1702} ; [ DW_TAG_pointer_type ]
!1793 = metadata !{i32 786478, i32 0, metadata !167, metadata !"producer0", metadata !"producer0", metadata !"", metadata !168, i32 56, metadata !1794, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 56} ; [ DW_TAG_subprogram ]
!1794 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1795, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1795 = metadata !{null, metadata !166, metadata !1796}
!1796 = metadata !{i32 786434, metadata !172, metadata !"sc_module_name", metadata !174, i32 581, i64 8, i64 8, i32 0, i32 0, null, metadata !1797, i32 0, null, null} ; [ DW_TAG_class_type ]
!1797 = metadata !{metadata !1798, metadata !1802}
!1798 = metadata !{i32 786478, i32 0, metadata !1796, metadata !"sc_module_name", metadata !"sc_module_name", metadata !"", metadata !174, i32 584, metadata !1799, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 584} ; [ DW_TAG_subprogram ]
!1799 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1800, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1800 = metadata !{null, metadata !1801, metadata !229}
!1801 = metadata !{i32 786447, i32 0, metadata !"", i32 0, i32 0, i64 64, i64 64, i64 0, i32 64, metadata !1796} ; [ DW_TAG_pointer_type ]
!1802 = metadata !{i32 786478, i32 0, metadata !1796, metadata !"sc_module_name", metadata !"sc_module_name", metadata !"", metadata !174, i32 585, metadata !1803, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 585} ; [ DW_TAG_subprogram ]
!1803 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1804, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1804 = metadata !{null, metadata !1801, metadata !1805}
!1805 = metadata !{i32 786448, null, null, null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !1806} ; [ DW_TAG_reference_type ]
!1806 = metadata !{i32 786470, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !1796} ; [ DW_TAG_const_type ]
!1807 = metadata !{i32 786478, i32 0, metadata !167, metadata !"hw_compute_latency", metadata !"hw_compute_latency", metadata !"_ZN11producer018hw_compute_latencyEi", metadata !168, i32 57, metadata !1808, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 57} ; [ DW_TAG_subprogram ]
!1808 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1809, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1809 = metadata !{null, metadata !166, metadata !322}
!1810 = metadata !{i32 786478, i32 0, metadata !167, metadata !"ComputeFor", metadata !"ComputeFor", metadata !"_ZN11producer010ComputeForEi", metadata !168, i32 61, metadata !1808, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 61} ; [ DW_TAG_subprogram ]
!1811 = metadata !{i32 786478, i32 0, metadata !167, metadata !"is_verbose", metadata !"is_verbose", metadata !"_ZN11producer010is_verboseEv", metadata !168, i32 62, metadata !1812, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 62} ; [ DW_TAG_subprogram ]
!1812 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1813, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1813 = metadata !{metadata !200, metadata !166}
!1814 = metadata !{i32 786478, i32 0, metadata !167, metadata !"GetVerbose", metadata !"GetVerbose", metadata !"_ZN11producer010GetVerboseEv", metadata !168, i32 63, metadata !1812, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 63} ; [ DW_TAG_subprogram ]
!1815 = metadata !{i32 786478, i32 0, metadata !167, metadata !"sc_stop", metadata !"sc_stop", metadata !"_ZN11producer07sc_stopEv", metadata !168, i32 64, metadata !164, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 64} ; [ DW_TAG_subprogram ]
!1816 = metadata !{i32 786478, i32 0, metadata !167, metadata !"name", metadata !"name", metadata !"_ZNK11producer04nameEv", metadata !168, i32 65, metadata !1817, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 65} ; [ DW_TAG_subprogram ]
!1817 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1818, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1818 = metadata !{metadata !229, metadata !1819}
!1819 = metadata !{i32 786447, i32 0, metadata !"", i32 0, i32 0, i64 64, i64 64, i64 0, i32 64, metadata !1820} ; [ DW_TAG_pointer_type ]
!1820 = metadata !{i32 786470, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !167} ; [ DW_TAG_const_type ]
!1821 = metadata !{i32 786478, i32 0, metadata !167, metadata !"ModuleRead_0", metadata !"ModuleRead_0", metadata !"_ZN11producer012ModuleRead_0EjjmPjm", metadata !168, i32 75, metadata !1822, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 75} ; [ DW_TAG_subprogram ]
!1822 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1823, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1823 = metadata !{metadata !1824, metadata !166, metadata !376, metadata !376, metadata !386, metadata !1825, metadata !386}
!1824 = metadata !{i32 786454, null, metadata !"eSpaceStatus", metadata !168, i32 29, i64 0, i64 0, i64 0, i32 0, metadata !357} ; [ DW_TAG_typedef ]
!1825 = metadata !{i32 786447, null, metadata !"", null, i32 0, i64 64, i64 64, i64 0, i32 0, metadata !376} ; [ DW_TAG_pointer_type ]
!1826 = metadata !{i32 786478, i32 0, metadata !167, metadata !"ModuleRead_0", metadata !"ModuleRead_0", metadata !"_ZN11producer012ModuleRead_0Ejjj", metadata !168, i32 85, metadata !1827, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 85} ; [ DW_TAG_subprogram ]
!1827 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1828, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1828 = metadata !{metadata !1824, metadata !166, metadata !376, metadata !376, metadata !1829}
!1829 = metadata !{i32 786454, null, metadata !"uint32_t", metadata !168, i32 26, i64 0, i64 0, i64 0, i32 0, metadata !1830} ; [ DW_TAG_typedef ]
!1830 = metadata !{i32 786454, null, metadata !"__uint32_t", metadata !168, i32 42, i64 0, i64 0, i64 0, i32 0, metadata !376} ; [ DW_TAG_typedef ]
!1831 = metadata !{i32 786478, i32 0, metadata !167, metadata !"ModuleWrite_0", metadata !"ModuleWrite_0", metadata !"_ZN11producer013ModuleWrite_0EjjmPKjm", metadata !168, i32 105, metadata !1832, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 105} ; [ DW_TAG_subprogram ]
!1832 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1833, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1833 = metadata !{metadata !1824, metadata !166, metadata !376, metadata !376, metadata !386, metadata !1834, metadata !386}
!1834 = metadata !{i32 786447, null, metadata !"", null, i32 0, i64 64, i64 64, i64 0, i32 0, metadata !375} ; [ DW_TAG_pointer_type ]
!1835 = metadata !{i32 786478, i32 0, metadata !167, metadata !"ModuleWrite_0", metadata !"ModuleWrite_0", metadata !"_ZN11producer013ModuleWrite_0Ejjj", metadata !168, i32 115, metadata !1827, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 115} ; [ DW_TAG_subprogram ]
!1836 = metadata !{i32 786478, i32 0, metadata !167, metadata !"MakeSignalsInactive", metadata !"MakeSignalsInactive", metadata !"_ZN11producer019MakeSignalsInactiveEv", metadata !168, i32 220, metadata !164, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 220} ; [ DW_TAG_subprogram ]
!1837 = metadata !{i32 786478, i32 0, metadata !167, metadata !"thread", metadata !"thread", metadata !"_ZN11producer06threadEv", metadata !168, i32 221, metadata !164, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !186, i32 221} ; [ DW_TAG_subprogram ]
!1838 = metadata !{i32 786447, null, metadata !"", null, i32 0, i64 64, i64 64, i64 0, i32 0, metadata !167} ; [ DW_TAG_pointer_type ]
!1839 = metadata !{i32 786447, null, metadata !"", null, i32 0, i64 64, i64 64, i64 0, i32 0, metadata !1840} ; [ DW_TAG_pointer_type ]
!1840 = metadata !{i32 786438, null, metadata !"producer0", metadata !168, i32 46, i64 8, i64 32, i32 0, i32 0, null, metadata !1841, i32 0, null, null} ; [ DW_TAG_class_field_type ]
!1841 = metadata !{metadata !1842}
!1842 = metadata !{i32 786438, metadata !172, metadata !"sc_in<bool>", metadata !174, i32 357, i64 8, i64 8, i32 0, i32 0, null, metadata !1843, i32 0, null, metadata !220} ; [ DW_TAG_class_field_type ]
!1843 = metadata !{metadata !1844}
!1844 = metadata !{i32 786438, metadata !172, metadata !"sc_port_b<_ap_sc_::sc_core::sc_signal_in_if<bool> >", metadata !174, i32 268, i64 8, i64 8, i32 0, i32 0, null, metadata !1845, i32 0, null, metadata !253} ; [ DW_TAG_class_field_type ]
!1845 = metadata !{metadata !1846}
!1846 = metadata !{i32 786438, metadata !172, metadata !"sc_signal_in_if<bool>", metadata !174, i32 172, i64 8, i64 8, i32 0, i32 0, null, metadata !1847, i32 0, null, metadata !220} ; [ DW_TAG_class_field_type ]
!1847 = metadata !{metadata !198}
!1848 = metadata !{i32 78, i32 19, metadata !162, null}
!1849 = metadata !{i32 790531, metadata !161, metadata !"producer0.reset.m_if.Val", null, i32 78, metadata !1839, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!1850 = metadata !{i32 790531, metadata !161, metadata !"producer0.fifo_in_0.m_if.Val.V", null, i32 78, metadata !1851, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!1851 = metadata !{i32 786447, null, metadata !"", null, i32 0, i64 64, i64 64, i64 0, i32 0, metadata !1852} ; [ DW_TAG_pointer_type ]
!1852 = metadata !{i32 786438, null, metadata !"producer0", metadata !168, i32 46, i64 32, i64 32, i32 0, i32 0, null, metadata !1853, i32 0, null, null} ; [ DW_TAG_class_field_type ]
!1853 = metadata !{metadata !1854}
!1854 = metadata !{i32 786438, metadata !172, metadata !"sc_fifo_in<ap_uint<32> >", metadata !174, i32 477, i64 32, i64 32, i32 0, i32 0, null, metadata !1855, i32 0, null, metadata !836} ; [ DW_TAG_class_field_type ]
!1855 = metadata !{metadata !1856}
!1856 = metadata !{i32 786438, metadata !172, metadata !"sc_port_b<_ap_sc_::sc_core::sc_fifo_in_if<ap_uint<32> > >", metadata !174, i32 268, i64 32, i64 32, i32 0, i32 0, null, metadata !1857, i32 0, null, metadata !863} ; [ DW_TAG_class_field_type ]
!1857 = metadata !{metadata !1858}
!1858 = metadata !{i32 786438, metadata !172, metadata !"sc_fifo_in_if<ap_uint<32> >", metadata !174, i32 212, i64 32, i64 32, i32 0, i32 0, null, metadata !1859, i32 0, null, metadata !836} ; [ DW_TAG_class_field_type ]
!1859 = metadata !{metadata !1860}
!1860 = metadata !{i32 786438, null, metadata !"ap_uint<32>", metadata !304, i32 194, i64 32, i64 32, i32 0, i32 0, null, metadata !1861, i32 0, null, metadata !817} ; [ DW_TAG_class_field_type ]
!1861 = metadata !{metadata !1862}
!1862 = metadata !{i32 786438, null, metadata !"ap_int_base<32, false>", metadata !308, i32 150, i64 32, i64 32, i32 0, i32 0, null, metadata !1863, i32 0, null, metadata !702} ; [ DW_TAG_class_field_type ]
!1863 = metadata !{metadata !1864}
!1864 = metadata !{i32 786438, null, metadata !"ssdm_int<32 + 1024 * 0, false>", metadata !312, i32 66, i64 32, i64 32, i32 0, i32 0, null, metadata !1865, i32 0, null, metadata !320} ; [ DW_TAG_class_field_type ]
!1865 = metadata !{metadata !314}
!1866 = metadata !{i32 790531, metadata !161, metadata !"producer0.fifo_out_0.m_if.Val.V", null, i32 78, metadata !1867, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!1867 = metadata !{i32 786447, null, metadata !"", null, i32 0, i64 64, i64 64, i64 0, i32 0, metadata !1868} ; [ DW_TAG_pointer_type ]
!1868 = metadata !{i32 786438, null, metadata !"producer0", metadata !168, i32 46, i64 32, i64 32, i32 0, i32 0, null, metadata !1869, i32 0, null, null} ; [ DW_TAG_class_field_type ]
!1869 = metadata !{metadata !1870}
!1870 = metadata !{i32 786438, metadata !172, metadata !"sc_fifo_out<ap_uint<32> >", metadata !174, i32 489, i64 32, i64 32, i32 0, i32 0, null, metadata !1871, i32 0, null, metadata !836} ; [ DW_TAG_class_field_type ]
!1871 = metadata !{metadata !1872}
!1872 = metadata !{i32 786438, metadata !172, metadata !"sc_port_b<_ap_sc_::sc_core::sc_fifo_out_if<ap_uint<32> > >", metadata !174, i32 268, i64 32, i64 32, i32 0, i32 0, null, metadata !1873, i32 0, null, metadata !936} ; [ DW_TAG_class_field_type ]
!1873 = metadata !{metadata !1874}
!1874 = metadata !{i32 786438, metadata !172, metadata !"sc_fifo_out_if<ap_uint<32> >", metadata !174, i32 235, i64 32, i64 32, i32 0, i32 0, null, metadata !1859, i32 0, null, metadata !836} ; [ DW_TAG_class_field_type ]
!1875 = metadata !{i32 790531, metadata !161, metadata !"producer0.RegisterWriteEnablePort_0.m_if.Val.V", null, i32 78, metadata !1876, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!1876 = metadata !{i32 786447, null, metadata !"", null, i32 0, i64 64, i64 64, i64 0, i32 0, metadata !1877} ; [ DW_TAG_pointer_type ]
!1877 = metadata !{i32 786438, null, metadata !"producer0", metadata !168, i32 46, i64 1, i64 32, i32 0, i32 0, null, metadata !1878, i32 0, null, null} ; [ DW_TAG_class_field_type ]
!1878 = metadata !{metadata !1879}
!1879 = metadata !{i32 786438, metadata !172, metadata !"sc_out<_ap_sc_::sc_dt::sc_lv<1> >", metadata !174, i32 430, i64 1, i64 8, i32 0, i32 0, null, metadata !1880, i32 0, null, metadata !1495} ; [ DW_TAG_class_field_type ]
!1880 = metadata !{metadata !1881}
!1881 = metadata !{i32 786438, metadata !172, metadata !"sc_inout<_ap_sc_::sc_dt::sc_lv<1> >", metadata !174, i32 409, i64 1, i64 8, i32 0, i32 0, null, metadata !1882, i32 0, null, metadata !1495} ; [ DW_TAG_class_field_type ]
!1882 = metadata !{metadata !1883}
!1883 = metadata !{i32 786438, metadata !172, metadata !"sc_port_b<_ap_sc_::sc_core::sc_signal_inout_if<_ap_sc_::sc_dt::sc_lv<1> > >", metadata !174, i32 268, i64 1, i64 8, i32 0, i32 0, null, metadata !1884, i32 0, null, metadata !1469} ; [ DW_TAG_class_field_type ]
!1884 = metadata !{metadata !1885}
!1885 = metadata !{i32 786438, metadata !172, metadata !"sc_signal_inout_if<_ap_sc_::sc_dt::sc_lv<1> >", metadata !174, i32 193, i64 1, i64 8, i32 0, i32 0, null, metadata !1886, i32 0, null, metadata !1433} ; [ DW_TAG_class_field_type ]
!1886 = metadata !{metadata !1887}
!1887 = metadata !{i32 786438, metadata !172, metadata !"sc_signal_in_if<_ap_sc_::sc_dt::sc_lv<1> >", metadata !174, i32 172, i64 1, i64 8, i32 0, i32 0, null, metadata !1888, i32 0, null, metadata !1433} ; [ DW_TAG_class_field_type ]
!1888 = metadata !{metadata !1889}
!1889 = metadata !{i32 786438, metadata !974, metadata !"sc_lv<1>", metadata !976, i32 880, i64 1, i64 8, i32 0, i32 0, null, metadata !1890, i32 0, null, metadata !1413} ; [ DW_TAG_class_field_type ]
!1890 = metadata !{metadata !1891}
!1891 = metadata !{i32 786438, null, metadata !"ap_int_base<1, false>", metadata !308, i32 150, i64 1, i64 8, i32 0, i32 0, null, metadata !1892, i32 0, null, metadata !1296} ; [ DW_TAG_class_field_type ]
!1892 = metadata !{metadata !1893}
!1893 = metadata !{i32 786438, null, metadata !"ssdm_int<1 + 1024 * 0, false>", metadata !312, i32 4, i64 1, i64 8, i32 0, i32 0, null, metadata !1894, i32 0, null, metadata !990} ; [ DW_TAG_class_field_type ]
!1894 = metadata !{metadata !984}
!1895 = metadata !{i32 790531, metadata !161, metadata !"producer0.RegisterWriteDataPort_0.m_if.Val.V", null, i32 78, metadata !1896, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!1896 = metadata !{i32 786447, null, metadata !"", null, i32 0, i64 64, i64 64, i64 0, i32 0, metadata !1897} ; [ DW_TAG_pointer_type ]
!1897 = metadata !{i32 786438, null, metadata !"producer0", metadata !168, i32 46, i64 32, i64 32, i32 0, i32 0, null, metadata !1898, i32 0, null, null} ; [ DW_TAG_class_field_type ]
!1898 = metadata !{metadata !1899}
!1899 = metadata !{i32 786438, metadata !172, metadata !"sc_out<_ap_sc_::sc_dt::sc_lv<32> >", metadata !174, i32 430, i64 32, i64 32, i32 0, i32 0, null, metadata !1900, i32 0, null, metadata !1692} ; [ DW_TAG_class_field_type ]
!1900 = metadata !{metadata !1901}
!1901 = metadata !{i32 786438, metadata !172, metadata !"sc_inout<_ap_sc_::sc_dt::sc_lv<32> >", metadata !174, i32 409, i64 32, i64 32, i32 0, i32 0, null, metadata !1902, i32 0, null, metadata !1692} ; [ DW_TAG_class_field_type ]
!1902 = metadata !{metadata !1903}
!1903 = metadata !{i32 786438, metadata !172, metadata !"sc_port_b<_ap_sc_::sc_core::sc_signal_inout_if<_ap_sc_::sc_dt::sc_lv<32> > >", metadata !174, i32 268, i64 32, i64 32, i32 0, i32 0, null, metadata !1904, i32 0, null, metadata !1666} ; [ DW_TAG_class_field_type ]
!1904 = metadata !{metadata !1905}
!1905 = metadata !{i32 786438, metadata !172, metadata !"sc_signal_inout_if<_ap_sc_::sc_dt::sc_lv<32> >", metadata !174, i32 193, i64 32, i64 32, i32 0, i32 0, null, metadata !1906, i32 0, null, metadata !1630} ; [ DW_TAG_class_field_type ]
!1906 = metadata !{metadata !1907}
!1907 = metadata !{i32 786438, metadata !172, metadata !"sc_signal_in_if<_ap_sc_::sc_dt::sc_lv<32> >", metadata !174, i32 172, i64 32, i64 32, i32 0, i32 0, null, metadata !1908, i32 0, null, metadata !1630} ; [ DW_TAG_class_field_type ]
!1908 = metadata !{metadata !1909}
!1909 = metadata !{i32 786438, metadata !974, metadata !"sc_lv<32>", metadata !976, i32 880, i64 32, i64 32, i32 0, i32 0, null, metadata !1861, i32 0, null, metadata !1610} ; [ DW_TAG_class_field_type ]
!1910 = metadata !{i32 790531, metadata !161, metadata !"producer0.axi_master_0.m_if.Val", null, i32 78, metadata !1911, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!1911 = metadata !{i32 786447, null, metadata !"", null, i32 0, i64 64, i64 64, i64 0, i32 0, metadata !1912} ; [ DW_TAG_pointer_type ]
!1912 = metadata !{i32 786438, null, metadata !"producer0", metadata !168, i32 46, i64 32, i64 32, i32 0, i32 0, null, metadata !1913, i32 0, null, null} ; [ DW_TAG_class_field_type ]
!1913 = metadata !{metadata !1914}
!1914 = metadata !{i32 786438, null, metadata !"AXI4M_bus_port<int>", metadata !1703, i32 6, i64 32, i64 32, i32 0, i32 0, null, metadata !1915, i32 0, null, metadata !1739} ; [ DW_TAG_class_field_type ]
!1915 = metadata !{metadata !1916}
!1916 = metadata !{i32 786438, metadata !172, metadata !"sc_port_b<hls_bus_if<int> >", metadata !174, i32 268, i64 32, i64 32, i32 0, i32 0, null, metadata !1917, i32 0, null, metadata !1765} ; [ DW_TAG_class_field_type ]
!1917 = metadata !{metadata !1918}
!1918 = metadata !{i32 786438, null, metadata !"hls_bus_if<int>", metadata !1711, i32 64, i64 32, i64 32, i32 0, i32 0, null, metadata !1919, i32 0, null, metadata !1739} ; [ DW_TAG_class_field_type ]
!1919 = metadata !{metadata !1714}
!1920 = metadata !{i32 79, i32 46, metadata !1921, null}
!1921 = metadata !{i32 786443, metadata !162, i32 79, i32 1, metadata !163, i32 5} ; [ DW_TAG_lexical_block ]
!1922 = metadata !{i32 80, i32 4, metadata !1921, null}
!1923 = metadata !{i32 81, i32 4, metadata !1921, null}
!1924 = metadata !{i32 82, i32 4, metadata !1921, null}
!1925 = metadata !{i32 83, i32 4, metadata !1921, null}
!1926 = metadata !{i32 84, i32 4, metadata !1921, null}
!1927 = metadata !{i32 85, i32 4, metadata !1921, null}
!1928 = metadata !{i32 86, i32 4, metadata !1921, null}
!1929 = metadata !{i32 786688, metadata !1921, metadata !"_ssdm_reset_v", metadata !163, i32 86, metadata !322, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!1930 = metadata !{i32 790531, metadata !1931, metadata !"producer0.clock.m_if.Val", null, i32 672, metadata !1839, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!1931 = metadata !{i32 786689, metadata !1932, metadata !"this", metadata !163, i32 16777888, metadata !1838, i32 64, metadata !1933} ; [ DW_TAG_arg_variable ]
!1932 = metadata !{i32 786478, i32 0, null, metadata !"MakeSignalsInactive", metadata !"MakeSignalsInactive", metadata !"_ZN11producer019MakeSignalsInactiveEv", metadata !163, i32 672, metadata !164, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, metadata !1836, metadata !186, i32 672} ; [ DW_TAG_subprogram ]
!1933 = metadata !{i32 82, i32 1, metadata !1921, null}
!1934 = metadata !{i32 672, i32 19, metadata !1932, metadata !1933}
!1935 = metadata !{i32 790531, metadata !1931, metadata !"producer0.reset.m_if.Val", null, i32 672, metadata !1839, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!1936 = metadata !{i32 790531, metadata !1931, metadata !"producer0.fifo_in_0.m_if.Val.V", null, i32 672, metadata !1851, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!1937 = metadata !{i32 790531, metadata !1931, metadata !"producer0.fifo_out_0.m_if.Val.V", null, i32 672, metadata !1867, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!1938 = metadata !{i32 790531, metadata !1931, metadata !"producer0.RegisterWriteEnablePort_0.m_if.Val.V", null, i32 672, metadata !1876, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!1939 = metadata !{i32 790531, metadata !1931, metadata !"producer0.RegisterWriteDataPort_0.m_if.Val.V", null, i32 672, metadata !1896, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!1940 = metadata !{i32 790531, metadata !1931, metadata !"producer0.axi_master_0.m_if.Val", null, i32 672, metadata !1911, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!1941 = metadata !{i32 790531, metadata !1942, metadata !"P.V", null, i32 108, metadata !1962, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!1942 = metadata !{i32 786689, metadata !1943, metadata !"P", metadata !174, i32 16777324, metadata !1946, i32 0, metadata !1950} ; [ DW_TAG_arg_variable ]
!1943 = metadata !{i32 786478, i32 0, metadata !174, metadata !"_ssdm_op_WRITE<_ap_sc_::sc_dt::sc_lv<1>, _ap_sc_::sc_dt::sc_lv<1> >", metadata !"_ssdm_op_WRITE<_ap_sc_::sc_dt::sc_lv<1>, _ap_sc_::sc_dt::sc_lv<1> >", metadata !"_Z14_ssdm_op_WRITEIN7_ap_sc_5sc_dt5sc_lvILi1EEES3_EvRVT_RKT0_", metadata !174, i32 108, metadata !1944, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, metadata !1947, null, metadata !186, i32 61} ; [ DW_TAG_subprogram ]
!1944 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1945, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1945 = metadata !{null, metadata !1946, metadata !1355}
!1946 = metadata !{i32 786448, null, null, null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !972} ; [ DW_TAG_reference_type ]
!1947 = metadata !{metadata !1948, metadata !1949}
!1948 = metadata !{i32 786479, null, metadata !"T1", metadata !973, null, i32 0, i32 0} ; [ DW_TAG_template_type_parameter ]
!1949 = metadata !{i32 786479, null, metadata !"T2", metadata !973, null, i32 0, i32 0} ; [ DW_TAG_template_type_parameter ]
!1950 = metadata !{i32 207, i32 13, metadata !1951, metadata !1957}
!1951 = metadata !{i32 786443, metadata !1952, i32 205, i32 73, metadata !174, i32 34} ; [ DW_TAG_lexical_block ]
!1952 = metadata !{i32 786478, i32 0, metadata !172, metadata !"write<_ap_sc_::sc_dt::sc_lv<1> >", metadata !"write<_ap_sc_::sc_dt::sc_lv<1> >", metadata !"_ZN7_ap_sc_7sc_core18sc_signal_inout_ifINS_5sc_dt5sc_lvILi1EEEE5writeIS4_EEvRKT_", metadata !174, i32 205, metadata !1953, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, metadata !1955, null, metadata !186, i32 205} ; [ DW_TAG_subprogram ]
!1953 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1954, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1954 = metadata !{null, metadata !1438, metadata !1355}
!1955 = metadata !{metadata !1956}
!1956 = metadata !{i32 786479, null, metadata !"_T2", metadata !973, null, i32 0, i32 0} ; [ DW_TAG_template_type_parameter ]
!1957 = metadata !{i32 417, i32 73, metadata !1958, metadata !1960}
!1958 = metadata !{i32 786443, metadata !1959, i32 417, i32 71, metadata !174, i32 33} ; [ DW_TAG_lexical_block ]
!1959 = metadata !{i32 786478, i32 0, metadata !172, metadata !"write", metadata !"write", metadata !"_ZN7_ap_sc_7sc_core8sc_inoutINS_5sc_dt5sc_lvILi1EEEE5writeERKS4_", metadata !174, i32 417, metadata !1479, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, metadata !1478, metadata !186, i32 417} ; [ DW_TAG_subprogram ]
!1960 = metadata !{i32 673, i32 2, metadata !1961, metadata !1933}
!1961 = metadata !{i32 786443, metadata !1932, i32 672, i32 1, metadata !163, i32 23} ; [ DW_TAG_lexical_block ]
!1962 = metadata !{i32 786447, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !1889} ; [ DW_TAG_pointer_type ]
!1963 = metadata !{i32 108, i32 72, metadata !1943, metadata !1950}
!1964 = metadata !{i32 66, i32 5, metadata !1965, metadata !1950}
!1965 = metadata !{i32 786443, metadata !1943, i32 61, i32 90, metadata !1966, i32 35} ; [ DW_TAG_lexical_block ]
!1966 = metadata !{i32 786473, metadata !"/tools/Xilinx/Vivado/2018.3/common/technology/autopilot/ap_sysc/ap_sc_extras.h", metadata !"/home/julien/education/inf8500/lab2/tutorial_ju/solutions/tutorial/architectures/hw_sw/build/estimation/hls/producer0", null} ; [ DW_TAG_file_type ]
!1967 = metadata !{i32 790531, metadata !1968, metadata !"P.V", null, i32 108, metadata !1987, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!1968 = metadata !{i32 786689, metadata !1969, metadata !"P", metadata !174, i32 16777324, metadata !1972, i32 0, metadata !1976} ; [ DW_TAG_arg_variable ]
!1969 = metadata !{i32 786478, i32 0, metadata !174, metadata !"_ssdm_op_WRITE<_ap_sc_::sc_dt::sc_lv<32>, _ap_sc_::sc_dt::sc_lv<32> >", metadata !"_ssdm_op_WRITE<_ap_sc_::sc_dt::sc_lv<32>, _ap_sc_::sc_dt::sc_lv<32> >", metadata !"_Z14_ssdm_op_WRITEIN7_ap_sc_5sc_dt5sc_lvILi32EEES3_EvRVT_RKT0_", metadata !174, i32 108, metadata !1970, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, metadata !1973, null, metadata !186, i32 61} ; [ DW_TAG_subprogram ]
!1970 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1971, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1971 = metadata !{null, metadata !1972, metadata !1552}
!1972 = metadata !{i32 786448, null, null, null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !1522} ; [ DW_TAG_reference_type ]
!1973 = metadata !{metadata !1974, metadata !1975}
!1974 = metadata !{i32 786479, null, metadata !"T1", metadata !1523, null, i32 0, i32 0} ; [ DW_TAG_template_type_parameter ]
!1975 = metadata !{i32 786479, null, metadata !"T2", metadata !1523, null, i32 0, i32 0} ; [ DW_TAG_template_type_parameter ]
!1976 = metadata !{i32 207, i32 13, metadata !1977, metadata !1983}
!1977 = metadata !{i32 786443, metadata !1978, i32 205, i32 73, metadata !174, i32 28} ; [ DW_TAG_lexical_block ]
!1978 = metadata !{i32 786478, i32 0, metadata !172, metadata !"write<_ap_sc_::sc_dt::sc_lv<32> >", metadata !"write<_ap_sc_::sc_dt::sc_lv<32> >", metadata !"_ZN7_ap_sc_7sc_core18sc_signal_inout_ifINS_5sc_dt5sc_lvILi32EEEE5writeIS4_EEvRKT_", metadata !174, i32 205, metadata !1979, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, metadata !1981, null, metadata !186, i32 205} ; [ DW_TAG_subprogram ]
!1979 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1980, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1980 = metadata !{null, metadata !1635, metadata !1552}
!1981 = metadata !{metadata !1982}
!1982 = metadata !{i32 786479, null, metadata !"_T2", metadata !1523, null, i32 0, i32 0} ; [ DW_TAG_template_type_parameter ]
!1983 = metadata !{i32 417, i32 73, metadata !1984, metadata !1986}
!1984 = metadata !{i32 786443, metadata !1985, i32 417, i32 71, metadata !174, i32 27} ; [ DW_TAG_lexical_block ]
!1985 = metadata !{i32 786478, i32 0, metadata !172, metadata !"write", metadata !"write", metadata !"_ZN7_ap_sc_7sc_core8sc_inoutINS_5sc_dt5sc_lvILi32EEEE5writeERKS4_", metadata !174, i32 417, metadata !1676, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, metadata !1675, metadata !186, i32 417} ; [ DW_TAG_subprogram ]
!1986 = metadata !{i32 674, i32 2, metadata !1961, metadata !1933}
!1987 = metadata !{i32 786447, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !1909} ; [ DW_TAG_pointer_type ]
!1988 = metadata !{i32 108, i32 72, metadata !1969, metadata !1976}
!1989 = metadata !{i32 66, i32 5, metadata !1990, metadata !1976}
!1990 = metadata !{i32 786443, metadata !1969, i32 61, i32 90, metadata !1966, i32 29} ; [ DW_TAG_lexical_block ]
!1991 = metadata !{i32 808, i32 19, metadata !1992, metadata !1997}
!1992 = metadata !{i32 786443, metadata !1993, i32 808, i32 17, metadata !174, i32 114} ; [ DW_TAG_lexical_block ]
!1993 = metadata !{i32 786443, metadata !1994, i32 807, i32 58, metadata !174, i32 113} ; [ DW_TAG_lexical_block ]
!1994 = metadata !{i32 786478, i32 0, metadata !172, metadata !"wait", metadata !"wait", metadata !"_ZN7_ap_sc_7sc_core4waitEi", metadata !174, i32 807, metadata !1995, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !186, i32 807} ; [ DW_TAG_subprogram ]
!1995 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !1996, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!1996 = metadata !{null, metadata !322}
!1997 = metadata !{i32 83, i32 1, metadata !1921, null}
!1998 = metadata !{i32 808, i32 19, metadata !1992, metadata !1999}
!1999 = metadata !{i32 84, i32 1, metadata !1921, null}
!2000 = metadata !{i32 84, i32 21, metadata !1921, null}
!2001 = metadata !{i32 97, i32 7, metadata !2002, metadata !2004}
!2002 = metadata !{i32 786443, metadata !2003, i32 96, i32 93, metadata !1711, i32 76} ; [ DW_TAG_lexical_block ]
!2003 = metadata !{i32 786478, i32 0, null, metadata !"write", metadata !"write", metadata !"_ZNV10hls_bus_ifIiE5writeEjPi", metadata !1711, i32 96, metadata !1734, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, metadata !1733, metadata !186, i32 96} ; [ DW_TAG_subprogram ]
!2004 = metadata !{i32 30, i32 9, metadata !2005, metadata !2007}
!2005 = metadata !{i32 786443, metadata !2006, i32 29, i32 94, metadata !1703, i32 75} ; [ DW_TAG_lexical_block ]
!2006 = metadata !{i32 786478, i32 0, null, metadata !"write", metadata !"write", metadata !"_ZNV14AXI4M_bus_portIiE5writeEjPi", metadata !1703, i32 29, metadata !1784, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, metadata !1783, metadata !186, i32 29} ; [ DW_TAG_subprogram ]
!2007 = metadata !{i32 190, i32 163, metadata !2008, metadata !2027}
!2008 = metadata !{i32 786443, metadata !2009, i32 190, i32 399, metadata !163, i32 72} ; [ DW_TAG_lexical_block ]
!2009 = metadata !{i32 786443, metadata !2010, i32 190, i32 354, metadata !163, i32 71} ; [ DW_TAG_lexical_block ]
!2010 = metadata !{i32 786443, metadata !2011, i32 189, i32 147, metadata !163, i32 70} ; [ DW_TAG_lexical_block ]
!2011 = metadata !{i32 786478, i32 0, metadata !163, metadata !"DeviceWriteBusBitwidthIsMultipleOfDataBitwidth_single<false, int, unsigned int, AXI4M_bus_port<int> >", metadata !"DeviceWriteBusBitwidthIsMultipleOfDataBitwidth_single<false, int, unsigned int, AXI4M_bus_port<int> >", metadata !"_Z53DeviceWriteBusBitwidthIsMultipleOfDataBitwidth_singleILb0Eij14AXI4M_bus_portIiEEN15space_enable_ifIXsr17space_is_integralIT1_EE5valueEhE4typeERT2_mPKS3_m", metadata !163, i32 189, metadata !2012, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, metadata !2022, null, metadata !186, i32 189} ; [ DW_TAG_subprogram ]
!2012 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !2013, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!2013 = metadata !{metadata !2014, metadata !2020, metadata !2021, metadata !1834, metadata !386}
!2014 = metadata !{i32 786454, metadata !2015, metadata !"type", metadata !168, i32 110, i64 0, i64 0, i64 0, i32 0, metadata !357} ; [ DW_TAG_typedef ]
!2015 = metadata !{i32 786434, null, metadata !"space_enable_if<true, unsigned char>", metadata !2016, i32 110, i64 8, i64 8, i32 0, i32 0, null, metadata !242, i32 0, null, metadata !2017} ; [ DW_TAG_class_type ]
!2016 = metadata !{i32 786473, metadata !"./space_type_traits.h", metadata !"/home/julien/education/inf8500/lab2/tutorial_ju/solutions/tutorial/architectures/hw_sw/build/estimation/hls/producer0", null} ; [ DW_TAG_file_type ]
!2017 = metadata !{metadata !2018, metadata !2019}
!2018 = metadata !{i32 786480, null, metadata !"B", metadata !200, i64 1, null, i32 0, i32 0} ; [ DW_TAG_template_value_parameter ]
!2019 = metadata !{i32 786479, null, metadata !"T", metadata !357, null, i32 0, i32 0} ; [ DW_TAG_template_type_parameter ]
!2020 = metadata !{i32 786448, null, null, null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !1702} ; [ DW_TAG_reference_type ]
!2021 = metadata !{i32 786454, null, metadata !"uintptr_t", metadata !168, i32 90, i64 0, i64 0, i64 0, i32 0, metadata !386} ; [ DW_TAG_typedef ]
!2022 = metadata !{metadata !2023, metadata !2024, metadata !2025, metadata !2026}
!2023 = metadata !{i32 786480, null, metadata !"IS_ARR", metadata !200, i64 0, null, i32 0, i32 0} ; [ DW_TAG_template_value_parameter ]
!2024 = metadata !{i32 786479, null, metadata !"BusDataT", metadata !322, null, i32 0, i32 0} ; [ DW_TAG_template_type_parameter ]
!2025 = metadata !{i32 786479, null, metadata !"DataT", metadata !376, null, i32 0, i32 0} ; [ DW_TAG_template_type_parameter ]
!2026 = metadata !{i32 786479, null, metadata !"BusType", metadata !1702, null, i32 0, i32 0} ; [ DW_TAG_template_type_parameter ]
!2027 = metadata !{i32 381, i32 0, metadata !2028, metadata !2033}
!2028 = metadata !{i32 786443, metadata !2029, i32 381, i32 1691, metadata !163, i32 69} ; [ DW_TAG_lexical_block ]
!2029 = metadata !{i32 786443, metadata !2030, i32 379, i32 109, metadata !163, i32 66} ; [ DW_TAG_lexical_block ]
!2030 = metadata !{i32 786478, i32 0, metadata !163, metadata !"DeviceWriteRouted_single<int, unsigned int, AXI4M_bus_port<int> >", metadata !"DeviceWriteRouted_single<int, unsigned int, AXI4M_bus_port<int> >", metadata !"_Z24DeviceWriteRouted_singleIij14AXI4M_bus_portIiEEN15space_enable_ifIXoosr19space_is_arithmeticIT0_EE5valuesr13space_is_enumIS3_EE5valueEhE4typeERT1_mPKS3_m", metadata !163, i32 379, metadata !2012, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, metadata !2031, null, metadata !186, i32 379} ; [ DW_TAG_subprogram ]
!2031 = metadata !{metadata !2024, metadata !2032, metadata !2026}
!2032 = metadata !{i32 786479, null, metadata !"NPtr", metadata !376, null, i32 0, i32 0} ; [ DW_TAG_template_type_parameter ]
!2033 = metadata !{i32 618, i32 10, metadata !2034, metadata !2042}
!2034 = metadata !{i32 786443, metadata !2035, i32 616, i32 27, metadata !163, i32 65} ; [ DW_TAG_lexical_block ]
!2035 = metadata !{i32 786443, metadata !2036, i32 615, i32 121, metadata !163, i32 64} ; [ DW_TAG_lexical_block ]
!2036 = metadata !{i32 786478, i32 0, null, metadata !"DeviceWrite_single<const unsigned int *>", metadata !"DeviceWrite_single<const unsigned int *>", metadata !"_ZN11producer018DeviceWrite_singleIPKjEEN15space_enable_ifIXaantsr14space_is_arrayIT_EE5valuesr16space_is_pointerIS4_EE5valueEhE4typeEjmS4_m", metadata !168, i32 169, metadata !2037, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, metadata !2040, null, metadata !186, i32 615} ; [ DW_TAG_subprogram ]
!2037 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !2038, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!2038 = metadata !{metadata !2014, metadata !166, metadata !376, metadata !2021, metadata !2039, metadata !386}
!2039 = metadata !{i32 786470, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !1834} ; [ DW_TAG_const_type ]
!2040 = metadata !{metadata !2041}
!2041 = metadata !{i32 786479, null, metadata !"T", metadata !1834, null, i32 0, i32 0} ; [ DW_TAG_template_type_parameter ]
!2042 = metadata !{i32 177, i32 9, metadata !2043, metadata !2050}
!2043 = metadata !{i32 786443, metadata !2044, i32 175, i32 84, metadata !168, i32 63} ; [ DW_TAG_lexical_block ]
!2044 = metadata !{i32 786478, i32 0, null, metadata !"DeviceWrite_single<unsigned int>", metadata !"DeviceWrite_single<unsigned int>", metadata !"_ZN11producer018DeviceWrite_singleIjEEN15space_enable_ifIXntsr25space_is_pointer_or_arrayIT_EE5valueEhE4typeEjmRKS2_", metadata !168, i32 175, metadata !2045, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, metadata !2048, null, metadata !186, i32 175} ; [ DW_TAG_subprogram ]
!2045 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !2046, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!2046 = metadata !{metadata !2014, metadata !166, metadata !376, metadata !2021, metadata !2047}
!2047 = metadata !{i32 786448, null, null, null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !375} ; [ DW_TAG_reference_type ]
!2048 = metadata !{metadata !2049}
!2049 = metadata !{i32 786479, null, metadata !"T", metadata !376, null, i32 0, i32 0} ; [ DW_TAG_template_type_parameter ]
!2050 = metadata !{i32 86, i32 3, metadata !2051, null}
!2051 = metadata !{i32 786443, metadata !1921, i32 85, i32 1, metadata !163, i32 6} ; [ DW_TAG_lexical_block ]
!2052 = metadata !{i32 84, i32 51, metadata !1921, null}
!2053 = metadata !{i32 790531, metadata !2054, metadata !"producer0.clock.m_if.Val", null, i32 175, metadata !1839, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2054 = metadata !{i32 786689, metadata !2044, metadata !"this", metadata !168, i32 16777391, metadata !1838, i32 64, metadata !2050} ; [ DW_TAG_arg_variable ]
!2055 = metadata !{i32 175, i32 3, metadata !2044, metadata !2050}
!2056 = metadata !{i32 790531, metadata !2054, metadata !"producer0.reset.m_if.Val", null, i32 175, metadata !1839, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2057 = metadata !{i32 790531, metadata !2054, metadata !"producer0.fifo_in_0.m_if.Val.V", null, i32 175, metadata !1851, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2058 = metadata !{i32 790531, metadata !2054, metadata !"producer0.fifo_out_0.m_if.Val.V", null, i32 175, metadata !1867, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2059 = metadata !{i32 790531, metadata !2054, metadata !"producer0.RegisterWriteEnablePort_0.m_if.Val.V", null, i32 175, metadata !1876, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2060 = metadata !{i32 790531, metadata !2054, metadata !"producer0.RegisterWriteDataPort_0.m_if.Val.V", null, i32 175, metadata !1896, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2061 = metadata !{i32 790531, metadata !2054, metadata !"producer0.axi_master_0.m_if.Val", null, i32 175, metadata !1911, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2062 = metadata !{i32 790531, metadata !2063, metadata !"producer0.clock.m_if.Val", null, i32 169, metadata !1839, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2063 = metadata !{i32 786689, metadata !2036, metadata !"this", metadata !168, i32 16777385, metadata !1838, i32 64, metadata !2042} ; [ DW_TAG_arg_variable ]
!2064 = metadata !{i32 169, i32 3, metadata !2036, metadata !2042}
!2065 = metadata !{i32 790531, metadata !2063, metadata !"producer0.reset.m_if.Val", null, i32 169, metadata !1839, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2066 = metadata !{i32 790531, metadata !2063, metadata !"producer0.fifo_in_0.m_if.Val.V", null, i32 169, metadata !1851, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2067 = metadata !{i32 790531, metadata !2063, metadata !"producer0.fifo_out_0.m_if.Val.V", null, i32 169, metadata !1867, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2068 = metadata !{i32 790531, metadata !2063, metadata !"producer0.RegisterWriteEnablePort_0.m_if.Val.V", null, i32 169, metadata !1876, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2069 = metadata !{i32 790531, metadata !2063, metadata !"producer0.RegisterWriteDataPort_0.m_if.Val.V", null, i32 169, metadata !1896, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2070 = metadata !{i32 790531, metadata !2063, metadata !"producer0.axi_master_0.m_if.Val", null, i32 169, metadata !1911, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2071 = metadata !{i32 179, i32 115, metadata !2072, metadata !2001}
!2072 = metadata !{i32 786443, metadata !2073, i32 179, i32 113, metadata !1966, i32 77} ; [ DW_TAG_lexical_block ]
!2073 = metadata !{i32 786478, i32 0, metadata !1966, metadata !"_ssdm_op_WRITE<int>", metadata !"_ssdm_op_WRITE<int>", metadata !"_Z14_ssdm_op_WRITEIiEvRViRKT_", metadata !1966, i32 179, metadata !2074, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, metadata !2078, null, metadata !186, i32 179} ; [ DW_TAG_subprogram ]
!2074 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !2075, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!2075 = metadata !{null, metadata !2076, metadata !2077}
!2076 = metadata !{i32 786448, null, null, null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !1715} ; [ DW_TAG_reference_type ]
!2077 = metadata !{i32 786448, null, null, null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !371} ; [ DW_TAG_reference_type ]
!2078 = metadata !{metadata !2079}
!2079 = metadata !{i32 786479, null, metadata !"T2", metadata !322, null, i32 0, i32 0} ; [ DW_TAG_template_type_parameter ]
!2080 = metadata !{i32 790531, metadata !2081, metadata !"producer0.clock.m_if.Val", null, i32 216, metadata !1839, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2081 = metadata !{i32 786689, metadata !2082, metadata !"this", metadata !168, i32 16777432, metadata !1838, i32 64, metadata !2086} ; [ DW_TAG_arg_variable ]
!2082 = metadata !{i32 786478, i32 0, null, metadata !"RegisterWrite<unsigned int>", metadata !"RegisterWrite<unsigned int>", metadata !"_ZN11producer013RegisterWriteIjEEN15space_enable_ifIXntsr25space_is_pointer_or_arrayIT_EE5valueEhE4typeEjjRKS2_", metadata !168, i32 216, metadata !2083, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, metadata !2085, null, metadata !186, i32 216} ; [ DW_TAG_subprogram ]
!2083 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !2084, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!2084 = metadata !{metadata !2014, metadata !166, metadata !376, metadata !1829, metadata !2047}
!2085 = metadata !{metadata !2032}
!2086 = metadata !{i32 87, i32 3, metadata !2051, null}
!2087 = metadata !{i32 216, i32 3, metadata !2082, metadata !2086}
!2088 = metadata !{i32 790531, metadata !2081, metadata !"producer0.reset.m_if.Val", null, i32 216, metadata !1839, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2089 = metadata !{i32 790531, metadata !2081, metadata !"producer0.fifo_in_0.m_if.Val.V", null, i32 216, metadata !1851, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2090 = metadata !{i32 790531, metadata !2081, metadata !"producer0.fifo_out_0.m_if.Val.V", null, i32 216, metadata !1867, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2091 = metadata !{i32 790531, metadata !2081, metadata !"producer0.RegisterWriteEnablePort_0.m_if.Val.V", null, i32 216, metadata !1876, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2092 = metadata !{i32 790531, metadata !2081, metadata !"producer0.RegisterWriteDataPort_0.m_if.Val.V", null, i32 216, metadata !1896, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2093 = metadata !{i32 790531, metadata !2081, metadata !"producer0.axi_master_0.m_if.Val", null, i32 216, metadata !1911, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2094 = metadata !{i32 790531, metadata !2095, metadata !"producer0.clock.m_if.Val", null, i32 210, metadata !1839, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2095 = metadata !{i32 786689, metadata !2096, metadata !"this", metadata !168, i32 16777426, metadata !1838, i32 64, metadata !2099} ; [ DW_TAG_arg_variable ]
!2096 = metadata !{i32 786478, i32 0, null, metadata !"RegisterWrite<unsigned int>", metadata !"RegisterWrite<unsigned int>", metadata !"_ZN11producer013RegisterWriteIjEEhjmPKT_", metadata !168, i32 210, metadata !2097, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, metadata !2048, null, metadata !186, i32 659} ; [ DW_TAG_subprogram ]
!2097 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !2098, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!2098 = metadata !{metadata !1824, metadata !166, metadata !376, metadata !386, metadata !1834}
!2099 = metadata !{i32 218, i32 9, metadata !2100, metadata !2086}
!2100 = metadata !{i32 786443, metadata !2082, i32 216, i32 88, metadata !168, i32 54} ; [ DW_TAG_lexical_block ]
!2101 = metadata !{i32 210, i32 16, metadata !2096, metadata !2099}
!2102 = metadata !{i32 790531, metadata !2095, metadata !"producer0.reset.m_if.Val", null, i32 210, metadata !1839, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2103 = metadata !{i32 790531, metadata !2095, metadata !"producer0.fifo_in_0.m_if.Val.V", null, i32 210, metadata !1851, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2104 = metadata !{i32 790531, metadata !2095, metadata !"producer0.fifo_out_0.m_if.Val.V", null, i32 210, metadata !1867, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2105 = metadata !{i32 790531, metadata !2095, metadata !"producer0.RegisterWriteEnablePort_0.m_if.Val.V", null, i32 210, metadata !1876, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2106 = metadata !{i32 790531, metadata !2095, metadata !"producer0.RegisterWriteDataPort_0.m_if.Val.V", null, i32 210, metadata !1896, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2107 = metadata !{i32 790531, metadata !2095, metadata !"producer0.axi_master_0.m_if.Val", null, i32 210, metadata !1911, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2108 = metadata !{i32 790531, metadata !2109, metadata !"module.clock.m_if.Val", null, i32 510, metadata !1839, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2109 = metadata !{i32 786689, metadata !2110, metadata !"module", metadata !163, i32 16777726, metadata !1838, i32 0, metadata !2113} ; [ DW_TAG_arg_variable ]
!2110 = metadata !{i32 786478, i32 0, metadata !163, metadata !"RegisterWrite_0<unsigned int>", metadata !"RegisterWrite_0<unsigned int>", metadata !"_Z15RegisterWrite_0IjEN15space_enable_ifIXaantsr25space_is_pointer_or_arrayIT_EE5valueoosr17space_is_integralIS1_EE5valuesr13space_is_enumIS1_EE5valueEhE4typeEP11producer0mPKS1_", metadata !163, i32 510, metadata !2111, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, metadata !2085, null, metadata !186, i32 510} ; [ DW_TAG_subprogram ]
!2111 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !2112, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!2112 = metadata !{metadata !2014, metadata !1838, metadata !386, metadata !1834}
!2113 = metadata !{i32 667, i32 10, metadata !2114, metadata !2099}
!2114 = metadata !{i32 786443, metadata !2115, i32 666, i32 49, metadata !163, i32 59} ; [ DW_TAG_lexical_block ]
!2115 = metadata !{i32 786443, metadata !2096, i32 659, i32 114, metadata !163, i32 55} ; [ DW_TAG_lexical_block ]
!2116 = metadata !{i32 510, i32 38, metadata !2110, metadata !2113}
!2117 = metadata !{i32 790531, metadata !2109, metadata !"module.reset.m_if.Val", null, i32 510, metadata !1839, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2118 = metadata !{i32 790531, metadata !2109, metadata !"module.fifo_in_0.m_if.Val.V", null, i32 510, metadata !1851, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2119 = metadata !{i32 790531, metadata !2109, metadata !"module.fifo_out_0.m_if.Val.V", null, i32 510, metadata !1867, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2120 = metadata !{i32 790531, metadata !2109, metadata !"module.RegisterWriteEnablePort_0.m_if.Val.V", null, i32 510, metadata !1876, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2121 = metadata !{i32 790531, metadata !2109, metadata !"module.RegisterWriteDataPort_0.m_if.Val.V", null, i32 510, metadata !1896, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2122 = metadata !{i32 790531, metadata !2109, metadata !"module.axi_master_0.m_if.Val", null, i32 510, metadata !1911, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2123 = metadata !{i32 786689, metadata !2124, metadata !"v", metadata !976, i32 33555387, metadata !376, i32 0, metadata !2125} ; [ DW_TAG_arg_variable ]
!2124 = metadata !{i32 786478, i32 0, metadata !974, metadata !"sc_lv", metadata !"sc_lv", metadata !"_ZN7_ap_sc_5sc_dt5sc_lvILi32EEC1Ej", metadata !976, i32 955, metadata !1574, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, metadata !1573, metadata !186, i32 955} ; [ DW_TAG_subprogram ]
!2125 = metadata !{i32 511, i32 2, metadata !2126, metadata !2113}
!2126 = metadata !{i32 786443, metadata !2110, i32 510, i32 91, metadata !163, i32 60} ; [ DW_TAG_lexical_block ]
!2127 = metadata !{i32 955, i32 66, metadata !2124, metadata !2125}
!2128 = metadata !{i32 786689, metadata !2129, metadata !"v", metadata !976, i32 33555387, metadata !376, i32 0, metadata !2130} ; [ DW_TAG_arg_variable ]
!2129 = metadata !{i32 786478, i32 0, metadata !974, metadata !"sc_lv", metadata !"sc_lv", metadata !"_ZN7_ap_sc_5sc_dt5sc_lvILi32EEC2Ej", metadata !976, i32 955, metadata !1574, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, metadata !1573, metadata !186, i32 955} ; [ DW_TAG_subprogram ]
!2130 = metadata !{i32 955, i32 87, metadata !2124, metadata !2125}
!2131 = metadata !{i32 955, i32 66, metadata !2129, metadata !2130}
!2132 = metadata !{i32 786689, metadata !2133, metadata !"op", metadata !308, i32 33554678, metadata !375, i32 0, metadata !2134} ; [ DW_TAG_arg_variable ]
!2133 = metadata !{i32 786478, i32 0, null, metadata !"ap_int_base", metadata !"ap_int_base", metadata !"_ZN11ap_int_baseILi32ELb0EEC2Ej", metadata !308, i32 246, metadata !373, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, metadata !372, metadata !186, i32 246} ; [ DW_TAG_subprogram ]
!2134 = metadata !{i32 955, i32 85, metadata !2129, metadata !2130}
!2135 = metadata !{i32 246, i32 72, metadata !2133, metadata !2134}
!2136 = metadata !{i32 790529, metadata !2137, metadata !"v.V", null, i32 206, metadata !1909, i32 0, metadata !2138} ; [ DW_TAG_auto_variable_field ]
!2137 = metadata !{i32 786688, metadata !1977, metadata !"v", metadata !174, i32 206, metadata !1523, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!2138 = metadata !{i32 417, i32 73, metadata !1984, metadata !2125}
!2139 = metadata !{i32 206, i32 21, metadata !1977, metadata !2138}
!2140 = metadata !{i32 790531, metadata !2141, metadata !"P.V", null, i32 108, metadata !1987, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2141 = metadata !{i32 786689, metadata !1969, metadata !"P", metadata !174, i32 16777324, metadata !1972, i32 0, metadata !2142} ; [ DW_TAG_arg_variable ]
!2142 = metadata !{i32 207, i32 13, metadata !1977, metadata !2138}
!2143 = metadata !{i32 108, i32 72, metadata !1969, metadata !2142}
!2144 = metadata !{i32 790529, metadata !2145, metadata !"tmp.V", null, i32 65, metadata !1909, i32 0, i32 0} ; [ DW_TAG_auto_variable_field ]
!2145 = metadata !{i32 786688, metadata !1990, metadata !"tmp", metadata !1966, i32 65, metadata !1523, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!2146 = metadata !{i32 65, i32 17, metadata !1990, metadata !2142}
!2147 = metadata !{i32 66, i32 5, metadata !1990, metadata !2142}
!2148 = metadata !{i32 790531, metadata !2149, metadata !"P.V", null, i32 108, metadata !1962, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2149 = metadata !{i32 786689, metadata !1943, metadata !"P", metadata !174, i32 16777324, metadata !1946, i32 0, metadata !2150} ; [ DW_TAG_arg_variable ]
!2150 = metadata !{i32 207, i32 13, metadata !1951, metadata !2151}
!2151 = metadata !{i32 417, i32 73, metadata !1958, metadata !2152}
!2152 = metadata !{i32 512, i32 2, metadata !2126, metadata !2113}
!2153 = metadata !{i32 108, i32 72, metadata !1943, metadata !2150}
!2154 = metadata !{i32 66, i32 5, metadata !1965, metadata !2150}
!2155 = metadata !{i32 808, i32 19, metadata !1992, metadata !2156}
!2156 = metadata !{i32 513, i32 2, metadata !2126, metadata !2113}
!2157 = metadata !{i32 790531, metadata !2158, metadata !"P.V", null, i32 108, metadata !1962, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2158 = metadata !{i32 786689, metadata !1943, metadata !"P", metadata !174, i32 16777324, metadata !1946, i32 0, metadata !2159} ; [ DW_TAG_arg_variable ]
!2159 = metadata !{i32 207, i32 13, metadata !1951, metadata !2160}
!2160 = metadata !{i32 417, i32 73, metadata !1958, metadata !2161}
!2161 = metadata !{i32 514, i32 2, metadata !2126, metadata !2113}
!2162 = metadata !{i32 108, i32 72, metadata !1943, metadata !2159}
!2163 = metadata !{i32 66, i32 5, metadata !1965, metadata !2159}
!2164 = metadata !{i32 790531, metadata !2165, metadata !"producer0.clock.m_if.Val", null, i32 115, metadata !1839, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2165 = metadata !{i32 786689, metadata !2166, metadata !"this", metadata !168, i32 16777331, metadata !1838, i32 64, metadata !2167} ; [ DW_TAG_arg_variable ]
!2166 = metadata !{i32 786478, i32 0, null, metadata !"ModuleWrite_0", metadata !"ModuleWrite_0", metadata !"_ZN11producer013ModuleWrite_0Ejjj", metadata !168, i32 115, metadata !1827, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, metadata !1835, metadata !186, i32 123} ; [ DW_TAG_subprogram ]
!2167 = metadata !{i32 90, i32 17, metadata !2051, null}
!2168 = metadata !{i32 115, i32 23, metadata !2166, metadata !2167}
!2169 = metadata !{i32 790531, metadata !2165, metadata !"producer0.reset.m_if.Val", null, i32 115, metadata !1839, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2170 = metadata !{i32 790531, metadata !2165, metadata !"producer0.fifo_in_0.m_if.Val.V", null, i32 115, metadata !1851, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2171 = metadata !{i32 790531, metadata !2165, metadata !"producer0.fifo_out_0.m_if.Val.V", null, i32 115, metadata !1867, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2172 = metadata !{i32 790531, metadata !2165, metadata !"producer0.RegisterWriteEnablePort_0.m_if.Val.V", null, i32 115, metadata !1876, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2173 = metadata !{i32 790531, metadata !2165, metadata !"producer0.RegisterWriteDataPort_0.m_if.Val.V", null, i32 115, metadata !1896, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2174 = metadata !{i32 790531, metadata !2165, metadata !"producer0.axi_master_0.m_if.Val", null, i32 115, metadata !1911, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2175 = metadata !{i32 790531, metadata !2176, metadata !"producer0.clock.m_if.Val", null, i32 574, metadata !1839, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2176 = metadata !{i32 786689, metadata !2177, metadata !"this", metadata !163, i32 16777790, metadata !1838, i32 64, metadata !2178} ; [ DW_TAG_arg_variable ]
!2177 = metadata !{i32 786478, i32 0, null, metadata !"ModuleWrite_0", metadata !"ModuleWrite_0", metadata !"_ZN11producer013ModuleWrite_0EjjmPKjm", metadata !163, i32 574, metadata !1832, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, metadata !1831, metadata !186, i32 574} ; [ DW_TAG_subprogram ]
!2178 = metadata !{i32 126, i32 11, metadata !2179, metadata !2167}
!2179 = metadata !{i32 786443, metadata !2166, i32 123, i32 1, metadata !168, i32 111} ; [ DW_TAG_lexical_block ]
!2180 = metadata !{i32 574, i32 34, metadata !2177, metadata !2178}
!2181 = metadata !{i32 790531, metadata !2176, metadata !"producer0.reset.m_if.Val", null, i32 574, metadata !1839, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2182 = metadata !{i32 790531, metadata !2176, metadata !"producer0.fifo_in_0.m_if.Val.V", null, i32 574, metadata !1851, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2183 = metadata !{i32 790531, metadata !2176, metadata !"producer0.fifo_out_0.m_if.Val.V", null, i32 574, metadata !1867, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2184 = metadata !{i32 790531, metadata !2176, metadata !"producer0.RegisterWriteEnablePort_0.m_if.Val.V", null, i32 574, metadata !1876, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2185 = metadata !{i32 790531, metadata !2176, metadata !"producer0.RegisterWriteDataPort_0.m_if.Val.V", null, i32 574, metadata !1896, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2186 = metadata !{i32 790531, metadata !2176, metadata !"producer0.axi_master_0.m_if.Val", null, i32 574, metadata !1911, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2187 = metadata !{i32 790531, metadata !2188, metadata !"module.clock.m_if.Val", null, i32 473, metadata !1839, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2188 = metadata !{i32 786689, metadata !2189, metadata !"module", metadata !163, i32 16777689, metadata !1838, i32 0, metadata !2192} ; [ DW_TAG_arg_variable ]
!2189 = metadata !{i32 786478, i32 0, metadata !163, metadata !"ModuleWriteMSA_0", metadata !"ModuleWriteMSA_0", metadata !"_Z16ModuleWriteMSA_0P11producer0jmPKjm", metadata !163, i32 473, metadata !2190, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !186, i32 473} ; [ DW_TAG_subprogram ]
!2190 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !2191, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!2191 = metadata !{metadata !1824, metadata !1838, metadata !376, metadata !386, metadata !1834, metadata !386}
!2192 = metadata !{i32 577, i32 10, metadata !2193, metadata !2178}
!2193 = metadata !{i32 786443, metadata !2177, i32 574, i32 1, metadata !163, i32 112} ; [ DW_TAG_lexical_block ]
!2194 = metadata !{i32 473, i32 44, metadata !2189, metadata !2192}
!2195 = metadata !{i32 790531, metadata !2188, metadata !"module.reset.m_if.Val", null, i32 473, metadata !1839, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2196 = metadata !{i32 790531, metadata !2188, metadata !"module.fifo_in_0.m_if.Val.V", null, i32 473, metadata !1851, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2197 = metadata !{i32 790531, metadata !2188, metadata !"module.fifo_out_0.m_if.Val.V", null, i32 473, metadata !1867, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2198 = metadata !{i32 790531, metadata !2188, metadata !"module.RegisterWriteEnablePort_0.m_if.Val.V", null, i32 473, metadata !1876, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2199 = metadata !{i32 790531, metadata !2188, metadata !"module.RegisterWriteDataPort_0.m_if.Val.V", null, i32 473, metadata !1896, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2200 = metadata !{i32 790531, metadata !2188, metadata !"module.axi_master_0.m_if.Val", null, i32 473, metadata !1911, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2201 = metadata !{i32 474, i32 3, metadata !2202, metadata !2192}
!2202 = metadata !{i32 786443, metadata !2203, i32 474, i32 2, metadata !163, i32 16} ; [ DW_TAG_lexical_block ]
!2203 = metadata !{i32 786443, metadata !2189, i32 473, i32 152, metadata !163, i32 15} ; [ DW_TAG_lexical_block ]
!2204 = metadata !{i32 475, i32 1, metadata !2202, metadata !2192}
!2205 = metadata !{i32 476, i32 2, metadata !2202, metadata !2192}
!2206 = metadata !{i32 790531, metadata !2207, metadata !"P.V", null, i32 157, metadata !2222, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2207 = metadata !{i32 786689, metadata !2208, metadata !"P", metadata !174, i32 16777373, metadata !2211, i32 0, metadata !2215} ; [ DW_TAG_arg_variable ]
!2208 = metadata !{i32 786478, i32 0, metadata !174, metadata !"_ssdm_op_TLM_CAN_PUT<const ap_uint<32> >", metadata !"_ssdm_op_TLM_CAN_PUT<const ap_uint<32> >", metadata !"_Z20_ssdm_op_TLM_CAN_PUTIK7ap_uintILi32EEEbRVT_", metadata !174, i32 157, metadata !2209, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, metadata !2213, null, metadata !186, i32 153} ; [ DW_TAG_subprogram ]
!2209 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !2210, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!2210 = metadata !{metadata !200, metadata !2211}
!2211 = metadata !{i32 786448, null, null, null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !2212} ; [ DW_TAG_reference_type ]
!2212 = metadata !{i32 786485, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !742} ; [ DW_TAG_volatile_type ]
!2213 = metadata !{metadata !2214}
!2214 = metadata !{i32 786479, null, metadata !"T", metadata !742, null, i32 0, i32 0} ; [ DW_TAG_template_type_parameter ]
!2215 = metadata !{i32 250, i32 20, metadata !2216, metadata !2218}
!2216 = metadata !{i32 786443, metadata !2217, i32 249, i32 69, metadata !174, i32 43} ; [ DW_TAG_lexical_block ]
!2217 = metadata !{i32 786478, i32 0, metadata !172, metadata !"num_free", metadata !"num_free", metadata !"_ZNK7_ap_sc_7sc_core14sc_fifo_out_ifI7ap_uintILi32EEE8num_freeEv", metadata !174, i32 249, metadata !907, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, metadata !906, metadata !186, i32 249} ; [ DW_TAG_subprogram ]
!2218 = metadata !{i32 496, i32 72, metadata !2219, metadata !2221}
!2219 = metadata !{i32 786443, metadata !2220, i32 496, i32 63, metadata !174, i32 42} ; [ DW_TAG_lexical_block ]
!2220 = metadata !{i32 786478, i32 0, metadata !172, metadata !"num_free", metadata !"num_free", metadata !"_ZN7_ap_sc_7sc_core11sc_fifo_outI7ap_uintILi32EEE8num_freeEv", metadata !174, i32 496, metadata !952, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, metadata !951, metadata !186, i32 496} ; [ DW_TAG_subprogram ]
!2221 = metadata !{i32 481, i32 23, metadata !2203, metadata !2192}
!2222 = metadata !{i32 786447, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !1860} ; [ DW_TAG_pointer_type ]
!2223 = metadata !{i32 157, i32 77, metadata !2208, metadata !2215}
!2224 = metadata !{i32 154, i32 12, metadata !2225, metadata !2215}
!2225 = metadata !{i32 786443, metadata !2208, i32 153, i32 80, metadata !1966, i32 44} ; [ DW_TAG_lexical_block ]
!2226 = metadata !{i32 484, i32 50, metadata !2227, metadata !2192}
!2227 = metadata !{i32 786443, metadata !2228, i32 484, i32 49, metadata !163, i32 19} ; [ DW_TAG_lexical_block ]
!2228 = metadata !{i32 786443, metadata !2229, i32 484, i32 13, metadata !163, i32 18} ; [ DW_TAG_lexical_block ]
!2229 = metadata !{i32 786443, metadata !2203, i32 483, i32 54, metadata !163, i32 17} ; [ DW_TAG_lexical_block ]
!2230 = metadata !{i32 485, i32 1, metadata !2227, metadata !2192}
!2231 = metadata !{i32 790531, metadata !2232, metadata !"P.V", null, i32 108, metadata !2222, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2232 = metadata !{i32 786689, metadata !2233, metadata !"P", metadata !174, i32 16777324, metadata !2236, i32 0, metadata !2240} ; [ DW_TAG_arg_variable ]
!2233 = metadata !{i32 786478, i32 0, metadata !174, metadata !"_ssdm_op_WRITE<ap_uint<32>, ap_uint<32> >", metadata !"_ssdm_op_WRITE<ap_uint<32>, ap_uint<32> >", metadata !"_Z14_ssdm_op_WRITEI7ap_uintILi32EES1_EvRVT_RKT0_", metadata !174, i32 108, metadata !2234, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, metadata !2237, null, metadata !186, i32 61} ; [ DW_TAG_subprogram ]
!2234 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !2235, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!2235 = metadata !{null, metadata !2236, metadata !741}
!2236 = metadata !{i32 786448, null, null, null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !302} ; [ DW_TAG_reference_type ]
!2237 = metadata !{metadata !2238, metadata !2239}
!2238 = metadata !{i32 786479, null, metadata !"T1", metadata !303, null, i32 0, i32 0} ; [ DW_TAG_template_type_parameter ]
!2239 = metadata !{i32 786479, null, metadata !"T2", metadata !303, null, i32 0, i32 0} ; [ DW_TAG_template_type_parameter ]
!2240 = metadata !{i32 244, i32 73, metadata !2241, metadata !2243}
!2241 = metadata !{i32 786443, metadata !2242, i32 244, i32 71, metadata !174, i32 37} ; [ DW_TAG_lexical_block ]
!2242 = metadata !{i32 786478, i32 0, metadata !172, metadata !"write", metadata !"write", metadata !"_ZN7_ap_sc_7sc_core14sc_fifo_out_ifI7ap_uintILi32EEE5writeERKS3_", metadata !174, i32 244, metadata !901, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, metadata !900, metadata !186, i32 244} ; [ DW_TAG_subprogram ]
!2243 = metadata !{i32 494, i32 72, metadata !2244, metadata !2246}
!2244 = metadata !{i32 786443, metadata !2245, i32 494, i32 70, metadata !174, i32 36} ; [ DW_TAG_lexical_block ]
!2245 = metadata !{i32 786478, i32 0, metadata !172, metadata !"write", metadata !"write", metadata !"_ZN7_ap_sc_7sc_core11sc_fifo_outI7ap_uintILi32EEE5writeERKS3_", metadata !174, i32 494, metadata !946, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, metadata !945, metadata !186, i32 494} ; [ DW_TAG_subprogram ]
!2246 = metadata !{i32 490, i32 3, metadata !2227, metadata !2192}
!2247 = metadata !{i32 108, i32 72, metadata !2233, metadata !2240}
!2248 = metadata !{i32 66, i32 5, metadata !2249, metadata !2240}
!2249 = metadata !{i32 786443, metadata !2233, i32 61, i32 90, metadata !1966, i32 38} ; [ DW_TAG_lexical_block ]
!2250 = metadata !{i32 494, i32 3, metadata !2227, metadata !2192}
!2251 = metadata !{i32 499, i32 3, metadata !2252, metadata !2192}
!2252 = metadata !{i32 786443, metadata !2203, i32 499, i32 2, metadata !163, i32 22} ; [ DW_TAG_lexical_block ]
!2253 = metadata !{i32 500, i32 1, metadata !2252, metadata !2192}
!2254 = metadata !{i32 501, i32 2, metadata !2252, metadata !2192}
!2255 = metadata !{i32 790531, metadata !2256, metadata !"producer0.clock.m_if.Val", null, i32 85, metadata !1839, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2256 = metadata !{i32 786689, metadata !2257, metadata !"this", metadata !168, i32 16777301, metadata !1838, i32 64, metadata !2258} ; [ DW_TAG_arg_variable ]
!2257 = metadata !{i32 786478, i32 0, null, metadata !"ModuleRead_0", metadata !"ModuleRead_0", metadata !"_ZN11producer012ModuleRead_0Ejjj", metadata !168, i32 85, metadata !1827, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, metadata !1826, metadata !186, i32 91} ; [ DW_TAG_subprogram ]
!2258 = metadata !{i32 91, i32 16, metadata !2051, null}
!2259 = metadata !{i32 85, i32 23, metadata !2257, metadata !2258}
!2260 = metadata !{i32 790531, metadata !2256, metadata !"producer0.reset.m_if.Val", null, i32 85, metadata !1839, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2261 = metadata !{i32 790531, metadata !2256, metadata !"producer0.fifo_in_0.m_if.Val.V", null, i32 85, metadata !1851, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2262 = metadata !{i32 790531, metadata !2256, metadata !"producer0.fifo_out_0.m_if.Val.V", null, i32 85, metadata !1867, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2263 = metadata !{i32 790531, metadata !2256, metadata !"producer0.RegisterWriteEnablePort_0.m_if.Val.V", null, i32 85, metadata !1876, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2264 = metadata !{i32 790531, metadata !2256, metadata !"producer0.RegisterWriteDataPort_0.m_if.Val.V", null, i32 85, metadata !1896, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2265 = metadata !{i32 790531, metadata !2256, metadata !"producer0.axi_master_0.m_if.Val", null, i32 85, metadata !1911, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2266 = metadata !{i32 790531, metadata !2267, metadata !"producer0.clock.m_if.Val", null, i32 552, metadata !1839, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2267 = metadata !{i32 786689, metadata !2268, metadata !"this", metadata !163, i32 16777768, metadata !1838, i32 64, metadata !2269} ; [ DW_TAG_arg_variable ]
!2268 = metadata !{i32 786478, i32 0, null, metadata !"ModuleRead_0", metadata !"ModuleRead_0", metadata !"_ZN11producer012ModuleRead_0EjjmPjm", metadata !163, i32 552, metadata !1822, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, metadata !1821, metadata !186, i32 552} ; [ DW_TAG_subprogram ]
!2269 = metadata !{i32 94, i32 11, metadata !2270, metadata !2258}
!2270 = metadata !{i32 786443, metadata !2257, i32 91, i32 1, metadata !168, i32 109} ; [ DW_TAG_lexical_block ]
!2271 = metadata !{i32 552, i32 34, metadata !2268, metadata !2269}
!2272 = metadata !{i32 790531, metadata !2267, metadata !"producer0.reset.m_if.Val", null, i32 552, metadata !1839, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2273 = metadata !{i32 790531, metadata !2267, metadata !"producer0.fifo_in_0.m_if.Val.V", null, i32 552, metadata !1851, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2274 = metadata !{i32 790531, metadata !2267, metadata !"producer0.fifo_out_0.m_if.Val.V", null, i32 552, metadata !1867, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2275 = metadata !{i32 790531, metadata !2267, metadata !"producer0.RegisterWriteEnablePort_0.m_if.Val.V", null, i32 552, metadata !1876, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2276 = metadata !{i32 790531, metadata !2267, metadata !"producer0.RegisterWriteDataPort_0.m_if.Val.V", null, i32 552, metadata !1896, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2277 = metadata !{i32 790531, metadata !2267, metadata !"producer0.axi_master_0.m_if.Val", null, i32 552, metadata !1911, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2278 = metadata !{i32 790531, metadata !2279, metadata !"module.clock.m_if.Val", null, i32 444, metadata !1839, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2279 = metadata !{i32 786689, metadata !2280, metadata !"module", metadata !163, i32 16777660, metadata !1838, i32 0, metadata !2283} ; [ DW_TAG_arg_variable ]
!2280 = metadata !{i32 786478, i32 0, metadata !163, metadata !"ModuleReadMSA_0", metadata !"ModuleReadMSA_0", metadata !"_Z15ModuleReadMSA_0P11producer0jmPjm", metadata !163, i32 444, metadata !2281, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !186, i32 444} ; [ DW_TAG_subprogram ]
!2281 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !2282, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!2282 = metadata !{metadata !1824, metadata !1838, metadata !376, metadata !386, metadata !1825, metadata !386}
!2283 = metadata !{i32 555, i32 10, metadata !2284, metadata !2269}
!2284 = metadata !{i32 786443, metadata !2268, i32 552, i32 1, metadata !163, i32 110} ; [ DW_TAG_lexical_block ]
!2285 = metadata !{i32 444, i32 43, metadata !2280, metadata !2283}
!2286 = metadata !{i32 790531, metadata !2279, metadata !"module.reset.m_if.Val", null, i32 444, metadata !1839, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2287 = metadata !{i32 790531, metadata !2279, metadata !"module.fifo_in_0.m_if.Val.V", null, i32 444, metadata !1851, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2288 = metadata !{i32 790531, metadata !2279, metadata !"module.fifo_out_0.m_if.Val.V", null, i32 444, metadata !1867, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2289 = metadata !{i32 790531, metadata !2279, metadata !"module.RegisterWriteEnablePort_0.m_if.Val.V", null, i32 444, metadata !1876, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2290 = metadata !{i32 790531, metadata !2279, metadata !"module.RegisterWriteDataPort_0.m_if.Val.V", null, i32 444, metadata !1896, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2291 = metadata !{i32 790531, metadata !2279, metadata !"module.axi_master_0.m_if.Val", null, i32 444, metadata !1911, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2292 = metadata !{i32 445, i32 3, metadata !2293, metadata !2283}
!2293 = metadata !{i32 786443, metadata !2294, i32 445, i32 2, metadata !163, i32 8} ; [ DW_TAG_lexical_block ]
!2294 = metadata !{i32 786443, metadata !2280, i32 444, i32 145, metadata !163, i32 7} ; [ DW_TAG_lexical_block ]
!2295 = metadata !{i32 446, i32 1, metadata !2293, metadata !2283}
!2296 = metadata !{i32 447, i32 2, metadata !2293, metadata !2283}
!2297 = metadata !{i32 790531, metadata !2298, metadata !"P.V", null, i32 154, metadata !2222, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2298 = metadata !{i32 786689, metadata !2299, metadata !"P", metadata !174, i32 16777370, metadata !2211, i32 0, metadata !2300} ; [ DW_TAG_arg_variable ]
!2299 = metadata !{i32 786478, i32 0, metadata !174, metadata !"_ssdm_op_TLM_CAN_GET<const ap_uint<32> >", metadata !"_ssdm_op_TLM_CAN_GET<const ap_uint<32> >", metadata !"_Z20_ssdm_op_TLM_CAN_GETIK7ap_uintILi32EEEbRVT_", metadata !174, i32 154, metadata !2209, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, metadata !2213, null, metadata !186, i32 148} ; [ DW_TAG_subprogram ]
!2300 = metadata !{i32 229, i32 20, metadata !2301, metadata !2303}
!2301 = metadata !{i32 786443, metadata !2302, i32 228, i32 74, metadata !174, i32 52} ; [ DW_TAG_lexical_block ]
!2302 = metadata !{i32 786478, i32 0, metadata !172, metadata !"num_available", metadata !"num_available", metadata !"_ZNK7_ap_sc_7sc_core13sc_fifo_in_ifI7ap_uintILi32EEE13num_availableEv", metadata !174, i32 228, metadata !832, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, metadata !831, metadata !186, i32 228} ; [ DW_TAG_subprogram ]
!2303 = metadata !{i32 485, i32 77, metadata !2304, metadata !2306}
!2304 = metadata !{i32 786443, metadata !2305, i32 485, i32 68, metadata !174, i32 51} ; [ DW_TAG_lexical_block ]
!2305 = metadata !{i32 786478, i32 0, metadata !172, metadata !"num_available", metadata !"num_available", metadata !"_ZN7_ap_sc_7sc_core10sc_fifo_inI7ap_uintILi32EEE13num_availableEv", metadata !174, i32 485, metadata !882, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, metadata !881, metadata !186, i32 485} ; [ DW_TAG_subprogram ]
!2306 = metadata !{i32 452, i32 20, metadata !2294, metadata !2283}
!2307 = metadata !{i32 154, i32 77, metadata !2299, metadata !2300}
!2308 = metadata !{i32 149, i32 12, metadata !2309, metadata !2300}
!2309 = metadata !{i32 786443, metadata !2299, i32 148, i32 80, metadata !1966, i32 53} ; [ DW_TAG_lexical_block ]
!2310 = metadata !{i32 455, i32 50, metadata !2311, metadata !2283}
!2311 = metadata !{i32 786443, metadata !2312, i32 455, i32 49, metadata !163, i32 11} ; [ DW_TAG_lexical_block ]
!2312 = metadata !{i32 786443, metadata !2313, i32 455, i32 13, metadata !163, i32 10} ; [ DW_TAG_lexical_block ]
!2313 = metadata !{i32 786443, metadata !2294, i32 454, i32 51, metadata !163, i32 9} ; [ DW_TAG_lexical_block ]
!2314 = metadata !{i32 790531, metadata !2315, metadata !"P.V", null, i32 99, metadata !2222, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2315 = metadata !{i32 786689, metadata !2316, metadata !"P", metadata !174, i32 16777315, metadata !2236, i32 0, metadata !2320} ; [ DW_TAG_arg_variable ]
!2316 = metadata !{i32 786478, i32 0, metadata !174, metadata !"_ssdm_op_READ<ap_uint<32> >", metadata !"_ssdm_op_READ<ap_uint<32> >", metadata !"_Z13_ssdm_op_READI7ap_uintILi32EEET_RVS2_", metadata !174, i32 99, metadata !2317, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, metadata !2319, null, metadata !186, i32 71} ; [ DW_TAG_subprogram ]
!2317 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !2318, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!2318 = metadata !{metadata !303, metadata !2236}
!2319 = metadata !{metadata !2238}
!2320 = metadata !{i32 221, i32 66, metadata !2321, metadata !2323}
!2321 = metadata !{i32 786443, metadata !2322, i32 221, i32 56, metadata !174, i32 48} ; [ DW_TAG_lexical_block ]
!2322 = metadata !{i32 786478, i32 0, metadata !172, metadata !"read", metadata !"read", metadata !"_ZN7_ap_sc_7sc_core13sc_fifo_in_ifI7ap_uintILi32EEE4readEv", metadata !174, i32 221, metadata !823, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, metadata !822, metadata !186, i32 221} ; [ DW_TAG_subprogram ]
!2323 = metadata !{i32 482, i32 65, metadata !2324, metadata !2326}
!2324 = metadata !{i32 786443, metadata !2325, i32 482, i32 56, metadata !174, i32 47} ; [ DW_TAG_lexical_block ]
!2325 = metadata !{i32 786478, i32 0, metadata !172, metadata !"read", metadata !"read", metadata !"_ZN7_ap_sc_7sc_core10sc_fifo_inI7ap_uintILi32EEE4readEv", metadata !174, i32 482, metadata !873, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, metadata !872, metadata !186, i32 482} ; [ DW_TAG_subprogram ]
!2326 = metadata !{i32 457, i32 10, metadata !2311, metadata !2283}
!2327 = metadata !{i32 99, i32 69, metadata !2316, metadata !2320}
!2328 = metadata !{i32 76, i32 5, metadata !2329, metadata !2320}
!2329 = metadata !{i32 786443, metadata !2316, i32 71, i32 72, metadata !1966, i32 49} ; [ DW_TAG_lexical_block ]
!2330 = metadata !{i32 790529, metadata !2331, metadata !"val.V", null, i32 72, metadata !1860, i32 0, i32 0} ; [ DW_TAG_auto_variable_field ]
!2331 = metadata !{i32 786688, metadata !2329, metadata !"val", metadata !1966, i32 72, metadata !806, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!2332 = metadata !{i32 467, i32 3, metadata !2333, metadata !2283}
!2333 = metadata !{i32 786443, metadata !2294, i32 467, i32 2, metadata !163, i32 14} ; [ DW_TAG_lexical_block ]
!2334 = metadata !{i32 468, i32 1, metadata !2333, metadata !2283}
!2335 = metadata !{i32 469, i32 2, metadata !2333, metadata !2283}
!2336 = metadata !{i32 93, i32 3, metadata !2051, null}
!2337 = metadata !{i32 786688, metadata !1921, metadata !"value", metadata !163, i32 80, metadata !1829, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!2338 = metadata !{i32 786688, metadata !1921, metadata !"reg_value", metadata !163, i32 79, metadata !1829, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!2339 = metadata !{i32 94, i32 3, metadata !2051, null}
!2340 = metadata !{i32 95, i32 2, metadata !2051, null}
!2341 = metadata !{i32 790531, metadata !2342, metadata !"producer0.clock.m_if.Val", null, i32 70, metadata !1839, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2342 = metadata !{i32 786689, metadata !2343, metadata !"this", metadata !163, i32 16777286, metadata !1838, i32 64, i32 0} ; [ DW_TAG_arg_variable ]
!2343 = metadata !{i32 786478, i32 0, null, metadata !"producer0", metadata !"producer0", metadata !"_ZN11producer0C2EN7_ap_sc_7sc_core14sc_module_nameE", metadata !163, i32 70, metadata !1794, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, metadata !1793, metadata !186, i32 71} ; [ DW_TAG_subprogram ]
!2344 = metadata !{i32 70, i32 14, metadata !2343, null}
!2345 = metadata !{i32 790531, metadata !2342, metadata !"producer0.reset.m_if.Val", null, i32 70, metadata !1839, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2346 = metadata !{i32 790531, metadata !2342, metadata !"producer0.fifo_in_0.m_if.Val.V", null, i32 70, metadata !1851, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2347 = metadata !{i32 790531, metadata !2342, metadata !"producer0.fifo_out_0.m_if.Val.V", null, i32 70, metadata !1867, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2348 = metadata !{i32 790531, metadata !2342, metadata !"producer0.RegisterWriteEnablePort_0.m_if.Val.V", null, i32 70, metadata !1876, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2349 = metadata !{i32 790531, metadata !2342, metadata !"producer0.RegisterWriteDataPort_0.m_if.Val.V", null, i32 70, metadata !1896, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2350 = metadata !{i32 790531, metadata !2342, metadata !"producer0.axi_master_0.m_if.Val", null, i32 70, metadata !1911, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2351 = metadata !{i32 790531, metadata !2352, metadata !"sc_port_b<_ap_sc_::sc_core::sc_fifo_in_if<ap_uint<32> > >.m_if.Val.V", null, i32 480, metadata !2356, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2352 = metadata !{i32 786689, metadata !2353, metadata !"this", metadata !174, i32 16777696, metadata !2354, i32 64, metadata !2355} ; [ DW_TAG_arg_variable ]
!2353 = metadata !{i32 786478, i32 0, metadata !172, metadata !"sc_fifo_in", metadata !"sc_fifo_in", metadata !"_ZN7_ap_sc_7sc_core10sc_fifo_inI7ap_uintILi32EEEC1Ev", metadata !174, i32 480, metadata !866, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, metadata !865, metadata !186, i32 480} ; [ DW_TAG_subprogram ]
!2354 = metadata !{i32 786447, null, metadata !"", null, i32 0, i64 64, i64 64, i64 0, i32 0, metadata !291} ; [ DW_TAG_pointer_type ]
!2355 = metadata !{i32 71, i32 2, metadata !2343, null}
!2356 = metadata !{i32 786447, null, metadata !"", null, i32 0, i64 64, i64 64, i64 0, i32 0, metadata !1854} ; [ DW_TAG_pointer_type ]
!2357 = metadata !{i32 480, i32 18, metadata !2353, metadata !2355}
!2358 = metadata !{i32 790531, metadata !2359, metadata !"sc_port_b<_ap_sc_::sc_core::sc_fifo_in_if<ap_uint<32> > >.m_if.Val.V", null, i32 480, metadata !2356, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2359 = metadata !{i32 786689, metadata !2360, metadata !"this", metadata !174, i32 16777696, metadata !2354, i32 64, metadata !2361} ; [ DW_TAG_arg_variable ]
!2360 = metadata !{i32 786478, i32 0, metadata !172, metadata !"sc_fifo_in", metadata !"sc_fifo_in", metadata !"_ZN7_ap_sc_7sc_core10sc_fifo_inI7ap_uintILi32EEEC2Ev", metadata !174, i32 480, metadata !866, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, metadata !865, metadata !186, i32 480} ; [ DW_TAG_subprogram ]
!2361 = metadata !{i32 480, i32 33, metadata !2353, metadata !2355}
!2362 = metadata !{i32 480, i32 18, metadata !2360, metadata !2361}
!2363 = metadata !{i32 790531, metadata !2364, metadata !"sc_fifo_in_if<ap_uint<32> >.Val.V", null, i32 216, metadata !2369, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2364 = metadata !{i32 786689, metadata !2365, metadata !"this", metadata !174, i32 16777432, metadata !862, i32 64, metadata !2366} ; [ DW_TAG_arg_variable ]
!2365 = metadata !{i32 786478, i32 0, metadata !172, metadata !"sc_fifo_in_if", metadata !"sc_fifo_in_if", metadata !"_ZN7_ap_sc_7sc_core13sc_fifo_in_ifI7ap_uintILi32EEEC1EPKc", metadata !174, i32 216, metadata !819, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, metadata !818, metadata !186, i32 216} ; [ DW_TAG_subprogram ]
!2366 = metadata !{i32 272, i32 68, metadata !2367, metadata !2368}
!2367 = metadata !{i32 786478, i32 0, metadata !172, metadata !"sc_port_b", metadata !"sc_port_b", metadata !"_ZN7_ap_sc_7sc_core9sc_port_bINS0_13sc_fifo_in_ifI7ap_uintILi32EEEEEC2Ev", metadata !174, i32 272, metadata !839, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, metadata !838, metadata !186, i32 272} ; [ DW_TAG_subprogram ]
!2368 = metadata !{i32 480, i32 31, metadata !2360, metadata !2361}
!2369 = metadata !{i32 786447, null, metadata !"", null, i32 0, i64 64, i64 64, i64 0, i32 0, metadata !1858} ; [ DW_TAG_pointer_type ]
!2370 = metadata !{i32 216, i32 47, metadata !2365, metadata !2366}
!2371 = metadata !{i32 790531, metadata !2372, metadata !"sc_fifo_in_if<ap_uint<32> >.Val.V", null, i32 216, metadata !2369, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2372 = metadata !{i32 786689, metadata !2373, metadata !"this", metadata !174, i32 16777432, metadata !862, i32 64, metadata !2374} ; [ DW_TAG_arg_variable ]
!2373 = metadata !{i32 786478, i32 0, metadata !172, metadata !"sc_fifo_in_if", metadata !"sc_fifo_in_if", metadata !"_ZN7_ap_sc_7sc_core13sc_fifo_in_ifI7ap_uintILi32EEEC2EPKc", metadata !174, i32 216, metadata !819, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, metadata !818, metadata !186, i32 216} ; [ DW_TAG_subprogram ]
!2374 = metadata !{i32 219, i32 9, metadata !2365, metadata !2366}
!2375 = metadata !{i32 216, i32 47, metadata !2373, metadata !2374}
!2376 = metadata !{i32 217, i32 12, metadata !2377, metadata !2374}
!2377 = metadata !{i32 786443, metadata !2373, i32 216, i32 85, metadata !174, i32 106} ; [ DW_TAG_lexical_block ]
!2378 = metadata !{i32 218, i32 12, metadata !2377, metadata !2374}
!2379 = metadata !{i32 790531, metadata !2380, metadata !"sc_port_b<_ap_sc_::sc_core::sc_fifo_out_if<ap_uint<32> > >.m_if.Val.V", null, i32 492, metadata !2383, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2380 = metadata !{i32 786689, metadata !2381, metadata !"this", metadata !174, i32 16777708, metadata !2382, i32 64, metadata !2355} ; [ DW_TAG_arg_variable ]
!2381 = metadata !{i32 786478, i32 0, metadata !172, metadata !"sc_fifo_out", metadata !"sc_fifo_out", metadata !"_ZN7_ap_sc_7sc_core11sc_fifo_outI7ap_uintILi32EEEC1Ev", metadata !174, i32 492, metadata !939, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, metadata !938, metadata !186, i32 492} ; [ DW_TAG_subprogram ]
!2382 = metadata !{i32 786447, null, metadata !"", null, i32 0, i64 64, i64 64, i64 0, i32 0, metadata !885} ; [ DW_TAG_pointer_type ]
!2383 = metadata !{i32 786447, null, metadata !"", null, i32 0, i64 64, i64 64, i64 0, i32 0, metadata !1870} ; [ DW_TAG_pointer_type ]
!2384 = metadata !{i32 492, i32 18, metadata !2381, metadata !2355}
!2385 = metadata !{i32 790531, metadata !2386, metadata !"sc_port_b<_ap_sc_::sc_core::sc_fifo_out_if<ap_uint<32> > >.m_if.Val.V", null, i32 492, metadata !2383, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2386 = metadata !{i32 786689, metadata !2387, metadata !"this", metadata !174, i32 16777708, metadata !2382, i32 64, metadata !2388} ; [ DW_TAG_arg_variable ]
!2387 = metadata !{i32 786478, i32 0, metadata !172, metadata !"sc_fifo_out", metadata !"sc_fifo_out", metadata !"_ZN7_ap_sc_7sc_core11sc_fifo_outI7ap_uintILi32EEEC2Ev", metadata !174, i32 492, metadata !939, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, metadata !938, metadata !186, i32 492} ; [ DW_TAG_subprogram ]
!2388 = metadata !{i32 492, i32 34, metadata !2381, metadata !2355}
!2389 = metadata !{i32 492, i32 18, metadata !2387, metadata !2388}
!2390 = metadata !{i32 790531, metadata !2391, metadata !"sc_fifo_out_if<ap_uint<32> >.Val.V", null, i32 239, metadata !2396, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2391 = metadata !{i32 786689, metadata !2392, metadata !"this", metadata !174, i32 16777455, metadata !935, i32 64, metadata !2393} ; [ DW_TAG_arg_variable ]
!2392 = metadata !{i32 786478, i32 0, metadata !172, metadata !"sc_fifo_out_if", metadata !"sc_fifo_out_if", metadata !"_ZN7_ap_sc_7sc_core14sc_fifo_out_ifI7ap_uintILi32EEEC1EPKc", metadata !174, i32 239, metadata !897, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, metadata !896, metadata !186, i32 239} ; [ DW_TAG_subprogram ]
!2393 = metadata !{i32 272, i32 68, metadata !2394, metadata !2395}
!2394 = metadata !{i32 786478, i32 0, metadata !172, metadata !"sc_port_b", metadata !"sc_port_b", metadata !"_ZN7_ap_sc_7sc_core9sc_port_bINS0_14sc_fifo_out_ifI7ap_uintILi32EEEEEC2Ev", metadata !174, i32 272, metadata !912, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, metadata !911, metadata !186, i32 272} ; [ DW_TAG_subprogram ]
!2395 = metadata !{i32 492, i32 32, metadata !2387, metadata !2388}
!2396 = metadata !{i32 786447, null, metadata !"", null, i32 0, i64 64, i64 64, i64 0, i32 0, metadata !1874} ; [ DW_TAG_pointer_type ]
!2397 = metadata !{i32 239, i32 47, metadata !2392, metadata !2393}
!2398 = metadata !{i32 790531, metadata !2399, metadata !"sc_fifo_out_if<ap_uint<32> >.Val.V", null, i32 239, metadata !2396, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2399 = metadata !{i32 786689, metadata !2400, metadata !"this", metadata !174, i32 16777455, metadata !935, i32 64, metadata !2401} ; [ DW_TAG_arg_variable ]
!2400 = metadata !{i32 786478, i32 0, metadata !172, metadata !"sc_fifo_out_if", metadata !"sc_fifo_out_if", metadata !"_ZN7_ap_sc_7sc_core14sc_fifo_out_ifI7ap_uintILi32EEEC2EPKc", metadata !174, i32 239, metadata !897, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, metadata !896, metadata !186, i32 239} ; [ DW_TAG_subprogram ]
!2401 = metadata !{i32 242, i32 9, metadata !2392, metadata !2393}
!2402 = metadata !{i32 239, i32 47, metadata !2400, metadata !2401}
!2403 = metadata !{i32 240, i32 13, metadata !2404, metadata !2401}
!2404 = metadata !{i32 786443, metadata !2400, i32 239, i32 86, metadata !174, i32 103} ; [ DW_TAG_lexical_block ]
!2405 = metadata !{i32 241, i32 13, metadata !2404, metadata !2401}
!2406 = metadata !{i32 790531, metadata !2407, metadata !"sc_port_b<hls_bus_if<int> >.m_if.Val", null, i32 12, metadata !2409, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2407 = metadata !{i32 786689, metadata !2408, metadata !"this", metadata !1703, i32 16777228, metadata !1792, i32 64, i32 0} ; [ DW_TAG_arg_variable ]
!2408 = metadata !{i32 786478, i32 0, null, metadata !"AXI4M_bus_port", metadata !"AXI4M_bus_port", metadata !"_ZN14AXI4M_bus_portIiEC1Ev", metadata !1703, i32 12, metadata !1768, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, metadata !1767, metadata !186, i32 12} ; [ DW_TAG_subprogram ]
!2409 = metadata !{i32 786447, null, metadata !"", null, i32 0, i64 64, i64 64, i64 0, i32 0, metadata !1914} ; [ DW_TAG_pointer_type ]
!2410 = metadata !{i32 12, i32 43, metadata !2408, metadata !2355}
!2411 = metadata !{i32 790531, metadata !2412, metadata !"sc_port_b<hls_bus_if<int> >.m_if.Val", null, i32 12, metadata !2409, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!2412 = metadata !{i32 786689, metadata !2413, metadata !"this", metadata !1703, i32 16777228, metadata !1792, i32 64, i32 0} ; [ DW_TAG_arg_variable ]
!2413 = metadata !{i32 786478, i32 0, null, metadata !"AXI4M_bus_port", metadata !"AXI4M_bus_port", metadata !"_ZN14AXI4M_bus_portIiEC2Ev", metadata !1703, i32 12, metadata !1768, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, metadata !1767, metadata !186, i32 12} ; [ DW_TAG_subprogram ]
!2414 = metadata !{i32 12, i32 43, metadata !2413, metadata !2415}
!2415 = metadata !{i32 14, i32 5, metadata !2408, metadata !2355}
!2416 = metadata !{i32 70, i32 5, metadata !2417, metadata !2419}
!2417 = metadata !{i32 786443, metadata !2418, i32 69, i32 83, metadata !1711, i32 85} ; [ DW_TAG_lexical_block ]
!2418 = metadata !{i32 786478, i32 0, null, metadata !"hls_bus_if", metadata !"hls_bus_if", metadata !"_ZN10hls_bus_ifIiEC2EPKc", metadata !1711, i32 69, metadata !1717, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, metadata !1716, metadata !186, i32 69} ; [ DW_TAG_subprogram ]
!2419 = metadata !{i32 72, i32 4, metadata !2420, metadata !2421}
!2420 = metadata !{i32 786478, i32 0, null, metadata !"hls_bus_if", metadata !"hls_bus_if", metadata !"_ZN10hls_bus_ifIiEC1EPKc", metadata !1711, i32 69, metadata !1717, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, metadata !1716, metadata !186, i32 69} ; [ DW_TAG_subprogram ]
!2421 = metadata !{i32 272, i32 68, metadata !2422, metadata !2423}
!2422 = metadata !{i32 786478, i32 0, metadata !172, metadata !"sc_port_b", metadata !"sc_port_b", metadata !"_ZN7_ap_sc_7sc_core9sc_port_bI10hls_bus_ifIiEEC2Ev", metadata !174, i32 272, metadata !1742, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, metadata !1741, metadata !186, i32 272} ; [ DW_TAG_subprogram ]
!2423 = metadata !{i32 12, i32 60, metadata !2413, metadata !2415}
!2424 = metadata !{i32 71, i32 5, metadata !2417, metadata !2419}
!2425 = metadata !{i32 13, i32 9, metadata !2426, metadata !2415}
!2426 = metadata !{i32 786443, metadata !2413, i32 12, i32 60, metadata !1703, i32 83} ; [ DW_TAG_lexical_block ]
!2427 = metadata !{i32 72, i32 3, metadata !2428, null}
!2428 = metadata !{i32 786443, metadata !2343, i32 71, i32 2, metadata !163, i32 4} ; [ DW_TAG_lexical_block ]
!2429 = metadata !{i32 73, i32 3, metadata !2428, null}
!2430 = metadata !{i32 74, i32 3, metadata !2428, null}
!2431 = metadata !{i32 75, i32 3, metadata !2428, null}
!2432 = metadata !{i32 76, i32 3, metadata !2428, null}
!2433 = metadata !{i32 77, i32 3, metadata !2428, null}
!2434 = metadata !{i32 78, i32 3, metadata !2428, null}
!2435 = metadata !{i32 79, i32 3, metadata !2428, null}
!2436 = metadata !{i32 80, i32 3, metadata !2428, null}
!2437 = metadata !{i32 81, i32 3, metadata !2428, null}
!2438 = metadata !{i32 82, i32 3, metadata !2428, null}
!2439 = metadata !{i32 83, i32 3, metadata !2428, null}
!2440 = metadata !{i32 84, i32 3, metadata !2428, null}
!2441 = metadata !{i32 58, i32 1, metadata !2442, null}
!2442 = metadata !{i32 786443, metadata !2428, metadata !168} ; [ DW_TAG_lexical_block ]
