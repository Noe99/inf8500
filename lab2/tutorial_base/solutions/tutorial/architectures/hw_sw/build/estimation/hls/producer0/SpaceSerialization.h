///////////////////////////////////////////////////////////////////////////////
///
///         Space Codesign System Inc. - (https://www.spacecodesign.com)
///         (c) All Rights Reserved. 2020
///
///         No authorization to modify or use this file for
///         commercial purposes without prior written consent
///         of its author(s)
///
///////////////////////////////////////////////////////////////////////////////
#ifndef SPACE_SERIALIZATION_H
#define SPACE_SERIALIZATION_H

#include "space_type_traits.h"

#ifndef SPACE_LITTLE_ENDIAN
#define SPACE_LITTLE_ENDIAN	1234
#endif
#ifndef SPACE_BIG_ENDIAN
#define SPACE_BIG_ENDIAN	4321
#endif

#ifndef SPACE_BYTE_ORDER
#error "SPACE_BYTE_ORDER should be defined"
#endif

// Define HTON* and NTOH*, where the "host" is the current device
// and the "network" is the bus. Name was taken from the C functions
// of similar name. See:
// https://docs.oracle.com/cd/E36784_01/html/E36875/htonll-3socket.html
//
// Assume same endianness if BYTE_ORDER isn't defined.
#if !defined(BYTE_ORDER) || SPACE_BYTE_ORDER == BYTE_ORDER
	#define HTONS(s)   (s)
	#define HTONL(l)   (l)
	#define HTONLL(ll) (ll)
#else
	#define HTONS(s)   ( (((s) & 0xFF00) >> 8) | (((s) & 0x00FF) << 8) )
	#define HTONL(l)   ( (((l) & 0xFF000000) >> 24) | (((l) & 0x00FF0000) >> 8) | (((l) & 0x0000FF00) << 8) | (((l) & 0x000000FF) << 24) )
	#define HTONLL(ll) ( (((ll) & 0xFF00000000000000ULL) >> 56) | ((ll) & 0x00FF000000000000ULL) >> 40) | ((ll) & 0x0000FF0000000000ULL) >> 24) | ((ll) & 0x000000FF00000000ULL) >> 8) | ((ll) & 0x00000000FF000000ULL) << 8) | ((ll) & 0x0000000000FF0000ULL) << 24) | ((ll) & 0x000000000000FF00ULL) << 40) | ((ll) & 0x00000000000000FFULL) << 56))
#endif

#define NTOHS(s)   HTONS(s)
#define NTOHL(l)   HTONL(l)
#define NTOHLL(ll) HTONLL(ll)

#define PUT_LONG(out, l) (out) = HTONL(l)
#define GET_LONG(in, l)  (l)   = NTOHL(in)

#define PUT_LONGLONG(out, l) (out) = HTONLL(l)
#define GET_LONGLONG(in, l)  (l)   = NTOHLL(in)

#if SPACE_BYTE_ORDER == SPACE_BIG_ENDIAN
	#define PUT_CHAR1(out, c) (out).range(31,24) = (c)
	#define PUT_CHAR2(out, c) (out).range(23,16) = (c)
	#define PUT_CHAR3(out, c) (out).range(15,8) = (c)
	#define PUT_CHAR4(out, c) (out).range(7,0) = (c)
	#define PUT_SHORT1(out, s) (out).range(31,16) = HTONS(s)
	#define PUT_SHORT2(out, s) (out).range(15,0) = HTONS(s)

	#define GET_CHAR1(in, c) (c) = (in).range(31,24);
	#define GET_CHAR2(in, c) (c) = (in).range(23,16);
	#define GET_CHAR3(in, c) (c) = (in).range(15,8);
	#define GET_CHAR4(in, c) (c) = (in).range(7,0);
	#define GET_SHORT1(in, s) (s) = NTOHS((in).range(31,16));
	#define GET_SHORT2(in, s) (s) = NTOHS((in).range(15,0));

	#define WRITE_LONGLONG(out, ll) \
		(out).write( HTONL((ll).range(63,32)) ); \
		(out).write( HTONL((ll).range(31,0)) )

	#define READ_LONGLONG(in, ll) \
		(ll).range(63,32) = NTOHL((in).read()); \
		(ll).range(31,0) = NTOHL((in).read())

#else
	#define PUT_CHAR1(out, c) (out).range(7,0) = (c)
	#define PUT_CHAR2(out, c) (out).range(15,8) = (c)
	#define PUT_CHAR3(out, c) (out).range(23,16) = (c)
	#define PUT_CHAR4(out, c) (out).range(31,24) = (c)
	#define PUT_SHORT1(out, s) (out).range(15,0) = HTONS(s)
	#define PUT_SHORT2(out, s) (out).range(31,16) = HTONS(s)

	#define GET_CHAR1(in, c) (c) = (in).range(7,0);
	#define GET_CHAR2(in, c) (c) = (in).range(15,8);
	#define GET_CHAR3(in, c) (c) = (in).range(23,16);
	#define GET_CHAR4(in, c) (c) = (in).range(31,24);
	#define GET_SHORT1(in, s) (s) = NTOHS((in).range(15,0));
	#define GET_SHORT2(in, s) (s) = NTOHS((in).range(31,16));

	#define WRITE_LONGLONG(out, ll) \
		(out).write( HTONL((ll).range(31,0)) ); \
		(out).write( HTONL((ll).range(63,32)) )

	#define READ_LONGLONG(in, ll) \
		(ll).range(31,0) = NTOHL((in).read()); \
		(ll).range(63,32) = NTOHL((in).read())

#endif

template <class T>
static inline typename space_integral_from_floating_point<T>::type space_ieee_floatingpoint_to_bytes(T value) {
	space_floating_point_and_integral_union<T> space_reinterpret;
	space_reinterpret.space_f = value;
	return space_reinterpret.space_i;
}

template <class T>
static inline T bytes_to_space_ieee_floatingpoint(typename space_integral_from_floating_point<T>::type bytes) {
	space_floating_point_and_integral_union<T> space_reinterpret;
	space_reinterpret.space_i = bytes;
	return space_reinterpret.space_f;
}

#define PUT_FLOAT(out, f) PUT_LONG(out, space_ieee_floatingpoint_to_bytes(f))
#define GET_FLOAT(in , f) GET_LONG(bytes_to_space_ieee_floatingpoint<float>(in), f)

#define PUT_DOUBLE(out, f) PUT_LONGLONG(out, space_ieee_floatingpoint_to_bytes(f))
#define GET_DOUBLE(in , f) GET_LONGLONG(bytes_to_space_ieee_floatingpoint<double>(in), f)

#define PUT_LONGDOUBLE(out, f) PUT_LONGLONG(out, space_ieee_floatingpoint_to_bytes(f))
#define GET_LONGDOUBLE(in , f) GET_LONGLONG(bytes_to_space_ieee_floatingpoint<long double>(in), f)

#endif
