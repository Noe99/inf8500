///////////////////////////////////////////////////////////////////////////////
///
///         Space Codesign System Inc. - (http://www.spacecodesign.com)
///         (c) All Rights Reserved. 2017
///
///         No authorization to modify or use this file for
///         commercial purposes without prior written consent
///         of its author(s)
///
///////////////////////////////////////////////////////////////////////////////
#ifndef SPACE_SERIALIZATION_VIVADO_H
#define SPACE_SERIALIZATION_VIVADO_H

#ifdef WRITE_LONGLONG
#undef WRITE_LONGLONG
#endif

#ifdef READ_LONGLONG
#undef READ_LONGLONG
#endif

#if SPACE_BYTE_ORDER == SPACE_BIG_ENDIAN


	#define WRITE_LONGLONG(out, ll) \
		{\
			_Pragma("HLS PROTOCOL fixed")\
			(out).write( HTONL((ll).range(63,32)) ); \
		}\
		{\
			_Pragma("HLS PROTOCOL fixed")\
			(out).write( HTONL((ll).range(31,0)); )\
		}\

	#define READ_LONGLONG(in, ll) \
		{\
			_Pragma("HLS PROTOCOL fixed")\
			(ll).range(63,32) = NTOHL((in).read()); \
		}\
		{\
			_Pragma("HLS PROTOCOL fixed")\
			(ll).range(31,0) = NTOHL((in).read());\
		}\

#else

	#define WRITE_LONGLONG(out, ll) \
		{\
			_Pragma("HLS PROTOCOL fixed")\
			(out).write( HTONL((ll).range(31,0).to_uint()) ); \
		}\
		{\
			_Pragma("HLS PROTOCOL fixed")\
			(out).write( HTONL((ll).range(63,32).to_uint()) );\
		}\

	#define READ_LONGLONG(in, ll) \
		{\
			_Pragma("HLS PROTOCOL fixed")\
			(ll).range(31,0) = NTOHL((in).read()); \
		}\
		{\
			_Pragma("HLS PROTOCOL fixed")\
			(ll).range(63,32) = NTOHL((in).read());\
		}\

#endif



#endif
