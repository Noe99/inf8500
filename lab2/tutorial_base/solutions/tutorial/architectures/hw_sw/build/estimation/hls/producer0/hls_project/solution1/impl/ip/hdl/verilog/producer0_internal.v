// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC
// Version: 2018.3
// Copyright (C) 1986-2018 Xilinx, Inc. All Rights Reserved.
// 
// ===========================================================

`timescale 1 ns / 1 ps 

(* CORE_GENERATION_INFO="producer0_internal,hls_ip_2018_3,{HLS_INPUT_TYPE=sc,HLS_INPUT_FLOAT=0,HLS_INPUT_FIXED=0,HLS_INPUT_PART=xc7z020clg484-1,HLS_INPUT_CLOCK=10.000000,HLS_INPUT_ARCH=others,HLS_SYN_CLOCK=7.000000,HLS_SYN_LAT=10,HLS_SYN_TPT=none,HLS_SYN_MEM=0,HLS_SYN_DSP=0,HLS_SYN_FF=77,HLS_SYN_LUT=134,HLS_VERSION=2018_3}" *)

module producer0_internal (
        clock,
        reset,
        fifo_in_0_dout,
        fifo_in_0_empty_n,
        fifo_in_0_read,
        fifo_out_0_din,
        fifo_out_0_full_n,
        fifo_out_0_write,
        RegisterWriteEnablePort_0,
        RegisterWriteDataPort_0,
        axi_master_0_req_din,
        axi_master_0_req_full_n,
        axi_master_0_req_write,
        axi_master_0_rsp_empty_n,
        axi_master_0_rsp_read,
        axi_master_0_address,
        axi_master_0_datain,
        axi_master_0_dataout,
        axi_master_0_size
);


input   clock;
input   reset;
input  [31:0] fifo_in_0_dout;
input   fifo_in_0_empty_n;
output   fifo_in_0_read;
output  [31:0] fifo_out_0_din;
input   fifo_out_0_full_n;
output   fifo_out_0_write;
output  [0:0] RegisterWriteEnablePort_0;
output  [31:0] RegisterWriteDataPort_0;
output   axi_master_0_req_din;
input   axi_master_0_req_full_n;
output   axi_master_0_req_write;
input   axi_master_0_rsp_empty_n;
output   axi_master_0_rsp_read;
output  [31:0] axi_master_0_address;
input  [31:0] axi_master_0_datain;
output  [31:0] axi_master_0_dataout;
output  [31:0] axi_master_0_size;

reg[0:0] RegisterWriteEnablePort_0 = 1'd0;
reg[31:0] RegisterWriteDataPort_0 = 32'd0;

wire    grp_producer0_internal_thread_fu_84_fifo_in_0_read;
wire   [31:0] grp_producer0_internal_thread_fu_84_fifo_out_0_din;
wire    grp_producer0_internal_thread_fu_84_fifo_out_0_write;
wire   [0:0] grp_producer0_internal_thread_fu_84_RegisterWriteEnablePort_0;
wire    grp_producer0_internal_thread_fu_84_RegisterWriteEnablePort_0_ap_vld;
wire   [31:0] grp_producer0_internal_thread_fu_84_RegisterWriteDataPort_0;
wire    grp_producer0_internal_thread_fu_84_RegisterWriteDataPort_0_ap_vld;
wire    grp_producer0_internal_thread_fu_84_axi_master_0_req_din;
wire    grp_producer0_internal_thread_fu_84_axi_master_0_req_write;
wire    grp_producer0_internal_thread_fu_84_axi_master_0_rsp_read;
wire   [31:0] grp_producer0_internal_thread_fu_84_axi_master_0_address;
wire   [31:0] grp_producer0_internal_thread_fu_84_axi_master_0_dataout;
wire   [31:0] grp_producer0_internal_thread_fu_84_axi_master_0_size;

producer0_internal_thread grp_producer0_internal_thread_fu_84(
    .ap_clk(clock),
    .ap_rst(reset),
    .fifo_in_0_dout(fifo_in_0_dout),
    .fifo_in_0_empty_n(fifo_in_0_empty_n),
    .fifo_in_0_read(grp_producer0_internal_thread_fu_84_fifo_in_0_read),
    .fifo_out_0_din(grp_producer0_internal_thread_fu_84_fifo_out_0_din),
    .fifo_out_0_full_n(fifo_out_0_full_n),
    .fifo_out_0_write(grp_producer0_internal_thread_fu_84_fifo_out_0_write),
    .RegisterWriteEnablePort_0(grp_producer0_internal_thread_fu_84_RegisterWriteEnablePort_0),
    .RegisterWriteEnablePort_0_ap_vld(grp_producer0_internal_thread_fu_84_RegisterWriteEnablePort_0_ap_vld),
    .RegisterWriteDataPort_0(grp_producer0_internal_thread_fu_84_RegisterWriteDataPort_0),
    .RegisterWriteDataPort_0_ap_vld(grp_producer0_internal_thread_fu_84_RegisterWriteDataPort_0_ap_vld),
    .axi_master_0_req_din(grp_producer0_internal_thread_fu_84_axi_master_0_req_din),
    .axi_master_0_req_full_n(axi_master_0_req_full_n),
    .axi_master_0_req_write(grp_producer0_internal_thread_fu_84_axi_master_0_req_write),
    .axi_master_0_rsp_empty_n(axi_master_0_rsp_empty_n),
    .axi_master_0_rsp_read(grp_producer0_internal_thread_fu_84_axi_master_0_rsp_read),
    .axi_master_0_address(grp_producer0_internal_thread_fu_84_axi_master_0_address),
    .axi_master_0_datain(axi_master_0_datain),
    .axi_master_0_dataout(grp_producer0_internal_thread_fu_84_axi_master_0_dataout),
    .axi_master_0_size(grp_producer0_internal_thread_fu_84_axi_master_0_size)
);

always @ (posedge clock) begin
    if (reset == 1'b1) begin
        RegisterWriteDataPort_0 <= 32'd0;
    end else begin
        if ((grp_producer0_internal_thread_fu_84_RegisterWriteDataPort_0_ap_vld == 1'b1)) begin
            RegisterWriteDataPort_0 <= grp_producer0_internal_thread_fu_84_RegisterWriteDataPort_0;
        end
    end
end

always @ (posedge clock) begin
    if (reset == 1'b1) begin
        RegisterWriteEnablePort_0 <= 1'd0;
    end else begin
        if ((grp_producer0_internal_thread_fu_84_RegisterWriteEnablePort_0_ap_vld == 1'b1)) begin
            RegisterWriteEnablePort_0 <= grp_producer0_internal_thread_fu_84_RegisterWriteEnablePort_0;
        end
    end
end

assign axi_master_0_address = grp_producer0_internal_thread_fu_84_axi_master_0_address;

assign axi_master_0_dataout = grp_producer0_internal_thread_fu_84_axi_master_0_dataout;

assign axi_master_0_req_din = grp_producer0_internal_thread_fu_84_axi_master_0_req_din;

assign axi_master_0_req_write = grp_producer0_internal_thread_fu_84_axi_master_0_req_write;

assign axi_master_0_rsp_read = grp_producer0_internal_thread_fu_84_axi_master_0_rsp_read;

assign axi_master_0_size = grp_producer0_internal_thread_fu_84_axi_master_0_size;

assign fifo_in_0_read = grp_producer0_internal_thread_fu_84_fifo_in_0_read;

assign fifo_out_0_din = grp_producer0_internal_thread_fu_84_fifo_out_0_din;

assign fifo_out_0_write = grp_producer0_internal_thread_fu_84_fifo_out_0_write;

endmodule //producer0_internal
