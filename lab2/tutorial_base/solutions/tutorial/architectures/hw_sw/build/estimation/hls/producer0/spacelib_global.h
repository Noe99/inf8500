///////////////////////////////////////////////////////////////////////////////
///
///         Space Codesign Systems Inc. (https://www.spacecodesign.com)
///         (c) All Rights Reserved. 2005-2022
///
///         This file contains proprietary, confidential information of Space Codesign
///         Systems Inc. and may be used only pursuant to the terms of a valid license
///         agreement with Space Codesign Systems Inc. For more information about licensing,
///         please contact your Space Codesign representative.
///
///////////////////////////////////////////////////////////////////////////////
#ifndef SPACELIB_GLOBAL_H
#define SPACELIB_GLOBAL_H

#include "SpaceTypes.h"
#include "tlm_endianness.h"
#include <string>

namespace spacelib_global {

	typedef struct {
		unsigned int id;
		int endianness;
		eSpaceComponentType type;
	} component_spec;

	typedef struct {
		unsigned int size;
		component_spec* specs;
	} component_container;

	typedef struct {
	  int id;
	  uint32_t base_address;
	  uint32_t high_address;
	} addressable_id;

	typedef struct {
		unsigned int size;
		addressable_id* specs;
	} addressable_id_container;

	typedef struct {
	  int id;
	  uint32_t base_address;
	  uint32_t high_address;
	} addr;

	typedef struct bus_container bus_container;

	typedef struct bus_connection {
	  uint32_t index;
	  addr address;
	  struct bus_container* slave;
	} bus_connection;

	struct bus_container {
	  unsigned char size;
	  bus_connection* connections;
	};

	std::string get_project_path();

	tlm::tlm_endianness	get_endianness(unsigned int id);
	eSpaceComponentType get_type(unsigned int id);

	uint32_t get_base_address(unsigned int id);
	uint32_t get_high_address(unsigned int id);
	bool is_offset_valid(unsigned int id, uint32_t offset);
	uint32_t get_size(unsigned int id);

	uint32_t get_reserved_base_address(unsigned int id);
	uint32_t get_reserved_high_address(unsigned int id);

	uint32_t get_base_address(unsigned int id, void* ptr);
	uint32_t get_high_address(unsigned int id, void* ptr);
	bool is_offset_valid(unsigned int id, void* ptr, uint32_t offset);
	uint32_t get_size(unsigned int id, void* ptr);
	unsigned int get_id(uint32_t address, void* ptr);

	uint32_t unknown_address();
	unsigned int unknown_id();
	unsigned char unknown_priority();
}

#endif

/* (c) Space Codesign Systems Inc. 2005-2022 */
