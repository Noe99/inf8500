set clock_constraint { \
    name clk \
    module producer0::producer0 \
    port ap_clk \
    period 10 \
    uncertainty 3 \
}

set MultiClock_number 1

set MultiClock_constraint {  clock 10 }

set all_path {}

set false_path {}

