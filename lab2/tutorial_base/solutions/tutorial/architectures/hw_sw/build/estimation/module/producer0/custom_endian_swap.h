//////////////////////////////////////////////////////////////////////////////
///
/// Space Codesign System Inc. - (https://www.spacecodesign.com)
/// Copyright 2005-2022. All rights reserved
///
/// No authorization to modify or use this file for
/// commercial purposes without prior written consent
/// of its author(s)
///
/// Created by: SpaceStudio generation engine
///
/// Warning : This file content will be overwritten by the next generation process.
///
//////////////////////////////////////////////////////////////////////////////
#ifndef CUSTOM_ENDIAN_SWAP_H
#define CUSTOM_ENDIAN_SWAP_H

#include "application_definitions.h"
#include "platform_definitions.h"

	template <typename T> void NetworkToHostEndianSwap(T* data, unsigned long nb_elements);
	template <typename T> void HostToNetworkEndianSwap(T* data, unsigned long nb_elements);
	template <typename T> void NetworkToHostEndianSwap(const T* data, unsigned long nb_elements);
	template <typename T> void HostToNetworkEndianSwap(const T* data, unsigned long nb_elements);


#endif