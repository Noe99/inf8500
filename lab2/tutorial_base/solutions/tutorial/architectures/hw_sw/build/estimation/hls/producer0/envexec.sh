#!/bin/bash

# Set environment variables
source $1

# Function to list all descendants of a process
list_descendants()
{
	local children=$(ps -o pid= --ppid "$1")

	for pid in $children
	do
		list_descendants "$pid"
	done
	
	echo "$children"
}

# SIGTERM handler
handler()
{
	kill $(list_descendants $PID)
}

trap handler SIGTERM

# Shift the arguments
shift
is_valid=false
if [ $1 != "" ]; then
	is_valid=true
	command_name=$1
fi

shift

# Launch executable
if [ $is_valid == true ]; then
   $command_name $@ &
   PID=$!
   wait $PID
fi
