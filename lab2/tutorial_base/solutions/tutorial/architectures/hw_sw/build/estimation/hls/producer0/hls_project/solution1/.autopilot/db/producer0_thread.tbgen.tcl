set moduleName producer0_thread
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isPipelined 0
set pipeline_type none
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {producer0::thread}
set C_modelType { void 0 }
set C_modelArgList {
	{ fifo_in_0 int 32 regular {fifo 0 volatile }  }
	{ fifo_out_0 int 32 regular {fifo 1 volatile }  }
	{ RegisterWriteEnablePort_0 int 1 regular {pointer 1 volatile }  }
	{ RegisterWriteDataPort_0 int 32 regular {pointer 1 volatile }  }
	{ axi_master_0 int 32 regular {bus 1}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "fifo_in_0", "interface" : "fifo", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "producer0.fifo_in_0.m_if.Val.V","cData": "uint32","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "fifo_out_0", "interface" : "fifo", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "producer0.fifo_out_0.m_if.Val.V","cData": "uint32","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "RegisterWriteEnablePort_0", "interface" : "wire", "bitwidth" : 1, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":0,"cElement": [{"cName": "producer0.RegisterWriteEnablePort_0.m_if.Val.V","cData": "uint1","bit_use": { "low": 0,"up": 0},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "RegisterWriteDataPort_0", "interface" : "wire", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "producer0.RegisterWriteDataPort_0.m_if.Val.V","cData": "uint32","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "axi_master_0", "interface" : "bus", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "producer0.axi_master_0.m_if.Val","cData": "int","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} ]}
# RTL Port declarations: 
set portNum 21
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ fifo_in_0_dout sc_in sc_lv 32 signal 0 } 
	{ fifo_in_0_empty_n sc_in sc_logic 1 signal 0 } 
	{ fifo_in_0_read sc_out sc_logic 1 signal 0 } 
	{ fifo_out_0_din sc_out sc_lv 32 signal 1 } 
	{ fifo_out_0_full_n sc_in sc_logic 1 signal 1 } 
	{ fifo_out_0_write sc_out sc_logic 1 signal 1 } 
	{ RegisterWriteEnablePort_0 sc_out sc_lv 1 signal 2 } 
	{ RegisterWriteEnablePort_0_ap_vld sc_out sc_logic 1 outvld 2 } 
	{ RegisterWriteDataPort_0 sc_out sc_lv 32 signal 3 } 
	{ RegisterWriteDataPort_0_ap_vld sc_out sc_logic 1 outvld 3 } 
	{ axi_master_0_req_din sc_out sc_logic 1 signal 4 } 
	{ axi_master_0_req_full_n sc_in sc_logic 1 signal 4 } 
	{ axi_master_0_req_write sc_out sc_logic 1 signal 4 } 
	{ axi_master_0_rsp_empty_n sc_in sc_logic 1 signal 4 } 
	{ axi_master_0_rsp_read sc_out sc_logic 1 signal 4 } 
	{ axi_master_0_address sc_out sc_lv 32 signal 4 } 
	{ axi_master_0_datain sc_in sc_lv 32 signal 4 } 
	{ axi_master_0_dataout sc_out sc_lv 32 signal 4 } 
	{ axi_master_0_size sc_out sc_lv 32 signal 4 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "fifo_in_0_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "fifo_in_0", "role": "dout" }} , 
 	{ "name": "fifo_in_0_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "fifo_in_0", "role": "empty_n" }} , 
 	{ "name": "fifo_in_0_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "fifo_in_0", "role": "read" }} , 
 	{ "name": "fifo_out_0_din", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "fifo_out_0", "role": "din" }} , 
 	{ "name": "fifo_out_0_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "fifo_out_0", "role": "full_n" }} , 
 	{ "name": "fifo_out_0_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "fifo_out_0", "role": "write" }} , 
 	{ "name": "RegisterWriteEnablePort_0", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "RegisterWriteEnablePort_0", "role": "default" }} , 
 	{ "name": "RegisterWriteEnablePort_0_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "RegisterWriteEnablePort_0", "role": "ap_vld" }} , 
 	{ "name": "RegisterWriteDataPort_0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "RegisterWriteDataPort_0", "role": "default" }} , 
 	{ "name": "RegisterWriteDataPort_0_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "RegisterWriteDataPort_0", "role": "ap_vld" }} , 
 	{ "name": "axi_master_0_req_din", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "axi_master_0", "role": "req_din" }} , 
 	{ "name": "axi_master_0_req_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "axi_master_0", "role": "req_full_n" }} , 
 	{ "name": "axi_master_0_req_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "axi_master_0", "role": "req_write" }} , 
 	{ "name": "axi_master_0_rsp_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "axi_master_0", "role": "rsp_empty_n" }} , 
 	{ "name": "axi_master_0_rsp_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "axi_master_0", "role": "rsp_read" }} , 
 	{ "name": "axi_master_0_address", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "axi_master_0", "role": "address" }} , 
 	{ "name": "axi_master_0_datain", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "axi_master_0", "role": "datain" }} , 
 	{ "name": "axi_master_0_dataout", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "axi_master_0", "role": "dataout" }} , 
 	{ "name": "axi_master_0_size", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "axi_master_0", "role": "size" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "",
		"CDFG" : "producer0_thread",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "0", "ap_start" : "0", "ap_ready" : "0", "ap_done" : "0", "ap_continue" : "0", "ap_idle" : "0",
		"Pipeline" : "None", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "0",
		"VariableLatency" : "1", "ExactLatency" : "-1", "EstimateLatencyMin" : "19", "EstimateLatencyMax" : "19",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "0",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "clock", "Type" : "None", "Direction" : "I"},
			{"Name" : "reset", "Type" : "None", "Direction" : "I"},
			{"Name" : "fifo_in_0", "Type" : "Fifo", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "fifo_in_0_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "fifo_out_0", "Type" : "Fifo", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "fifo_out_0_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "RegisterWriteEnablePort_0", "Type" : "Vld", "Direction" : "O"},
			{"Name" : "RegisterWriteDataPort_0", "Type" : "Vld", "Direction" : "O"},
			{"Name" : "axi_master_0", "Type" : "Bus", "Direction" : "O"}]}]}


set ArgLastReadFirstWriteLatency {
	producer0_thread {
		clock {Type I LastRead -1 FirstWrite -1}
		reset {Type I LastRead -1 FirstWrite -1}
		fifo_in_0 {Type I LastRead 9 FirstWrite -1}
		fifo_out_0 {Type O LastRead 7 FirstWrite 7}
		RegisterWriteEnablePort_0 {Type O LastRead -1 FirstWrite 0}
		RegisterWriteDataPort_0 {Type O LastRead -1 FirstWrite 0}
		axi_master_0 {Type O LastRead 4 FirstWrite 4}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "19", "Max" : "19"}
	, {"Name" : "Interval", "Min" : "19", "Max" : "19"}
]}

set PipelineEnableSignalInfo {[
]}

set Spec2ImplPortList { 
	fifo_in_0 { ap_fifo {  { fifo_in_0_dout fifo_data 0 32 }  { fifo_in_0_empty_n fifo_status 0 1 }  { fifo_in_0_read fifo_update 1 1 } } }
	fifo_out_0 { ap_fifo {  { fifo_out_0_din fifo_data 1 32 }  { fifo_out_0_full_n fifo_status 0 1 }  { fifo_out_0_write fifo_update 1 1 } } }
	RegisterWriteEnablePort_0 { ap_vld {  { RegisterWriteEnablePort_0 out_data 1 1 }  { RegisterWriteEnablePort_0_ap_vld out_vld 1 1 } } }
	RegisterWriteDataPort_0 { ap_vld {  { RegisterWriteDataPort_0 out_data 1 32 }  { RegisterWriteDataPort_0_ap_vld out_vld 1 1 } } }
	axi_master_0 { ap_bus {  { axi_master_0_req_din fifo_data 1 1 }  { axi_master_0_req_full_n fifo_status 0 1 }  { axi_master_0_req_write fifo_update 1 1 }  { axi_master_0_rsp_empty_n fifo_status 0 1 }  { axi_master_0_rsp_read fifo_update 1 1 }  { axi_master_0_address unknown 1 32 }  { axi_master_0_datain unknown 0 32 }  { axi_master_0_dataout unknown 1 32 }  { axi_master_0_size unknown 1 32 } } }
}
