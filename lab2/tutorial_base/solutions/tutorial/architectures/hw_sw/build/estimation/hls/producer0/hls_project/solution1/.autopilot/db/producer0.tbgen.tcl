set moduleName producer0
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isPipelined 0
set pipeline_type none
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {producer0::producer0}
set C_modelType { void 0 }
set C_modelArgList {
	{ clock int 1 unused {pointer 0}  }
	{ reset int 1 unused {pointer 0}  }
	{ fifo_in_0 int 32 regular {fifo 0 volatile }  }
	{ fifo_out_0 int 32 regular {fifo 1 volatile }  }
	{ RegisterWriteEnablePort_0 int 1 regular {pointer 1 volatile }  }
	{ RegisterWriteDataPort_0 int 32 regular {pointer 1 volatile }  }
	{ axi_master_0 int 32 regular {bus 1}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "clock", "interface" : "wire", "bitwidth" : 1, "direction" : "READONLY", "bitSlice":[{"low":0,"up":0,"cElement": [{"cName": "producer0.clock.m_if.Val","cData": "bool","bit_use": { "low": 0,"up": 0},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "reset", "interface" : "wire", "bitwidth" : 1, "direction" : "READONLY", "bitSlice":[{"low":0,"up":0,"cElement": [{"cName": "producer0.reset.m_if.Val","cData": "bool","bit_use": { "low": 0,"up": 0},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "fifo_in_0", "interface" : "fifo", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "producer0.fifo_in_0.m_if.Val.V","cData": "uint32","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "fifo_out_0", "interface" : "fifo", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "producer0.fifo_out_0.m_if.Val.V","cData": "uint32","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "RegisterWriteEnablePort_0", "interface" : "wire", "bitwidth" : 1, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":0,"cElement": [{"cName": "producer0.RegisterWriteEnablePort_0.m_if.Val.V","cData": "uint1","bit_use": { "low": 0,"up": 0},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "RegisterWriteDataPort_0", "interface" : "wire", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "producer0.RegisterWriteDataPort_0.m_if.Val.V","cData": "uint32","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "axi_master_0", "interface" : "bus", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "producer0.axi_master_0.m_if.Val","cData": "int","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} ]}
# RTL Port declarations: 
set portNum 19
set portList { 
	{ clock sc_in sc_logic 1 clock -1 } 
	{ reset sc_in sc_logic 1 reset -1 active_high_sync clock } 
	{ fifo_in_0_dout sc_in sc_lv 32 signal 2 clock } 
	{ fifo_in_0_empty_n sc_in sc_logic 1 signal 2 clock } 
	{ fifo_in_0_read sc_out sc_logic 1 signal 2 clock } 
	{ fifo_out_0_din sc_out sc_lv 32 signal 3 clock } 
	{ fifo_out_0_full_n sc_in sc_logic 1 signal 3 clock } 
	{ fifo_out_0_write sc_out sc_logic 1 signal 3 clock } 
	{ RegisterWriteEnablePort_0 sc_out sc_lv 1 signal 4 clock } 
	{ RegisterWriteDataPort_0 sc_out sc_lv 32 signal 5 clock } 
	{ axi_master_0_req_din sc_out sc_logic 1 signal 6 clock } 
	{ axi_master_0_req_full_n sc_in sc_logic 1 signal 6 clock } 
	{ axi_master_0_req_write sc_out sc_logic 1 signal 6 clock } 
	{ axi_master_0_rsp_empty_n sc_in sc_logic 1 signal 6 clock } 
	{ axi_master_0_rsp_read sc_out sc_logic 1 signal 6 clock } 
	{ axi_master_0_address sc_out sc_lv 32 signal 6 clock } 
	{ axi_master_0_datain sc_in sc_lv 32 signal 6 clock } 
	{ axi_master_0_dataout sc_out sc_lv 32 signal 6 clock } 
	{ axi_master_0_size sc_out sc_lv 32 signal 6 clock } 
}
set NewPortList {[ 
	{ "name": "clock", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "clock", "role": "default" }} , 
 	{ "name": "reset", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "reset", "role": "default" }} , 
 	{ "name": "fifo_in_0_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "fifo_in_0", "role": "dout" }} , 
 	{ "name": "fifo_in_0_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "fifo_in_0", "role": "empty_n" }} , 
 	{ "name": "fifo_in_0_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "fifo_in_0", "role": "read" }} , 
 	{ "name": "fifo_out_0_din", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "fifo_out_0", "role": "din" }} , 
 	{ "name": "fifo_out_0_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "fifo_out_0", "role": "full_n" }} , 
 	{ "name": "fifo_out_0_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "fifo_out_0", "role": "write" }} , 
 	{ "name": "RegisterWriteEnablePort_0", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "RegisterWriteEnablePort_0", "role": "default" }} , 
 	{ "name": "RegisterWriteDataPort_0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "RegisterWriteDataPort_0", "role": "default" }} , 
 	{ "name": "axi_master_0_req_din", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "axi_master_0", "role": "req_din" }} , 
 	{ "name": "axi_master_0_req_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "axi_master_0", "role": "req_full_n" }} , 
 	{ "name": "axi_master_0_req_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "axi_master_0", "role": "req_write" }} , 
 	{ "name": "axi_master_0_rsp_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "axi_master_0", "role": "rsp_empty_n" }} , 
 	{ "name": "axi_master_0_rsp_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "axi_master_0", "role": "rsp_read" }} , 
 	{ "name": "axi_master_0_address", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "axi_master_0", "role": "address" }} , 
 	{ "name": "axi_master_0_datain", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "axi_master_0", "role": "datain" }} , 
 	{ "name": "axi_master_0_dataout", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "axi_master_0", "role": "dataout" }} , 
 	{ "name": "axi_master_0_size", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "axi_master_0", "role": "size" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "", "Child" : ["1"],
		"CDFG" : "producer0",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "0", "ap_start" : "0", "ap_ready" : "0", "ap_done" : "0", "ap_continue" : "0", "ap_idle" : "0",
		"Pipeline" : "Dataflow", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "1",
		"II" : "0",
		"VariableLatency" : "1", "ExactLatency" : "-1", "EstimateLatencyMin" : "0", "EstimateLatencyMax" : "20",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "0",
		"HasNonBlockingOperation" : "0",
		"InputProcess" : [],
		"OutputProcess" : [],
		"Port" : [
			{"Name" : "clock", "Type" : "None", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "grp_producer0_thread_fu_84", "Port" : "clock"}]},
			{"Name" : "reset", "Type" : "None", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "grp_producer0_thread_fu_84", "Port" : "reset"}]},
			{"Name" : "fifo_in_0", "Type" : "Fifo", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "grp_producer0_thread_fu_84", "Port" : "fifo_in_0"}]},
			{"Name" : "fifo_out_0", "Type" : "Fifo", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "grp_producer0_thread_fu_84", "Port" : "fifo_out_0"}]},
			{"Name" : "RegisterWriteEnablePort_0", "Type" : "Vld", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "grp_producer0_thread_fu_84", "Port" : "RegisterWriteEnablePort_0"}]},
			{"Name" : "RegisterWriteDataPort_0", "Type" : "Vld", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "grp_producer0_thread_fu_84", "Port" : "RegisterWriteDataPort_0"}]},
			{"Name" : "axi_master_0", "Type" : "Bus", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "grp_producer0_thread_fu_84", "Port" : "axi_master_0"}]},
			{"Name" : "producer0_ssdm_thread_M_thread", "Type" : "None", "Direction" : "I"}]},
	{"ID" : "1", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.grp_producer0_thread_fu_84", "Parent" : "0",
		"CDFG" : "producer0_thread",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "0", "ap_start" : "0", "ap_ready" : "0", "ap_done" : "0", "ap_continue" : "0", "ap_idle" : "0",
		"Pipeline" : "None", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "0",
		"VariableLatency" : "1", "ExactLatency" : "-1", "EstimateLatencyMin" : "19", "EstimateLatencyMax" : "19",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "0",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "clock", "Type" : "None", "Direction" : "I"},
			{"Name" : "reset", "Type" : "None", "Direction" : "I"},
			{"Name" : "fifo_in_0", "Type" : "Fifo", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "fifo_in_0_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "fifo_out_0", "Type" : "Fifo", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "fifo_out_0_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "RegisterWriteEnablePort_0", "Type" : "Vld", "Direction" : "O"},
			{"Name" : "RegisterWriteDataPort_0", "Type" : "Vld", "Direction" : "O"},
			{"Name" : "axi_master_0", "Type" : "Bus", "Direction" : "O"}]}]}


set ArgLastReadFirstWriteLatency {
	producer0 {
		clock {Type I LastRead -1 FirstWrite -1}
		reset {Type I LastRead -1 FirstWrite -1}
		fifo_in_0 {Type I LastRead 9 FirstWrite -1}
		fifo_out_0 {Type O LastRead 7 FirstWrite 7}
		RegisterWriteEnablePort_0 {Type O LastRead -1 FirstWrite 0}
		RegisterWriteDataPort_0 {Type O LastRead -1 FirstWrite 0}
		axi_master_0 {Type O LastRead 4 FirstWrite 4}
		producer0_ssdm_thread_M_thread {Type I LastRead -1 FirstWrite -1}}
	producer0_thread {
		clock {Type I LastRead -1 FirstWrite -1}
		reset {Type I LastRead -1 FirstWrite -1}
		fifo_in_0 {Type I LastRead 9 FirstWrite -1}
		fifo_out_0 {Type O LastRead 7 FirstWrite 7}
		RegisterWriteEnablePort_0 {Type O LastRead -1 FirstWrite 0}
		RegisterWriteDataPort_0 {Type O LastRead -1 FirstWrite 0}
		axi_master_0 {Type O LastRead 4 FirstWrite 4}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "0", "Max" : "20"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "21"}
]}

set PipelineEnableSignalInfo {[
]}

set Spec2ImplPortList { 
	fifo_in_0 { ap_fifo {  { fifo_in_0_dout fifo_data 0 32 }  { fifo_in_0_empty_n fifo_status 0 1 }  { fifo_in_0_read fifo_update 1 1 } } }
	fifo_out_0 { ap_fifo {  { fifo_out_0_din fifo_data 1 32 }  { fifo_out_0_full_n fifo_status 0 1 }  { fifo_out_0_write fifo_update 1 1 } } }
	RegisterWriteEnablePort_0 { ap_vld {  { RegisterWriteEnablePort_0 out_data 1 1 } } }
	RegisterWriteDataPort_0 { ap_vld {  { RegisterWriteDataPort_0 out_data 1 32 } } }
	axi_master_0 { ap_bus {  { axi_master_0_req_din fifo_data 1 1 }  { axi_master_0_req_full_n fifo_status 0 1 }  { axi_master_0_req_write fifo_update 1 1 }  { axi_master_0_rsp_empty_n fifo_status 0 1 }  { axi_master_0_rsp_read fifo_update 1 1 }  { axi_master_0_address unknown 1 32 }  { axi_master_0_datain unknown 0 32 }  { axi_master_0_dataout unknown 1 32 }  { axi_master_0_size unknown 1 32 } } }
}

set busDeadlockParameterList { 
}

# RTL port scheduling information:
set fifoSchedulingInfoList { 
	fifo_in_0 { fifo_read 1 has_conditional }
	fifo_out_0 { fifo_write 1 has_conditional }
}

# RTL bus port read request latency information:
set busReadReqLatencyList { 
	{ axi_master_0 1 }
}

# RTL bus port write response latency information:
set busWriteResLatencyList { 
	{ axi_master_0 1 }
}

# RTL array port load latency information:
set memoryLoadLatencyList { 
}
