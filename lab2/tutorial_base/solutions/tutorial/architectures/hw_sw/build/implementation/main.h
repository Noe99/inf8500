//////////////////////////////////////////////////////////////////////////////
///
/// Space Codesign System Inc. - (https://www.spacecodesign.com)
/// Copyright 2005-2022. All rights reserved
///
/// No authorization to modify or use this file for
/// commercial purposes without prior written consent
/// of its author(s)
///
/// Created by: SpaceStudio generation engine
///
/// Warning : This file content will be overwritten by the next generation process.
///
//////////////////////////////////////////////////////////////////////////////
#ifndef MAIN_H
#define MAIN_H

#include "application_definitions.h"
#include "axi4_interconnect.h"
#include "ddr.h"
#include "generated_definitions.h"
#include "generic_fifo.h"
#include "memory_map_if.h"
#include "monitor_engine.h"
#include "msa_fifo_read.h"
#include "msa_fifo_write.h"
#include "pic.h"
#include "platform_definitions.h"
#include "processor_fifo_adapter.h"
#include "producer0.h"
#include "register_adapter.h"
#include "register_file.h"
#include "register_read_if.h"
#include "register_write_if.h"
#include "reset_manager.h"
#include "simulation_helper.h"
#include "spacelib_global.h"
#include "systemc"
#include "systemc_terminators.h"
#include "zynq.h"
#include "zynq_apu.h"

extern int spacelib_initialize();
#endif
