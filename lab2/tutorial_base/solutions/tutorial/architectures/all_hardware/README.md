# This is an example of documentation feature
    
You can use the documentation feature to describe the solution, architecture and application 
components.

# Graphviz DOT example
    
~~~ dot

digraph {
    subgraph cluster_0 {
        label="main()";
        ModuleRead -> compute;
        compute -> ModuleWrite;
    }
}
~~~

# C++ code example

~~~ cpp
#include <iostream>

int main(int argc, char *argv[]) {

  /* "Hello World" example */
  for (auto i = 0; i < 0xFFFF; i++)
    cout << "Hello, World!" << endl;

  return 0;
}
~~~

# List is also supported

* First item
* Second
* And last one!

# Mathematics example

This is an in-line $(x+1)^2$ equation.

### This is a block of math

$$
(x+1)^2=(x+1)(x+1)
$$

$$
a+1 \above 1pt b
$$

$$
 u(x) =
  \begin{cases}
   \exp{x} & \text{if } x \geq 0 \\
   1       & \text{if } x < 0
  \end{cases}
$$

# LaTex equation is supported

\begin{equation} 
    f(x)=(x+a)(x+b)
\end{equation}

# PlanUML example

@startuml
participant P as "producer0"
participant F as "fifo"
participant C as "consumer0"

P --> F: ModuleWrite
F --> C: ModuleRead
@enduml